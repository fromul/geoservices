
function w_objectview(object) {
    this.object = object;
    this.node='';	
	var widget_name = 'objectview';
	this.user_visible = true;
	this.rname = 'objectview';
	this.visible=false;
	

    this.assign = function(form_element, value){	
		this.form_element = form_element;

		this.input = $('<input type="hidden"/>');
		//this.input.attr('id', this.object.fieldname);		
		form_element.append(this.input);		
		if(value!=undefined && value!='')
			this.input.val(value);
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
    this.getVal = function (){
		return this.input.val();
    }
    

    
    this.viewPropForm = function (container){
		var widget=this;
		var props = this.object.widget.properties;
		var sz="\
			<div class='classify'/>\
			<label>Select object type </label>\
			<div  id='db_table'>\
			</div>\
			\
		";
		container.append(sz);
		var db_table=$("#db_table",container);
			

		var metaTableSelect = {
			"fieldname": "",
			"title": "",
			"description": "",
			"visible": true,
			"widget": {
				"name": "theme_select",
				properties: {}
			}
		};
		
		this.tableSelect = $.widgets.get_widget(metaTableSelect);
		if(props){
			widget.dataset_id=props.dataset_id;			
		}
		this.tableSelect.assign(db_table, widget.dataset_id);
		
		
		helpobject={};
		helpobject.changevalue=function(fieldname, value){
			widget.dataset_id=value;
		}
		this.tableSelect.listnerlist.push(helpobject);


    }  
    
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(val==null)
			return '';
		var unique=randomString(10);
		var widget=this;
		setTimeout(function(){
				$('.'+unique).click(function(){
					var trow=$(this).parents('tr');
					var aData=trow.data('doc');
					if(!widget.object.fieldname || widget.object.fieldname==''){
						widget.object.fieldname='id';
					}
					if(!aData){
						aData={};
						aData[widget.object.fieldname]=$('.'+unique).html();

					}
					if(aData){
						var eRowform;
						if(widget.object.widget.properties.dataset_id){
							
							getdatasetmeta(widget.object.widget.properties.dataset_id, function(error, table_json){
								if(error!=null)
									return;
							
								table_json.metaid = widget.object.widget.properties.dataset_id;
								var doc_frm= new doc_template(table_json);
								$.getJSON("/dataset/list?f="+widget.object.widget.properties.dataset_id+"&iDisplayStart=0&iDisplayLength=1&f_id=" + aData[widget.object.fieldname], function(subdata) {
									if(subdata.aaData.length>0){
										var doc=subdata.aaData[0];
										eRowform=$('<div title="'+doc_frm.doc_description.title+'"><div class="dlg_form"></div></div>');
										
										doc_frm.init_form(function(){
											// setTimeout(function(){
												doc_frm.show_form($('.dlg_form',eRowform), doc, '', true);
												eRowform.dialog({
													  resizable: true,
													  height:'auto',
													  width: 1100,
													  maxWidth: 1400,
													  maxHeight: 600,
													  modal: true,
													  buttons: {
														"Close": function() {								
															doc_frm.close();
															eRowform.remove();
														}
													  },
													  close: function( event, ui ) {
															doc_frm.close();															
															eRowform.remove();
													  }
												});
											//}, 500);
										});

									}
								});
								
							});
							
							
							
						}
						
						
					}
				});
			}, 200);
		return ' <a class="CustomView '+unique+'" href="#" title="'+val+'" disabled="disabled">'+val+'</a>';
    }
	
	this.getPropForm = function (container){
				
		return {
				dataset_id: this.dataset_id,				
			};
    }
    
    this.getDatatableProperties=function (){
		var tempobj = new Object();
		tempobj.bVisible = this.object.visible;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sWidth = '30px';
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'text';		
		tempobj.sWidget = 'text';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';	
	    return tempobj;
    }
    return this;
}


extend(w_objectview, w_base);
$.widgets.RegisterWidget('objectview', w_objectview);

