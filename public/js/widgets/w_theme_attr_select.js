function w_theme_attr_select(object) {
	// Object for creation of widget   
	this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'w_theme_attr_select';
	// Self instance
	var _self = this;
	// Current theme
	var _currentTheme = false;


	var _form_element;

	
	var input;

	

	this.changevalue = function (fieldname, value) {
		var widgetContainer = _self.form_element.find(".js-widget-container").first();
		widgetContainer.empty();
		input = $('<select id="' + this.object.fieldname + '"/>');
		widgetContainer.append(input);

		if (value) {
			getdatasetmeta(value  , function(error, data){
				if(error!=null)
					return;
				_currentTheme = data;
				var options = '<option value="">Select theme attribute</option>';
				for (var i = 0; i < data.columns.length; i++) {
					options += '<option value="' + data.columns[i].fieldname + '">' + data.columns[i].title + ' (' + data.columns[i].fieldname + ') </option>';
				}

				input.html(options);
			});
			

			input.change(function () {
				var themeColumn = false;
				var currentVal = _self.getVal();
				for (var i = 0; i < _currentTheme.columns.length; i++) {
					if (_currentTheme.columns[i].fieldname === currentVal) {
						themeColumn = _currentTheme.columns[i];
					}
				}

				if (themeColumn) {
					_self.sendmessage({ value: currentVal, themeColumn: themeColumn });
				} else {
					throw "Unable to detect theme column";
				}
			});
		}
	};


	this.assign = function (form_element, value) {
		this.form_element = form_element;

		var widgetContainer = $('<div class="js-widget-container"/>');
		this.form_element.append(widgetContainer);
		if (this.relationlist.length === 1) {
			var span = $('<span class="muted">Waiting for theme_select change</span>');
			widgetContainer.append(span);
		} else {
			var span = $('<span class="muted">Unable to find related theme_select</span>');
			widgetContainer.append(span);
		}
	}

	// Getting value of input field
	this.getVal = function () {
		tablename = $('#' + this.object.fieldname, this.form_element).val();
		result = tablename;
		return result;
	}

	this.viewPropForm = function (container) {
		var node = $("<label>Corresponding fieldname to watch</label><input type='text' class='js-fieldname-to-watch' />");
		container.append(node);

		$('.js-fieldname-to-watch', container).val("");
		if (this.object.widget.properties.filter && this.object.widget.properties.filter.themeSelectToWatch) {
			$('.js-fieldname-to-watch', container).val(this.object.widget.properties.filter.themeSelectToWatch);
		}
	}

	this.getPropForm = function (container) {
		var val = $('.js-fieldname-to-watch', container).val();
		var obj = { filter: {} };
		obj.filter.themeSelectToWatch = val;
		return obj;
	}

	this.getDatatableProperties = function () {
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}
	//extend(this, new w_edit(object)); 		
	return this;
}


extend(w_theme_attr_select, w_base);
$.widgets.RegisterWidget('theme_attr_select', w_theme_attr_select);