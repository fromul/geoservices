function w_tableaccess(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'tableaccess';
	this.fieldtype = 'tableaccess';
	
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr( 'widget', widget_name)
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	/**
	*	getUserVal
	*/
    this.getUserVal = function (val) {
		if(this.gr_fn=='none' )
			return '';		
		//return val;
		if (val === undefined) {
			return '';
		}
		$('div').first().append('<div id="vrBerg234gsdv" style="display:none;"></div>');
		var temp_div=$('#vrBerg234gsdv');
		
		var obj = $('<a href="#">'+val+'</a>');
		temp_div.append(obj);
		obj.attr('onclick', 'createDataTable( '+this.cntr+', null); $("ul.nav li.active").removeClass("active"); $("#table_list").hide();');
	
		var tempo = temp_div.html();
		temp_div.remove();
		return tempo;
    }
  
	
	
    this.viewPropForm = function (container){

		return;
    }
	this.hasFilter = function (){
		return true;
	}
    this.getPropForm = function (container){
		
		return;
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';
		tempobj.oWidget = this;
		tempobj.sWidget = 'tableaccess';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		return tempobj;
	}
    return this;
}

extend(w_tableaccess, w_base);
$.widgets.RegisterWidget('tableaccess', w_tableaccess);
