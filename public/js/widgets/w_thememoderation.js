function w_thememoderation(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'thememoderation';
	this.user_visible = true;
	this.rname = 'Moderation';
	this.fieldtype = 'thememoderation';

	// Additional properties array
	var addproparr = new Array();

	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr('widget', widget_name)		
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	/**
	*	getUserVal
	*/
    this.getUserVal = function (val) {	
		$('div').first().append('<div id="scsfkjefqscwkc" style="display:none;"></div>');			
		var obj = $('<span />');

		val = parseInt(val);
		//0 - accepted, 1 - waiting moderation, 2 - rejected 
		if (val >= 0) {
			newobj = $('<div />');
			newobj.addClass('btn-group');
			newobj.attr('style', 'padding-right: 20px;');

			if (val < 0) 				
				val=-val;			
				
			if (val === 1) {
				obj.html('<span class="label label-success">Accepted record</span>');
				$('#moderation_accept_button', newobj).attr('disabled', 'disabled');
			} else if (val === 2) {
				obj.html('<span class="label label-info">Waiting for moderation</span>');
			} else if (val === 3) {
				obj.html('<span class="label label-important">Rejected record</span>');
				$('#moderation_reject_button', newobj).attr('disabled', 'disabled');
			}

			$('#scsfkjefqscwkc').append(newobj);
		} else {//else values - waiting moderation but there is not rigths
			obj.html('<span class="label label-info">Waiting for moderation</span>');
			//c('Error parsing the widget value ' + val);
		} //end if
		
		$('#scsfkjefqscwkc').append(obj);
		var tempo = $('#scsfkjefqscwkc').html();
		$('#scsfkjefqscwkc').remove();
		return tempo;
    }
	
	/*
	*	applyTaxonomy - substituting predefined constants
	*/
	var taxonomy = new Array();
	/* Filling the contants */
	taxonomy['CONST_TABLE_ID'] = this.cntr;
	taxonomy['ROWID'] = this.cntr;
	this.applyTaxonomy = function(link) {
		taxonomy['CONST_TABLE_ID'] = this.cntr;
		taxonomy['ROWID'] = this.cntr;
		for(var index in taxonomy) {
			link = link.replace(index, taxonomy[index]);
		}
		return link;
	}
	
	
    this.viewPropForm = function (container){
		return;
    }
	
    this.getPropForm = function (container){
		return;
    }
	
	this.hasFilter = function (){
		return true;
	}	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'thememoderation';
		tempobj.oWidget = this;
		tempobj.sWidget = 'thememoderation';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'ThemeModerationFilterWidget';
		return tempobj;
	}
    return this;
}

extend(w_thememoderation, w_base);
$.widgets.RegisterWidget('thememoderation', w_thememoderation);
