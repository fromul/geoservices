
function w_hidden(object) {
    this.object = object;
    this.node='';	
	var widget_name = 'hidden';
	this.user_visible = true;
	this.rname = 'Hidden';
	this.visible=false;
	this.assign = function(form_element, value){	
		this.form_element = form_element;

		this.input = $('<input type="hidden"/>');
		this.input.attr('id', this.object.fieldname);		
		form_element.append(this.input);		
		if(value!=undefined && value!='')
			this.input.val(value);
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
    this.getVal = function (){
		return this.input.val();
    }
	
    
    

    
    this.viewpropForm=function (){
	    return;
    }    
    
    this.getprop=function (){
	    return this.object;
    }
    
    this.getDatatableProperties=function (){
		var tempobj = new Object();
		tempobj.bVisible = this.object.visible;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sWidth = '30px';
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'text';		
		tempobj.sWidget = 'text';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';	
	    return tempobj;
    }
    return this;
}


extend(w_hidden, w_base);
$.widgets.RegisterWidget('hidden', w_hidden);

