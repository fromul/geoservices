
(function($) {
	//var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);
	var geos_init=false;

	$.fn.extend({
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		numberTest: function(settings) {
			settings = $.extend({
				onlypositive:false,
				before: "10",
				after: "10",
			}, settings);	
			
			var input = $(this[0]);
			var buffer=input.val();
			
			
			function testNumber(vl){
				var numbers='0123456789';
				if(vl=='')
					return true;
				if(!test_number(vl))
					return false;				
				if(vl<0){
					if(settings.onlypositive)
						return false;
					vl=-vl;
				}
				vl=parseFloat(vl)+'';				
				arr  =  vl.split('.');
				if(arr[0].length>parseInt(settings.before))
					return false;				
				if(arr.length>1 && arr[1].length>parseInt(settings.after))
					return false;
				
				return true;
			};
		

			function keydownEvent(e) {
				var trg=$(e.target);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;
					
					var s_val= trg.val();
					var future_val='';
					if(pos.end-pos.begin!=0){						
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.end,s_val.length);
					}
					else if (k==8){
						future_val=s_val.substring(0,pos.begin-1)+s_val.substring(pos.begin,s_val.length);						
					}
					else if (k==46){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.begin+1,s_val.length);						
					}					
					if (testNumber(future_val)) {
						buffer=future_val;
						input.val(future_val);
						if(k == 8)
							trg.caret(pos.begin-1);
						else
							trg.caret(pos.begin);
					}
					return false;
				}								
				return true;
			};
			
			
			
			function keypressEvent(e) {
				var trg=$(e.target);			
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					var c = String.fromCharCode(k);
					if(c==',')
						c='.';					
					var s_val= trg.val();
					var future_val='';
					future_val=s_val.substring(0,pos.begin)+c+s_val.substring(pos.end,s_val.length);					
					if (testNumber(future_val)) {
						buffer=future_val;
						input.val(future_val);
						trg.caret(pos.begin+1);
						trg.trigger('change');
					}					
					return false;
				}
			};

			input.bind("keydown", keydownEvent);			
			input.bind("keypress", keypressEvent);
			buffer=input.val();	
		}
		
	});
	
	
})(jQuery);


function w_number(object) {
	var widget=this;
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'number';
	// Additional properties array
	var addproparr = new Array();
	var form_cont;
	this.user_visible = true;
	this.rname = 'Число';	
	this.fieldtype = 'number';
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		//var label = $('<div style="height: 24px; width:80px;display: inline-block;text-align: right;">'+this.object.title+'</div>');
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		var input = $('<input class="form-control" title="'+this.object.description+'" type="text" />');
		this.input=input;
		input.attr('id', this.object.fieldname);
		/*
		if(this.lvis){
			form_element.append(label);
		}*/
		form_element.append(input);
		
		after=10;
		if(this.object.widget.properties!=undefined && this.object.widget.properties.after!=undefined  && this.object.widget.properties.after!='')
		   after=this.object.widget.properties.after;
		before=10;
		if(this.object.widget.properties!=undefined && this.object.widget.properties.before!=undefined && this.object.widget.properties.before!='' )
		   before=this.object.widget.properties.before;
		
		
		
		if(value!=undefined && value!='')
			input.val(value);
		else if(object.default_value && object.default_value!='empty'){
			this.input.val(removeescapeHtml(object.default_value));
		}
		input.numberTest({default_point: ".", before:before, after:after} );
		this.changevalue('test1212121','');	
		this.input.bind('change', function(select){
			widget.val=widget.input.val();
			widget.sendmessage(widget.val);
		});
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';

		val=this.input.val();
		//val=$('#'+this.object.fieldname).val();
		if(val=='.') val='';
		if(val.length>1 && val.charAt(val.length-1)=='.')
			val=val.slice(0,val.length-1);
	    return val;
    }
	
    // Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(val==null)
			return '';
		if(this.gr_fn=='none' )
			return '';
		after=10;
		if(this.object.widget.properties!=undefined && this.object.widget.properties.after!=undefined  && this.object.widget.properties.after!='')
		   after=this.object.widget.properties.after;		
		val=val+'';
		arr  =  val.split('.');
		if(arr.length>1 && arr[1].length>parseInt(after)){
			if(parseInt(after)==0)
				arr[1]='';
			else
				arr[1]=arr[1].substr(0,parseInt(after));			
		}
		if (arr.length>1 && arr[1]!='' )
			arr[0]=arr[0]+'.'+arr[1];
		
		return arr[0];
		if(this.object.widget.properties!=undefined && this.object.widget.properties.measure!=undefined ){
			return val;//+' '+this.object.widget.properties.suffix_user;
		}else{
			return val;
		}
	   
    }
	
	
	this.olap = function(node, value){	
		this.init_olap(node, value);		
		this.group.append($('<option value="max">max</optiion>'));
		this.group.append($('<option value="min">min</optiion>'));
		this.group.append($('<option value="avg">average</optiion>'));
		this.group.append($('<option value="sum">sum</optiion>'));
	}

	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'number';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'number_filter';
		tempobj.sWidth = '30px';

		return tempobj;
	}
	
	this.hasFilter = function (){
		return true;
	}	

	// Additional properties form
    this.viewPropForm = function (container){
		var widget=this;
		function mesure_options(lvl,parent) {
			var option='';
			return option;
			var res=JSON.parse($.ajax({
				type: "GET",
				url: '/geothemes/getMesure?mode=prop&lvl='+lvl+'&parent='+parent,
				async: false,
			}).responseText);
			if($.isArray(res))
				res.forEach( function(item){ option=option+'<option value="'+item.value+'">'+item.label+'</option>'; } );
			return option;
		}
		var ps=$("<label>Measure:</label><input type='radio' id='measure1' name='measure' value=1 /><label>yes</label><input type='radio' id='measure2' name='measure' value=0 /><label>no</label>");
		//container.append(ps);
		ps=$("<div id='measure_form'><label>Type: </label><select id='measure_type' name='measure_type'>"+mesure_options(0,0)+"</select><br><label>Value: </label><select id='measure_value' name='measure_value'>"+mesure_options(1,1)+"</select><br><label>Unit for base: </label><select id='measure_unitbase' name='measure_unitbase'>"+mesure_options(2,1)+"</select><br/><label>Unit for user: </label><select id='measure_unituser' name='measure_unituser'>"+mesure_options(2,1)+"</select></div>")
		//container.append(ps);
		$('#measure_form', container).css('display', 'none');
		if(this.object.widget.properties!=undefined && this.object.widget.properties.measure!=undefined ){
		   $('#measure'+this.object.widget.properties.measure, container).prop('checked', true);;
		   $('#measure_form', container).css('display', 'block');
		   $('#measure_type', container).val(this.object.widget.properties.measure_type);
		   setTimeout(function() {
			$('#measure_value', container).val(widget.object.widget.properties.measure_val);
		   }, 200);
		   setTimeout(function() {
		   $('#measure_unitbase', container).val(widget.object.widget.properties.measure_user);
		   $('#measure_unituser', container).val(widget.object.widget.properties.measure_base);
		   },500);
		}
		container.on('change','input[name=measure]',function(){
			if ($('input[name=measure]:radio:checked', container).val() == 1) 	
				$('#measure_form', container).css('display', 'block');
			if ($('input[name=measure]:radio:checked', container).val() == 0) 
				$('#measure_form', container).css('display', 'none');
		});
		container.on('change','#measure_type',function(){
			$('#measure_value', container).html(mesure_options(1,$('#measure_type', container).val()));
			$('#measure_unitbase', container).html(mesure_options(2,$('#measure_value', container).val()));
			$('#measure_unituser', container).html(mesure_options(2,$('#measure_type', container).val()));
		});
		container.on('change','#measure_value',function(){
			$('#measure_unitbase', container).html(mesure_options(2,$('#measure_value', container).val()));
			$('#measure_unituser', container).html(mesure_options(2,$('#measure_type', container).val()));
		});
		
		var ps=$("<div class='tab_field'><label class='tab_field_label'>Default value</label><input class='tab_field_input' type='text' id='default_value' /></div>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.default_value!=undefined )
		   $('#default_value', container).val(this.object.widget.properties.default_value);

		var ps=$("<div class='tab_field'><label class='tab_field_label'>Digits before the point </label><input class='tab_field_input' type='text' id='before' /></div>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.before!=undefined )
		   $('#before', container).val(this.object.widget.properties.before);

 	    var ps=$("<div class='tab_field'><label class='tab_field_label'>Digits after the point </label><input class='tab_field_input' type='text' id='after' /></div>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.after!=undefined )
		   $('#after', container).val(this.object.widget.properties.after);
		   
		return;
    }    
    
    this.getPropForm = function (container){
		function mesure_unit(id) {
			if(id==null || id==undefined)
				return {suffix: '', factor: ''};
			var option='';
			var res=JSON.parse($.ajax({
				type: "GET",
				url: '/geothemes/getMesure?id='+id,
				async: false,
			}).responseText);
			if($.isArray(res)){
				res.forEach( function(item){ option=option+'<option value="'+item.value+'">'+item.label+'</option>'; } );
				return {suffix: res[0].suffix, factor: res[0].factor};
			}
			else
				return {suffix: '', factor: ''};
		}
		var default_value=$('#default_value',container).val();
		var before=$('#before',container).val();
		var after=$('#after',container).val();
		if ($('#measure', container).val() == 0) 
			return {before: before, default_value:default_value, after:after, measure: 0}
		else {
			var measure_user = $('#measure_unituser', container).val();
			var measure_base = $('#measure_unitbase', container).val();
			var suffix_user = mesure_unit(measure_user).suffix;
			return {before: before, default_value:default_value, after:after, measure:1, measure_user: measure_user, measure_base: measure_base, suffix_user : suffix_user};
		}
    }
	
    return this;
}

extend(w_number, w_base);
$.widgets.RegisterWidget('number', w_number);