function w_checkbox(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'checkbox';
	// Additional properties array
	var addproparr = new Array();
	addproparr[0] = new Array();
	addproparr[0]['propname'] = 'options';
	addproparr[0]['proptitle'] = 'Опции';
	addproparr[0]['propdescription'] = 'First, Second..';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var optlist = '';
		var optarr = explode(',', this.object.widget.properties.options);
		for (var i = 0; i < optarr.length; i++) {
			optlist = optlist + '<input type="checkbox" name="' + this.object.htmlname + '" value="' + optarr[i].replace(/^\s+|\s+$/g, "") + '" name="" />' + optarr[i].replace(/^\s+|\s+$/g, "") + '<br>';
		}
		var unique = this.object.fieldname;
		var tempstr = this.object.title + '<br>' +  optlist;
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		el.attr( 'name', unique);
		el.attr( 'widget', widget_name);
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	// Getting JSON of field for further saving
    // this.getUserVal=function (){
	    // return $('#'+this.object.fieldname).val();
    // }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Checkbox options</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		try {
			$('#select_options',container).val(this.object.widget.properties.options);
		} catch(e) {
		
		}
		return;
    } 
	
    this.getPropForm = function (container){
		var wsize=$('#select_options',container).val();
	    return {options:wsize};
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'checkbox';		
		tempobj.sWidget = 'checkbox';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		tempobj.oWidget = this;
		return tempobj;
	}	
    
	extend(this, new w_edit(object)); 		
    return this;
}

extend(w_checkbox, w_base);
$.widgets.RegisterWidget('checkbox', w_checkbox);
