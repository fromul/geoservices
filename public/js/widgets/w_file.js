function w_file(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'file';
	// Additional properties array
	var addproparr = new Array();
	this.form_element = '';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;

		var form_element_container = $('<div/>');
		form_element_container.addClass('form-inline');
		var input = $('<input valign="top" type="text" id="' + this.object.fieldname + '"/>');		
		form_element_container.append(input);
		var button = $('<button class="btn" type="button">Open</button>');
		form_element_container.append(button);
		form_element.append(form_element_container);
		
		var options = new Object();
		options.filter = this.object.widget.properties.exts;

		button.click(function() {
			OpenFileDlg(function(path){
				input.val(path);
				input.trigger('change');
			},options);
		});
		
		if (value!=undefined && value!='') {
			input.val(value);
		}
		
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
		return $('#'+this.object.fieldname, this.form_element).val();
    }
   
    this.viewPropForm = function (container){
		var ps=$("<label>File extensions (write extensions using comma)</label><input type='text' id='f_ext' value=''/><br/>");
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.exts!=undefined   )
		   $('#f_ext', container).val(this.object.widget.properties.exts);

		return;
    }    
    this.getPropForm = function (container){		
	    return {exts: $('#f_ext', container).val()};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
//		tempobj.type = 'text';		
		tempobj.sWidget = 'file';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		tempobj.type = 'widget_proxy';				

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_file, w_base);
$.widgets.RegisterWidget('file', w_file);