function w_params(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'parameters';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		input = $( '<div id="'+this.object.fieldname+'"/>' );
		form_element.append($('<label>'+this.object.title+'</label>'));
		form_element.append(input);
		if(value!=undefined)
			createparamsform(value,input);
		else
			createparamsform(null,input);	
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
		var params=get_params_json(this.form_element);
	    return JSON.stringify(params);
    }
    

    this.viewPropForm = function (container){
		var ps=$("<label>Selet options</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		$('#select_options',ps).val(this.object.widget.properties.options);
		return;		
		
		var htmlstr = '';
		for (var i = 0; i < addproparr.length; i++) {
			var ttitle = '';
			try {
				eval('ttitle =  this.object.widget.properties.' + addproparr[i]['propname'] + ';');
			} catch (e) {
				c(e.message);
			}
			htmlstr = htmlstr + '<div class="form-field"><label>' + addproparr[i]['proptitle'] + '</label><input type="text" name="additionalprops_' + addproparr[i]['propname'] + '_' + this.cntr + '" value="' + ttitle + '" placeholder="' + addproparr[i]['propdescription'] + '"/><input type="hidden" name="additionalprops_' + this.cntr + '_' + i + '" value="' + addproparr[i]['propname'] + '"></div>';
		}
		$('[name="type' + this.cntr + '"]').val('string');
	    return htmlstr;
    }
    this.getPropForm = function (container){
		var wsize=$('#w_edit_size',container).val();
	    return {size:wsize};
    }
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'textarea';		
		tempobj.sWidget = 'select';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';

		return tempobj;
	}	
	
    return this;
}


extend(w_params, w_base);
$.widgets.RegisterWidget('parameters', w_params);
