var geodatasetguid='2ba9414e-f63b-469d-b971-74e74c74bba8';

var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;'
};

function escapeHtml(string) {
	return String(string).replace(/[&<>"'\/]/g, function (s) {
	  return entityMap[s];
	});
}

function removeescapeHtml(string) {
	string=String(string).replace(/&amp;/g, '&');
	string=String(string).replace(/&lt;/g, '<');
	string=String(string).replace(/&gt;/g, '>');
	string=String(string).replace(/&quot;/g, '"');
	string=String(string).replace(/&#39;/g, "'");
	return String(string).replace(/&#x2F;/g, "/");
}

function test_number(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
};


function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ ) text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

// Inheritance functions
function extend(Child, Parent) {
	var F = function() { }
	F.prototype = Parent.prototype
	Child.prototype = new F()
	Child.prototype.constructor = Child
	Child.superclass = Parent.prototype
	mixin(Child.prototype, Parent)
}


function mixin(dst, src){
	var tobj = {}
	for(var x in src){
		if((typeof tobj[x] == "undefined") || (tobj[x] != src[x])){
			dst[x] = src[x];
		}
	}
	if(document.all && !document.isOpera){
		var p = src.toString;
		if(typeof p == "function" && p != dst.toString && p != tobj.toString &&
		 p != "\nfunction toString() {\n    [native code]\n}\n"){
			dst.toString = src.toString;
		}
	}
}
// Service functions
function explode( delimiter, string ) {
	var emptyArray = { 0: '' };
	if ( arguments.length != 2	|| typeof arguments[0] == 'undefined' || typeof arguments[1] == 'undefined' ) {	return null; }
	if ( delimiter === '' || delimiter === false || delimiter === null ) { return false; }
	if ( typeof delimiter == 'function' || typeof delimiter == 'object' || typeof string == 'function' || typeof string == 'object' ) {	return emptyArray;}
	if ( delimiter === true ) { delimiter = '1'; }
	return string.toString().split ( delimiter.toString() );
}

function c(msg) { console.log(msg);	}

function randomString(length) {
    var chars = '0123456789abcdefghiklmnopqrstuvwxyz'.split('');
    if (! length) {
        length = Math.floor(Math.random() * chars.length);
    }
    var str = '';
    for (var i = 0; i < length; i++) {
        str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
}
function substr( f_string, f_start, f_length ) {	
	if(f_start < 0) {
		f_start += f_string.length;
	}
	if(f_length == undefined) {
		f_length = f_string.length;
	} else if(f_length < 0){
		f_length += f_string.length;
	} else {
		f_length += f_start;
	}
	if(f_length < f_start) {
		f_length = f_start;
	}
	return f_string.substring(f_start, f_length);
}

// Adding widget manager
(function($) {
	$.widgets = new function() {
		this.widgetlist = new Array();
		this.formlist = new Array();
		this.createWidget = function(input_json) {
			for (var j = 0; j < this.widgetslist.count; j++) {
				if (input_json['fieldname'] == widgetlist[j]['widget_name']) {	//????????				
					gen_elem_name = widgetlist[j]['widget_create'](input_json);
					return gen_elem_name;
				}
			}
		};

		this.RegisterWidget = function(widget, cr_func){
			this.widgetlist.push([widget, cr_func]);
		};

		this.triggerOpts = function(widget_name, cntr) {
			for (var i = 0; i < this.widgetlist.length; i++) {
				if (this.widgetlist[i][0] == widget_name) {
					var tempf = this.widgetlist[i][1]();
					tempf.cntr = cntr;
					var topaste = tempf.viewPropForm();
					if ($('#additionalprops' + cntr + '-wrapper').length > 0) {
						$('#additionalprops' + cntr + '-wrapper').html(topaste);
					} else {
						$('#f' + cntr + '_widget_wrapper').after('<div id="additionalprops' + cntr + '-wrapper">' + topaste + '</div>');
					}
				}
			}
		}

		this.get_widget = function(attr_json){
			wname='edit';

			if (attr_json.widget!=undefined && attr_json.widget.name != null) {
				wname=attr_json.widget.name;
			}

			for (var j = 0; j < $.widgets.widgetlist.length; j++) {
				if (wname == $.widgets.widgetlist[j][0]) {						
					object_widget = new $.widgets.widgetlist[j][1](attr_json);					
					object_widget.listnerlist = new Array();
					object_widget.relationlist = new Array();
					return object_widget;
					
				}
			}

			object_widget = new w_hidden(attr_json);
			object_widget.listnerlist = new Array();
			return object_widget;
		}
		this.get_widget_byname = function(wname, fieldname,description, properties){
			
			if(fieldname===undefined)
				fieldname='fieldname';
			if(description===undefined)
				description='';
			if(properties===undefined)
				description={};			
			attr_json={fieldname:fieldname, description:description, widget:{name:wname,properties:properties}};

			for (var j = 0; j < $.widgets.widgetlist.length; j++) {
				if (wname == $.widgets.widgetlist[j][0]) {						
					object_widget = new $.widgets.widgetlist[j][1](attr_json);
					object_widget.listnerlist = new Array();
					object_widget.relationlist = new Array();
					return object_widget;
					
				}
			}
			return null;
		}		
	};
}(jQuery));

// Base class with common properties and methods
function w_base(object) {
	// JSON Object for creation of widget
	this.object = object;
	// Common name of widget
	var widget_name = 'base';
	
	// Getting JSON of field for further saving
    return this;
}

//Labele visiability
w_base.prototype.lvis = true;

//Widget visiability for users in field_widget_select
w_base.prototype.user_visible = false;

//Russian name for widget in field_widget_select
w_base.prototype.rname = '������� ������';

// HTML element creation
w_base.prototype.assign = function(nodeid, value){	
	this.nodeid = nodeid;
}



var TerminalSigns='!()[] +-*/=<>&|';
var keywords=['or','and'];

function test_expression(expression, values){
	var cur_pos=0;
	var cword='';
	function check_id(id){
		if(id=='')
			return '';
		if(id in values)
			return "'"+values[id]+"'";
		v=parseFloat(id); 
		if (!isNaN(v))
			return v;
		return undefined;
	}
	var new_expression='';
	while(cur_pos<expression.length){
		c_sign=expression.charAt(cur_pos);
		if(TerminalSigns.indexOf(c_sign)!=-1){
			resid=check_id(cword);
			if( resid===undefined )
				return false;
			new_expression+=resid+c_sign;
			cword='';
		}
		else{
			cword+=c_sign;			
		}		
		cur_pos++;
	}
	
	resid=check_id(cword);
	if( resid===undefined )
		return false;
	new_expression+=resid;
	
	var eval_value=false;
	expression='eval_value='+new_expression;
	try {
		eval(expression);
	}
	catch(err) {
		alert(err);
		return false;  
	}
	
	return eval_value;
}

w_base.prototype.checkVisibility = function(fieldname, value){	
	if(this.object===undefined || this.object.conditions===undefined){
		this.form_element.show();	
		return true;
	}
	if(this.fvalues===undefined)
		this.fvalues={};
	this.fvalues[fieldname]=value;
	this.visible=false;
	value=value+'';
	if(this.object.condition===undefined || this.object.condition==''){
		this.visible=true;
	}
	else{
		values={};
		for(var i=0; i<this.relationlist.length; i++){
			values[this.relationlist[i].object.fieldname]=this.relationlist[i].getVal();
		}
		this.visible=test_expression(this.object.condition, values);
	}

	if( this.form_element ){
		if (this.visible){
			this.form_element.show();
			this.form_element.parents('.abletohide').show();
		}
		else{
			this.form_element.hide();
			this.form_element.parents('.abletohide').hide();
		}
	}
	return this.visible;
}
	




w_base.prototype.changevalue = function(fieldname, value){	
	this.checkVisibility(fieldname, value);
}

w_base.prototype.sendmessage = function(value){	
	for (var i = 0; i < this.listnerlist.length; i++) {
		this.listnerlist[i].changevalue(this.object.fieldname, value);
	}	
}


w_base.prototype.init_olap = function(node, value){	
	group = $( '<select style="height: 26px;width: 70px;" id="theme_select"/>' );
	//node.append($('<label>'+this.object.title+'</label>'));
	node.append(group);
	group.append($('<option class="lang" value="groupby">Group by</optiion>'));
	group.append($('<option class="lang" value="count">Count</optiion>'));
	group.append($('<option class="lang" value="none">-</optiion>'));
	group.val('none');
	this.group=group;
	this.gr_fn=null;
}


w_base.prototype.olap = function(node, value){	
	this.init_olap(node, value);
}


// Additional properties form
w_base.prototype.viewPropForm = function (container){
	var htmlstr = '';
	return htmlstr;
}

// Getting value of input field
w_base.prototype.getVal = function (){
	if(this.visible!=undefined && this.visible==false)
		return '';
	return $('#'+this.object.fieldname).val();
}


w_base.prototype.fieldtype = 'string';


w_base.prototype.getPropForm = function (container){
	return {};
}

w_base.prototype.getDatatableProperties = function (){
	var tempobj = new Object();
	tempobj.bVisible = true;	
	tempobj.sTitle = this.object.fieldname;
	tempobj.sName = this.object.fieldname;
	tempobj.sType = this.object.type;
	tempobj.type = 'text';		
	tempobj.sWidget = 'edit';
	tempobj.sWidgetProperties = this.object.widget.properties;
	tempobj.sFilterName = 'AutocompleteFilterWidget';
	return tempobj;
}


w_base.prototype.activateDrawing = function(){

}

w_base.prototype.hasFilter = function (){
	return false;
}

w_base.prototype.getUserVal=function (val){
	return val;
}
