function w_user(object) {	
	var widget=this;
    this.object = object;
	// Common name of widget
	var widget_name = 'user';
	this.form_element = '';
	this.input;
	
	this.user_visible = true;
	this.rname = 'User';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var con=form_element;
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		input = $('<input title="'+this.object.description+'" class="form-control" id="inp_'+this.object.fieldname+'" type="text"/>');		
		form_element.append(input);
		this.input=$('input',form_element);
		if(value!=undefined && value!=''){
			this.input.val(removeescapeHtml(value));
			this.val=value;
		}
		
			w_settings = {
				autofill    : true,
				cacheLength : 10,
				max         : 5,
				autofocus   : true,
				highlight   : true,
				mustMatch   : true,
				selectFirst : true,
				delay: 500,
				source		: function (request, response) {
					if(request.term.length<3)
						return;
					var result=[];					
					$.ajax({
						url: '/dataset/list?f=5&iDisplayStart=0&iDisplayLength=10&iSortingCols=1&iSortCol_0=1&distinct=t&f_username=' + request.term + '&s_fields=username,id',
						dataType: "json",						
						success: function(data) {
							response($.map(data.aaData, function (item) {
									
									return {
										label: item.username,
										id: item.id
									};
								}));
						}
					});
				},
				appendTo    : form_element
			};

			this.input.autocomplete(w_settings, {
				dataType:'json',
				change: function(event, ui) {
					con.change();
					widget.sendmessage(widget.val);
				},
				select: function (event, ui) {
							event.preventDefault();
							widget.input.attr('userid', ui.item.id);
							//sel_id = ui.item.id;
							widget.input.val(ui.item.label);						
							widget.val=ui.item.id;
							widget.username=ui.item.label;
							widget.sendmessage(ui.item.id);	
						},
				formatItem: function(row, i, n) {                                                        
				  return row.value;
				},
			});	
			
			/*this.input.change(function(){
				widget.val=widget.input.val();
				widget.sendmessage(widget.val);
			});*/
		
		this.changevalue('test1212121','');	
	
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';
		
		return this.val;
    }
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(val==null)
			return '';		
		var uid=randomString(10);
		$.ajax({
			url: '/user_list/list?f_id='+ val,
			dataType: "json",						
			success: function(data) {
				if(!data.aaData || data.aaData.length==0)
					return;
				$('#'+uid).html(data.aaData[0].fullname);
				
			}
		});
		return '<b><a href="/user/profile/'+val+'" class="purple user i-forum-author"> <span id="'+uid+'"></span></a></b>';
    }
    // Additional properties form
    this.viewPropForm = function (container){
			
    }
    this.getPropForm = function (container){	
		
	    return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		tempobj.sWidth = this.object.widget.properties.size*10+'px';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_user, w_base);
$.widgets.RegisterWidget('user', w_user);