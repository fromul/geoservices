(function($) {
/**
	* A filter widget based on data in a table column.
	* 
	* @class AutocompleteFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/

	
	$.AutocompleteFilterWidget = function( $Container, column, dt_table ) {
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;

		widget.asFilters = [];
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		
		//widget.$Autocomplete = $( '<input type="text" id="f_'+widget.oColumn.sName+'" size="'+widget.oColumn.sWidth+'"/>' );
		widget.$Autocomplete = $( '<input class="form-control" type="text" id="'+widget.column.fieldname+'"/>' );
		widget.$Autocomplete.autocomplete({		
				//source: widgets.$AutocompleteSource + "&field=" + widget.oColumn.sName,
				source: widget.column.$AutocompleteSource + "&field=" + widget.column.fieldname,
				
				minLength: 2,
				select: function(event, ui) {
					var sSelected = ui.item.value, sText, $TermLink, $SelectedOption; 
					ui.item.value = "";
					if ( '' === sSelected ) {
						// The blank option is a default, not a filter, and is re-selected after filtering
						return;
					}
					if  (!($.inArray(sSelected, widget.asFilters))) {
						return;
					}
					sText = $( '<div>' + sSelected + '</div>' ).text();
					$TermLink = $( '<a class="filter-term" href="#"></a>' )
						.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
						.text( sText )
						.click( function() {
							// Remove from current filters array
							widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
								return sFilter != sSelected;
							} );
							$TermLink.remove();
							if ( widgets.$TermContainer && 0 === widgets.$TermContainer.find( '.filter-term' ).length ) {
								widgets.$TermContainer.hide();
							}
							widget.fnFilter();
							return false;
						} );
					widget.asFilters.push( sSelected );
					if ( widgets.$TermContainer ) {
						widgets.$TermContainer.show();
						widgets.$TermContainer.prepend( $TermLink );
					} else {
						widget.$Autocomplete.after( $TermLink );
					}
				
					widget.$Autocomplete.val( '' );
					widget.fnFilter();
				
				
				}
			});		
		widget.$label = $('<label for="f_' + widget.column.fieldname + '">' + widget.column.title + '</label><br>');
		widget.$Container.append( widget.$label );
		widget.$Container.append( widget.$Autocomplete );
		//widget.fnDraw();
	};
	
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.AutocompleteFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		var asEscapedFilters = [];
		var sFilterStart, sFilterEnd;
		if ( widget.asFilters.length > 0 ) {
			// Filters must have RegExp symbols escaped
			$.each( widget.asFilters, function( i, sFilter ) {
				asEscapedFilters.push( $.fnRegExpEscape( sFilter ) );
			} );
			// This regular expression filters by either whole column values or an item in a comma list
			sFilterStart = widget.sSeparator ? '(^|' + widget.sSeparator + ')(' : '^(';
			sFilterEnd = widget.sSeparator ? ')(' + widget.sSeparator + '|$)' : ')$';
			widget.oDataTable.fnFilter( sFilterStart + asEscapedFilters.join('|') + sFilterEnd, widget.iColumn, true, false );
		} else { 
			// Clear any filters for this column
			widget.oDataTable.fnFilter( '', widget.iColumn );
		}
		setTimeout( function() {
			if($.openlayermap !== undefined)
				$.openlayermap.rdrw("brio"); // TODO Refactoring !!!
        }, 150 );

	};
}(jQuery));


