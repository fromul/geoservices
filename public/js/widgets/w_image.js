var slider_str='<div id="slider" style="position: absolute;top: 10px; width: 100%; display: none;">\
    <div id="slider1_container" style="position: relative; width: 600px; margin: auto;    height: 300px;">\
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">\
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block; background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;"> </div>\
            <div style="position: absolute; display: block; background: url(/img/slider/loading.gif) no-repeat center center; top: 0px; left: 0px;width: 100%;height:100%;"> </div>\
        </div>\
        <div id="slider_content" u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">\
        </div>\
        <div u="thumbnavigator" class="jssort09" style="position: absolute; bottom: 0px; left: 0px; height:60px; width:600px;">\
            <div style="filter: alpha(opacity=40); opacity:0.4; position: absolute; display: block; background-color: #ffffff; top: 0px; left: 0px; width: 100%; height: 100%;"></div>\
            <div u="slides">\
                <div u="prototype" style="POSITION: absolute; WIDTH: 600px; HEIGHT: 60px; TOP: 0; LEFT: 0;">\
                    <div u="thumbnailtemplate" style="font-family: verdana; font-weight: normal; POSITION: absolute; WIDTH: 100%; HEIGHT: 100%; TOP: 0; LEFT: 0; color:#000; line-height: 60px; font-size:20px; padding-left:10px;"></div>\
                </div>\
            </div>\
        </div>\
        <div u="navigator" class="jssorb01" style="bottom: 16px; right: 10px;"> <div u="prototype"></div> </div>\
        <span u="arrowleft" class="jssora05l" style="top: 123px; left: 8px;"> </span>\
        <span u="arrowright" class="jssora05r" style="top: 123px; right: 8px;"> </span>\
		<div style="width: 20px;right: 10px;position: absolute;">\
			<a href="#" id="close_btn" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a>\
		</div>\
    </div>\
</div>';
var jssor_slider1;




function viewimage(a){
	var cImage = $('<div class="image_form" title="Image">\
			<img src="'+$(a).attr('src')+'"/>\
			</div>');

			cImage.dialog({
				resizable: true,
				height: 'auto',
				width: '50%',
				modal: false,
				buttons: {
					"Close": function () {				
						$(this).dialog("close");
						cImage.remove();
					}
				}
			});
}


function w_image(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	var widget_name = 'image';
	this.user_visible = true;
	this.rname = 'Изображение';
	// Additional properties array
	var addproparr = new Array();
	this.form_element = '';
	var images;
	var pluscard;
	var props={};
	if(this.object && this.object.widget && this.object.widget.properties)
		props = this.object.widget.properties;
	var userDataString = $.cookie('userData') || localStorage.user || '{}';
	var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));		

	this.addImg=function(user_id,path, title, link, mode){
		//if(user.id==user_id){
		//	link="/fm?cmd=file&target="+gethash(path);
		//}
		
		var exist=false;
		$( ".img_path" , this.form_element).each(function( index ) {
			var file_div=$(this);
			if(path==file_div.data("path")){
				//exist=true;
			}			
		});	
		if(exist)
			return;
		if(title===undefined || title==='undefined')
			title='';
		
		if(mode=='view'){
			if(props.notitle===undefined || props.notitle===true)
// 				var card=$('<div class="span2 img_path"> \
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ></a>\
// </div>');
				var card=$('<div class="card" style="width: 18rem;">\
				<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
				<div class="card-body">\
				</div>\
				</div>');
				
			else
				var card=$('<div class="card" style="width: 18rem;">\
					<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
					<div class="card-body">\
					<h5 class="card-title">'+title+'</h5>\
					</div>\
				</div>');
// 				var card=$('<div class="span2 img_path"> \
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ><p>'+title+'</p></a>\
// </div>');
		}
		else{
			if(props.notitle===undefined || props.notitle===true)
// 				var card=$('<div class="span2 img_path"> <i title="Delete image" class="icon-remove close"/>\
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ></a>\
// </div>');
				var card=$('<div class="card" style="width: 18rem;"><i title="Delete image" class="fas fa-window-close close"/>\
				<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
				<div class="card-body">\
				  <h5 class="card-title"></h5>\
				  <p class="card-text"> </p>\
				</div>\
			  </div>');
			else
				var card=$('<div class="card" style="width: 18rem;"><i title="Delete image" class="fas fa-window-close close"/>\
					<img onclick="viewimage(this)" src="'+link+'" class="card-img-top" >\
					<div class="card-body">\
					<h5 class="card-title"></h5>\
					<p class="card-text"></p>\
					</div>\
				</div>');
// 				var card=$('<div class="span2 img_path"> <i title="Delete image" class="icon-remove close"/>\
// <a href="#" class="thumbnail"><img onclick="viewimage(this)" src="'+link+'" class="smallimage" alt="" onload="" style="margin-left: 0px; margin-right: 0px; margin-top: 2px;" ></a><input type="text" class="span2" id="title" value="'+title+'"/>\
// </div>');
		}
		images.prepend(card);
		
		
		card.data('path', path);
		card.data('user_id', user_id);
		$('.close', card).click(function() {
			card.remove();
		});
	}
	
	// HTML element creation
    this.assign = function(form_element, value){

		var widget=this;
		this.form_element = form_element;
		this.form_element.addClass('imagecontainer');
		
					


		//var current_image_path=$.cookie("current_image_path");
		var current_image_path=localStorage.getItem('current_image_path');		
		
		/*
		if(current_image_path==null && $.cookie('userData')!=null){
			userdata=$.cookie('userData');
			ud=JSON.parse(decodeURIComponent(userdata == 'undefined' ? '{username:""}' : userdata));
			current_image_path='/' + ud.username;
		}
		
		var label = $('<span>Current path for upload (click here to change):</span>');
		form_element.append(label);

		
		var cpath = $('<div></div>');
		label.append(cpath);
		label.click(function() {
			OpenFolderDlg(function(path){
				localStorage.setItem('current_image_path', path);
				//$.cookie('current_image_path', path);
				current_image_path=path;
				cpath.html(current_image_path);
			});
		});	
		
		
		cpath.html(current_image_path);
		

		var btnsdiv=$('<div style=""></div>');
		form_element.append(btnsdiv);
		var select_image=$('<button class="btn" type="button">Select file in the cloud</button>');
		btnsdiv.append(select_image);
		
		var uploaddiv=$('<div class="fileUpload btn"><span>Upload</span><input type="file" id="image_upload" class="upload" /></div>');
		var upload_image=$("input", uploaddiv);
		btnsdiv.append(uploaddiv);		
		var label = $('<label>Image list</label>');
		form_element.append(label);		
		*/
		/*
		images = $('<ul class="thumbnails"/>');
		form_element.append(images);*/
		images=form_element;
// 		pluscardvar=$('<li class="span2 addcard"> \
// <a href="#" class="thumbnail"><i class="icon-plus"></i>Add image</a>\
// </li>');
		pluscardvar=$('<a href="#" class="thumbnail"><i class="icon-plus"></i>Add image</a>');
		images.append(pluscardvar);
		
		
		/*
		upload_image.change(function(){
			uploadfilename='';
			var control = document.getElementById("image_upload");
			var i = 0,
			files = control.files,
			len = files.length;		
			if(len==0){			
				return false;
			}			
			if (current_image_path=='' || current_image_path===undefined || current_image_path==null){
				alert('You should select dir to upload!');
				return false;
			}
			uploadfilename=current_image_path+dir_separator+files[0].name;		
			
			var form = new FormData();
			form.append("cmd", "upload");
			dir=get_directory(uploadfilename);
			form.append("target", gethash(dir));
			form.append("files", control.files[0]);			
			
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				console.log("Отправка завершена");
				widget.addImg(user.id, uploadfilename,'');		
			};
			xhr.open("post", "/fm", true);
			xhr.send(form);
		});*/
		var options = new Object();
		options.filter='gif,png,jpg,jpeg';
		pluscardvar.click(function() {
			OpenFileDlg(function(path){
				widget.addImg(user.id, path, "", "/fm/op?cmd=file&target="+gethash(path));
			},options);
		});
		
		
		if(value!=undefined && value!=''){	
			if(widget.doc)
				var row_id=widget.doc.id;		
			else
				var row_id=-1;
			files=value.split(';');
			for (i=0; i < files.length; i++) {
				s=files[i].split('###');
				if(s!=undefined && s.length>0){
					p='';
					n='';
					user_id='';
					if(s.length==2){
						p=s[0];
						n=s[1];
					}
					else if (s.length==3){
						p=s[1];
						n=s[2];
						user_id=s[0];						
					}
					else
						continue;
					this.addImg(user_id,getpath(p), n, '/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id);
				}
			}
		}
/*
		var view_image=$('<div>View</div>');
		view_image.click(function() {			
			$( ".img_path" , widget.form_element).each(function( index ) {
				var file_div=$(this);
				path=file_div.data("path");
				links.push('/geothemes/bl_file?dataset_id='+widget.metaid+'&row_id='+widget.curent_row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+gethash(path));				
			});	
			var gallery = blueimp.Gallery(links);
			
		});
		this.form_element.append(view_image);*/
    }
    
	// Getting value of input field
    this.getVal = function (){
		var files=new Array();
		$( ".card" , this.form_element).each(function( index ) {
			var file_div=$(this);
			title=$('#title',this).val();
			path=file_div.data("path");
			
			
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
			if(user.id)
				user_id=user.id;
			else		
				user_id=file_div.data("user_id");
			files[files.length]=user_id+'###'+gethash(path)+'###'+title;
		});	
		return files.join(';');
    }
	this.hasFilter = function (){
		return false;
	}

    this.getUserVal=function (val, view){
		if(this.gr_fn=='none' )
			return '';		
		var widget=this;
		var row_id=widget.cntr;
		if(widget.doc)
				var row_id=widget.doc.id;	
		if(this.gr_fn!=null || typeof val =='number' )
			return val;
		images = $('<ul class="thumbnails"/>');
		//form_element.append(images);
		
		
		res='';
		var links=new Array();
		if(val!=undefined && val!=''){			
			var files=val.split(';');	
			
			
			var unique = 'img_'+randomString(10);
			
			if(files.length>0){				
				res='<i id="'+unique+'" class="icon-play-circle"></i> ';//icon-play-circle
				if(view){
					res+='';
					var default_value=''
					for (i=0; i < files.length; i++) {
						s=files[i].split('###');
						if(s!=undefined && s.length>0 ){
							p='';
							n='';
							user_id='';
							if(s.length==2){
								p=s[0];
								n=s[1];
							}
							else if (s.length==3){
								p=s[1];
								n=s[2];
								user_id=s[0];						
							}
							else
								continue;
							if(n==''){
								n=default_value;										
							}
							link='/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id;							
							
							res+='<img title="'+n+'" class="smallimagelist" src="'+link+'" />';
							this.addImg(user_id,getpath(p), n, '/dataset/file?f='+widget.metaid+'&row_id='+row_id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id, "view");

						}
					}
					return images.html()
					
				}
					setTimeout(function() {
						$('#'+unique).click(function(){
							$('#slider').remove();
							$('body').append(slider_str);
							
							var slider_div=$('#slider_content');
							slider_div.empty();
							
							var trow=$(this).parents('tr');
							aData=trow.data('doc');
							var default_value=''
							if(aData){								
								for (var i = 0; i < widget.doc_template.doc_description.columns.length; i++) {
									if(widget.doc_template.doc_description.columns[i].widget_object===undefined)
											continue;
									if(widget.doc_template.doc_description.columns[i].widget_object.widget_name===undefined || !widget.doc_template.doc_description.columns[i].widget_object.widget_name in ['edit','Classificator','date'])
										continue;
									if (widget.doc_template.doc_description.columns[i].fieldname in aData){
										value=aData[widget.doc_template.doc_description.columns[i].fieldname];
										if(value=='' || value===undefined || value==null)
											continue;
										value=widget.doc_template.doc_description.columns[i].widget_object.getUserVal(value);
										
										if(default_value!='')
											default_value+=', ';
										default_value+=value;
									}									
								}
							}
							var images='';
							for (i=0; i < files.length; i++) {
								s=files[i].split('###');
								if(s!=undefined && s.length>0 ){
									p='';
									n='';
									user_id='';
									if(s.length==2){
										p=s[0];
										n=s[1];
									}
									else if (s.length==3){
										p=s[1];
										n=s[2];
										user_id=s[0];						
									}
									else
										continue;
									if(n==''){
										n=default_value;										
									}
									link='/dataset/file?f='+widget.metaid+'&row_id='+aData.id+'&fieldname='+widget.object.fieldname+'&filehesh='+p+'&userid='+user_id;
									/*									
									obj={title:n, href:link, type: 'image/jpeg'};
									links.push(obj);									*/
									
									images+='<div>\
										<img u=image src="'+link+'" />\
										<div u="thumb">'+n+'</div>\
									</div>';

								}
							}
							
							slider_div.html(images);
							
							//var gallery = blueimp.Gallery(links);
							var options = {
								$AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
								$FillMode: 1,
								$AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
								$AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
								$PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

								$ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
								$SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
								$MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
								//$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
								//$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
								$SlideSpacing: 0, 					                //[Optional] Space between each slide in pixels, default value is 0
								$DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
								$ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
								$UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
								$PlayOrientation: 2,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
								$DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

								$BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
									$Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
									$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
									$ActionMode: 3,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
									$Lanes: 2,                                      //[Optional] Specify lanes to arrange items, default value is 1
									$SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
									$SpacingY: 10                                    //[Optional] Vertical space between each item in pixel, default value is 0
								},

								$ArrowNavigatorOptions: {
									$Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
									$ChanceToShow: 1                                //[Required] 0 Never, 1 Mouse Over, 2 Always
								},

								$ThumbnailNavigatorOptions: {
									$Class: $JssorThumbnailNavigator$,              //[Required] Class to create thumbnail navigator instance
									$ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
									$ActionMode: 0,                                 //[Optional] 0 None, 1 act by click, 2 act by mouse hover, 3 both, default value is 1
									$DisableDrag: true,                             //[Optional] Disable drag or not, default value is false
									$Orientation: 2                                 //[Optional] Orientation to arrange thumbnails, 1 horizental, 2 vertical, default value is 1
								}

							};


							$('.ui-icon-closethick', $('#slider').show()).click(function(){
								$('#slider').remove();
							});						

							jssor_slider1 = new $JssorSlider$("slider1_container", options);
							var bodyWidth = document.body.clientWidth;
							//var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
							if (bodyWidth)
								//jssor_slider1.$ScaleWidth(Math.min(parentWidth-300, 600));
								jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1200));
							$("#slider1_container").zIndex(7200);

							return false;
						});
						
						$(document).keyup(function(e) {
							if (e.which == 27) $('#slider').remove();
						});
					}, 100);				
				

			}
			
		}
		
		
	    return res;
    }
	
   
    this.viewPropForm = function (container){
	   
		var ps=$("<label>Default image directory</label><input type='text' id='image_dir' value=''/><br/>");
		var input = $('#image_dir', ps);
		container.append(ps);
		if(this.object.widget.properties!=undefined && this.object.widget.properties.image_dir!=undefined   )
		   $('#image_dir', ps).val(this.object.widget.properties.image_dir);

		var img=$('<button class="btn" type="button">Open</button>');
		container.append(img);
		
		var ps1=$("<label>Has not title:</label><input type='checkbox'  value=true /><br/>");
		container.append(ps1);
		this.notitle = $('input', ps1);
		
		if(this.object.widget.properties!=undefined && this.object.widget.properties.notitle   )
		   this.notitle.attr('checked', true);
		var options = new Object();
		 
		img.click(function() {
			OpenFileDlg(function(path){
				input.val(path);
				input.trigger('change');
			},options);
		});
		   
		return;
    }    
    this.getPropForm = function (container){
		
	    return {image_dir: $('#image_dir', container).val(), notitle: this.notitle.attr('checked')};
    } 
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'file';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sWidth = '12px';

		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_image, w_base);

$.widgets.RegisterWidget('image', w_image);

