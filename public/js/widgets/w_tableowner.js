function w_tableowner(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'tableowner';
	// Additional properties array
	var addproparr = new Array();
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		var unique = this.object.fieldname;
		if (this.object.htmlname === undefined) {
			this.object.htmlname = this.object.fieldname;
		}
		var tempstr = '';
		form_element.html(tempstr);
		var el = $('#' + unique, form_element);
		if(value!=undefined && value!='')
			el.val(value);		
		
		el.attr( 'widget', widget_name)
		$.widgets.formlist.push([widget_name, form_element]); 
    }
    
	// Getting value of input field
    this.getVal = function (){
	    return $('#'+this.object.fieldname).val();
    }
    
	/**
	*	getUserVal
	*/
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';				
		if (val===undefined) {
			return '';
		}
		
		
		
		res=JSON.parse($.ajax({
					type: "GET",
					url  : '/user_list/list?s_fields=fullname,id&f_id='+val,
					async: false,
				}).responseText);
			
		if (res.aaData===undefined || res.aaData.length!=1) return 'id:'+val;
		return res.aaData[0].fullname;
			
    }
	
    this.viewPropForm = function (container){
		return;
    }
	
    this.getPropForm = function (container){		
		return {};
    }
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'autoedit';
		tempobj.oWidget = this;
		tempobj.sWidget = 'tableowner';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'OwnerFilterWidget';
		return tempobj;
	}
	
	this.hasFilter = function (){
		return true;
	}
    return this;
}

extend(w_tableowner, w_base);

$.widgets.RegisterWidget('tableowner', w_tableowner);
