function w_boolean(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	var widget_name = 'boolean';
	this.fieldtype = 'bool';
	this.user_visible = true;
	this.rname = 'Логический тип';		
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		this.input = $( '<select title="'+this.object.description+'" class="form-control" />' );
		var input=this.input;
		form_element.append(this.input);
		var vals={'да':true, 'нет': false, 'неизвестно':'NULL'};
		if(value===undefined || value==='')
			value='NULL';
		for (index in vals) {
			if(vals[index]==value || vals[index]==''+value)
				option = $('<option selected value="'+vals[index]+ '">' + index+ '</option>');
			else
				option = $('<option value="'+vals[index]+ '">' + index+ '</option>');
			input.append(option);
		}
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.input)
			return this.input.val();
		else
			return 'NULL';
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='count')
			return val;
		if(this.gr_fn=='none' )
			return '';
			
		if(val==true || val=='true')
			return '<img src="/images/icons/glyphicons_152_check.png"/>';
		else if(val==false || val=='false')
			return '<img src="/images/icons/glyphicons_153_unchecked.png"/>';
		else
			return '';
    }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Checkbox options</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		try {
			$('#select_options',container).val(this.object.widget.properties.options);
		} catch(e) {
		
		}
		return;
    } 
	
    this.getPropForm = function (container){
		var wsize=$('#select_options',container).val();
	    return {options:wsize};
	}
	
	this.hasFilter = function (){
		return true;
	}	
	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'wboolean';		
		tempobj.sWidget = 'wboolean';
		tempobj.onblur = 'submit';
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'BooleanFilterWidget';
		tempobj.sWidth = '12px';

		return tempobj;
	}	
    
	extend(this, new w_edit(object)); 		
    return this;
}

extend(w_boolean, w_base);
$.widgets.RegisterWidget('boolean', w_boolean);
