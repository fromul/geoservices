

/**
	* A filter widget based on data in a table column.
	* 
	* @class GeometryFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.OwnerFilterWidget = function( $Container, column, dt_table, filtervalue ){
		var widget = this;	
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;		
		
		
		
		widget.asFilter = 'All';
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		widget.$GeometryRange = $('<select style="width:110px;" />');
		var v='All';
		var option = $('<option />').val(v).append('All');
		widget.$GeometryRange.append(option);

		var v='curent';
		var option = $('<option />').val(v).append('Mine');
		widget.$GeometryRange.append(option);

			
		var SelectionItems;
		widget.$GeometryRange.change(function() {	
			widget.asFilter=$(this).val();
			widget.fnFilter();	
		});
		if(filtervalue){
			widget.$GeometryRange.val('curent');
			widget.oDataTable.fnFilter( filtervalue, widget.column.fieldname);
		}
		
		//widget.$label = $('<label for="from_' + widget.oColumn.sName + '">' + widget.oColumn.sTitle + '</label><br>');
		//widget.$Container.append( widget.$label );
		widget.$Container.append( widget.$GeometryRange );
	};	
	
	
	
	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.OwnerFilterWidget.prototype.fnFilter = function() {		
		var widget = this;	
		if ( widget.asFilter=='' || widget.asFilter=='All') {
		    widget.oDataTable.fnFilter( '', widget.column.fieldname);			
		}
		else {
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
			widget.oDataTable.fnFilter( user.id, widget.column.fieldname, true, false );
		}
	};	

