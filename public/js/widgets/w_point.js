function w_point(object) {
	// Object for creation of widget
    this.object = object;
	// Common name of widget
	this.fieldtype = 'point';
	this.geometry_type = 'Point';
	var widget_name = 'Point';
	// Additional properties array
	var addproparr = new Array();
	this.mapfile='map_point_'+makeid()+'.map';
	this.parser=null;
	this.user_visible = true;
	this.rname = 'Точка';
	// HTML element creation
	this.pnt_container;
	this.pnt_part;
	
    this.assign = function(form_element, value){
		if(this.object.widget.properties.mode == 'multi')
			this.multi=true;		
		else
			this.multi=false;
		this.g_type='point';		
		this.g_assign(form_element, value);
		return;
    }
    
	// Getting value of input field
    this.getVal = function (){
		return this.getGeometry();
	    //return $('#'+this.object.fieldname,this.form_element).val();
    }
    
	// Getting JSON of field for further saving
    this.getUserVal=function (val){
		if(this.gr_fn=='none' )
			return '';		
		if(this.gr_fn=='count' || typeof val =='number' )
			return val;
			
		if (val !== null && (typeof val === 'object') && (val.name)) {
			return val.name;
		}
		if (val=== undefined || val==null) return "";
		var tempval = str_replace(')', '', str_replace('POINT(', '',str_replace('MULTIPOINT(', '', val)));		
		var temparr = tempval.split(',');
		var re=/MULTIPOINT\(([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\)/;
		var myArray = re.exec(val);
		if (myArray!=null && myArray.length<3) {
			var tempstr = '';
			var stopper = 2;
			if (temparr.length < stopper)
				stopper = temparr.length;
			for (var i = 0; i < stopper; i++) {
				tempstr += FromDec(temparr[i]) + '; ';
			}
			return tempstr + '... кликните для полного списка'
		}
		if(decimalcoordinates){
			coords=temparr[0].split(' ');
			return coords[1]+' '+coords[0];
		}
		else
			return FromDec(temparr[0]);
    }

	this.olap = function(node, value){	
		this.init_olap(node, value);		
		this.group.append($('<option value="regions">Group by regions</optiion>'));
		this.group.append($('<option value="districts">Group by districts</optiion>'));
	}
    
	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='point_mode' name='point_mode'><option value='single'>Одна точка</option><option value='multi'>Мультиточки</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		//if('properties' in  this.object.widget && 'label' in this.object.widget.properties && this.object.widget.properties.label!=undefined && this.object.widget.properties.label!='')
			
		try {
			$('#point_mode',container).val(this.object.widget.properties.mode);
			$('#label',container).val(this.object.widget.properties.label);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#point_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val(), gtype: 'point'};
    }
	
	
	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_point, w_geometry);
$.widgets.RegisterWidget('point', w_point);