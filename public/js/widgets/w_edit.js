function w_edit(object) {	
	var widget=this;
    this.object = object;
	// Common name of widget
	this.widget_name = 'edit';
	this.form_element = '';
	this.input;
	
	this.user_visible = true;
	this.rname = 'Строка';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var con=form_element;
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		input = $('<input title="'+this.object.description+'" class="form-control" type="text"/>');		
		form_element.append(input);
		this.input = input;
		if(value==='NULL')
			value='';
		else if(value!=undefined && value!='')
			this.input.val(removeescapeHtml(value));
		
		if (this.metaid !== undefined) {
			w_settings = {
				autofill    : true,
				cacheLength : 10,
				max         : 5,
				autofocus   : true,
				highlight   : true,
				mustMatch   : true,
				selectFirst : true,
				delay: 500,
				source		: function (request, response) {
					if(request.term.length<3)
						return;
					var result=[];					
					$.ajax({
						url: '/dataset/list?f='+ widget.metaid + '&iDisplayStart=0&iDisplayLength=10&iSortingCols=1&iSortCol_0=1&distinct=t&f_' + widget.object.fieldname +'=' + request.term + '&s_fields=' + widget.object.fieldname,
						dataType: "json",						
						success: function(data) {
							for (var i = 0; i < data.aaData.length; i++) {
								result.push(data.aaData[i][ widget.object.fieldname ]);
							}
							response(result);
						}
					});
				},
				appendTo    : form_element
			};

			this.input.autocomplete(w_settings, {
				dataType:'json',
				change: function(event, ui) {
					con.change();
					widget.sendmessage(widget.val);
				},
				formatItem: function(row, i, n) {                                                        
				  return row.value;
				},
			});	
			
			this.input.change(function(){
				widget.val=widget.input.val();
				widget.sendmessage(widget.val);
			});
		}
		this.changevalue('test1212121','');	
	
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';
		
		return this.input.val();
    }
    this.getUserVal=function (val){
		if(val==null || val==='NULL')
			return '';
		if(this.gr_fn=='none' )
			return '';
		return val;	   
    }
    // Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<div class='tab_field' ><label class='tab_field_label'>Поиск по полю (только тип tsvector)</label><input class='tab_field_input' type='text' id='tsvector'/></div>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined){
			$('#w_edit_size',container).val(this.object.widget.properties.size);
			$('#tsvector',container).val(this.object.widget.properties.tsvector);
		}		
    }
    this.getPropForm = function (container){
		//var wsize=$('#w_edit_size',container).val();
		var tsvector=$('#tsvector',container).val();
	    return {tsvector:tsvector};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterW_Edit';
		tempobj.sWidth = this.object.widget.properties.size*10+'px';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_edit, w_base);
$.widgets.RegisterWidget('edit', w_edit);