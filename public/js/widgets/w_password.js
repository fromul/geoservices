function w_password(object) {	
	var widget=this;
    this.object = object;
	// Common name of widget
	this.widget_name = 'password';
	this.form_element = '';
	this.input;
	
	this.user_visible = true;
	this.rname = 'Пароль';
	
	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;		
		var con=form_element;
		var label = $('<label for="inp_'+this.object.fieldname+'">'+this.object.title+'</label>');
		input = $('<input title="'+this.object.description+'" class="form-control" type="password"/>');		
		form_element.append(input);
		this.input = input;
		this.input.val(value);
    }
    
	// Getting value of input field
    this.getVal = function (){
		if(this.visible!=undefined && this.visible==false)
			return '';		
		return this.input.val();
    }
    this.getUserVal=function (val){
		return '';		
    }
    // Additional properties form
    this.viewPropForm = function (container){
		
    }
    this.getPropForm = function (container){		
	    return {};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'widget_proxy';		
		tempobj.sWidget = 'password';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterw_password';
		tempobj.sWidth = this.object.widget.properties.size*10+'px';
		return tempobj;
	}

	this.hasFilter = function (){
		return true;
	}	

    return this;
}

extend(w_password, w_base);
$.widgets.RegisterWidget('password', w_password);