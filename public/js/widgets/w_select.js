function w_select(object) {
	// Object for creation of widget   
	this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.user_visible = true;
	this.rname = 'Список';
	var widget_name = 'select';
	this.fieldtype = 'string';

	this.assign = function (form_element, value) {
		this.form_element = form_element;
		this.input = $('<select title="' + this.object.description + '" class="form-control" />');
		var input = this.input;
		form_element.append(this.input);
		vals = this.object.widget.properties.options.split(';');
		//var vals={};//{'true':true, 'false': false, 'unknown':'NULL'};
		for (var i = 0; i < vals.length; i++) {
			vs = vals[i].split(':');
			if (vs[0] == value || vs[0] == '' + value)
				option = $('<option selected value="' + vs[0] + '">' + vs[1] + '</option>');
			else
				option = $('<option value="' + vs[0] + '">' + vs[1] + '</option>');
			input.append(option);
		}
		//input.append($('<option value="NULL">--</option>'));
	}

	this.getVal = function () {
		return this.input.val();
	}
	this.getUserVal = function (value) {
		if(this.gr_fn=='none' )
			return '';		
		vals = this.object.widget.properties.options.split(';');
		//var vals={};//{'true':true, 'false': false, 'unknown':'NULL'};
		for (var i = 0; i < vals.length; i++) {
			vs = vals[i].split(':');
			if (vs[0] == value || vs[0] == '' + value)
				return vs[1];
		}
		return '';
	}

	this.viewPropForm = function (container) {
		var ps = $("<label>Select options, example: 1:ok; 2:true; 3:unknown</label><input type='text' id='select_options'  name='select_options' />");
		container.append(ps);
		try {
			$('#select_options', container).val(this.object.widget.properties.options);
		} catch (e) {

		}
		return;
	}
	this.getPropForm = function (container) {
		var options = $('#select_options', container).val();
		return { options: options };
	}

	this.hasFilter = function (){
		return true;
	}	

	this.getDatatableProperties = function () {
		var tempobj = new Object();
		tempobj.bVisible = true;
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;		
		tempobj.sFilterName = 'widget_filter';

		return tempobj;
	}

	//extend(this, new w_edit(object)); 		
	return this;
}


extend(w_select, w_base);
$.widgets.RegisterWidget('select', w_select);
