
function viewmapdetail(elmnt){
	var mapname=$(elmnt);
	ul=$('ul',mapname.parent().parent());
	if (ul.is(":visible") == true){
		ul.hide();
	}
	else{
		ul.show();
	}
};

function viewmapclick(elmnt){
	m_checkbox=$(elmnt);
	if(m_checkbox.css('display')=='none')
		return;
	var maps=$(".mapitem", $('#maplist_div'));
	for(var i=0; i<maps.length; i++){
		var map=$(maps[i]);
		var doc_data_base=map.data('doc');
		var value=doc_data_base['json'];
		if(typeof value == "string"){
			value=JSON.parse(value);
			doc_data_base['json']=value;
		}
		hidemap(value);
	}
	var map=m_checkbox.parents(".mapitem");
	var doc_data_base=map.data('doc');
	var value=doc_data_base['json'];
	if(typeof value == "string"){
		value=JSON.parse(value);
		doc_data_base['json']=value;
	}
	
	inputval=m_checkbox.is(':checked');
	$( "input" , $('#maplist_div')).prop( "checked", false );
	m_checkbox.prop( "checked", inputval);	
	if(inputval)
		viewmap(value, doc_data_base['mapcenter'], doc_data_base['mapscale'], doc_data_base['overlay'] );	
};


function w_map(object) {
	// Object for creation of widget   
    this.object = object;
	// Counter of element
	this.cntr = '';
	var input;
	// Common name of widget
	var widget_name = 'Map';
	this.user_visible = true;
	this.rname = 'Формирование карты';

	var _self = this;
	



	// HTML element creation
    this.assign = function(form_element, map){
		function itemchange(item){
			layer=item.data("layer");
			layer.name=$("input[type='text']",item).val();
			layer.viewtab=$("input[type='checkbox']", item).prop( "checked" );
		}
		function resetfilter(item){
			layer=item.data("layer");
			layer.filter=undefined;			
			$('#filter',item).html('');
		}
		function copyfilter(item){
			layer=item.data("layer");
			for(var i = 0; i < datasets.length; i++){
				if(datasets[i].dataset_id==layer.id){
					layer.filter=datasets[i].FilterValues;
					$('#filter',item).html(JSON.stringify(layer.filter));
					break;
				}
			}
			//layer.name=$("input[type='text']",item).val();
			//layer.viewtab=$("input[type='checkbox']", item).prop( "checked" );
		}
		this.form_element = form_element;		
		var input = $('<div class="row-fluid"><input id="mapname" class="widget_input"  style="width:280px;" type="text" /></div>');
		form_element.append(input);		
		var layerlist = $('<div class="row-fluid"><div class="span8"><ul id="layerlist"></ul></div></div>');
		form_element.append(layerlist);
		layerlist=$('#layerlist', form_element);
		if (typeof map == 'string') {
			try{
			   var map = JSON.parse(map);
			}
			catch(e){
			   map=undefined;
			}
		}
		if(map!==undefined){			
			$('#mapname',this.form_element).val(map.name);
			for(var i=0; i<map.layerlist.length; i++){
				var ch=''
				if(map.layerlist[i].viewtab)
					ch="checked";
				var l=$('<li><input type="text" value="' + map.layerlist[i].name + '"/> View table <input type="checkbox" '+ch+'/><a id="copy" href="#">Copy filter</a> filter: <div id="filter">'+JSON.stringify(map.layerlist[i].filter)+'</div> <a id="reset" href="#">Reset filter</a><div class="close box-close-btn">x</div></li>');
				layerlist.append(l);
				$('input',l).change(function(){
					item=$(this).parent('li');
					itemchange(item);					
				});				
				l.data("layer",map.layerlist[i]);
				$('.box-close-btn', l).click(function(){
					$(this).parent().remove();
				});
				$('#copy',l).click(function(){					
					copyfilter($(this).parent('li'));
				});
				$('#reset',l).click(function(){					
					resetfilter($(this).parent('li'));
				});
			}
		}
		
		layerlist.sortable({
			revert: true
		});
		
		inputWrapper = $('<div class="row-fluid"><div id="theme_select" class="span6"></div><div class="span1"><button id="btn_add" class="btn btn-small" type="button">Add</button></div></div>');
		form_element.append(inputWrapper);
		
		var theme_select=$.widgets.get_widget_byname('theme_select');
		cont=$('#theme_select', inputWrapper);
		theme_select.assign(cont, '');
		
		$('#btn_add', form_element).click(function(){
			var layer_id = theme_select.getVal();
			getdatasetmeta(layer_id, function(error, table_json){
				if(error!=null)
					return;
							
				var layer={id:layer_id, name:table_json.title, viewtab:false};
				var l=$('<li><input type="text" value="' + layer.name + '"/> View table <input type="checkbox"/><a id="copy" href="#">Copy filter</a>filter:<div id="filter">'+layer.filter+'</div> <a id="reset" href="#">Reset filter</a><div class="close box-close-btn">x</div></li>');
				layerlist.append(l);
				layerlist.sortable( "refresh" );	
				l.data("layer",layer);
				$('input',l).change(function(){					
					itemchange($(this).parent('li'));
				});
				$('.box-close-btn', l).click(function(){
					$(this).parent().remove();
				});				
				$('#copy',l).click(function(){					
					copyfilter($(this).parent('li'));
				});
				$('#reset',l).click(function(){					
					resetfilter($(this).parent('li'));
				});
			});
		});
    }
	
    this.getVal = function (){		
		var map={name:$('#mapname',this.form_element).val(), layerlist:[]};
		$('ul#layerlist > li',this.form_element).each(function(){
			l=$(this).data("layer");
			map.layerlist.push(l);
		});
	    return map;//JSON.stringify(map);;
    }
	
    this.getUserVal=function (map){
		if(this.gr_fn=='none' )
			return '';		
		if(map === undefined) 
			return '';		
		
		if (typeof map == 'string') {
			try{
			   var map = JSON.parse(map);
			}
			catch(e){
			   map=undefined;
			}
		}
		if(map === undefined) 
			return '';		
		var res='<div><div class="form-check" value="1">\
		<input class="mapwidget form-check-input" type="checkbox" onclick="viewmapclick(this)"/>\
		<label onclick="viewmapdetail(this)" class="mapname form-check-label">'+map.name+'</label>\
		</div><ul class="mapdetails">';		
		for(var i=0; i<map.layerlist.length; i++){
			res+='<li>' + map.layerlist[i].name + '</li>';			
		}
		res+="</ul></div>";
		return res;
    }
  

  
	// Additional properties form
    this.viewPropForm = function (){};
	
    this.getPropForm = function (){return {};};
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';		
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}
    return this;
}
extend(w_map, w_base);
$.widgets.RegisterWidget('Map', w_map);

