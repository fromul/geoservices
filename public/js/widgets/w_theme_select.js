
function w_theme_select(object) {
	// Object for creation of widget   
    this.object = object;
	var widget=this;
	// Counter of element
	this.cntr = '';
	var input;
	// Common name of widget
	var widget_name = 'theme_select';
	this.user_visible = true;
	this.rname = 'Выбор таблицы';

	
	var useSelectionControl;
	var correspondingDataset;
	
	this.dataset_id=-1;
	this.displayfields=[];
	this.tabledraw=null;
	this.value='';
	
		
		
	if(this.object && this.object.widget && this.object.widget.properties && this.object.widget.properties.dataset_id){
		this.dataset_id=this.object.widget.properties.dataset_id;
		if(this.object.widget.properties.fields){
			this.displayfields=this.object.widget.properties.fields.split(',');
			this.displayfields.push('id');		
		}
	}
	else{
		this.dataset_id=100;
	}
	getdatasetmeta(this.dataset_id, function(error, table_json){
		if(error!=null)
			return;
		table_json.metaid = widget.dataset_id;			
		for(var i=table_json.columns.length - 1; i>=0; i--){				
			//if( table_json.columns[i].fieldname==props.reftablename)
			//	table_json.columns[i].visible=false;				
			if((widget.displayfields.length>0 && widget.displayfields.indexOf(table_json.columns[i].fieldname)==-1) )
				table_json.columns.splice(i,1);
		}
		widget.table_json=table_json;
		widget.doc_frm= new doc_template(table_json);
		widget.doc_frm.init_form(function(){
			if(widget.tabledraw)
				widget.tabledraw();
	
		});
	});
	
	/**
	 * If global dataset storage is set, corresponding dataset is extracted.
	 *
	 * @param {Number} id Dataset id
	 *
	 * @return {Object}
	 */
	function getCorrespondingDataset(id) {
		var dataset = false;
		var id = parseInt(id);

		if (window.datasets !== undefined) {
			for (var i = 0; i < datasets.length; i++) {
				if (parseInt(datasets[i].dataset_id) === id) {
					dataset = datasets[i];
					break;
				}
			}
		}

		return dataset;
	}


	// HTML element creation
    this.assign = function(form_element, value){	
		this.form_element = form_element;
		this.value=[];
		if(typeof value)
			this.value=value;
		var props = this.object.widget.properties;
		var tab=$('\
		<div class="row no-gutters">\
			<div class="col-10 selectedval form-control px-lg-3">\
				Empty\
			</div>\
			<div class="col-2">\
				<button type="button" class="btn searchbtn">Select</button>\
			</div>\
		</div>\
		\
		');
		var selectDlg=$('<div class="subtab"></div>');
		//<a class="btn" href="#" id="btnEditTab" title="Edit the table"><i class="icon-pencil"></i></a>
		var subtab=selectDlg;//$('.subtab',selectDlg);
		
		this.table_container=subtab;
		//subtab.html(this.getUserVal(value));
		form_element.append(tab);

		
		$('.searchbtn',tab).click(function(){
			selectDlg.dialog({
				resizable: true,
				height: '800',
				width: '800px',
				modal: true,
				buttons: {
					"Close": function () {				
						$(this).dialog("close");						
					}
				}
			});
		})
		if(value && value!=''){
			var sAjaxSource="/dataset/list?f="+this.dataset_id+'&count_rows=false';
			$.ajax({
				url: sAjaxSource + '&iDisplayStart=0&iDisplayLength=1&f_id='+value,
				dataType: "json",
				success: function (data) {
					if (data.aaData) {
						setcontent(data.aaData[0]);
					}
				}
			});
		}

		function setcontent(doc){
			$('.selectedval', tab).empty();
			widget.curentdoc=doc;
			widget.doc=doc;
			widget.gtable.doc_frm.show_form($('.selectedval', tab), doc, '', true);
		}
		
		function SetVal(row){			
			
			doc=row.data('doc');
			setcontent(doc)
			if(widget.dataset_id==geodatasetguid ||widget.dataset_id==100)
				widget.value=doc.guid;
			else
				widget.value=doc.id;
			widget.sendmessage(widget.value);
			selectDlg.dialog("close");
		}
		
		function tableDraw(){
				var sAjaxSource = "/dataset/list?f=" + widget.dataset_id + '&count_rows=true';
				widget.gtable = new gdataset(widget.table_json, widget.dataset_id, subtab, sAjaxSource, undefined, false);
				widget.gtable.drawtablehead();
				widget.gtable.row_on_page_count=5;
				widget.gtable._fnReDraw();
				widget.gtable.rowselect.push(SetVal);
				var tr = $('#dt_fltr', subtab);			
				tr.show("slow");
				if(widget.value && widget.value!=''){
					$.getJSON("/dataset/list?f="+ widget.dataset_id+"&iDisplayStart=0&iDisplayLength=1&f_id=" + widget.value, function(data) {			
						if(data.aaData.length!=1)
							return;
						var doc    = data.aaData[0];
						$('.subtab', selectDlg).empty();			
						widget.gtable.doc_frm.show_form($('.subtab', selectDlg), doc, '', true);
					});					
				}
		}
		if(widget.table_json){
			tableDraw();
		}
		else{
			this.tabledraw=tableDraw;
		}
		
		
		
		
		return;	
		
    }

    this.getVal = function (){
		
		return this.value;
		
		var geos_row_selected = $('tr.geos_row_selected', this.table_container);
		if (geos_row_selected.length != 1)
			return '';
		
		var doc = geos_row_selected.data('doc');
		this.doc=doc;
		return 	doc.id;
		
		value = input.val();
		if (correspondingDataset !== false && useSelectionControl.find("input").prop("checked") === true) {
			value += ":" + btoa(JSON.stringify(correspondingDataset.FilterValues));
		}

	    return value;
    }
    
	// Additional properties form
    this.viewPropForm = function (container){
		var sz=$("<div class='tab_field'><label class='tab_field_label'>Список полей</label><input class='shownfields' type='text'/></div><div class='tab_field'><label class='tab_field_label'>dataset_id</label><input class='dataset_id' type='text' /></div>");
		container.append(sz);
		if(this.object!=undefined && this.object.widget!=undefined && this.object.widget.properties!=undefined && this.object.widget.properties.size!=undefined){
			$('.shownfields',container).val(this.object.widget.properties.fields);
			$('.dataset_id',container).val(this.object.widget.properties.dataset_id);
		}
	};
	
    this.getPropForm = function (){
		var fields=$('.shownfields',container).val();
		var dataset_id=$('.dataset_id',container).val();
		return {fields:fields, dataset_id:dataset_id};
	};
 	
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.title;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;
		tempobj.type = 'select';		
		tempobj.sWidget = 'select';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'AutocompleteFilterWidget';
		return tempobj;
	}
    return this;
}
extend(w_theme_select, w_base);
$.widgets.RegisterWidget('theme_select', w_theme_select);

