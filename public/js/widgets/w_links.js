﻿


function w_links(object) {
	var widget=this;
	var _self=this;
	// Object for creation of widget	
    this.object = object;
	
	this.cntr = '';
	// Common name of widget
	var widget_name = 'links';
	// Additional properties array
	var addproparr = new Array();
	this.user_visible = true;
	this.rname = 'links';		
	this.fieldtype = 'integer';
	// id of selected value
	var sel_id = '';
	var ft= new Array();
	this.tables=[];
	this.filter={};
	this.fields='';
	var widget=this;
	var askdataset_id='';
	
	if(this.object!=undefined && this.object.widget!=undefined){
		
		var props = this.object.widget.properties;
		if(props && props.dataset_id){
			askdataset_id = props.dataset_id;
			getdatasetmeta(props.dataset_id  , function(error, table_json){
				if(error!=null)
					return;
				table_json.metaid = props.dataset_id;			
				if(widget.fields!=''){
					var displayfields=widget.fields.split(',');
					if(displayfields.length>0){
						displayfields.push('id');			
						for(var i=table_json.columns.length - 1; i>=0; i--){				
							if( table_json.columns[i].fieldname==props.reftablename)
								table_json.columns[i].visible=false;				
							if((displayfields.length>0 && displayfields.indexOf(table_json.columns[i].fieldname)==-1) )
								table_json.columns.splice(i,1);
						}
					}
				}
				widget.table_json=table_json;
				widget.doc_frm= new doc_template(table_json);
				widget.doc_frm.init_form(function(){		
			
				});
			});
			if(props!=undefined && props.db_field!=null && props.db_field!=undefined)
				this.fields=props.db_field.join(',');
			
		}
	}
	
	this.drawobjects=function (view){
		this.object_container.empty()
		for (var i = 0; i < _self.objectlist.length; i++) {
			if(_self.objectlist[i].del === true)
				continue;
			if(!view){
				if(_self.objectlist[i].action=='add')
					var row=$('<div class="col-sm-4 cardlist">\
					<div class="card text-center" style="width: 22rem;">\
						<div class="card-header">\
						<ul class="nav nav-pills card-header-pills">\
						<li class="nav-item"><a class="nav-link active edit-link" href="#">Edit </a></li>\
						<li class="nav-item"><a class="nav-link del-link" href="#"> Delete </a></li>\
						</ul>\
						</div>\
						<div class="card-body object_text">\
						</div>\
						</div>\
					</div>');
				else
					var row=$('<div class="col-sm-4 cardlist">\
					<div class="card text-center" style="width: 22rem;">\
						<div class="card-header">\
						<ul class="nav nav-pills card-header-pills">\
						<li class="nav-item"><a class="nav-link del-link" href="#"> Delete </a></li>\
						</ul>\
						</div>\
						<div class="card-body object_text">\
						</div>\
						</div>\
					</div>');
			}
			else
				var row=$('<div class="col-sm-4 cardlist">\
				<div class="card text-center" style="width: 22rem;">\
					<div class="card-header">\
					<ul class="nav nav-pills card-header-pills">\
					</ul>\
					</div>\
					<div class="card-body object_text">\
					</div>\
					</div>\
				</div>');
			row.data('doc',_self.objectlist[i])
			this.object_container.append(row);
			widget.doc_frm.show_form($('.object_text',row), _self.objectlist[i], '', true);
		}
		$('.del-link', this.object_container).click(function(){
			if(confirm("Delete?")){						
				var doc=$(this).parents('.cardlist').data('doc');
				index=_self.objectlist.indexOf(doc);
				if(index!=-1)
					_self.objectlist.splice(index,1)
				
				_self.drawobjects(false);
			}
		});
		$('.edit-link', this.object_container).click(function(){
			var doc=$(this).parents('.cardlist').data('doc');
			var AddDlg = $('<div class="image_form" title="Item">\
			</div>');
			if(widget.doc_frm){
				var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div><div class="btn-group"> <button class="btn add">Save</button>  <button class="btn cancel">Cancel</button> </div></div>');
				AddDlg.append(row);
				widget.doc_frm.show_form($('.fields',row), doc, '', false);
				
				$('.add',row).click(function(){
					widget.doc_frm.save_form(function(newobject, msg){
						if (msg.status != 'ok') {
							var error_box = $('.text-error', container);
							if (error_box.length > 0) {
								error_box.html(msg);
							}
							else
								alert(msg);
							return;
						}
						else{
							index=_self.objectlist.indexOf(doc);
							if(index!=-1){
								_self.objectlist[index]=newobject;
								_self.objectlist[index].action='add';
							}
							_self.drawobjects(false);
							row.remove();
							AddDlg.dialog("close");
							AddDlg.remove();
						}
							
					}, false);
				})	
				$('.cancel',row).click(function(){
					
					row.remove();
					AddDlg.dialog("close");
					AddDlg.remove();
				})					
			}
			
			AddDlg.dialog({
				resizable: true,
				height: 'auto',
				width: 'auto',
				modal: false,
				buttons: {
				}
			});
						
			
		});		
	}
	
	this.AddItem=function (){		
		var AddDlg = $('<div class="image_form" title="Item">\
		</div>');
		if(widget.doc_frm){
			var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div><div class="btn-group"> <button class="btn add">Save</button>  <button class="btn cancel">Cancel</button> </div></div>');
			AddDlg.append(row);
			widget.doc_frm.show_form($('.fields',row), {}, '', false);
			//btns.hide();
			$('.add',row).click(function(){
				widget.doc_frm.save_form(function(newobject, msg){
					if (msg.status != 'ok') {
						var error_box = $('.text-error', container);
						if (error_box.length > 0) {
							error_box.html(msg);
						}
						else
							alert(msg);
						return;
					}
					else{
						newobject.action='add';
						_self.objectlist.push(newobject);
						_self.drawobjects(false);
						row.remove();
						AddDlg.dialog("close");
						AddDlg.remove();
					}
						
				}, false);
			})	
			$('.cancel',row).click(function(){				
				row.remove();
				AddDlg.dialog("close");
				AddDlg.remove();
			})					
		}
		
		AddDlg.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {
			}
		});
				
	}
	
	this.SelectItem=function (){		
		var AddDlg = $('<div class="image_form" title="Item">\
		</div>');

		// var metadataset_select = {
		// 	"fieldname": "row",
		// 	"title": "Row select",
		// 	"description": "",
		// 	"visible": true,
		// 	"widget": {
		// 		"name": "theme_select",
		// 		properties: {
		// 			dataset_id: askdataset_id
		// 		}
		// 	}
		// };
		
		// var dataset_select = $.widgets.get_widget(metadataset_select);
		function SetVal(row){
			doc=row.data('doc');
			newobject=doc;
			newobject.add=true;
			newobject.action='select';
			_self.objectlist.push(newobject);
			_self.drawobjects(false);				
			AddDlg.dialog("close");
			AddDlg.remove();
		}

		var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div></div>');
		AddDlg.append(row);
		// dataset_select.assign($('.fields', row), '');	
		var tabcon = $('.fields', row);


		var sAjaxSource = "/dataset/list?f=" + askdataset_id + '&count_rows=true';
		widget.gtable = new gdataset(widget.table_json, askdataset_id, tabcon, sAjaxSource, undefined, false);
		widget.gtable.drawtablehead();
		widget.gtable.row_on_page_count=5;
		widget.gtable._fnReDraw();
		widget.gtable.rowselect.push(SetVal);
		var tr = $('#dt_fltr', tabcon);			
		tr.show("slow");
			

		
		AddDlg.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {				
				"Cancel": function () {				
					$(this).dialog("close");
					AddDlg.remove();
				}
			}
		});	
	}

    this.assign = function(form_element, value){		
		this.form_element = form_element;
		var menu=$('<div class="container row"></div>');		
		form_element.append(menu);
		var object_container=$('<div class="container row"></div>');
		this.object_container=object_container;
		
		form_element.append(object_container);
		
		items = $('<ul class="thumbnails"/>');
		
		form_element.append(items);
		
		pluscardvar=$('<div class="addcard"> \
			<button type="button" class="btn btn-secondary btn-sm add">Add</button>\
			<button type="button" class="btn btn-secondary btn-sm select">Select</button>\
		</div>');
		menu.append(pluscardvar);
		$('.add',pluscardvar).click(function(){
			_self.AddItem();
		});
		$('.select',pluscardvar).click(function(){
			_self.SelectItem();
		});
		
		if(Array.isArray(value))
			_self.objectlist=value;
		else
			_self.objectlist=[];
		_self.drawobjects(false);
    }
    
	
    this.getVal = function (){
		res=JSON.stringify(this.objectlist, function(key, value) {
			if (key == 'trow') {
			  return undefined; // удаляем все строковые свойства
			}
			return value;
		});
		return JSON.parse(res);
		//return this.objectlist;
		
		
		var doclist=[];		
		for (var i = 0; i < this.objectlist.length; i++) {
			//if(this.objectlist[i].add===true){
				//var newobj={docid:doc_id, datasetid:master_id, classname:classname, rowid:this.objectlist[i].id, rowdatasetid:askdataset_id, action:'add'};
				doclist.push({id:this.objectlist[i].id, action:this.objectlist[i].action});
			//}
		}		
		return doclist;
    }
		
	
	this.getUserVal=function (val, view){
		if(this.gr_fn=='none' )
			return '';		
		if(val=='NULL')
			return '';
		var props = this.object.widget.properties;
		if($.isArray(val) && this.table_json && val.length>0){
			if(!view)
				return val.length;
			
			var object_container=$('<ul class="thumbnails object_container"></ul>');
			this.object_container=object_container;
			
			if(Array.isArray(val))
				_self.objectlist=val;
			else
				_self.objectlist=[];
			_self.drawobjects(false);
			return object_container.html();
			
		}
		else
			return '-';
    }    

	// Additional properties form
    this.viewPropForm = function (container){
		var widget=this;
		var props = this.object.widget.properties;
		if(props===undefined)
			props={};
		var sz="<div class='tabselect'></div>fields<input type='text' class='fields'/>mark<input type='text' class='mark'/>";
		container.append(sz);
		
		
		
		var metaTableSelect = {
					"fieldname": "",
					"title": "",
					"description": "",
					"visible": true,
					"widget": {
						"name": "theme_select",
						properties: {}
					}
				};
		var tableSelect = $.widgets.get_widget(metaTableSelect);
		tableSelect.assign($('.tabselect', container), props.dataset_id);
		$('.fields',container).val(props.fields);
		$('.mark',container).val(props.mark);
		
		this.tableSelect=tableSelect;
    }  

    this.getPropForm = function (container){
		return {
				dataset_id: this.tableSelect.getVal(),
				fields: $('.fields',container).val(),
				mark: $('.mark',container).val()
			};
    }
 
	this.getDatatableProperties = function (){
		var tempobj = new Object();
		tempobj.bVisible = true;	
		tempobj.sTitle = this.object.fieldname;
		tempobj.sName = this.object.fieldname;
		tempobj.sType = this.object.type;		
		tempobj.type = 'widget_proxy';	
		tempobj.sWidget = 'edit';
		tempobj.oWidget = this;
		tempobj.sWidgetProperties = this.object.widget.properties;
		tempobj.sFilterName = 'widget_filter';
		tempobj.sWidth = '200px';
		return tempobj;
	}

	this.hasFilter = function (){
		return false;
	}
	this.olap = function(node, value, meta){	
		this.init_olap(node, value);
		var vis=false;
		var sz=$("<div class='form-item' ><label>Select field</label><br>\
		<select  id='db_field' name='db_field' ><option>Select the field </option></select>min color<input style='width:100px;' class='min_color' value='000000'/>\
		max color<input style='width:100px;' class='max_color' value='000000'/><button id='change_map' class='btn btnApply' type='button'>Apply</button></div>");
		var change_map=$('#change_map',sz);
		change_map.click(function(){
			meta.layer.update_map();
		});
		
		sz.hide();
		this.group.after(sz);
		// $(".min_color",sz).colorpicker();
		// $(".max_color",sz).colorpicker();
		this.group.change(function(){
			if(vis)
				sz.hide();
			else
				sz.show();
			vis=!vis;
		});
		select_input=$('#db_field',sz);
		for (var i = 0; i < meta.columns.length; i++) {
			option = $('<option value="'+meta.columns[i].fieldname+ '">'+ meta.columns[i].title+ ' (' + meta.columns[i].fieldname+ ') </option>');
			select_input.append(option);
		}
	}
	

    return this;
}
extend(w_links, w_base);
$.widgets.RegisterWidget('links', w_links);