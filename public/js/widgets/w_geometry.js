var clipboard_buffer=''

function addzero(v, cnt){  
	v=v+'';
	while (cnt>v.length){
	  v='0'+v;
	}  
	return v;
}

function addsec(v){  
	v=v+'';
	ps=v.split('.');
	for(i=ps[0].length; i<2; i++){
		v='0'+v;
	}
	/*
	while (2>v.length){
	  v='0'+v;
	} */ 	
	while (5>v.length){
		if(v.length==2)
			v=v+'.';		
		else
			v=v+'0';
	}  
	return v;
}

function roundPlus(x, n) { //x - число, n - количество знаков 
	if(isNaN(x) || isNaN(n)) return false;
	var m = Math.pow(10,n);
	return Math.round(x*m)/m;
}
  
  
function Dec2minute_sec(val) {
	if(val<0)
		val=-val;
	
	var sec=val*3600;
	var gr=Math.floor(sec/3600);
	sec=sec % 3600;
	var mn = Math.floor(sec / 60);
	sec = roundPlus(sec % 60, 2);
	/*
	var gr=Math.floor(val);
	var lon_gr = addzero(gr,3);
	var mn=Math.floor((val - gr) * 60);
	var lon_mn = addzero(mn,2);	
	var sec=Math.floor( (((val - gr) * 60 - mn) * 60) * 100 ) / 100;
	var lon_sec = addsec(sec,5);
	*/
	var lon_gr = addzero(gr,3);
	var lon_mn = addzero(mn,2);
	var lon_sec = addsec(sec,5);
	var value =lon_gr+"°"+lon_mn+"'"+lon_sec+"''";
	return value;
};
  

function minute_sec2Dec(val){
	if(val ==undefined || val=='')
		return 0;
	var sign=val.toUpperCase().charAt(0);
	sign=sign=='E' || sign=='N';
	val=val.substr(1);
	arr = val.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var sec = sec/3600;
	var mn = arr[1]/60;
	var g = parseInt(arr[0],10)+mn+sec;	
	if(!sign)
		g=-g;	
	return g;
}

function wkt2decimal(val){
	var re=/MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3)
		return {lat:0, lon:0};
	return {lon:arr[1], lat:arr[2]};
}

function wkt2minute_sec(val){
	var re=/MULTIPOINT\((-?[0-9]*\.?[0-9]*)\s(-?[0-9]*\.?[0-9]*)\)/
	var arr = re.exec(val);			
	if (arr==null || arr.length<3)
		return {lat:0, lon:0};
	lat=Dec2minute_sec(arr[2]);
	lon=Dec2minute_sec(arr[1]);
/*	
	if(arr[2]<0)
		lat='S'+lat;
	else
		lat='N'+lat;
	if(arr[1]<0)
		lon='W'+lon;
	else
		lon='E'+lon;
*/	
	if(arr[2]<0)
		lat='S'+lat;
	else
		lat='N'+lat;
	if(arr[1]<0)
		lon='W'+lon;
	else
		lon='E'+lon;	
	return {lat:lat, lon:lon};
}



function ToDec(string) {
	in_arr  =  string.split(' ');
	
	var lon_vl = in_arr[1];	
	var lon_sign=lon_vl.toUpperCase().charAt(0);
	if(lon_sign=='E' || lon_sign=='W'){		
		lon_vl=lon_vl.substr(1);
	}
	else
		lon_sign='E';
	
	var lat_vl = in_arr[0];
	var lat_sign=lat_vl.toUpperCase().charAt(0);
	if(lat_sign=='N' || lat_sign=='S'){		
		lat_vl=lat_vl.substr(1);
	}
	else
		lat_sign='N';
		
	arr = lon_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lon_sec = sec/3600;
	var lon_mn = arr[1]/60;
	var lon = parseInt(arr[0],10)+lon_mn+lon_sec;	
	if(lon_sign!='E')
		lon=-lon;
	
	arr = lat_vl.split(/[\D]/);
	var sec=arr[2]+'.'+arr[3];
	var lan_sec = sec/3600;
	var lan_mn = arr[1]/60;
	var lat = parseInt(arr[0],10)+lan_mn+lan_sec;
	if(lat_sign!='N')
		lat=-lat;
	
					
	var value = lon + " " + lat;
	return value;
};


function FromDec4coord(string, lon) {
	if(string=='')
		string = '0';		
	
	sign='N';
	if(lon){
		sign='E';
		if(string+0<0)
			sign='W';		
	}
	else{
		if(string+0<0)
			sign='S';		
	}
	res=Dec2minute_sec(string)

	
	var value =sign+res;
	return value;
};

function FromDec(string) {
	if(string=='')
		arr={0:'0',1:'0'};	
	else
		arr  =  string.split(' ');
	
	var value =FromDec4coord(arr[1],false) +" "+ FromDec4coord(arr[0],true);
	return value;
};

(function($) {
	//var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);
	var geos_init=false;

	$.coordinates = {
		//Predefined character definitions
		definitions: {
			'9': "[0-9]",
//			'N': "[NW]",
			'a': "[A-Za-z]",
			'*': "[A-Za-z0-9]"
		},
		dataName:"rawMaskFn"
	};
	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		coordinates: function(coordinates, settings) {
			geos_init=true;
			if (!coordinates && this.length > 0) {
				var input = $(this[0]);
				return input.data($.coordinates.dataName)();
			}
			settings = $.extend({
				editpointmode: "default",
				placeholder: "_",
				degree_template:"N999°99'99.99''",
				completed: null,
				decimal:false
			}, settings);
	
			// Создание дополнительных элементов управления
			var input = $(this[0]);
			string=input.val();
			
			var lon = $( '<input class="coordinate form-control" id="lon_geos_editing" size="10"/>' );
			lon.attr( 'autocomplete','off' );
			var lon_buffer=settings.degree_template.replace(/9/g,"0").replace(/N/g,"E");
			var lon_firstValue='';
			this.after(lon);

			var lat= $( '<input class="coordinate form-control" id="lat_geos_editing" size="10"/>' );
			lat.attr( 'autocomplete','off' );				
			this.after(lat);
			var lat_buffer=settings.degree_template.replace(/9/g,"0");
			var lat_firstValue='';
			
			var initval=false;
			input.attr('style', 'display: none;');
			input.val('MULTIPOINT()');
			
			input.bind("setvalue.coordinates", function (e) {
					//var input=$('#'+e.target.id, this.form);
					var input=$(this);
					val=input.val();
					if(settings.decimal){
						r=wkt2decimal(val);
						lat_buffer=r.lat;
						lat_firstValue=r.lat;
						lon_buffer=r.lon;
						lon_firstValue=r.lon;
					}
					else{
						r=wkt2minute_sec(val);
						lat_buffer=r.lat;
						lat_firstValue=r.lat;
						lon_buffer=r.lon;
						lon_firstValue=r.lon;
					}
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);
			
			input.bind("decimal.coordinates", function (e) {					
					settings.decimal=true;
					var input=$(this);
					val=input.val();
					
					r=wkt2decimal(val);
					lat_buffer=r.lat;
					lat_firstValue=r.lat;
					lon_buffer=r.lon;
					lon_firstValue=r.lon;
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);
			
			input.bind("minute_sec.coordinates", function (e) {					
					settings.decimal=false;
					var input=$(this);
					val=input.val();
					
					r=wkt2minute_sec(val);
					lat_buffer=r.lat;
					lat_firstValue=r.lat;
					lon_buffer=r.lon;
					lon_firstValue=r.lon;					
					
					initval=true;					
					lon.val(lon_buffer);
					lat.val(lat_buffer);
					writeBuffer();				
					initval=false;
				}
			);			
			
			function clearBuffer(sub_input, start, end) {
				for (var i = start; i < end && i < len; i++) {
					if (tests[i])
						if(sub_input.attr('id')=='lon_geos_editing') buffer_lon[i] = settings.placeholder;
						else buffer_lan[i] = settings.placeholder;
				}
			};
			function writeBuffer() {
				var vlat, vlon;
				if(settings.decimal){
					vlon=lon_buffer;
					vlat=lat_buffer;
				}
				else{
					vlon=minute_sec2Dec(lon_buffer);
					vlat=minute_sec2Dec(lat_buffer);
				}
				test=vlon+' '+vlat;				
				test='MULTIPOINT('+test+')';				
				input.val(test);
				//return s;
			};			
		
			function testCoordinate(vl, lon){
				var numbers='0123456789';
				if(settings.decimal){
					if(!test_number(vl))
						return false;
					vl=parseFloat(vl);					
					if( (lon && (vl>180 || vl<-180)) || (!lon && (vl>90 || vl<-90)) )
						return false;
					else
						return true;
				}
				else{
					if (vl.length<3) return false;
					fs=vl.charAt(0);
					if(!(((fs=='N' || fs=='S') && !lon) || ((fs=='E' || fs=='W') && lon) ))
						return false;
					//degree_template:"N999°99'99''",
					
					for (var j=1; j < settings.degree_template.length; j++){
						if(settings.degree_template.charAt(j)=='9'){
							if ((numbers.indexOf(vl.charAt(j))==-1) )
								return false;
						}
						else{
							if (vl.charAt(j)!=settings.degree_template.charAt(j))
								return false;					
						}					
					}
					vl=vl.substr(1);
					var parts = vl.split(/\D/,4);
					if (parts[0]<0 || parts[0]>180)  return false;
					if (parts[1]<0 || parts[1]>=60)  return false;
					if (parts[2]<0 || parts[2]>=60)  return false;
					if (parts[3]<0 || parts[3]>=100)  return false;					
				}
				return true;
			};
		
///////////////////////
			var defs = $.coordinates.definitions;
			var tests = [];
			var partialPosition = coordinates.length;
			var firstNonMaskPos = null;
			var len = settings.degree_template.length;
			function keydownEvent(e) {
				var trg=$(e.target);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;
					
					var s_val= trg.val();
					var future_val='';
					if(pos.end-pos.begin!=0){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.end,s_val.length);
					}
					else if (k==8){
						future_val=s_val.substring(0,pos.begin-1)+s_val.substring(pos.begin,s_val.length);						
					}
					else if (k==46){
						future_val=s_val.substring(0,pos.begin)+s_val.substring(pos.begin+1,s_val.length);						
					}
					
					if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
						if(e.target.id=='lon_geos_editing'){
							lon_buffer=future_val;
							lon.val(lon_buffer);
						}
						else{
							lat_buffer=future_val;
							lat.val(lat_buffer);
						}
						writeBuffer();
//						var next = seekNext(p);
						trg.caret(pos.begin);						
					}
					return false;
				} else if (k == 27) {//escape
					if(e.target.id=='lon_geos_editing')
						lon.val(lon_firstValue);
					else
						lat.val(lat_firstValue);					
					
					//trg.caret(0, checkVal(trg));
					return false;
				}
				else{
					//console.log('write buffer');					
					//writeBuffer(trg);
				}				
				return true;
			};
			
			function seekNext(pos) {
				while (++pos <= settings.degree_template.length && settings.degree_template[pos]!='9');
				if(pos==0)
					return 1;
				return pos;
			};
			
			function seekPrev(pos) {
				while (--pos >= 0 && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function keypressEvent(e) {
				var trg=$(e.target);
				//console.log('keypress');		
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					var c = String.fromCharCode(k);
					if(c==',')
						c='.';
					c=c.toUpperCase();
					var s_val= trg.val();
					var future_val='';
					var p=0;
					if(settings.decimal){
						future_val=s_val.substring(0,pos.begin)+c+s_val.substring(pos.end,s_val.length);
					}
					else{						
						if(pos.begin==0){
							future_val=c+s_val.substr(1);
							p=0;
						}
						else {
							p = seekNext(pos.begin - 1);
							if (p < len) {
								future_val=s_val.substring(0,p)+c+s_val.substring(p+1,s_val.length);
							}
							else
								return false;
						}					
					}
					
					if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
						if(e.target.id=='lon_geos_editing'){
							lon_buffer=future_val;
							lon.val(lon_buffer);
						}
						else{
							lat_buffer=future_val;
							lat.val(lat_buffer);
						}
						writeBuffer();
						var next = seekNext(p);
						if(!settings.decimal)
							trg.caret(p+1);
						else
							trg.caret(pos.begin+1);
					}					
					return false;
				}
			};
			
			function pasteval(e) {
				e.preventDefault();
				var future_val = (e.originalEvent || e).clipboardData.getData('text/plain') || prompt('Paste something..');				
				future_val=future_val.replace(/,/g,".");
				if (testCoordinate(future_val, e.target.id=='lon_geos_editing')/* tests[p].test(c)*/) {							
					if(e.target.id=='lon_geos_editing'){
						lon_buffer=future_val;
						lon.val(lon_buffer);
					}
					else{
						lat_buffer=future_val;
						lat.val(lat_buffer);
					}
					writeBuffer();					
				}	
			};

			lon.bind("keydown.coordinates", keydownEvent);
			lat.bind("keydown.coordinates", keydownEvent);
			lon.bind("keypress.coordinates", keypressEvent);
			lat.bind("keypress.coordinates", keypressEvent);			
			lon.bind("paste",pasteval);
			lat.bind("paste",pasteval);
			writeBuffer();			
		}
		
	});
	
	
})(jQuery);


function w_geometry(object) {
	// Object for creation of widget
    this.object = object;
	// Counter of element
	this.cntr = '';
	// Common name of widget
	this.fieldtype = 'point';
	var widget_name = 'point';
	// Additional properties array
	var addproparr = new Array();
	this.mapfile='map_point_'+makeid()+'.map';
	this.fullpath='F:/DATA/GISSTORAGE/mapkeeper/'+this.mapfile;
	this.parser=null;
	this.geometry_type='';
	

	// HTML element creation
	this.pnt_container;
	this.pnt_part;
	

	// Additional properties form
    this.viewPropForm = function (container){
		var ps=$("<label>Режим: </label><select id='point_mode' name='point_mode'><option value='single'>Одна точка</option><option value='multi'>Мультиточки</option></select><br>");
		container.append(ps);
		var ps=$("<label>Подпись объекта: </label><input type='text' id='label'/><br>");
		container.append(ps);
		$('#label',container).val(this.object.widget.properties.label);
		try {
			$('#point_mode',container).val(this.object.widget.properties.mode);
		} catch(e) { }
		var ps=$("<label>Ввод координат: </label><select id='edit_mode' name='edit_mode'><option value='default'>Градусы</option><option value='extended'>Градусы и десятичное представление</option></select>");
		container.append(ps);
		try {
			$('#edit_mode',container).val(this.object.widget.properties.editpointmode);
		} catch(e) { }
		return;		
    } 
    this.getPropForm = function (container){
	    return {mode: $('#point_mode',container).val(), editpointmode: $('#edit_mode',container).val(), label:$('#label',container).val()};
    }
	

	this.hasFilter = function (){
		return true;
	}

    return this;
}

extend(w_geometry, w_base);

	// Getting value of input field
w_geometry.prototype.getVal = function (){
	return this.getGeometry();	
}


w_geometry.prototype.getDatatableProperties = function (){
	var tempobj = new Object();
	tempobj.bVisible = true;	
	tempobj.sTitle = this.object.title;
	tempobj.sName = this.object.fieldname;
	tempobj.sType = this.object.type;			
	tempobj.type = 'widget_proxy';	
	tempobj.oWidget = this;
	tempobj.sWidgetProperties = this.object.widget.properties;
	tempobj.sFilterName = 'GeometryFilterWidget';
/*	tempobj.submit ='Ok';
	tempobj.cancel ='Cancel';*/
	tempobj.onblur = '';
	return tempobj;
}

w_geometry.prototype.addpart=function(){
		if(this.pnt_container===undefined)
			return;
		var tabs=$('#tabs',this.pnt_container);
		$(tabs).addClass('gp_activate');
		cnt=$( ".geom_part" , tabs).length;
		if(this.g_type!='point' && this.multi){
			var link_container=$('<li><a class="part_href" href="#tabs-'+cnt+'">'+cnt+' sketch</a> <i  title="Delete field" id="del_img" class="icon-remove"></i> </div>');
			//class="ui-button-icon-primary ui-icon ui-icon-closethick"
			$("#tab_list",tabs).append(link_container);
		}		
		var part_container=$('<div id="tabs-'+cnt+'" class="nav nav-list geom_part"/>');//btn-group btn-group-vertical
				
		tabs.append(part_container);
		var widget=this;
		//btn=$('<button class="new btn frigth" type="button">+</button>');
		if(this.g_type!='point' ){
			btn = $('<a class="new btn frigth" href="#"><i class="icon-plus-sign"></i></a>');
			btn.click(function() {
				widget.addpoint(part_container, $('.w_points',part_container).length, 0, 0);				
			});
			part_container.append(btn);
		}
		this.pnt_part=part_container;
		if(this.g_type!='point' && this.multi){
			tabs.tabs("refresh");
			tabs.tabs( "option", "active", cnt-1 );
		
			var del_btn=$("#del_img", link_container);
			del_btn.click(function(){
				link_container.remove();
				part_container.remove();
				var cnt=1;
				$('.part_href',tabs).each(function(){
					input=$(this);
					input.text(cnt+' sketch');
					cnt++;
				});
				
				return false
			});
		}
		
		var pnt_list=$('<div id="pnt_list"/>');//btn-group btn-group-vertical
		part_container.append(pnt_list);
		//part_container.append(btn);
		//tabs.sortable( "refresh" );
		tabs.find(".ui-tabs-nav").sortable({
			axis: "x",
			stop: function () {
				tabs.tabs("refresh");
			},
			sort: function(event, ui) {
				if ($(ui.item).hasClass("w_add_part")) {
					return false;
				}
			}
		});
		if(this.g_type!='point' && this.multi){
			tabs.tabs('option','active',cnt);
		}
		
		return part_container;
}

w_geometry.prototype.addpoint=function (part,index,x,y){
	while(x<-180)
		x+=360;
		
	var pnts=$('.w_points',part);
	var widget=this;
	if(index<pnts.length){
		input=$($('input',pnts[index])[0]);
		if(x!=undefined && y!=undefined){
			tr_coord = 'MULTIPOINT('+x + ' ' + y+')';
			input.val(tr_coord);
			input.trigger('setvalue.coordinates');
		}		
	}
	else{
		var d=$('<div class="w_points form-inline"/>');		
		pnt_list=$('#pnt_list',part);
		pnt_list.append( d ); 
		var input = $( '<input type="text"/>' );
        input.attr( 'autocomplete','off' );        
        d.append( input ); 
		input.coordinates("999°99\'99\'\'N", {placeholder: "0", decimal:decimalcoordinates} );	
		input.trigger('setvalue.coordinates');
		input.change(function() {
			//widget.Layer.destroyFeatures();
			widget.updateLayer();
		});
		if(x!=undefined && y!=undefined){
			tr_coord = 'MULTIPOINT('+x + ' ' + y+')';
			input.val(tr_coord);
			input.trigger('setvalue.coordinates');
		}
		//var btns=$("<i title='Move field down' id='movedown_field' class='icon-arrow-down'></i><i id='moveup_field' title='Move field up' class='icon-arrow-up'></i><i  title='Delete field' id='delete_field' class='icon-remove'></i>");
		//|| (this.g_type=='point' && !this.multi
		if(this.g_type!='point' ){
			var btns=$("<i  title='Delete field' id='delete_field' class='icon-remove'></i>");
			d.append(btns);
			
			$('#delete_field',d).click(function(){
				d.remove();
				//widget.Layer.destroyFeatures();
				widget.updateLayer();
				return false
			});
		}
		var cp=$("<i  title='Copy' id='copy_field' class='fas fa-copy'></i>");
		d.append(cp); 
		cp.click(function(){
			clipboard_buffer=input.val();			
			return false
		});
		d.append($('<span>&nbsp;</span>'));
		var pst=$("<i  title='Paste' id='paste_field' class='fas fa-paste'></i>");
		d.append(pst);
		pst.click(function(){
			input.val(clipboard_buffer);
			input.trigger('setvalue.coordinates');
			return false
		});
		d.click(function(){
			//$('.gp_activate', this.parentNode).addClass('gp_unactivate');
			$('.gp_activate', this.parentNode).removeClass('gp_activate');	
			//$(this).removeClass('gp_unactivate');
			$(this).addClass('gp_activate');	
		});

		all_pnts=$('.w_points', part);
		if(all_pnts.length>1){
			pnt_list.sortable({
				revert: true
			});
		}
		else if (all_pnts.length>2)
			pnt_list.sortable( "refresh" );		
	}
}

w_geometry.prototype.getGeometry=function(){
		var widget=this;
		var parts = new Array();
		var IsNULL=true;
		$( ".geom_part" , this.pnt_container).each(function( index ) {
			var pnts = new Array();
			var first_v=undefined;
			$( ".w_points" , this).each(function( index ) {
				input=$($('input', this)[0]);
				value=input.val();
				if(value=='MULTIPOINT()')
					return;
				wkt = new Wkt.Wkt();
				wkt.read(value);
				
				pnts[pnts.length]=wkt.components[0][0].x+' '+wkt.components[0][0].y;
				if(wkt.components[0][0].x!=0 || wkt.components[0][0].y!=0) 
					IsNULL=false;
				if(first_v===undefined)
					first_v=pnts[0];
			
			});
			if (widget.g_type=='polygon' && first_v!=undefined)
				pnts[pnts.length]=first_v;
			if (pnts.length>0)
				parts[parts.length]=pnts;
		});
		if(parts.length==0)
			return '';
		var str=widget.geometry_type;
		if(widget.geometry_type=='POINT')
			str='MULTIPOINT';
		if(widget.geometry_type=='LINESTRING')
			str='MULTILINESTRING';
		if(widget.geometry_type=='POLYGON')
			str='MULTIPOLYGON';
		hackmulti=true;
	
	
		if(this.g_type=='polygon' && hackmulti)
			str+='((';
		if(this.g_type=='line' && hackmulti)
			str+='(';			
		for(var i=0; i < parts.length; i++){			
			if (i!=0)
				str+=', ';
			str+='(';
			str+=parts[i].join(", ");
			str+=')';
		}
		if(this.g_type=='polygon' && hackmulti)
			str+='))';
		if(this.g_type=='line' && hackmulti)
			str+=')';
		if(IsNULL)
			return '';
		else
			return str;
}

w_geometry.prototype.setvalue = function(value, only_part){
	var part;
	if(only_part){
		if(this.g_type!='point' && this.multi){
		var activeTabIdx = this.tabs.tabs('option','active').index()-1;
			part=$($( ".geom_part" , this.pnt_container)[activeTabIdx]);
		}
		else
			part=this.pnt_part;
	}
	if(value!=undefined && value!=''){
		wkt = new Wkt.Wkt();
		try { // Catch any malformed WKT strings
			wkt.read(value);
		} catch (e1) {
			alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
			return;
		}
		
		config = map.defaults;

		if(wkt.type=='point'){
			if(wkt.components.length==1){
				var p_ind=0;
				$('.w_points', this.pnt_part).each(function( index ) {
					if($( this ).hasClass('gp_activate')){
						p_ind=index;					
					}
				});
				this.addpoint(this.pnt_part, p_ind, wkt.components[0].x, wkt.components[0].y);			
			}
			else{
				for(var i=0; i<wkt.components.length; i++){
					this.addpoint(this.pnt_part, i, wkt.components[i].x, wkt.components[i].y);
				}
			}
		}		
		if(wkt.type=='multipoint' ){
			for(var i=0; i<wkt.components[0].length; i++){
				this.addpoint(this.pnt_part, i, wkt.components[0][i].x, wkt.components[0][i].y);
			}
		}		
/*		else if(wkt.type=='point'){
			this.addpoint(this.pnt_part, 0, wkt.x, wkt.y);
		}*/
		else if(wkt.type=='linestring'){
			if(wkt.components.length==0)
				return;
			
			part=$($( ".geom_part" , this.pnt_container)[0]);
			if(part.length==0)
				part=this.addpart();
			$('.w_points',part).remove();					
			for(var j=0; j<wkt.components.length; j++){
				this.addpoint(part, j, wkt.components[j].x, wkt.components[j].y);
			}
								
		}		
		else if(wkt.type=='multilinestring'){
			if(wkt.components.length==0)
				return;
			for(var i=0; i<wkt.components.length; i++){
				part=$($( ".geom_part" , this.pnt_container)[i]);
				if(part.length==0)
					part=this.addpart();
				$('.w_points',part).remove();					
				for(var j=0; j<wkt.components[i].length; j++){
					this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
				}
			}					
		}
		else if(wkt.type=='polygon'){
			if(wkt.components.length==0)
				return;
			for(var i=0; i<wkt.components.length; i++){
				part=$($( ".geom_part" , this.pnt_container)[i]);
				if(part.length==0)
					part=this.addpart();
				$('.w_points',part).remove();					
				for(var j=0; j<wkt.components[i].length-1; j++){
					this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
				}
			}					
		}		
		else if(wkt.type=='multipolygon'){
			if(wkt.components.length==0)
				return;
			for(var i=0; i<wkt.components[0].length; i++){
				part=$($( ".geom_part" , this.pnt_container)[i]);
				if(part.length==0)
					part=this.addpart();
				$('.w_points',part).remove();					
				for(var j=0; j<wkt.components[0][i].length-1; j++){
					this.addpoint(part, j, wkt.components[0][i][j].x, wkt.components[0][i][j].y);
				}
			}					
		}
	}
}


w_geometry.prototype.getrect = function(value){
	var part;
	var max_x=-1000, min_x=1000, max_y=-1000, min_y=1000;
	
	
	if(value!=undefined && value!=''){
		wkt = new Wkt.Wkt();
		try { // Catch any malformed WKT strings
			wkt.read(value);
		} catch (e1) {
			alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
			return;
		}		
		

		if(wkt.type=='point'){			
			for(var i=0; i<wkt.components.length; i++){					
				min_x=Math.min(wkt.components[i].x, min_x);
				max_x=Math.max(wkt.components[i].x, max_x);
				min_y=Math.min(wkt.components[i].y, min_y);
				max_y=Math.max(wkt.components[i].y, max_y);				
			}			
		}		
		if(wkt.type=='multipoint' ){
			for(var i=0; i<wkt.components[0].length; i++){				
				min_x=Math.min(wkt.components[0][i].x, min_x);
				max_x=Math.max(wkt.components[0][i].x, max_x);
				min_y=Math.min(wkt.components[0][i].y, min_y);
				max_y=Math.max(wkt.components[0][i].y, max_y);				
			}
		}
		else if(wkt.type=='linestring'){			
			for(var j=0; j<wkt.components.length; j++){
				//this.addpoint(part, j, wkt.components[j].x, wkt.components[j].y);
				min_x=Math.min(wkt.components[j].x, min_x);
				max_x=Math.max(wkt.components[j].x, max_x);
				min_y=Math.min(wkt.components[j].y, min_y);
				max_y=Math.max(wkt.components[j].y, max_y);				
			}
		}		
		else if(wkt.type=='multilinestring'){
			for(var i=0; i<wkt.components.length; i++){								
				for(var j=0; j<wkt.components[i].length; j++){
//					this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
					min_x=Math.min(wkt.components[i][j].x, min_x);
					max_x=Math.max(wkt.components[i][j].x, max_x);
					min_y=Math.min(wkt.components[i][j].y, min_y);
					max_y=Math.max(wkt.components[i][j].y, max_y);					
				}				
			}					
		}
		else if(wkt.type=='polygon'){
			for(var i=0; i<wkt.components.length; i++){								
				for(var j=0; j<wkt.components[i].length-1; j++){
					//this.addpoint(part, j, wkt.components[i][j].x, wkt.components[i][j].y);
					min_x=Math.min(wkt.components[i][j].x, min_x);
					max_x=Math.max(wkt.components[i][j].x, max_x);
					min_y=Math.min(wkt.components[i][j].y, min_y);
					max_y=Math.max(wkt.components[i][j].y, max_y);					
				}
			}					
		}		
		else if(wkt.type=='multipolygon'){
			for(var i=0; i<wkt.components[0].length; i++){									
				for(var j=0; j<wkt.components[0][i].length-1; j++){
					//this.addpoint(part, j, wkt.components[0][i][j].x, wkt.components[0][i][j].y);
					min_x=Math.min(wkt.components[0][i][j].x, min_x);
					max_x=Math.max(wkt.components[0][i][j].x, max_x);
					min_y=Math.min(wkt.components[0][i][j].y, min_y);
					max_y=Math.max(wkt.components[0][i][j].y, max_y);					
					
				}
			}					
		}
	}
	return {max_x:max_x, min_x: min_x, max_y: max_y, min_y:min_y};
}

w_geometry.prototype.activateDrawing = function(){
//	if(this.Layer==null) return;
	var widget=this;	
	
	//g_unactivate
	if(!this.form_element || this.form_element.length==0)
		return;
	$('.g_activate', this.form_element[0].parentNode).addClass('g_unactivate');
	$('#tabs', this.form_element[0].parentNode).hide();
	$('.g_activate', this.form_element[0].parentNode).removeClass('g_activate');	
	this.form_element.removeClass('g_unactivate');
	this.form_element.addClass('g_activate');
	$('#tabs', this.form_element).show();

	if(typeof add_map_handle !=="undefined" ){
		add_map_handle(function (gobject, mode){
			var wkt = new Wkt.Wkt();
			wkt.fromObject(gobject);
			var value = wkt.write();
			widget.setvalue(value);			
			widget.updateLayer();
		}, this.g_type, widget);
		widget.updateLayer();
	}
		
}

w_geometry.prototype.g_assign = function(form_element, value){	
	this.form_element = form_element;
	this.pnt_container = form_element;
	var widget=this;
	this.geom_object=null;
	
	if(this.g_type=='point'){
		if(this.multi)
			this.geometry_type='POINT';
		else
			this.geometry_type='MULTIPOINT';
	}
	else if(this.g_type=='line'){
		if(this.multi)
			this.geometry_type='MULTILINESTRING';
		else
			this.geometry_type='LINESTRING';	
			this.geometry_type='MULTILINESTRING';
	}
	else if(this.g_type=='polygon'){
		if(this.multi)
			this.geometry_type='MULTIPOLYGON';
		else
			this.geometry_type='POLYGON';	
			//this.geometry_type='MULTIPOLYGON';
	}
	
	
		
	form_element.click(function() {
		widget.activateDrawing();		
	});
	form_element.on("remove", function () {
		stop_map_edit(widget);
	})
	/*
	if(this.lvis){
		var label = $('<div style="display: inline-block;text-align: right;vertical-align: top">'+this.object.title+'</div>');
		this.pnt_container.append(label);
	}*/
	/*
	maplink=$('<a href="#">Map</a>');
	this.pnt_container.append(maplink);
	maplink.click(function(){
		if(!$('#mapcontainer').is(":visible")){
			$('#mapcontainer').dialog({
					  resizable: false,
					  height:'800',
					  width: '800',
					  modal: true,
					  buttons: {
						"Close": function() {							
							$( this ).dialog( "close" );							
						}
					  }
				});
		}
		
		map.invalidateSize(true);
	});*/
	var tabs=$('<div id="tabs"/>');
	//tabs.hide();
	this.pnt_container.append(tabs);
	
	ul=$('<ul id="tab_list" style="margin-bottom: 0px;" class="nav nav-tabs" />');
	tabs.append(ul);
	if(this.g_type!='point' && this.multi){
		var link_container=$('<li class="w_add_part" id="li-o"><a href="#tabs-0"></a><button id="add_part" class="new">+</button> </li>');		
		var add_btn=$('#add_part',link_container);
		add_btn.click(function() {
			widget.addpart();				
		});
		ul.append(link_container);
	}
//	var part_container=$('<li id="tabs-0" style="margin-bottom: 0px;" class="nav nav-lis"/>');
//	tabs.append(part_container);
	if(this.g_type!='point' && this.multi)
		tabs.tabs();
	var part=this.addpart();
	this.tabs=tabs;	
	this.setvalue(value);
	if(this.g_type=='point' && !this.multi){
		gparts=$( ".geom_part" , this.pnt_container);
		if(gparts.length==1){
			if ($( ".w_points" , gparts[0]).length==0){
				widget.addpoint(gparts[0],0,0,0);
			}
		};
	}
	this.form_element.addClass('g_unactivate');
}

w_geometry.prototype.updateLayer = function(){
	if(window.drawnItems !== undefined ){
		drawnItems.clearLayers();	

		val=this.getGeometry();
		if(val=='') return;	

		wkt = new Wkt.Wkt();
		try { // Catch any malformed WKT strings
			wkt.read(val);
		} catch (e1) {
			alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
			return;
		}
		if(this.g_type!='point')
			var activeTabIdx = this.tabs.tabs('option','active').index()-1;
		
		//this.geom_object = wkt.toObject(map.defaults); // Make an object	
			if(wkt.type=='point'){			
				for(var i=0; i<wkt.components.length; i++){
					this.geom_object=new L.circle([wkt.components[i].y,wkt.components[i].x], 200);				
					drawnItems.addLayer(this.geom_object);
				}
			}	
			if(wkt.type=='multipoint'){			
				for(var i=0; i<wkt.components[0].length; i++){
					this.geom_object=new L.circle([wkt.components[0][i].y,wkt.components[0][i].x], 500, {
						color: 'red',
						fillColor: '#f03',
						radius: 500
					});
					/*
					this.geom_object=new L.circle([wkt.components[0][i].y,wkt.components[0][i].x], {
						color: 'red',
						fillColor: '#f03',
						fillOpacity: 0.5,
						radius: 500
					})*/;
					//this.geom_object.addTo(map);
					drawnItems.addLayer(this.geom_object);
				}
			}		
	/*		else if(wkt.type=='point'){
				this.addpoint(this.pnt_part, 0, wkt.x, wkt.y);
			}*/
			else if(wkt.type=='linestring'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[activeTabIdx].length;i++){
					el=[wkt.components[i].y, wkt.components[i].x];
					pnts.push(el);
				}	
				this.geom_object=new L.Polyline(pnts);
				drawnItems.addLayer(this.geom_object);
			}		
			else if(wkt.type=='multilinestring'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[activeTabIdx].length;i++){
					el=[wkt.components[activeTabIdx][i].y, wkt.components[activeTabIdx][i].x];
					pnts.push(el);
				}							
				this.geom_object=new L.Polyline(pnts);
				drawnItems.addLayer(this.geom_object);
			}
			else if(wkt.type=='polygon'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[activeTabIdx].length;i++){
					el=[wkt.components[activeTabIdx][i].y, wkt.components[0][activeTabIdx][i].x];
					pnts.push(el);
				}
				this.geom_object=new L.Polygon(pnts);					
				drawnItems.addLayer(this.geom_object);
			}		
			else if(wkt.type=='multipolygon'){
				if(wkt.components.length==0)
					return;
				pnts=[];
				for (var i=0; i<wkt.components[0][activeTabIdx].length;i++){
					el=[wkt.components[0][activeTabIdx][i].y, wkt.components[0][activeTabIdx][i].x];
					pnts.push(el);
				}			
				this.geom_object=new L.Polygon(pnts);			
				drawnItems.addLayer(this.geom_object);
			}		
	}
}

