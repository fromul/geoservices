(function($) {
	/**
	* A filter widget based on data in a table column.
	* 
	* @class DateRangeFilterWidget
	* @constructor
	* @param {object} $Container The jQuery object that should contain the widget.
	* @param {object} oSettings The target table's settings.
	* @param {number} i The numeric index of the target table column.
	* @param {object} widgets The FilterWidgets instance the widget is a member of.
	*/
	$.DateRangeFilterWidget = function( $Container, column, dt_table ){
		var widget = this;
		widget.column = column;		
		widget.$Container = $Container;
		widget.oDataTable = dt_table;		
		widget.asFilters = [];
		widget.sSeparator = ';';
		widget.iMaxSelections = -1;
		var m1={sName:'from_'+widget.column.fieldname, title:'From', fieldname:'from_'+widget.column.fieldname, widget:widget.column.widget};
		var m2={sName:'to_'+widget.column.fieldname, title:'To', fieldname:'to_'+widget.column.fieldname, widget:widget.column.widget};
		var w_d_from=new w_date(m1);
		var w_d_to=new w_date(m2);
		
		var d_from = 'from_' + widget.column.fieldname;
		var d_to   = 'to_' + widget.column.fieldname;

		widget.$DateRange = $( '<div class="input-group fltr_date"></div>');
		//widget.$DateRange = $( '<div class="row fltr_date"><div class="col-5 fltr_date_div" id="d_from_' + widget.column.fieldname + '"/><div class="col-1"><a class="btn btnretweet" id="btnretweet" href="#"><i class="far fa-clone"></i></a></div><div class="col-5 fltr_date_div" id="d_to_' + widget.column.fieldname + '"/></div>' );
		
		
		
		widget.$Container.append( widget.$DateRange );		
		w_d_from.assign(widget.$DateRange);
		widget.$DateRange.append($('<div class="input-group-append"><span class="input-group-text btnretweet"><i class="far fa-clone"></i></span></div>'));
		w_d_to.assign(widget.$DateRange);
		widget.$DateRange.append($('<div class="input-group-append"><span class="input-group-text addconstraint"><i class="fas fa-plus"></span></div>'));
	
		
		$('.addconstraint', widget.$DateRange).click(function(event, ui) {
			var val_d_from = w_d_from.getVal(),
				val_d_to = w_d_to.getVal();
			if (( val_d_from == '' ) && ( val_d_to == '' )) {
				// The blank range
				return;
			}				
			var str_d_from = w_d_from.getUserVal(val_d_from),
				str_d_to = w_d_to.getUserVal(val_d_to);			
			val_d_from = (val_d_from == '') ? '∞' : val_d_from ;
			val_d_to = (val_d_to == '') ? '∞' : val_d_to ;
			str_d_from = (str_d_from == '') ? '∞' : str_d_from ;
			str_d_to = (str_d_to == '') ? '∞' : str_d_to ;
			
			var sSelected = val_d_from  + ' - ' + val_d_to, sText, $TermLink, $SelectedOption; 
			var strSelected = str_d_from  + ' - ' + str_d_to;
			$('input',widget.$Container).val('');
			$('input',widget.$Container).val('');
			if ( '' === sSelected ) {
				// The blank option is a default, not a filter, and is re-selected after filtering
				return;
			}
			if  (!($.inArray(sSelected, widget.asFilters))) {
				return;
			}
			sText = $( '<div>' + strSelected + '</div>' ).text();
			$TermLink = $( '<a class="filter-term" href="#"></a>' )
				.addClass( 'filter-term-' + sText.toLowerCase().replace( /\W/g, '' ) )
				.text( sText )
				.click( function() {
					// Remove from current filters array
					widget.asFilters = $.grep( widget.asFilters, function( sFilter ) {
						return sFilter != sSelected;
					} );
					$TermLink.remove();
					if ( widget.$TermContainer && 0 === widget.$TermContainer.find( '.filter-term' ).length ) {
						widget.$TermContainer.hide();
					}
					widget.fnFilter();
					return false;
				} );
			widget.asFilters.push( sSelected );
			if ( widget.$TermContainer ) {
				widget.$TermContainer.show();
				widget.$TermContainer.prepend( $TermLink );
			} else {
				//$( this ).after( $TermLink ); //  Add $TermLink to From <input>
				widget.$DateRange.after( $TermLink ); //  Add $TermLink to From <input>
			}
		
			//widget.$DateRange.val( '' );
			widget.fnFilter();	
		});
		
		$('.btnretweet',widget.$DateRange).click(function(){
			var str_d_from = w_d_from.getVal(),
				str_d_to = w_d_to.getVal();
			if(str_d_from=='' && str_d_to!='')
				w_d_from.setval(str_d_to);
			else if(str_d_from!='' && str_d_to=='')
				w_d_to.setval(str_d_from);
		
		});
		
		
		//val_d_to.append( widget.$button );
		//widget.fnDraw();
	};


	/**
	* Perform filtering on the target column.
	* 
	* @method fnFilter
	*/
	$.DateRangeFilterWidget.prototype.fnFilter = function() {
		var widget = this;
		//var aDateRange = [];
		//var sFilterStart, sFilterEnd; // TODO Необходимо добавить обработку c одной датой
		if ( widget.asFilters.length > 0 ) {
			var asEscapedFilters = [];		
			$.each( widget.asFilters, function( i, sFilter ) {
				aDateRange = sFilter.split(' - ');
				if(aDateRange[0]!='∞')
					sFilterStart = aDateRange[0];//$.datepicker.formatDate( "yy-mm-dd HH:MM:SS", $.datepicker.parseDate( $.datepicker._defaults.dateFormat, aDateRange[0] ));
				else
					sFilterStart = '∞';
				if(aDateRange[1]!='∞')
					sFilterEnd = aDateRange[1];//$.datepicker.formatDate( "yy-mm-dd HH:MM:SS", $.datepicker.parseDate( $.datepicker._defaults.dateFormat, aDateRange[1]));
				else
					sFilterEnd ='∞'
				asEscapedFilters.push( sFilterStart + ' - ' + sFilterEnd );				
			} );
			widget.oDataTable.fnFilter( asEscapedFilters.join(';'), widget.column.fieldname );
		} else { 
			// Clear any filters for this column
			widget.oDataTable.fnFilter( '', widget.column.fieldname );
		}
		

	};		
	
}(jQuery));