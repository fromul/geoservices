	// @@@ 1st Level Class @@@
	// options - options of WPS server
	// callback - actions to be performed over the received data
	function WPSServer(options, callback) {
		// ### PUBLIC ### 
		//Parameters separator
		this.PSPRTR = "&";
		//Equality mark
		this.PEQ = "=";
		//Default options of
		var defaultOptions = {
			//WPS server
			wpshost: "http://geos.icc.ru/cgi-bin/wps/zoo_loader.cgi?",
			// WPS port
			wpsport: 80,
			//Service
			service: "WPS",
			//Enabling/Disabling asynchronous querying
			asyncenabled: "false",
			// Proxy adress
			proxy: '/proxy',
			// WPS Path
			wpspath: '',
			identifier:'',			
		}
		//Parsing and applying parameters
		for(var option in defaultOptions) this[option] = options && options[option]!==undefined ? options[option] : defaultOptions[option];
		
		//Getting method parameters
		this.GetMethodPars = function () {
			alert("GetMethodPars");
			return true;
		}
		
		// Applying proxy
		this.applyProxy = function (request) {
			var host = window.location.host;
			var output = 'http://' + host + '/wps/proxy?host=' + encodeURIComponent(this.wpshost) + '&port=' + this.wpsport + '&wpspath=' + encodeURIComponent(this.wpspath) + '&request=' + encodeURIComponent(request);
			return output;
		}
		
		//Appending parameters to URL
		this.appendPar = function(overallstr, parkey, parval) {
			var resStr = overallstr + parkey + this.PEQ + parval + this.PSPRTR;
			return resStr;
		}
		
		//Getting available methods
		this.GetCapabilities = function (action, par) {
			//Constructing request following the OGC WPS 1.0.0
			var REQUEST = "GetCapabilities";
			var methodURL = '';
			var response;
			methodURL = this.appendPar(methodURL, "service", this.service);
			methodURL = this.appendPar(methodURL, "request", REQUEST);
			methodURL = this.applyProxy(methodURL);
			methodexec = new this.getXmlHttp();
			methodexec.open("GET", methodURL, this.asyncenabled);
			methodexec.send(null);
			methodexec.onreadystatechange = function() {
				if (methodexec.readyState == 4) {
					response = methodexec;
					//Applying provided callback function
					if (Object.prototype.toString.call(callback) == '[object Function]') {
						callback(response, REQUEST, action, par);
					} else {
						// Default action on response
						alert(response);
					}
				}
			};
			return true;
		}
		this.GetCapabilitiesAsItem = function (callback) {
			//Constructing request following the OGC WPS 1.0.0
			var REQUEST = "GetCapabilities";
			var methodURL = '';
			var response;
			methodURL = this.appendPar(methodURL, "service", this.service);
			methodURL = this.appendPar(methodURL, "request", REQUEST);
			methodURL = this.applyProxy(methodURL);
			methodexec = new this.getXmlHttp();
			methodexec.open("GET", methodURL, this.asyncenabled);
			methodexec.send(null);
			methodexec.onreadystatechange = function() {
				if (methodexec.readyState == 4 && methodexec.responseText!='') {
					//Applying provided callback function
					var xmlDoc = $.parseXML(methodexec.responseText);
					$xml = $( xmlDoc ),
					$xml.find('wps\\:Process').each(function() {
						callback($(this).find('ows\\:Identifier').text());
					});
				}
			};
			return true;
		}
		this.DescribeProcessAsJSON = function (callback) {
			//Constructing request following the OGC WPS 1.0.0
			var REQUEST = "DescribeProcess";
			var methodURL;
			methodURL = '';
			methodURL = this.appendPar(methodURL, "service", this.service);
			methodURL = this.appendPar(methodURL, "version", this.version);
			methodURL = this.appendPar(methodURL, "identifier", this.identifier);
			methodURL = this.appendPar(methodURL, "request", REQUEST);
			methodURL = this.applyProxy(methodURL);		
			methodexec = new this.getXmlHttp();
			methodexec.open("GET", methodURL, this.asyncenabled);
			methodexec.send(null);
			methodexec.onreadystatechange = function() {
				if (methodexec.readyState == 4 && methodexec.responseText!='') {
					//Applying provided callback function
					var params=new Array();
	
					var xmlDoc = $.parseXML(methodexec.responseText);
					$xml = $( xmlDoc ),
					$xml.find('Input').each(function() {
						var param={};
						param.fieldname=$(this).find('ows\\:Identifier').text();
						param.type=$(this).find('ows\\:DataType').text();
						param.title=$(this).find('ows\\:Title').text();
						param.description=$(this).find('ows\\:Abstract').text();							
						param.widget={name:"edit", properties:{}};
						params[params.length]=param;														
					});
					callback(params);
				}
			};
			return true;		
		}
		
		//Getting service info
		this.GetInfo = function (wpid) {
			var ovstr;
			ovstr = this.wpshost;
		}
		
		//Cross-browser object creation
		this.getXmlHttp = function(){
			var xmlhttp;
			try { xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");}
			catch (e) {
				try { xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");}
				catch (E) { xmlhttp = false; }
			}
			if (!xmlhttp && typeof XMLHttpRequest!='undefined') { xmlhttp = new XMLHttpRequest();}
			return xmlhttp;
		}	
		
	}	

	//Creating temporary example of WPSServer
	var TEMPSERV = new WPSServer();

	// @@@ 2nd Level Class @@@
	// options - parameters of method
	// callback - function to be performed over server output
	function WPSMethod(options, callback) {
		// ### PUBLIC ###
		var defaultOptions = {
			//WPS server
			wpshost: "http://geos.icc.ru/cgi-bin/wps/zoo_loader.cgi?",
			// WPS port
			wpsport: 80,
			// Proxy adress
			proxy: '/proxy',
			// WPS Path
			wpspath: '',
			//Service
			service: "WPS",
			//Enabling/Disabling asynchronous querying
			asyncenabled: "false",
			//Version
			version: "1.0.0",
			//WPS method identifier
			identifier: "Not defined",
			// Internal (Zoo Loader) id
			internalid: 0,
			//WPS method parameters (see DataInput section in WPS 1.0.0 Documentation)
			params: {
				data: "Data not provided",
			}
		}
		//Parsing and applying parameters
		for(var option in defaultOptions) this[option] = options && options[option]!==undefined ? options[option] : defaultOptions[option];
		
		this.applyProxy = function (request) {
			var host = window.location.host;
			var output = 'http://' + host + '/wps/proxy?host=' + encodeURIComponent(this.wpshost) + '&port=' + this.wpsport + '&wpspath=' + encodeURIComponent(this.wpspath) + '&request=' + encodeURIComponent(request) + '&internalid=' + this.internalid;
			return output;
		}
			
		//GetCapabilities method
		this.GetCapabilities = function (action, par) {
			var REQUEST = "GetCapabilities";
			var methodURL;
			methodURL = '';
			methodURL = TEMPSERV.appendPar(methodURL, "service", this.service);
			methodURL = TEMPSERV.appendPar(methodURL, "version", this.version);
			methodURL = TEMPSERV.appendPar(methodURL, "identifier", this.identifier);
			methodURL = TEMPSERV.appendPar(methodURL, "request", REQUEST);
			methodURL = this.applyProxy(methodURL);
			methodexec = new TEMPSERV.getXmlHttp();
			methodexec.open("GET", methodURL, this.asyncenabled);
			methodexec.send(null);
			methodexec.onreadystatechange = function() {
				if (methodexec.readyState == 4) {
					response = methodexec;
					//Applying provided callback function
					if (Object.prototype.toString.call(callback) == '[object Function]') {
						callback(response, REQUEST, action, par);
					} else {
						// Default action on response
						alert(response);
					}
				}
			};
			return true;
		}
		
		//DescribeProcess method
		this.DescribeProcess = function (action, par) {
			var REQUEST = "DescribeProcess";
			var methodURL;
			methodURL = '';
			methodURL = TEMPSERV.appendPar(methodURL, "service", this.service);
			methodURL = TEMPSERV.appendPar(methodURL, "version", this.version);
			methodURL = TEMPSERV.appendPar(methodURL, "identifier", this.identifier);
			methodURL = TEMPSERV.appendPar(methodURL, "request", REQUEST);
			methodURL = this.applyProxy(methodURL);
			methodexec = new TEMPSERV.getXmlHttp();
			methodexec.open("GET", methodURL, this.asyncenabled);
			methodexec.send(null);
			methodexec.onreadystatechange = function() {
				if (methodexec.readyState == 4) {
					response = methodexec;
					//Applying provided callback function
					if (Object.prototype.toString.call(callback) == '[object Function]') {
						callback(response, REQUEST, action, par);
					} else {
						// Default action on response
						alert(response);
					}
				}
			};
			return true;
		}

		//Applying new parameters if they were not provided before
		this.RenewParams = function (objarr) {
			for(var name in objarr) {
				this[name] = objarr[name];
			}
			return true;
		}
		
		
		//Appending DataInputs section (see WPS 1.0.0 Documentation)
		function appendDataInput(overallstr, objarr) {
			var appstr = "";
			for(var name in objarr) {
				appstr = appstr + name + "=" + objarr[name] + ";";
			}
			appstr = overallstr + "DataInputs=" + appstr;
			return appstr;
		}
	}

	// # Inheritance #
	WPSMethod.prototype = new WPSServer();

	var WXML2HTML = function (options) {
		//Default options of WXML2HTML
		var defaultOptions = {
			//Provided data
			data: "",
		}
		//Parsing and applying parameters
		for(var option in defaultOptions) this[option] = options && options[option]!==undefined ? options[option] : defaultOptions[option];
		
		this.constructBrowseSelect = function (divid) {
			var xmlDoc = $.parseXML(this.data.responseText);
			htmlstr = "<select id='exec_single_methods'>";
			$(xmlDoc).find('ows\\:Identifier').each( function () {
				htmlstr = htmlstr + "<option value=" + $(this).text() + ">" + $(this).text() + "</option>";
			});
			htmlstr = htmlstr + "</select>";
			$("#"+divid).html(htmlstr);
			$("#add_method_content_select").html(htmlstr);
		}
		
		this.makeParametersForm = function (divid) {
			$('#submit_container').show();
			var xmlDoc = $.parseXML(this.data.responseText);
			var status_supported = xmlDoc.getElementsByTagName("ProcessDescription");
			if(status_supported[0].getAttribute('statusSupported') === 'true') {
				$('#wps_params_status').show();
				$('#longprocess_allowed').val('true');
			} else {
				$('#wps_params_status').hide();
				$('#longprocess_allowed').val('false');
			}
			
			var method = $(xmlDoc).find('DataInputs').text();
			
			$(xmlDoc).find('Input').each(function() {
				var dummy = {
					fieldname: $(this).find('ows\\:Identifier').text(),
					title: $(this).find('ows\\:Identifier').text(),
					description: $(this).find('ows\\:Abstract').text(),
					visible: true,
					type: 'string',
					widget: {
						name: 'edit',
						properties: {
							size: 20
						}
					}
				};

				if (parseInt($(this).attr('minOccurs')) >= 1) {
					dummy.minoccurs = parseInt($(this).attr('minOccurs'));
				} else {
					dummy.minoccurs = 0;
				}
				
				$('#input_params_container').append(createInputField('wps', dummy, false));
			});
			
			$(xmlDoc).find('Output').each(function() {
				var dummy = {
					fieldname: $(this).find('ows\\:Identifier').text(),
					title: $(this).find('ows\\:Identifier').text(),
					description: $(this).find('ows\\:Abstract').text(),
					visible: true,
					type: 'string',
					widget: {
						name: 'edit',
						properties: {
							size: 20
						}
					}
				};

				$('#output_params_container').append(createInputField('wps', dummy, false));
			});
		}

		this.makeOptions = function (divid) {
			htmlstr = "";
			var method = $(xmlDoc).find('wps\\:ProcessOfferings').text();
			var xmlDoc = $.parseXML(this.data.responseText);
			$(xmlDoc.getElementsByTagName("wps:Process")).each(function(){ 
				console.log(this);
			})
			
			$(xmlDoc).find('wps\\:Process').each(function() {
				htmlstr = htmlstr + '<option value="' + $(this).find('ows\\:Identifier').text() + '">' + $(this).find('ows\\:Identifier').text() + '</option>';
			});
			htmlstr = '<option value="default">-choose-</option>' + htmlstr;
			$("#method_select_wrapper").html('<div id="edit-method-wps-wpsmet-wrapper"><p>Choose one of the available methods <a href="#" data-toggle="tooltip" title="Listed methods are available at the remote service. If the desired method is not present here, please contact the remote setvice administrator">(?)</a><br><select id="metsel">' + htmlstr + '</select></p></div>');

			$('#metsel').change(function() {
				var method = $(this).val();
				$('#parameters_section').show();
				$('#edit-method-wpsmet').val(method);
				var server = $('#edit-method-wps-wpsurl').val();
				if (method != 'default') {
					$('#method_wpsmet').val(method);
					buildFormForMethod(method);
				}
			});
		}
	}


		
	//Callbacker hub for WPSServer object
	var ServCallback = function(resp, request, action, par) {
		if (request == "GetCapabilities") {
			if (action == "makeOptions") {
				var receivedxml = new WXML2HTML({data: resp});
				receivedxml.makeOptions(par);
			}
		}
	}

	// User callbacker hub
	var MethCallback = function(resp, request, action, par) {
		receivedxmla = new WXML2HTML({data: resp});
		//Checking for 500 Server Error
		var testfh = resp.responseText;
		var check = testfh.search("500 Internal Server Error");
		if (check != -1) {
			alert("500 Internal Server Error was encountered! Please, contact the administrator");
		} else {
			if (request == "GetCapabilities") {

			}
			if (request == "DescribeProcess") {
				if (action == "makeParametersForm") {
					receivedxmla.makeParametersForm(par);
				}
			}
		}
	}
	
/**
*	Response Parser
*/
function RP(response) {
	this.response = response;
	this.checkForErrors = function () {
		if ($(this.response).find('ExceptionReport').length > 0) {
			alert($(this.response).find('ExceptionReport').find('ExceptionText').text());
			return false;
		} else {
			return true;
		};
	}
	
	this.getError = function () {
		if ($(this.response).find('ExceptionReport').length > 0) {
			return $(this.response).find('ExceptionReport').find('ExceptionText').text();
		} else {
			return true;
		};
	}
	
	this.returnResponseArray = function() {
		var obj = new Object();
		obj.data = new Object();
		if (this.checkForErrors()) {
			obj.status = 'success';
		} else {
			obj.status = 'error';
			obj.error = this.getError();
		}
		$(this.response).find('Output').each(function() {
			obj.data[$(this).find('Identifier').text()] = $(this).find('LiteralData').text();
		});
		return obj;
	};
	
	this.getValue = function(identifier) {
		alert('NO LONGER SUPPORTED - WPS method returs array, not pure XML');
		return null;
	}

	return this;
}
	