/**
* Case of editing existing method
*/
var mid = getQueryVariable('id');
var registeredServices = [];
var METHOD, METHODID, JSFNAME;
var widgets = new Array();
var editor;

/**
*	sendData - send method info to the server
*
*	@param obj - object to send
*/
function sendData(obj, exit) {
	
	var method;
	if(obj.id && obj.id!='NULL'){
		doc_id=obj.id;
		method='update';
		obj.f_id=obj.id;
		obj.id=undefined;
	}
	else
		method='add';
	str_res=JSON.stringify({document:obj});
	$.ajax({
		type: "POST",
		url: "/dataset/"+method+"?f=185",
		contentType : 'application/json',
		data: str_res,
		success: function(msg){			
			response=JSON.parse(msg);
			if (response.status == 'ok') {
				if(method=='add')
					obj.id=response.data;
				
				if (exit) {
					alert('Method is saved. You will be redirected to the method page');
					document.location.href = '/?action=showwpsservice&id=' + obj.id;
				} else {
					alert('Method is saved.');
				}
			} else {
				alert('Error ocured: ' + response.data);
			}
		},
		error: function(){
			alert('Error ocured!');
		}
	});
	// $.post('/wpsmanager/create', {data: JSON.stringify(obj)}).done(function(data) {
	// 	var obj = JSON.parse(data);
	// 	if (obj.status == 'success') {
	// 		if (exit) {
	// 			alert('Method is saved. You will be redirected to the method page');
	// 			document.location.href = '/?action=showwpsservice&id=' + obj.id;
	// 		} else {
	// 			alert('Method is saved.');
	// 		}
	// 	} else {
	// 		alert('Error ocured: ' + obj.error);
	// 	}
	// });
}


/**
 * Generates interface for input parameter editing
 */

function createInputField(methodtype, field_json, editable) {
	var parametercontrols = '';
	if (methodtype === 'js') {
		 parametercontrols = "<div class='btn-group'><button type='button' class='btn' title='Move field down' id='movedown_field'><i class='icon-chevron-down'></i></button><button type='button' class='btn' title='Move field up' id='moveup_field'><i class='icon-chevron-up'></i></button><button type='button' class='btn' title='Delete field' id='delete_field'><i class='icon-remove-sign'></i></button></div>";
	} else if (methodtype === 'wps') {
		if (field_json.minoccurs >= 1) {
			parametercontrols = '<span id="mandatory_element" minoccurs="' + field_json.minoccurs + '" class="label label-important">Mandatory element</span>';
		}
	}	//end if

	var fieldcontainer = $("<div id='fieldcontainer'><table border=0 style='width: 650px;'><tr><td style='width: 300px;'><div class='form-field'><label>Identificator</label><input type='text' name='field_name' id='field_name' placeholder=''/></div><div class='form-field'><label>Parameter name</label><input type='text' name='title' id='title' placeholder='Enter parameter name'/></div><div class='form-field'><label>Description</label><TEXTAREA name='description' id='description' placeholder='Enter the parameter description' style='width: 100%; height: 50px;'/></div></td><td style='width: 30px;'>&nbsp;</td><td><div class='form-field' id='div_select'><label>Widget</label><select id='field_widget_select' name='field_widget_select'/></div><div id='widget_options'/></td>	<td valign='top' align='rigth'>" + parametercontrols + "</td></tr></table><hr></div>");

	var wselect = $("#field_widget_select", fieldcontainer);
	for (var k = 0; k < $.widgets.widgetlist.length; k++) {
		wselect.append( $('<option value="'+$.widgets.widgetlist[k][0]+'">'+$.widgets.widgetlist[k][0]+'</option>'));
	} //end for

	wselect.change(function (){
		widget_options = $('#widget_options', fieldcontainer);
		widget_options.empty();
		if (field_json == null)
		{
			var attr_json={ widget:{name:$(this).val()} };
		}
		else
		{
			var attr_json=field_json;
		} //end if

		var widget = $.widgets.get_widget(attr_json);
		widget.viewPropForm(widget_options);
	});

	var field_name = $("#field_name", fieldcontainer);
	field_name.attr('disabled', 'disabled');
	var field_title = $("#title", fieldcontainer);
	field_title.change(function() {
		if(field_name.val()=='')
			field_name.val(getcorrectobjname(field_title.val()));
	});

	//translit
	if(field_json != null){
		field_name.val(field_json.fieldname);
		var field_description=$("#description", fieldcontainer);
		field_description.val(field_json.description);
		field_title.val(field_json.title);
		$("#field_widget_select [value='"+field_json.widget.name+"']", fieldcontainer).attr("selected", "selected");
	}

	wselect.trigger('change');

	$("#moveup_field", fieldcontainer).click(function(){
		var pdiv = $(this).parents('#fieldcontainer');
		pdiv.insertBefore(pdiv.prev());
		return false
	});
	$("#movedown_field", fieldcontainer).click(function(){
		var pdiv = $(this).parents('#fieldcontainer');
		pdiv.insertAfter(pdiv.next());
		return false
	});
	$("#delete_field", fieldcontainer).click(function(){
		var pdiv = $(this).parents('#fieldcontainer');
		pdiv.remove();
		return false
	});
	if (field_json !== null) {
		if ((field_json.fieldname == 'fid') || (field_json.fieldname == 'id')) {
			$(fieldcontainer).hide();
		}
	}
	field_json = null;
	return fieldcontainer;
}


function createOutputField(methodtype, field_json, editable) {
	var fieldcontainer = $("<div id='fieldcontainer'><table border=0 style='width: 650px;'><tr><td style='width: 300px;'><div class='form-field' for='field_name'><label>Identificator</label><input type='text' name='field_name' id='field_name' placeholder=''/></div><div class='form-field'><label>Parameter name</label><input type='text' name='title' id='title'/></div><div class='form-field'><label style='display: block; float: left;'>Check if output is <strong>file</strong>&nbsp;&nbsp;&nbsp;</label><input type='checkbox' name='field_isfile'/></div></td><td><div class='form-field'><label>Description</label><TEXTAREA name='description' id='description' style='width: 100%; height: 50px;'/></div></td></tr></table><hr></div>");

	var field_name = $("#field_name", fieldcontainer);
	var field_title = $("#title", fieldcontainer);
	var field_description = $("#description", fieldcontainer);

	field_title.change(function() {
		if (field_name.val()=='') {
			field_name.val(getcorrectobjname(field_title.val()));
		}
	});

	if (editable === true) {
		field_name.attr('disabled', 'disabled');
	} else {
		field_title.attr('disabled', 'disabled');
		field_description.attr('disabled', 'disabled');
		$('[for="field_name"]', fieldcontainer).hide();
	}

	if (field_json !== null) {
		field_name.val(field_json.fieldname);
		var field_description=$("#description", fieldcontainer);
		field_description.val(field_json.description);
		field_title.val(field_json.title);

		if ((field_json.fieldname == 'fid') || (field_json.fieldname == 'id')) {
			$(fieldcontainer).hide();
		}
	}

	return fieldcontainer;
}


function get_params_json(table_container){
	var columns = new Array();
	try {
		table_container.find('[id="fieldcontainer"]').each(function( index ) {
			var fieldcontainer = $(this);
			if ($("#field_name", fieldcontainer).val()=='') {
				throw "the field name is empty";
			}

			if ($("#description", fieldcontainer).val()=='') {
				$("#description", fieldcontainer).val($("#field_name", fieldcontainer).val());
			}

			var field = {
				fieldname   : $("#field_name",fieldcontainer).val(),
				title       : $("#title", fieldcontainer).val(),
				description : $("#description",fieldcontainer).val(),
				visible     : true
			};

			/*
				For WPS - check if field is mandatory
			*/
			if ($("#mandatory_element", fieldcontainer).length > 0) {
				field.minoccurs = parseInt($("#mandatory_element", fieldcontainer).attr('minoccurs'));
			} //end if

			var widget_name = $("#field_widget_select option:selected", fieldcontainer).val();
			var attr_json = {
				widget : {
					name : widget_name
				}
			};

			var widget           = $.widgets.get_widget(attr_json);
			var widget_propoties = widget.getPropForm($("#widget_options", fieldcontainer));
			field.type           = widget.fieldtype;

			field.widget = {
				name       : widget_name,
				properties : widget_propoties
			};

			columns[columns.length] = field;
		});
	} catch(err) {
		alert(err);
		throw "err";
	}

	return columns;
}

function get_method_params_json(table_container){
	var params = get_params_json(table_container);
	return params;
}


/**
 * Initiates the form where user can set multilple WPS servers for a single service.
 *
 * @todo Check if the record is an actual WPS server
 *
 * @return void
 */
function initiateWPSServersForm() {
	function addField(serverinfo) {
		if (serverinfo === undefined) {
			var serverinfo = {
				host: "",
				port: "",
				path: "",
				cpunumber: "",
				cpufrequency: "",
				ram: "",
				bandwidth: "",
			};
		}

		$("#wpsserversettings").append('<div class="control-group well wpsserverdescription">\
		<input type="text" id="wpshost" placeholder="hostname or IP adress" value="' + serverinfo.host + '" required/><span style="color: red;">*</span>:<input type="text" class="input-mini" id="wpsport" placeholder="Port" value="' + serverinfo.port + '" size="5" required/><span style="color: red;">*</span>\
		<input type="text" id="wpspath" placeholder="Path to WPS platform" value="' + serverinfo.path + '" required/><span style="color: red;">*</span>\
		<br/>\
		<input type="text" class="input-medium" id="wpscpunumber" placeholder="Number of cores" value="' + serverinfo.cpunumber + '">\
		<input type="text" class="input-medium" id="wpscpufrequency" placeholder="Cores frequency (GHz)" value="' + serverinfo.cpufrequency + '">\
		<input type="text" class="input-medium" id="wpsram" placeholder="RAM size (GB)" value="' + serverinfo.ram + '">\
		<input type="text" class="input-medium" id="wpsbandwidth" placeholder="Net bandwidth (Mbit/s)" value="' + serverinfo.bandwidth + '">\
		<div style="float: right"><button type="button" class="btn btn-small deletethewpsserverbutton" title="Delete the server"><i class="fas fa-trash-alt"></i></button></div>\
		</div>');

		$(".deletethewpsserverbutton").click(function() {
			$(this).closest(".wpsserverdescription").remove();
		});
	}

	$("#addthewpsserverbutton").click(function() {
		addField();
	});

	if (METHOD === undefined) {
		addField();
	} else {
		var servers = JSON.parse(METHOD.wpsservers);
		for (serverkey in servers) {
			addField(servers[serverkey]);
		}
	}
} //end initiateWPSServersForm()


/**
 * Returns the formatted dataset from the WPS servers form.
 *
 * @return Array of Objects
 */
function getDataFromWPSServersForm() {
	var notcompletedfields = 0;

	function getProperty(caller, propertyname, storage, ismandatory) {
		var value = $(caller).find("#wps" + propertyname).val();
		if (value !== undefined && value.length > 0) {
			storage[propertyname] = value;
		} else {
			storage[propertyname] = "";
			if (ismandatory === true) {
				notcompletedfields++;
			}
		}

		return storage;
	} //end getProperty()

	var servers = new Array();
	$(".wpsserverdescription").each(function() {
		var description = new Object();

		description = getProperty(this, "host", description, true);
		description = getProperty(this, "port", description, true);
		description = getProperty(this, "path", description, true);

		description["method"] = $("#metsel").val();

		if (notcompletedfields > 0) {
			alert("Please, fill all required fields or delete the incomplete server record. Fields to fill: " + notcompletedfields);
		} else {
			servers.push(description);
		}
	});
	
	var servers = JSON.parse(JSON.stringify(servers));
	return servers;
} //end getDataFromWPSServersForm()

/**
 * Generates raw function code for WPS service.
 * Input parameters for generated function: "input" (object with properties, property names correspond to
 * names of the WPS service parameters) and "mapping" (maps externally defined ValuesStores to output data
 * of the WPS service).
 * Asynchronous callWPS() takes following parameters: object with properties of WPS service, object with mapping of WPS
 * service parameters to selected widgets (described later), actual input data as object and output parameters
 * mappings to ValuesStores.
 * Object that maps widget names to parameters has following structure: {input: {paramIN1: "file", ..}, output: {paramOUT1: "file_save"}}
 *
 * @param params Array
 * @param name string
 * @param status boolean
 * @param array specificparams
 *
 * @return string Raw function code
 */

function _generateFunctionCodeForWPS(params, name, method, status, specificparams) {
	var widgetmapping = "{";
	var numberofparameters = params.length;
	for (var i = 0; i < numberofparameters; i++) {
		var currentparameter = params[i];
		widgetmapping  += currentparameter.fieldname + ': "' + params[i].widget.name + '"';
		if (i < (numberofparameters - 1)) {
			widgetmapping += ",";
		}
	}

	widgetmapping += "}";
	if (status === true || status === "true") {
		status = "true";
	} else {
		status = "false";
	}

	var serverssettings = "[";
	var numberofservers = specificparams.length
	for (var i = 0; i < numberofservers; i++) {
		serverssettings += '{method: "' + method + '", status: "' + status + '",';

		var j = 0;
		var currentserver      = specificparams[i];
		var numberofproperties = Object.keys(currentserver).length;
		for (var property in currentserver) {
			if (currentserver.hasOwnProperty(property) === true) {
				serverssettings += property + ': "' + currentserver[property] + '"';
				if (j < (numberofproperties - 1)) {
					serverssettings += ",";
				}

				j++;
			}
		}

		serverssettings += "}";
		if (i < (numberofservers - 1)) {
			serverssettings += ",";
		}
	}

	serverssettings += "]";

	var rawcode = "function " + name + "(input, mapping, specification){ callWPS(" + serverssettings + ", " + widgetmapping + ", input, mapping, specification);}";
	return rawcode;
}

/**
 * Check if the provided JSON string is a valid JSON
 *
 * @return bool
 */
function isValidJSON(jsonString) {
	var isValid = true;
	try {
		JSON.parse(jsonString);
	} catch(e) {
		isValid = false;
	}

	return isValid;
} //end isValidJSON()


/**
* setEnv - setting visible envirionment
*
* @param mode - are we editing WPS or JS method
*/
function setEnv(mode) {
	$('#form_wrapper').html('');

	if (!METHOD) $('#specifications_wrapper').find('textarea').val('');
	$('#specifications_wrapper').find('textarea').unbind();
	$('#specifications_wrapper').find('textarea').keyup(function () {
		var specification = $(this).val();
		if (specification) {
			if (isValidJSON(specification)) {
				$('#specifications_wrapper').find('.js-error').hide();
			} else {
				$('#specifications_wrapper').find('.js-error').show();
			}
		}
	});

	switch (mode) {
		case 'wps':
			initiateWPSServersForm();

			$('#host_wrapper').show();
			$('#specifications_wrapper').show();
			$('#method_wrapper').show();
			$('#parnum_wrapper').hide();
			$('#jsfunc_wrapper').hide();
			$('#parameters_section').hide();
			$('#input_params_container').html('');
			$('#output_params_container').html('');
			$('#prettyprint').hide();
			break;
		case 'js':
			$('#add_input_js_param').unbind();
			$('#delete_input_js_param').unbind();
			$('#host_wrapper').hide();
			$('#specifications_wrapper').hide();
			$('#method_wrapper').hide();
			$('#jsfunc_wrapper').show();
			$('#parameters_section').show();
			$('#input_params_container').html('');
			$('#output_params_container').html('');
			$('#prettyprint').show();
			$('#form_wrapper').html('<div id="js_params_cont"></div><div style="text-align: right;"><input type="hidden" name="param_cnt" value="0"></div>');

			$('#input_params_buttons').append('<div class="btn-group"><button class="btn" type="button" id="add_input_js_param">Add parameter</button><button class="btn" type="button" id="delete_input_js_param">Delete parameter</button></div>');
			$('#output_params_buttons').append('<div class="btn-group"><button class="btn" type="button" id="add_output_js_param">Add parameter</button><button class="btn" type="button" id="delete_output_js_param">Delete parameter</button></div>');

			$('#add_input_js_param').click(function () {
				$('#submit_container').show();
				$('#input_params_container').append(createInputField('js', null));
			});
			$('#add_output_js_param').click(function () {
				$('#submit_container').show();
				$('#output_params_container').append(createInputField('js', null));
			});

			$('#delete_input_js_param').click(function () {
				$('#fieldcontainer:last', $('#input_params_container')).remove();
			});
			$('#delete_output_js_param').click(function () {
				$('#fieldcontainer:last', $('#output_params_container')).remove();
			});
			break;
		default:
			return;
			break;
	}
}

/**
*	saveMethod - saving method through AJAX
*
*	@param mode - are we editing WPS or JS method
*/
function saveMethod(mode, exit) {
	function proceedWithSaving() {
		if ($('#name').val() == '') {
			alert('Enter the method name');
			return false;
		}

		var specification = "";
		var specificationRaw = $('#specifications_wrapper').find('textarea').val();
		if (specificationRaw && isValidJSON(specificationRaw)) {
			specification = specificationRaw;
		}

		// if (obj.type === "js") {
		// 	extraparams.funcbody = obj.funcbody;
		// } else if (obj.type === "wps") {
		// 	extraparams.wpsservers = obj.servers;
		// } else {
		// 	throw new Error("Wrong type value: " + obj.type);
		// }

		switch (mode) {
			case 'js':
				var params        = get_method_params_json($('#input_params_container'));
				var output_params = get_method_params_json($('#output_params_container'));

				var methobj = {
					name: translit($('#name').val()),
					description: str_replace('\'', '', str_replace('"', '', str_replace('\n', 'XXNEWLINEXX', $('#description').val()))),
					type: mode,
					js_body: functionApplyTags(editor.getValue()),
					params: params,
					output_params: output_params,
					map_reduce_specification: specification
				};

				if (isset(METHODID)) {
					methobj.id = METHODID;
					if (JSFNAME !== functionGetName(editor.getValue())) {
						if (confirm('Are you sure that you need to change the function\'s name? Calling it with the old name will cause an error')) {
							sendData(methobj, exit);
						}
					} else {
						sendData(methobj, exit);
					}
				} else {
					sendData(methobj, exit);
				} //end if
				break;
			case 'wps':
				var checked = 'undefined';
				if ($('#longprocess_allowed').val() == 'true') {
					if ($('[name="longprocess"]').attr('checked') === 'checked') {
						checked = 'true';
					} else {
						checked = 'false';
					}
				} else {
					checked = 'undefined';
				}

				var params        = get_method_params_json($('#input_params_container'));
				var output_params = get_method_params_json($('#output_params_container'));

				var serversinfo = getDataFromWPSServersForm();

				var serviceId = $("#metsel").val();
				if (METHOD) {
					var servers = JSON.parse(METHOD.wpsservers);
					for (var i = 0; i < servers.length; i++) {
						if (servers[i].method) {
							serviceId = servers[i].method;
							break;
						}
					}
				}

				for (var i = 0; i < serversinfo.length; i++) {
					serversinfo[i].method = serviceId;
				}
				var name = translit($('#name').val());
				var method = $('#metsel').val();
				var rawjscode = _generateFunctionCodeForWPS(params, name, method, checked, serversinfo);
						

				var methobj = {
					name          : name,
					description   : $('#description').val(),
					type          : mode,
					status        : checked,
					wpsservers       : serversinfo,
					method        : method,
					params        : params,
					js_body		  :rawjscode,
					output_params : output_params,
					map_reduce_specification: specification
				}

				if (isset(METHODID)) {
					methobj.id = METHODID;
				} //end if

				sendData(methobj, exit);
				break;
			default:
				console.log('ERROR: Unable to detect method type');
				break;
		}
	}

	if (METHOD !== undefined) {
		if (METHOD.name !== translit($('#name').val())) {
			var process = confirm("You have changed the name of the function - methods that are already using your function WILL NOT BE ABLE TO USE IT in this case. Proceed?");
			if (process === true) {
				proceedWithSaving();
			} else {
				var replace = confirm("Restore previous method name?");
				if (replace === true) {
					$('#name').val(METHOD.name);
				}

				return false;
			} //end if
		} else {
			proceedWithSaving();
		} //end if
	} else {
		proceedWithSaving();
	}	//end if
}


/**
*	buildFormForMethod - bulding methods (proxy for WPSM instance)
*
*	@param method - ID of method
*/
function buildFormForMethod(method) {
	var servers  = getDataFromWPSServersForm();
	exWPSM       = new WPSMethod({identifier: method, wpshost: servers[0].host, wpsport: servers[0].port, wpspath: servers[0].path }, MethCallback);
	var maincont = exWPSM.DescribeProcess("makeParametersForm", "paramholder");
}


$(document).ready(function() {
	jsfunc=$('#jsfunc');
	if(jsfunc.length>0){
		editor = ace.edit("jsfunc");
		editor.setTheme("ace/theme/eclipse");
		editor.session.setMode("ace/mode/javascript");
	}
	$.post("/dataset/list?f=185&count_rows=true&unique=7149599661&iDisplayStart=0&iDisplayLength=100", function (data) {
		registeredServices = JSON.parse(data);

		$(".js-service-list-container").empty();
		for (var i = 0; i < registeredServices.aaData.length; i++) {
			var shortened = registeredServices.aaData[i].js_body.substr(0, registeredServices.aaData[i].js_body.indexOf("{"));

			var paramsRaw = "Input: ";
			var inputParams = JSON.parse(registeredServices.aaData[i].params);
			for (var j = 0; j < inputParams.length; j++) {
				paramsRaw += inputParams[j].fieldname;
				if (j < (inputParams.length - 1)) paramsRaw += ", ";
			}

			paramsRaw += "; Output: ";
			var outputParams = JSON.parse(registeredServices.aaData[i].output_params);
			for (var j = 0; j < outputParams.length; j++) {
				paramsRaw += outputParams[j].fieldname;
				if (j < (outputParams.length - 1)) paramsRaw += ", ";
			}

			$(".js-service-list-container").append("<div class=\"create-service__service-list-container-line_green\">/* " + paramsRaw + " */</div>");
			$(".js-service-list-container").append("<div>" + shortened + "{ ... }}</div>");
		}

		$(".js-service-list-container").append("<div class=\"create-service__service-list-container-line_green\">/* Input: msg */</div>");
		$(".js-service-list-container").append("<div>" + print + "</div>");
	});

	$('#get_remote_methods').click(function() {
		var servers = getDataFromWPSServersForm();
		if (servers.length === 0) {
			alert("Enter at least one correct WPS server information");
		} else {
			exWPSS = new WPSServer({wpshost: servers[0].host, wpsport: servers[0].port, wpspath: servers[0].path}, ServCallback);
			exWPSS.GetCapabilities("makeOptions", "form_wrapper");
		}
	});

	$('#type').change(function() {
		setEnv($('#type').val());
	});

	$('.js-save-and-exit').click(function() {
		saveMethod($('#type').val(), true);
	});

	$('.js-save').click(function() {
		saveMethod($('#type').val());
	});

	$('#generatejs').click(function () {
		var number_of_input_params  = $('#input_params_container').children('div').length;
		var number_of_output_params = $('#output_params_container').children('div').length;

		if ((number_of_input_params == 0) || (number_of_output_params == 0)) {
			alert('Enter at least one input and output parameter');
		} else {
			var name = $('#name').val();
			if (name.length == 0) {
				alert('Enter method name');
			} else {
				var wrapper = '';
				var helpers = '';

				$('#input_params_container').children('div').each(function() {
					helpers += '/* input.' + $('#field_name', $(this)).val() + ' - ' + $('#title', $(this)).val() + ' */<br>';
				});

				/*
					Already existing code, entered by user
				*/
				var alreadyexistingcode = '';
				if (editor.getValue().length > 0) {
					var functionbody = editor.getValue();
					var lines = explode('\n', functionbody);

					for (var i = 1; i < (lines.length - 4); i++) {
						alreadyexistingcode += lines[i] + '\n';
					} //end for
				} //end if

				$('#helpers_wrapper').html(helpers);
				wrapper += 'function ' + translit(name) + '(input, mapping){\n' +  alreadyexistingcode + '\n' + '}';
				editor.setValue(wrapper);
			} //end if
		} //end if
	});

	$('#metsel').on('change', function() {
		$('#input_params_container').html('');
		$('#output_params_container').html('');
	});

	if (mid !== null) {
		$('#type_wrapper').hide();
		$.ajax({url: '/dataset/list?f=185&f_id=' + mid, type: 'get', dataType: 'json', cache: false, async:false, success: function(data) {
			METHOD = data.aaData[0]; 
			METHOD.params=JSON.parse(METHOD.params);
			METHOD.output_params=JSON.parse(METHOD.output_params);			
		}});

		$("#metsel").remove();
		$("body").append('<input type="hidden" name="metsel" id="metsel" value="' + METHOD.wpsmethod + '"/>');

		$('#wps_params_status').show();
		if (METHOD.status == 'true') {
			$('[name="longprocess"]').attr('checked', 'checked');
			$('#longprocess_allowed').val('true');
		} else if (METHOD.status == 'false') {
			$('[name="longprocess"]').attr('checked', false);
			$('#longprocess_allowed').val('true');
		} else {
			$('#longprocess_allowed').val('false');
		}

		METHODID = METHOD.id;
console.log(METHOD);
		if (METHOD.wpsservers) {
			var methodName = JSON.parse(METHOD.wpsservers)[0].method;
			$("#metsel").val(methodName);
		}

		$('#name').val(METHOD.name);
		$('#description').val(functionReverseTags(METHOD.description));
		$('#specifications_wrapper').find('textarea').val(METHOD.map_reduce_specification);
		$('#type').val(METHOD.type).trigger('change');

		for (var i = 0; i < METHOD.params.length; i++) {
			$('#input_params_container').append(createInputField(METHOD.type, METHOD.params[i]));
		}

		if (METHOD.output_params !== null) {
			for (var i = 0; i < METHOD.output_params.length; i++) {
				if (METHOD.type == 'js') {
					$('#output_params_container').append(createInputField('js', METHOD.output_params[i]));
				} else if (METHOD.type == 'wps'){
					$('#output_params_container').append(createInputField('wps', METHOD.output_params[i], false));
				}
			}
		}

		if (METHOD.type === 'js') {
			$('#main_header').html('Modifying JavaScript method <i>' + METHOD.name + '</i>');
			editor.setValue(functionReverseTags(METHOD.js_body));
			JSFNAME = functionGetName(METHOD.js_body);
		}

		$('#parameters_section').show();
		$('#submit_container').show();
	}
});