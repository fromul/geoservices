function map_info(info_type){	
	this.activated=false;	
	var smart_obj=this;
	var marker;
	var smart_content=$('#smart_content_body');

	
	
	var search_layers={};
	
	
	this.smart_markers = new L.LayerGroup();	
	this.smart_markers_added=false;
	this.empty_info=function(){
		smart_content.empty();
	};
	
	function onmapclick (e){
		if(!smart_obj.activated )
			return;
		var radius=5;
		l_t_pnt=e.containerPoint;
		r_b_pnt=e.containerPoint;
		var l_t_pnt = L.point(e.containerPoint.x, e.containerPoint.y);
		var r_b_pnt = L.point(e.containerPoint.x + radius, e.containerPoint.y);
		g_l_t_pnt=map.containerPointToLatLng(l_t_pnt);
		g_r_b_pnt=map.containerPointToLatLng(r_b_pnt);
		
		while(g_l_t_pnt.lng<-180)
			g_l_t_pnt.lng+=360;
		while(g_r_b_pnt.lng<-180)
			g_r_b_pnt.lng+=360;
			
		
		var map_dx=Math.abs((g_l_t_pnt.lng - g_r_b_pnt.lng));
		
		if(marker===undefined){
			var queryIcon = new L.icon({iconUrl: '/images/markers/250.png', iconSize: [23, 27], iconAnchor:   [11, 26]});
			marker = L.marker(e.latlng,{icon:queryIcon}).addTo(map);
		}
		else{
			marker.setLatLng(e.latlng);
			marker.update();
		}
		// marker.bindPopup('<div id="popup_content">Info</div>').openPopup();
		smart_obj.get_info(g_l_t_pnt.lng, g_l_t_pnt.lat, map_dx);
	
	
	}

	
	
	this.activate=function(state){
		if(state){
			$('#map').css('cursor','help');			
			map.on('click', onmapclick);
			if(!smart_obj.smart_markers_added){
				map.addLayer(smart_obj.smart_markers);
				smart_obj.smart_markers_added=true;
			}
		}else{
			$('#map').css('cursor','default');
			map.off('click', onmapclick);
			if(smart_obj.smart_markers_added){
				map.removeLayer(smart_obj.smart_markers);
				smart_obj.smart_markers_added=false;
			}
		}
		smart_obj.activated=state;
	}
	
	this.get_info=function(lng, lat, map_dx){
		$('#info_address').html('');
		$('#info_reestr').html('');
		$('#info_reestr_detail').html('');
		//$('#info_place').html(lng+' '+lat);
		var search_point='POINT('+lng+' '+lat+')';		
		
		var latlng = L.latLng(lat, lng);
		
		$('#info_place').html(latlng.toString());
		smart_content.empty();
		var select_list=[];
		var layer_list=[];
		var popup_activated=false;
		function update_popup(){
			smart_content.empty();
			var popup_content=smart_content;
			if(!smart_content || smart_content.length==0){				
				popup_content=$('#popup_content');
			}
				
			

			//layerGroup
			if(true){					
				for(var i=0; i<select_list.length; i++){
					var tabdiv=$('<div class="smarttabdiv" />');
					popup_content.append(tabdiv);
					var h4=$('<div class="smart_h4_header">'+select_list[i].name + '</div>');
					tabdiv.data('objectlist', select_list[i]);
					tabdiv.append(h4);
					tab_content=$('<div class="popup_accordion"/>');
					tabdiv.append(tab_content);
					for(var j=0; j<select_list[i].objs.length; j++){
						var div_obj;
						if(select_list[i].objs[j].distance!=0)
							div_obj=$('<h3 class="smart_h3_header">'+select_list[i].objs[j].title + ' (' + select_list[i].objs[j].distance+' km)</h3><div class="obj_smart_content">' + select_list[i].objs[j].str_template + '</div>');
						else
							div_obj=$('<h3 class="smart_h3_header">'+select_list[i].objs[j].title + '</h3><div class="obj_smart_content">' + select_list[i].objs[j].str_template + '</div>');
						$(div_obj).data('obj', select_list[i].objs[j]);
						tab_content.append(div_obj);
					}
				}
				
				
				$( ".popup_accordion", smart_content ).accordion({
					active: false,
					collapsible: true,
					heightStyle: "auto"
				});
				$('.smarttabdiv1', smart_content).bind({
					mouseenter: function(e) {
						//console.log("enter smarttabdiv");
						h4=$(this);
						objlist=h4.data('objectlist');
						if(objlist.icon!='')
							var cIcon = new L.icon({iconUrl: objlist.icon, iconSize: [23, 27], iconAnchor:   [11, 26]});
						
						
						for(var i=0; i<objlist.objs.length; i++){
							wkt = new Wkt.Wkt();
							try { // Catch any malformed WKT strings
								wkt.read(objlist.objs[i].geom);
							} catch (e1) {
								alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
								return;
							}
							config = map.defaults;
							obj = wkt.toObject(map.defaults); // Make an object
							if(objlist.icon!=''){
								obj.eachLayer(function (layer) {
									layer.setIcon(cIcon);
								});
							}
							
							smart_obj.smart_markers.addLayer(obj);								  
						}							  
					  // Hover event handler
						//console.log(objlist);
						
					},
					mouseleave: function(e) {
						  smart_obj.smart_markers.clearLayers();
					  // Hover event handler
					   //console.log("leave smarttabdiv");
					}
				});
				var markerobj;
				$('h3,.obj_smart_content1', smart_content).bind({
					mouseenter: function(e) {
						h3=$(this);							
						obj=h3.data('obj');
						if(!obj.geom)
							return;
	
						if(obj.icon!='' && obj.icon!=undefined)
							var cIcon = new L.icon({iconUrl: obj.icon, iconSize: [29, 33], iconAnchor:   [13, 31]});
						wkt = new Wkt.Wkt();
						try { // Catch any malformed WKT strings
							wkt.read(obj.geom);
						} catch (e1) {
							alert('Wicket could not understand the WKT string you entered. Check that you have parentheses balanced, and try removing tabs and newline characters.');
							return;
						}
						config = map.defaults;//							
						markerobj = wkt.toObject(map.defaults); // Make an object
						if(obj.icon!='' && obj.icon!=undefined){
							markerobj.eachLayer(function (layer) {
								layer.setIcon(cIcon);
							});							
						}
						smart_obj.smart_markers.addLayer(markerobj);
					},
					mouseleave: function(e) {
						if(markerobj){
							smart_obj.smart_markers.removeLayer(markerobj);
							markerobj=undefined;
						}
					}
				});
					
			}
			else{
				p_html='';
				for(var i=0; i<select_list.length; i++){
					p_html+='<h4 style="text-align: left; color: #46A036; font-size: 14px;">'+select_list[i].name + '</h4>';
					p_html+='<div class="popup_accordion">';
					for(var j=0; j<select_list[i].objs.length; j++){
						if(select_list[i].objs[j].distance!=0)
							p_html+='<h3>'+select_list[i].objs[j].title + ' (' + select_list[i].objs[j].distance+' km)</h3><div>' + select_list[i].objs[j].str_template + '</div>';
						else
							p_html+='<h3>'+select_list[i].objs[j].title + '</h3><div>' + select_list[i].objs[j].str_template + '</div>';
					}
					p_html+='</div>';
				}					
				var popup = L.popup({autoPan: false})
					.setLatLng(latlng)
					.setContent('<div id="popup_content">' + p_html + '</div>')
					.openOn(map);
				$( ".popup_accordion" ).accordion({
					active: false,
					collapsible: true,
					heightStyle: "auto"
				});
			}
			
		}
		
		function layer_search(table_json, distance, limit, icon, selectedicon){
			if(limit===undefined)
				limit="5";
			var filter_value=distance + '--' + search_point;
			for (var i = 0; i < table_json.columns.length; i++) {		
				if(table_json.columns[i].widget===undefined)
					continue;
				if(table_json.columns[i].widget.name=='point' || table_json.columns[i].widget.name=='polygon' || table_json.columns[i].widget.name=='polyline'){
					var doc_frm= new doc_template(table_json);
					all_filter='';
					if(datasets){
						for(var j=0; j<datasets.length; j++){
							if(datasets[j].dataset_id==table_json.metaid){
								$.each(datasets[j].FilterValues, function(index, value) {
									if(table_json.columns[i].fieldname!=index)
										all_filter+='&f_'+index + '=' + value;
								});
								break;
							}
						}
					}
					var fldname=table_json.columns[i].fieldname;
					var data={sort:[{fieldname: 'calc_'+fldname, dir: true}]};
					str_res=JSON.stringify(data);
					//console.log(table_json.metaid);
					$.ajax({
						url: "/dataset/list?f=" + table_json.metaid +"&unique=" + table_json.unique + "&iDisplayStart=0&mapDisplayLength=true&iDisplayLength="+limit+"&f_" + fldname+"=" + filter_value+all_filter+'&calc_'+fldname+"=distance--" + search_point,
						dataType: "json",	
						contentType : 'application/json',
						data: str_res,
						method: "POST",
						type: "POST",
						success: function(data) {									
							if(data.aaData.length>0){
								doc_frm.init_form(function(){
									var table_list={name:doc_frm.doc_description.title, objs:[], icon:icon };
									for(var i=0; i<data.aaData.length; i++){
										var str_template=''
										var name='';
										var distance='';
										var geom=undefined;
										for (var j = 0; j < doc_frm.doc_description.columns.length; j++) {
											if(!doc_frm.doc_description.columns[j].visible)
												continue;
											if(doc_frm.doc_description.columns[j].fieldname=='name' && data.aaData[i][doc_frm.doc_description.columns[j].fieldname])
												name=data.aaData[i][doc_frm.doc_description.columns[j].fieldname];
											if (farr.indexOf(doc_frm.doc_description.columns[j].widget.name) != -1 ){
												geom=data.aaData[i][doc_frm.doc_description.columns[j].fieldname];
											}												
											var title=doc_frm.doc_description.columns[j].fieldname;
											if(doc_frm.doc_description.columns[j].title)
												title=doc_frm.doc_description.columns[j].title;
											if(fldname==doc_frm.doc_description.columns[j].fieldname){
												distance=data.aaData[i]['calc_'+doc_frm.doc_description.columns[j].fieldname];
											}
											else{
												val=data.aaData[i][doc_frm.doc_description.columns[j].fieldname];
												if(val && val!='')
													user_val=doc_frm.doc_description.columns[j].widget_object.getUserVal(val);
												else
													user_val='';
											}
											if(user_val!='')
												str_template+='<div>'+title+': '+user_val + ' </div>';
										}
										if(name=='')
											name=table_json.title;
										// if(distance!='')
										// 	str_template='<div>Distance: '+ distance + ' кm. </div>'+str_template;
										obj={title:name, str_template:str_template, distance: distance, geom: geom, icon:selectedicon, objectlist:table_list};
										table_list.objs.push(obj);
										
									}
									if(table_list.objs.length>0)
										select_list.push(table_list);
									update_popup();
								});
							}
							//table_json.layer.redraw_map();
						}
					});
					break;
				}
			
			}
		}
		
		function layer_load_search(search_dataset_id, distance, limit, icon, selectedicon){
			if(search_dataset_id in search_layers){
				layer_search(search_layers[search_dataset_id], distance, limit, icon, selectedicon);
			}
			else{
				if(isNaN(search_dataset_id))
					url="/dataset/list?f=100&iDisplayStart=0&iDisplayLength=5&s_fields=JSON&f_guid=" + search_dataset_id;
				else
					url="/dataset/list?f=100&iDisplayStart=0&iDisplayLength=5&s_fields=JSON&f_id=" + search_dataset_id;

				$.getJSON(url, function(data) {
					if(data.aaData.length==0)
						return;
					var table_json    = JSON.parse(data.aaData[0].JSON);
					search_layers[search_dataset_id]=table_json;
					table_json.metaid = search_dataset_id;				
					//wmsDraw(table_json, null);						
					layer_search(table_json, distance, limit, icon, selectedicon);
				});	
			}
		}
		

		
		$('#rastr_info').empty();
	
		$('.g_layer',$('#layer_list')).each(function( index ) {
			var ml=$(this).data("mlayer");
			if(ml.Mtype=="tiff"){
				$.getJSON("/datafile/info?f=" + ml.layer_id + '&lng=' + lng + '&lat=' + lat, function(res) {
					if(res.status=='ok' && res.data.values && res.data.values.length>0)
						$('#rastr_info').append($('<div>'+ml.title+': '+res.data.values[0]+'</div>'));
				});	
			}
			else if(layer_list.indexOf(ml.layer_id)==-1){
				layer_load_search(ml.layer_id, '4326:'+map_dx);
				layer_list.push(ml.layer_id);
			}
			
		});
	}




	
	
	return this;
}