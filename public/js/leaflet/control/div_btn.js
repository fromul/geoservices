/*
 * Calipso MainMenu plugin that look good.
 */
L.Control.div_btn = L.Control.extend({
	options: {
		id: 'sss',
		position: 'topleft',
		title: 'Menu',
		email: '',
		intentedIcon: 'icon-th',
		show_dialog: undefined,
		toggle: false
	},

	onAdd: function( map ) {
		this._map = map;
		var container = L.DomUtil.create('div', 'leaflet-bar');
		var wrapper = document.createElement('div');
		$(wrapper).attr("id", this.options.id);
		container.appendChild(wrapper);
		
		var link = L.DomUtil.create('a', '', wrapper);
		L.DomUtil.create('i', this.options.intentedIcon+' glyphicon', link);
		link.href = '#';
		link.style.width = '26px';
		link.style.height = '26px';
		//link.style.backgroundImage = 'url(' + this._icon + ')';
		//link.style.backgroundSize = '26px 26px';
		//link.style.backgroundRepeat = 'no-repeat';
		link.title = this.options.title;
		
		//this._addImage();
		//L.DomUtil.create('i', this.options.intentedIcon + extraClasses, this.link);

		var stop = L.DomEvent.stopPropagation;
		L.DomEvent
			.on(link, 'click', stop)
			.on(link, 'mousedown', stop)
			.on(link, 'dblclick', stop)
			.on(link, 'click', L.DomEvent.preventDefault)
			.on(link, 'click', this._toggle, this);

		//this.options.show_dialog = document.getElementById('adminMenu');//document.createElement('div');
		//menu.div = 'adminMenu';
		//menu.style.display = 'none';
		//menu.style.position = 'absolute';
		//menu.style.left = '27px';
		//menu.style.top = '0px';
		//menu.style.width = '95%';
		//menu.style.zIndex = -10;
		//container.appendChild(menu);
		return container;
	},

	_toggle: function() {		
		
			if( !this.options.toggle ) {
				$('.btn_div').hide();
				
				$('.btn_div').each( function( ) {
					div_btn=$(this).data('div_btn');
					if(div_btn.options.clickfunc)
						div_btn.options.clickfunc(false);
				});
				
				
				if(this.options.clickfunc)
					this.options.clickfunc(true);
				if(this.options.show_dialog && this.options.show_dialog.length>0){
					this.options.show_dialog[0].style.display = 'block';
					$('input[type="text"]:first', this.options.show_dialog).focus();//[name="user[username]"]
				}
				this.options.toggle = true;
			} else {
				this._collapse();
			}
		
		
	},
	
	visible: function (show){
		if(show)
			this._container.style.display = 'block';
		else
			this._container.style.display = 'none';
	},	
	

	_collapse: function() {
		this.options.toggle = false;
		if(this.options.show_dialog && this.options.show_dialog.length>0){
			this.options.show_dialog[0].style.display = 'none';
			if(this.options.clickfunc)
				this.options.clickfunc(false);
			
		}
	},

	_nominatimCallback: function( results ) {
		if( results && results.length > 0 ) {
			var bbox = results[0].boundingbox;
			this._map.fitBounds(L.latLngBounds([[bbox[0], bbox[2]], [bbox[1], bbox[3]]]));
		}
		this._collapse();
	},

	_callbackId: 0,

	/* jshint laxbreak: true */
	_icon: "../img/glyphicons-halflings.png"
});


L.div_Button = function( btnIcon , btnTitle , show_div , clickfunc ) {
  show_div.css( "zIndex", 801);
  var newControl = new L.Control.div_btn;
  
  if (clickfunc) newControl.options.clickfunc = clickfunc;
  
  if (btnIcon) newControl.options.intentedIcon = btnIcon;
  
  if (btnTitle) newControl.options.title = btnTitle;
  
  if (btnTitle) newControl.options.show_dialog = show_div;  
  
  head=$('<div class="modal-header">\
  <h5 class="modal-title">' + btnTitle + '</h5>\
  <button type="button" class="close" aria-label="Close">\
  <span aria-hidden="true">&times;</span></button>\
</div>');

	
  show_div.prepend(head);
  
  show_div.addClass('btn_div');  
  show_div.hide(); 
  show_div.data('div_btn',newControl);
  
  $('.close',show_div).click(function(){
	  newControl._toggle();
//		show_div[0].style.display = 'none';
  });

  return newControl;
};
