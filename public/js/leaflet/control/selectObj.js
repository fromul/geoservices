/*
 * Leaflet Geocoding plugin that look good.
 */
L.Control.ObjSel = L.Control.extend({
	clicked: false,
	options: {
		position: 'topleft',
		title: 'Object Selection',
		email: ''
	},

	onAdd: function( map ) {
		this._map = map;
		var container = L.DomUtil.create('div', 'leaflet-bar');
		var wrapper = document.createElement('div');
		container.appendChild(wrapper);
		var link = L.DomUtil.create('a', '', wrapper);
		link.href = '#';
		link.style.width = '26px';
		link.style.height = '26px';
		link.style.backgroundImage = 'url(' + this._icon + ')';
		link.style.backgroundSize = '26px 26px';
		link.style.backgroundRepeat = 'no-repeat';
		link.title = this.options.title;

		var stop = L.DomEvent.stopPropagation;
		L.DomEvent
			.on(link, 'click', stop)
			.on(link, 'mousedown', stop)
			.on(link, 'dblclick', stop)
			.on(link, 'click', L.DomEvent.preventDefault)
			.on(link, 'click', function(){
				this.clicked = !this.clicked;
				if(this.clicked){
					$('#map').css('cursor','help');
				}else{
					$('#map').css('cursor','default');
				}
			}, this);


		var form = this._form = document.createElement('form');
		form.style.display = 'none';
		form.style.position = 'absolute';
		form.style.left = '27px';
		form.style.top = '0px';
		form.style.zIndex = -10;
		L.DomEvent.on(form, 'submit', function() {
			alert('ok'); 
			return false; 
		}, this).on(form, 'submit', L.DomEvent.preventDefault);
		container.appendChild(form);

		return container;
	},
	
	Clicked: function(){
		return this.clicked;
	},
	
	dropState: function(){
		$('#map').css('cursor','default');
		this.clicked = false;	
	},
	
	_callbackId: 0,

	/* jshint laxbreak: true */
	_icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA7CAYAAAAn+enKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjExR/NCNwAADFJJREFUaEPlm3lw1dUVx18iAaVVWsfS1rpMR4aZasdap3/YGS0G8rYskAUCJCSQCCGELRCWhFWUXRZlFdmUVSSyLwHZYShboEBJ2CoqFQXRoSVAyPJuP+d6f69ZXtKXBIcmnpk7eb/f7/7uvd97zvmec+57sZWVXr162VJTU21LliwxdxqwZGdn2xISEmzx8fG2N99809aqVSvzpAGLw+HQrW3btja73d7wQVuAIyIiNOAGD9oCPG7cOFtcXJwtLCzM1qlTJ1t4eLjp0cDE7XZrwJa89tprTZKSkn41duzY57OysgLN7YYj4rsul0t/hsQCYmNj27Rv3z4f5v46NDT0Rcy7YYEuC1gEbQfT8jBthT/vDgkJsQP6AfO4/ktVgHv37n0JbV8EeF7Hjh0T8OnGpkv9loqA8WkBfGr37t2lly9fLh44cOAF+uTD4rFo+0HTrf5KRcBcBwP65M6dO0uvXLmiTp06VfTGG28UREZGHmnXrl0i2v6J6Vo/pQrAedu2bVNoWH322Wfq6NGjirBVgol/Q2Y2/tVXX21kuv8gsmHDhoB169YFfvzxx4EQaeD69evvHXFWBXjLli0a7KeffqouXryoQU+ZMuULGPxfmPbgNm3aPGpeuaeyevXqFitXroz56KOPcvibs2LFihzu5SxbtuzPXD+3du3aus1bFeCNGzeqCxcuqPPnz6uzZ8+q/Px8dfr06dIZM2Z4eH6DfrO7du36W/NanQUwLT744IP0xYsX5zPHnTFjxqghQ4YoOEQNHjxY4Va35syZc40iZ+ry5cubm9dqLlUBxozUuXPnNNAzZ84IWHXy5EmVm5ur5s+fr8jGCtH2yujo6BZoO8C8XmMBaMDChQv/MGvWrJyJEyeWZGRkqA4dOigJi06nU0GguhElZE6VmZl5c/bs2bPef//9X5shaiYsWmdblliAMZ1yQE+cOKHBHjt2TB06dEjNmzdPkYr+m0XN4Z3fde7cuVZ+hraavffee4uo1jxkeYpkR6EAFRUVpRITE5Xc69atm2IOfR/iVEOHDi2dO3fuQkz9N2YY/wXWtTGJufovYMiiHFDx4cOHD2uwBw8eVHv27NGgWVQBWt7N3zYkKDXW9NKlS53vvPPO59TlGqxoNi0tTUjyDvePvfvuuzlo9PqwYcM0cOkj4MXM2awtZhj/BeYtBzgmJiZYkg1IQx0/flxr9MiRI16gBw4cUPv27VN79+5VO3bsUJiWwpc/J07nYtov1xQ05pyEKSssTZtuv379FBovwdTXQFS/x2cf42/M22+/PWb06NFa47Ip0vD1QjOM/1IRMNfBLD6PCSsB3b9/vwa6a9cuRZxWn3zyiZLwtWbNmrtx8XGXWfAJzM6B1fidla1ataoTllLQp08fj2iZz8W4ExSyvqnpomXy5MlNKGgGY87al8W/8fe79Pu56eKfVAUYc6mkUbKvckBzcnLU5s2blTA6WlA9e/a8yfvnIZ1k3OJhM2S1wsYGocVktPo3rOUSYOcxXjmwImg4CEvoNWrUKG3a4s89evTwEK+3my7+SVWAWYRXoxWBbt261QuUJEExqWhZ8U7BgAEDbpGV5VF49MDEHzHDVitsblM27CVIyMGYPzO3y8miRYtiJ0yYsFw0DEFqDZPve9igewOYeOhTo5KQbNq0qRxQITjx+Q8//FAtWLDAQ+wsZszLLGw2hNbEDF1rwWxHEoOvQFwFlK2atMTfp06dWsSzV0w3/0RON3wBZkc10O3bt1cJlNRPA8UPlfi8mDWsq0ggVHp6f8U4Kj4+fhmarmSi/grzPYwF7IW1NWFZYCUhwaLumG7+S5cuXfSxjiVsQDCD5kly4QuoxGcBivl5gbIgL1CxDJhXh6yMjIHEzbY3WOBk4mpLxvY7VmMtjRjzBfx6Df5bKuwtYMV32UT11ltvXWUN40x3/6UiYK6DCe55xD8NVPy0JkAxaQ1W3idsqKGZQ1VycvINzHs9Y7/kb9hirOYzZ86cP2LEiMLu3bvrhEP8luJFYvQ55uzOWoJMd/9FzqTLAuZaAybYVwtUzLciUJhWA8XfFKmiBjx9+nRZYBHavQRzb6TweL5169bVgma+RphrItnXVUKc1qqAlXA0fvz4UrS+kLU8ZrrXTKoCzO5WAkrlooEKg0vCIX5eFijpngYq75IlabAQiyKGekaMHPFNTPuYSyx8B0nGK9VpmrmC2MjhI0eO1Dxg+ey0adOKmHcD5i5VU42zOi3maPZJcynX2ofxGy/z+gIqWv1fQPEzNWnSJCWZFJqRHFiJxmDui5hmDIzrk8GZI4gxh0t/KRoEMONcZf6uWF1L0612YgA/bS5t+JoGLAu2TNcXUPFTASqmXxEodbMXqLArGZKUd+r111+X7KgQAruKlr8icUjt27dvJT/EJYLQ5vD09HSdQgpg5jhvHtdNAChh6QVzqQFLWBLtWEDFT8V0hbl9ARVr8KXRskAlQ4KAVFZWViEp5FWSE8mfvyUPzwBYMzO9FjFZxj4mMRe//0EAP2sudVgSwLJoS6O+gFqEZGkUP1VkQrqKIS+WLEhJbiyLJuVUKSkpkgoqYVxhWmFdAQNnXKcoiDfTa8GF2oiLSH8LMPPeG8AAFMDevNcCLBqqCNRiXtEoJucFamlUTinYQA+segvf+w7X+A5g36HNSk3uU5l9RSo6l3GfMtNrAXBz5p0HaRWJz8umQWS55nHdxAD2VjdCWsLShAS/gIpWxU+lf2ZmppDSZUipCwXEI7SmZG5N2QRvY3zd5DNxuSmgfMZS3OmXbPg4/Pki4e+vEOgvzKO6iQlL3tTPYmnxu6qAip+WBSqalVo1rXeakMx5CodQWq2/rYCJAwiHT8AfMYDdBYf0w6//aB7XTQxLe49d2XkNWEhGgFYMMQIUcHd7pfUq6Ne/nyYiiZfDhw9XgwYNKu4Q2+EMYCfTfFY9/ghx/xnCXTZh6Ut8vxiT/pL5c4kYkaZL7cUA9sZDASw+LAB8mW5sx1jRosfldpW43a67kNAdmNcj5jxkyBAPZHUD/z0C4BAzZI2EhOJRwuEFwpecjmrCkiYHeyjgc3y5velaOwFgOR+2qiVAlI2lHjagVLIdnhfjo7s6x3XcHR4Rdg3GLUWzol2dDcHQdwk3X7tczjUOh/1pWP4h2N7vr2iowh7Bd7dKZWSBlcYmixX9k83oZLrWTgxgL3Gwk8FoKE/OhEWj4pvEyeKkpKTbiYmJp7m/HnN/KrVPypNR0ZFjIqMiv0LLhfTxSEVDIqFPPiC+62i5KyGsGYB/aob3S3ChFxnrAuvyAmajS7G4baS7dTsLh1UFsLdsswDL+bCAhdSEeT0AOYjW3ZDY05hd0KCs9IDIqHZPsJgl9L+ZlpYm5qxjr8TdmJjoQrvDvhPQz1CkN6a8fILKy6/ykEyuKS4SSxw+I2GJ5EQ2X1JLu+lSe2HnfGpYNCW5LGmgB3M9hv+8wg6XY96IiLAAYm4GoG9ER0fdlnhpJRcsshQzvMKzuXEJnR9CM43xP7+/k4IIGxGSXITjU5Sg1wHbgjHq/h0TAOUg3gvE8mEhDAjtNlqdSgx82TyuJGxIIKA2sWm3kpK6XaP21YdsmL/4+t+dLmdpaJg7mfD1oGjOvHb/pCJgzDIYf82TxB3/W2VuVyswdhdAX4+OibqWkJhQQkIhmyV+p3iGD9pX0x433e+vGMBeUyHBeJyEvyPmlJqdne3Xl1YR7SIeDg0LnesOdRcBsgQi1GAZ29Szdkzb7qAWrrtJ1lXEh/G1Ov2GIzklOQCwz6HlXCkKBCh5sv5+iCRGTixKeLYuom24t+6+byK5NIDNVe0FNn8AUCmAk8JAV0MCVuKnOaK5HRERPiK1f0oTGLt2pxX3QjBhG+HHXNVOILUA2oOA+hOgjwvhwfTebwLlPIr7Kiw89B/R7dv9xXpHv1yfBQ02c7ocywFYXBaoaaW0AuJyn5CQkCb1HjCxMqBnakqQy+2UXwEdohXRPLTvATsdHqfTUQhgKSz8+t6pXgg+2xiAg2DlAocrpMDptn9hgQboUVoYnxvGb74sAWxLgG2j3aR9C8B82hhMuTnt/oemey2tWrUKBGgP2gJaX9qztB/0p073XdDo44BsQavzt4f1QghJ5lMDFmJvAJolAbEHOtwhNlfo9z8+txralr8NS+OEHu+/ElhAicc2yEq3BicQllebZVuDlrKAfxTyoyCs/1+x2f4DCfiwucEGs/kAAAAASUVORK5CYII='
	
	});

L.control.objsel = function( options ) {
	return new L.Control.ObjSel(options);
};