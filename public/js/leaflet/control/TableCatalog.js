/*
 * Calipso TableCatalog plug-in.
 */
L.Control.TableCatalog = L.Control.extend({
	options: {
		position: 'topleft',
		title: 'Table catalog',
		email: ''
	},

	onAdd: function( map ) {
		this._map = map;
		var container = L.DomUtil.create('div', 'leaflet-bar');
		var wrapper = document.createElement('div');
		container.appendChild(wrapper);
		var link = L.DomUtil.create('a', '', wrapper);		
		link.href = '#';
		link.style.width = '26px';
		link.style.height = '26px';
		//link.style.backgroundImage = 'url(' + this._icon + ')';
		//link.style.backgroundSize = '26px 26px';
		//link.style.backgroundRepeat = 'no-repeat';		
		link.title = this.options.title;
		
		L.DomUtil.create('i', 'icon-list glyphicon', link);

		var stop = L.DomEvent.stopPropagation;
		L.DomEvent
			.on(link, 'click', stop)
			.on(link, 'mousedown', stop)
			.on(link, 'dblclick', stop)
			.on(link, 'click', L.DomEvent.preventDefault)
			.on(link, 'click', this._toggle, this);

		var menu = this._menu = document.getElementById('legend_div');
		return container;
	},

	_toggle: function() {
		if( this._menu.style.display != 'block' ) {
			this._menu.style.display = 'block';
		} else {
			this._collapse();
		}
	},

	_collapse: function() {
		this._menu.style.display = 'none';
	},

	_nominatimCallback: function( results ) {
		if( results && results.length > 0 ) {
			var bbox = results[0].boundingbox;
			this._map.fitBounds(L.latLngBounds([[bbox[0], bbox[2]], [bbox[1], bbox[3]]]));
		}
		this._collapse();
	},

	_callbackId: 0,

	/* jshint laxbreak: true */
	_icon: "../img/glyphicons-halflings.png"
});

L.control.tableCatalog = function( options ) {
	return new L.Control.TableCatalog(options);
};
