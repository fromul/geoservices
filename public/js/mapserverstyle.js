var extend = require('extend');


function hex2rgb(hex) { // 16-тиричный в rgb
	try{
		hex=hex.trim();
		if(!hex || hex=='')
			return '0 0 0';
		if (hex[0]=="#") hex=hex.substr(1);
		if (hex.length<7){
		if (hex.length==1)hex='00000'+hex;else
		if (hex.length==2)hex='0000'+hex;else
		if (hex.length==4)hex='00'+hex;else
		if (hex.length==5)hex='0'+hex;else
		if (hex.length==3) {
		var temp=hex; hex='';
		temp = /^([a-f0-9])([a-f0-9])([a-f0-9])$/i.exec(temp).slice(1);
		for (var i=0;i<3;i++) hex+=temp[i]+temp[i];
		}
		var triplets = /^([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})$/i.exec(hex).slice(1);
		return ' '+parseInt(triplets[0],16)+' '+parseInt(triplets[1],16)+' '+parseInt(triplets[2],16);
		}
	}
	catch{
		return ' 0 0 0';
	}
}

function style_rule(name){
	this.name=name;
	this.styletype=0;	//0 - simple; 1- exspression
	this.operator='PropertyIsEqualTo';
	this.mark=name;	
	this.PropertyName='';
	this.val='';
	this.val2='';
	this.expression='';
	this.labelfieldname='';
	this.font_size=7;
	this.font_color='#000000';
	this.font_bold=false;
	this.font_italic=false;
	this.rastropacity=100;
	this.gradientfrom='';
	this.gradientto='';
	this.style_type='';
	
	//this.compare_sign='PropertyIsEqualTo';
	this.symbolizers=new Array();
	this.x=1;
	this.y=1;
	var f=this;

	this.DrawPolygonMAP=function(sign_layer){
		res='';
		if(sign_layer.fill_type=='solid')
			res+='\
	STYLE\n\
		COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
		OUTLINEWIDTH 10\n\
		OPACITY '+sign_layer.fill_opacity+'\n\
	END\n\
	';
	
		else if(sign_layer.fill_type!='none'){
			if(!sign_layer.fill_width || sign_layer.fill_width==null)
				sign_layer.fill_width=2;
			res+='\
	STYLE\n\
		SYMBOL "' + sign_layer.fill_type + '"\n\
		WIDTH ' + sign_layer.fill_width + '\n\
		COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
		SIZE '+sign_layer.fill_sign_size+'\n\
		GAP '+sign_layer.fill_gap+'\n\
		OFFSET '+sign_layer.fill_sign_dx+' '+sign_layer.fill_sign_dy+'\n\
		OPACITY '+sign_layer.fill_opacity+'\n\
	END\n\
	';
		}
		if(sign_layer.stroke_type=='solid')
			res+='\
	STYLE\n\
		OUTLINECOLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		WIDTH '+sign_layer.stroke_width+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
		return res;
	}

	this.DrawRastrMAP=function(sign_layer,  mapstyle){		
		var res='';
		if(this.style_type=='gradient'){
			res+='\
			STYLE\n\
				COLORRANGE  '+hex2rgb(this.gradientfrom)+'	'+hex2rgb(this.gradientto)+'\n\
				OPACITY '+this.rastropacity+'\n\
				DATARANGE '+mapstyle.layer_json.min+' '+mapstyle.layer_json.max+'\n\
				RANGEITEM "foobar"\n\
			END\n\
			';
		}
		else if(this.style_type=='initial'){
			res='';
		}
		else{
			res='\
			STYLE\n\
				COLOR'+hex2rgb(sign_layer.fill_color)+'\n\
				OPACITY '+sign_layer.fill_opacity+'\n\
			END\n\
			';
		}
	
	return res;
		
		
	// 	if(sign_layer.fill_type=='solid')
	// 		res+='\
	// STYLE\n\
	// 	COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
	// 	OUTLINEWIDTH 10\n\
	// 	OPACITY '+sign_layer.fill_opacity+'\n\
	// END\n\
	// ';
	
	// 	else if(sign_layer.fill_type!='none')
	// 		res+='\
	// STYLE\n\
	// 	SYMBOL "' + sign_layer.fill_type + '"\n\
	// 	COLOR  '+hex2rgb(sign_layer.fill_color)+'\n\
	// 	SIZE '+sign_layer.fill_sign_size+'\n\
	// 	GAP '+sign_layer.fill_gap+'\n\
	// 	OFFSET '+sign_layer.fill_sign_dx+' '+sign_layer.fill_sign_dy+'\n\
	// 	OPACITY '+sign_layer.fill_opacity+'\n\
	// END\n\
	// ';
	// 	if(sign_layer.stroke_type=='solid')
	// 		res+='\
	// STYLE\n\
	// 	OUTLINECOLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
	// 	OUTLINEWIDTH '+sign_layer.stroke_width+'\n\
	// 	OPACITY '+sign_layer.stroke_opacity+'\n\
	// END\n\
	// ';
	// 	return res;
	}
	this.DrawLineMAP=function(sign_layer){
		res='';
		
		if(sign_layer.stroke_type=='solid')
			res+='\
	STYLE\n\
		OUTLINECOLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		OUTLINEWIDTH '+sign_layer.stroke_width+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
		else if(sign_layer.stroke_type!='none')
			res+='\
	STYLE\n\
		SYMBOL "' + sign_layer.stroke_type + '"\n\
		COLOR  '+hex2rgb(sign_layer.stroke_color)+'\n\
		SIZE '+sign_layer.stroke_sign_size+'\n\
		GAP '+sign_layer.stroke_gap+'\n\
		OPACITY '+sign_layer.stroke_opacity+'\n\
	END\n\
	';
	
		return res;
	}
	
	this.DrawPointMAP=function(sign_layer){
		try{
		if(sign_layer.point_type=='font symbol')
			res='\
	STYLE\n\
		SYMBOL "'+sign_layer.point_font + sign_layer.point_font_character+'"\n\
		COLOR  '+hex2rgb(sign_layer.point_color)+'\n\
		SIZE '+sign_layer.point_sign_size +'\n\
		OFFSET '+sign_layer.point_sign_dx+' '+sign_layer.point_sign_dy+'\n\
		OPACITY '+sign_layer.point_opacity+'\n\
	END\n\
	';
		else if(sign_layer.point_type=='image')
			res='\
	STYLE\n\
		SYMBOL "'+sign_layer.image+'"\n\
		COLOR  '+hex2rgb(sign_layer.point_color)+'\n\
		SIZE '+sign_layer.point_sign_size+'\n\
		OFFSET '+sign_layer.point_sign_dx+' '+sign_layer.point_sign_dy+'\n\
		OPACITY '+sign_layer.point_opacity+'\n\
	END\n\
	';
		else
			res='\
	STYLE\n\
		SYMBOL "'+sign_layer.point_type+'"\n\
		COLOR  '+hex2rgb(sign_layer.point_color)+'\n\
		SIZE '+sign_layer.point_sign_size+'\n\
		OFFSET '+sign_layer.point_sign_dx+' '+sign_layer.point_sign_dy+'\n\
		OPACITY '+sign_layer.point_opacity+'\n\
	END\n\
	';
	}
	catch{
		var stack = new Error().stack
		console.log( stack )
		console.log( 'sign_layer=',sign_layer )

	}
		
		return res;
	}	
	
	this.DrawMAP=function(expression, mapstyle){
		if(this.title)
			res='\
CLASS\n\
			NAME    "'+this.title+'"    	\n';
		else
			res='\
CLASS\n\
			NAME    "unnamedclass"    	\n';
			if(expression && this.expression && this.expression!=''){
				res+='\
	EXPRESSION	('+this.expression+') \n\
	';
			}
			
			for(var j=0; j<this.symbolizers.length; j++){
				// update_layer(this.symbolizers[j]);
				/*
				if(this.symbolizers[j].stroke_width===undefined){
					this.symbolizers[j].stroke_width=1;			
				}*/
				if(this.geometry_type=='point'){
					res+=this.DrawPointMAP(this.symbolizers[j]);					
				}
				else if(this.geometry_type=='line'){
					res+=this.DrawLineMAP(this.symbolizers[j]);
				}
				if(this.geometry_type=='polygon'){
					res+=this.DrawPolygonMAP(this.symbolizers[j]);
				}
				if(this.geometry_type=='tiff'){
					// console.log('mapstyle DrawRastrMAP',mapstyle)
					res+=this.DrawRastrMAP(this.symbolizers[j], mapstyle);
				}
			}			
			var font='verdana';
			if(this.font_bold && this.font_italic)
				font='verdana-bold-italic';
			else if(this.font_bold)
				font='verdana-bold';
			else if(this.font_italic)
				font='verdana-italic';				
			if(this.font_size && this.font_size!=''){
				if(this.geometry_type=='line'){
					res+='\
	LABEL\n\
		COLOR  '+hex2rgb(this.font_color)+'\n\
		FONT "'+font+'"\n\
		TYPE truetype\n\
		SIZE '+this.font_size+'\n\
		ANGLE AUTO\n\
		POSITION AUTO\n\
		PARTIALS FALSE\n\
	END \n';
				}
				else
					res+='\
	LABEL\n\
		COLOR  '+hex2rgb(this.font_color)+'\n\
		FONT "'+font+'"\n\
		TYPE truetype\n\
		SIZE '+this.font_size+'\n\
		ANGLE 0\n\
		POSITION AUTO\n\
		PARTIALS FALSE\n\
	END \n';
			}
			res+='	END\n';
		// console.log('end res=',res);
		return res;
	}
	
	return this;
}


function mapserverstyle(){	
	// this.form_div=$(Map_style_editor_str);
	// var base_symbolizer=$("#base_symbolizer", this.form_div);
	var m_style=this;
	this.layer_name='';
	this.style_type='single'; //0 - simple; 1- exspression
	this.labelfieldname='';	
	this.geometry_type='';
	this.layer_json;
	//this.BAND=0;
	this.rules=[];		
	this.base_rule=new style_rule('');
	this.base_rule.geometry_type=m_style.geometry_type;
	this.rules.push(this.base_rule);
	
	this.chart_type='pie';
	this.chart_size='static';
	this.chart_dx=0;
	this.chart_dy=0;
	this.static_chart_size=30;
	this.chartfields=[];
	this.bar_width=20;
	this.bar_heigth=20;
	this.pie_minsize=10;
	this.pie_maxsize=100;
	this.pie_minval=10;
	this.pie_maxval=50;
	

	this.title='';
	this.flds=[];
	this.readFromJSON=function(json_text){
		obj=JSON.parse(json_text);
		// console.log('step in readFromJSON 0');
		var tmp_geometry_type=this.geometry_type;
		extend(this, obj);
		// console.log('step in readFromJSON 1');
		if(obj.geometry_type===undefined || obj.geometry_type=='')
			this.geometry_type=tmp_geometry_type;

		for(var i=0; i<obj.rules.length; i++){
			r=new style_rule('');
			extend(r, this.rules[i]);
			r.geometry_type=this.geometry_type;
			this.rules[i]=r;
			if((!r.expression || r.expression=='') && (r.PropertyName!='' && r.val!='') && r.operator){
				switch(r.operator) {
					case 'PropertyIsBetween':
						r.expression="'["+r.PropertyName+"]' >= '"+r.val+"' AND '["+r.PropertyName+"]' <='"+r.val2+"'";
						break
					case 'PropertyIsNotEqualTo':
						r.expression="'["+r.PropertyName+"]' != '"+r.val+"'";
						break					
					case 'PropertyIsLessThanOrEqualTo':
						r.expression="'["+r.PropertyName+"]' <= '"+r.val+"'";
						break
					case 'PropertyIsLessThan':
						r.expression="'["+r.PropertyName+"]' <'"+r.val+"'";
						break
					case 'PropertyIsGreaterThan':
						r.expression="'["+r.PropertyName+"]' >'"+r.val+"'";
						break
					case 'PropertyIsGreaterThanOrEqualTo':
						r.expression="'["+r.PropertyName+"]' >='"+r.val+"'";
						break
					default:
						r.expression="'["+r.PropertyName+"]' =='"+r.val+"'";
						break
				}
			}			
		}
		br=new style_rule('');
		extend(br, this.base_rule);
		br.geometry_type=this.geometry_type;
		this.base_rule=br;
		
		// console.log('step in readFromJSON 2');
		//this.base_rule=this.rules[0];
		
		// if(obj.flds){
		// 	console.log('step in readFromJSON 2.0 obj.flds=',obj.flds);
		// 	for(var i=0; i<obj.flds.length; i++){
		// 		f=new field_classify(this.layer_json);
		// 		console.log('step in readFromJSON 2.1');
		// 		extend(f, this.flds[i]);
		// 		if(obj.flds[i].classes){
		// 			console.log('step in readFromJSON 2.15');
		// 			var len=obj.flds[i].classes.length;
		// 			for(var j=0; j<len; j++){
		// 				cls=new fieldclass();
		// 				console.log('step in readFromJSON 2.2');
		// 				extend(cls, obj.flds[i].classes[j]);
		// 				console.log('step in readFromJSON 2.3');
		// 				obj.flds[i].classes[j]=cls;
		// 				//f.classes.push(cls);
		// 			}
		// 		}
		// 		this.flds[i]=f;			
		// 	}
		// }
		// console.log('step in readFromJSON 3');
		if(obj.chartfields){
			for(var i=0; i<obj.chartfields.length; i++){
				f=new chartfield(this.layer_json);
				extend(f, this.chartfields[i]);				
				this.chartfields[i]=f;			
			}
		}
		// console.log('step in readFromJSON 4');
	};
//var sign_layer={	sign_type: '',	stroke_width: $(node).find("CssParameter[name='stroke-width']").text() , stroke_color: $(node).find("CssParameter[name='stroke']").text() ,	bgcolor: $(node).find("CssParameter[name='fill']").text() ,	dx:0,	dy:0, angle:0, fillStyle: $(node).find("WellKnownName").text() };
			
	this.save2JSON=function(){
			this.bar_width=20;
			/*
	this.bar_heigth=20;
	this.pie_minsize=10;
	this.pie_maxsize=50;
	this.pie_minval=10;
	this.pie_maxval=50;*/
		

		var keys=['rules', 'base_rule','sym','font_bold','font_italic','pie_maxval','chart_dx', 'chart_dy','pie_minval','pie_maxsize','pie_minsize', 'chart_type','chart_size', 'bar_width', 'bar_heigth','static_chart_size','chartfields', 'title', 'style_type', 'geometry_type', 'LABELITEM', 'label', 'PropertyName','symbolizers', 'val', 'val2', 'operator', 'mark', 'BAND','expression', 'classes', 'flds','gradientfrom', 'gradientto','rastropacity'];
		
		for (var key in def_layer) {
			keys.push(key);			
		}
		s = new field_classify(this.layer_json);
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		s = new fieldclass();
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		s = new style_rule();
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		
		s = new chartfield();
		for (var key in s) {
			if(typeof s[key] ==='number' || typeof s[key] ==='string')
				keys.push(key);
		}
		for (var key in def_label) {
			keys.push(key);			
		}
		
		var ind=keys.indexOf('form_div');
		keys.splice(ind,1);
		return JSON.stringify(this, keys);
    }
    
	
		
	this.setgeometrytype=function(geom){
		this.geometry_type=geom;
		this.base_rule.geometry_type=geom;
		for(var i=0; i<this.rules.length; i++){
			rule=this.rules[i];	
			rule.geometry_type=geom;			
		}
	};
	this.save2Map=function(){
			
			var res='';
			if(this.geometry_type!='tiff')
				res="TYPE "+this.geometry_type+"\n";
			// console.log('style_type	=', this.style_type);
			if(this.labelfieldname && this.labelfieldname!=''){
				res+='LABELITEM    "'+this.labelfieldname+'"\n			';
			}
			else if(this.label &&  this.label.labelfieldname !='none'  &&  this.label.labelfieldname !=null){
				res+='LABELITEM    "'+this.label.labelfieldname+'"\n			';
			}
			// console.log('style res1	=',this.geometry_type);
			// console.log('style res	=', res);
			
			if(this.style_type=='single' || this.style_type==0){
				this.base_rule.style_type=this.style_type;
				if(this.geometry_type=='tiff'){
					this.base_rule.expression='[pixel] > -999999999'
					res+=this.base_rule.DrawMAP(true, this);
				}
				else					
					res+=this.base_rule.DrawMAP(false, this);
			}
			else if(this.style_type=='chart'){
				res='	TYPE CHART\n\
		PROCESSING "CHART_TYPE='+this.chart_type+'"\n\
	';
				if(this.chart_type!='pie'){
					res+='PROCESSING "CHART_SIZE='+this.bar_width+' '+this.bar_heigth+'"\n';
				}
				else{
					if(this.chart_size=='static')
						res+='PROCESSING "CHART_SIZE='+this.static_chart_size+'"\n';
					else
						res+='PROCESSING "CHART_SIZE_RANGE='+this.chart_size+' '+this.pie_minsize+' '+this.pie_maxsize+' '+this.pie_minval+' '+this.pie_maxval+'"\n';
				}
					for(var i=0; i<this.chartfields.length; i++){			
						res+='CLASS\n\
			NAME "'+this.chartfields[i].fieldname+'"\
			STYLE\n\
				SIZE ['+this.chartfields[i].fieldname+']\n\
				COLOR   '+hex2rgb(this.chartfields[i].color)+'\n\
			END\n\
		END\n';

				}
			}
			else if(this.style_type=='gradient'){
				res+=this.base_rule.DrawMAP(true, this);
			}
			else if(this.style_type=='initial'){
				res+='';
			}			
			else {
				// console.log('before rules', this.rules);
				for(var i=0; i<this.rules.length; i++){
					rule=this.rules[i];
					// console.log('before draw rule', i);
					res+=rule.DrawMAP(true, this);	
				}
				// console.log('after rules');
			
			}
	/*		if(this.rules[0].PropertyName!=undefined && this.rules[0].PropertyName!='')
				res+='\
			CLASSITEM    "'+this.rules[0].PropertyName+'"\n\
			';*/
			//console.log(res);
			// console.log('style res	=',res);
			return res;		
		
	};

	
	return this;
};	


if(typeof (module)!== 'undefined')
	module.exports.mapserverstyle = mapserverstyle;