/**
 * HomepageRouter is used for catching the provided get parameters and performing certain actions associated with them.
 */
 var router;
var HomepageRouter = function() {
	/**
	 * Processes the request string.
	 */
	this.route = function(requeststring){
		var pairs = [];
		var methodname='';
		if(requeststring.indexOf('?')!=-1){
			pts=requeststring.split("?");
			if(pts.length>1){
				methodname=pts[0];
				requeststring=pts[1];
			}
		}

		var keyvaluepairs = requeststring.split("&");
		for (var i = 0; i < keyvaluepairs.length; i++) {
			var keyvaluedivided = keyvaluepairs[i].split("=");	
			if (keyvaluedivided.length === 2) {
				var name  = keyvaluedivided[0];
				var value = keyvaluedivided[1];

				if (name.charAt(0) === "?") {
					name = name.substr(1, (name.length - 1));
				}

				pairs[name] = value;
			}
		}

		if ("action" in pairs) {
			if (pairs["action"] === "showwpsservice") {
				require(['wpslauncher', 'visjs'], function (wpslauncher, visjs) {					
					window.vis = visjs;
					callWPSAction("viewservice", pairs["id"]);
				});
				return;
			}
			if (pairs["action"] === "showtable" || pairs["action"] ==="managetable") {
				createDataTable( pairs["id"], null); 
			}
			if (pairs["action"] === "confirmuser" ) {
				alert('You was successfuly registered.');
			}
			if (pairs["action"] === "recovery" ) {
				$('#recovery_password_form').dialog({
					title: 'Password recovery',
					position: ["center", "center"],
					close: function( event, ui ) {
						
					},
					buttons: {
					
					},
					modal:true
				});
				var u=new gp_user();
				//alert('You can change password.');
				u.recovery_change_password($('#recovery_password_form'), pairs["username"], pairs["hash"] );				
			}
			if (pairs["action"] === "confirm" ) {
				activate_div($('.confreg a')[0], 'reg');			
			}

		}
		if('managetable'==methodname){
			createDataTable( pairs["id"], null);
		}
		if(requeststring in this.actions){
			$('.content_div').hide();
			adiv=$("a[href='"+requeststring+"']").parent('li');
			active=adiv.hasClass('active');
			$('.nav li.active').removeClass('active');
			if(!active){				
				adiv.addClass('active');
				if(typeof this.actions[requeststring]==='function')
					this.actions[requeststring]();
				else
					this.actions[requeststring].show();
			}
		}
		if(requeststring.indexOf('/content')==0 || requeststring==''){
			content_id=requeststring.substring('/content'.length + 1);
			if(content_id=='')
				return;
			if(content_id!=''){			
				$('.content_div').hide();
				adiv=$("a[href='"+requeststring+"']").parent('li');
				active=adiv.hasClass('active');
				$('.nav li.active').removeClass('active');
				if(!active){				
					adiv.addClass('active');
					$.ajax({
						url: '/dataset/list?f=6&iDisplayStart=0&iDisplayLength=100&f_id='+content_id,
						dataType: "json",									
						success: function(data) {
							content=$('#content_body').show();
							$('h2',content).html(data.aaData[0].title);
							$('.content_body',content).html(data.aaData[0].body);
						}		
					});
				}				
			}
		}
	};
	
	this.actions={};
	this.register_action=function(link, div){		
		this.actions[link]=div;
	}

} //end HomepageRouter()

router = new HomepageRouter();
var init_page_page_specific = function () {

	router.route(window.location.pathname.substring(1)+window.location.search);
}