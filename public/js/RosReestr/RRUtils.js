var PortalObjectTypes = {
	cadastreOkrug: 15,
  	cadastreRayon: 9,
  	cadastreKvartal: 5,
  	parcel: 2,
  	oks: 0,
  	point: 10000,
  	zone:1000,
  	subject:100,
  	mo1Level:101,
  	mo2Level:102,
  	settlement:103,
  	street:104,
  	building:105,
  	addressLocator: 300
};

getUrlByObjectType = function(ObjectType){
	switch (ObjectType) {
  		case PortalObjectTypes.cadastreOkrug:
    		return LAYERS_URL.cadastreOkrug;
  		case PortalObjectTypes.cadastreRayon:
    		return LAYERS_URL.cadastreRayon;
		case PortalObjectTypes.cadastreKvartal:
    		return LAYERS_URL.cadastreKvartal;
  		case PortalObjectTypes.parcel:
    		return LAYERS_URL.parcel;
  		case PortalObjectTypes.oks:
    		return LAYERS_URL.oks;
        case PortalObjectTypes.point:
            return '';
        case PortalObjectTypes.zone:
			return LAYERS_URL.zone;
        case PortalObjectTypes.subject:
			return LAYERS_URL.subject;
        case PortalObjectTypes.mo1Level:
			return LAYERS_URL.mo1Level;
        case PortalObjectTypes.mo2Level:
			return LAYERS_URL.mo2Level;
        case PortalObjectTypes.settlement:
			return LAYERS_URL.settlement;
        case PortalObjectTypes.addressLocator:
			return LAYERS_URL.addressLocator;   
    }  
}

function getCadastreObjectTypeById(id) {
    switch (id.length) {
        case 2:
            return PortalObjectTypes.cadastreOkrug;
        case 4:
            return PortalObjectTypes.cadastreRayon;
        case 11:
            return PortalObjectTypes.cadastreKvartal;
        case 16:
		case 18:
            return PortalObjectTypes.parcel;
		case 21:
            return PortalObjectTypes.oks;
    }
}