var _geometry = {'x':'0','y':'0','spatialReference':{'wkid':'102100'}}
var _mapExtent = {'xmin':'0','ymin':'0','xmax':'0','ymax':'0','spatialReference':{'wkid':'102100'}}
var _statusIco = '/css/images/final.gif';

function startUpdReq(){
	//d=document.getElementById('Globe');
	//d.style.visibility = 'visible';
}

function endUpdReq(){
	//d=document.getElementById('Globe');
	//d.style.visibility = 'hidden';
}

function getCrHmlHttp(){
	var request;
	var isIE8 = window.XDomainRequest ? true : false;
	if(isIE8){
    	request = new window.XDomainRequest();
	}else{
        request = new XMLHttpRequest();		
	}
	return request;
}

TParametrs = function(){
	this.f = 'f=json';
	this.url = LAYERS_URL.cadastreIdentify+'/identify?'+this.f;
	this.geometryType = 'geometryType=esriGeometryPoint';
	this.geom = 'geometry=';
	this.mapExt = 'mapExtent=';
	this.returnGeom = 'returnGeometry=';
	this.imageDisplay = 'imageDisplay=';
	this.toler = 'tolerance=';
	this.sr = 'sr=';
	this.layers = 'layers=';
	return this;
}

TParametrs.prototype.getUrl = function(){
	var s='';var sep='&';var comma=',';
	s+=this.url+sep+this.geometryType;
	if(this.geometry){s+=sep+this.geom+this.geometry.getJSON()};
	if(this.tolerance!=='undefined')(s+=sep+this.toler+this.tolerance);
	if(this.returnGeometry!=='undefined')(s+=sep+this.returnGeom+this.returnGeometry);
	if(this.mapExtent)(s+=sep+this.mapExt+this.mapExtent.getJSON());
	if(this.width&&this.height){s+=sep+this.imageDisplay+this.width+comma+this.height+comma+'96'}
	if(this.layerOption){s+=sep+this.layers+this.layerOption};
	if(this.wkid){s+=sep+this.sr+this.wkid};
	return s;
}

TSelPoint = function(point){
    this.x = point.x;
    this.y = point.y;
    return this;
}

TSelPoint.prototype.getJSON = function(){
	return JSON.stringify(this);
}

TMapExtent = function(LT,RB){
	this.xmin = LT.x;
	this.ymin = LT.y;
	this.xmax = RB.x;
	this.ymax = RB.y;
	this.spatialReference = new Object();
	this.spatialReference.wkid = 102100;
	return this;
}

TMapExtent.prototype.getJSON = function(){
	return JSON.stringify(this);
}

TParcelParams = function(cadNums){
	this.url = LAYERS_URL.parcel+'?cadNums=';
	this.endOfUrl = '&onlyAttributes=false&returnGeometry=true&f=json';
	this.cadNums ='';
	if(cadNums!=='undefined')this.cadNums = cadNums;
}

TParcelParams.prototype.getUrl = function(){
	return this.url + this.cadNums + this.endOfUrl;
}

function identifyObject(clikedPoint) {
    var parameters = new TParametrs();
    var Ext = map.getBounds().extend();
    var LT = L.CRS.EPSG900913.project(Ext.getNorthWest());
    var RB = L.CRS.EPSG900913.project(Ext.getSouthEast());
    var p = L.CRS.EPSG900913.project(clikedPoint);
    var geometry = new TSelPoint(p);
    var mapExtent = new TMapExtent(LT,RB);
    var sz = map.getSize();
    parameters.tolerance = 0;
    parameters.returnGeometry = false;
    parameters.layerOption = 'top';
    parameters.wkid = 102100;
    parameters.width = sz.x;
    parameters.height = sz.y;
    parameters.geometry = geometry;
    parameters.mapExtent = mapExtent;
    var reqUrl = parameters.getUrl();	
	var request = getCrHmlHttp();
	request.open('GET', reqUrl, true);
	request.onload = function() {
  		//if (request.status >= 200 && request.status < 400){
  			data = JSON.parse(request.responseText);
  			featureSet = data.results[0];
			var cadNumbers = '';
			var whereClause = '';
			whereClause = "'" + featureSet.value + "'";
			cadNumbers = featureSet.attributes['Кадастровый номер земельного участка'];
			if(cadNumbers===undefined)cadNumbers=featureSet.attributes['Строковый идентификатор ИПГУ'];
			if(cadNumbers===undefined)cadNumbers=featureSet.attributes['Кадастровый номер'];
  			var query;
  			if (data.results.length > 0) {
				var objectType;
				if (typeof featureSet.attributes["Тип ОКС"] !== "undefined") {
					objectType = PortalObjectTypes.oks;
				}else if(cadNumbers.split(':').length == 4) {
					objectType = PortalObjectTypes.parcel;
					query = new TParcelParams("['"+cadNumbers+"']");
	  				var X = featureSet.attributes['X центра'];
	  				X = X.replace(',','.');
					var Y = featureSet.attributes['Y центра'];
					Y = Y.replace(',','.');
					//var latlng = unproject(X,Y);
					var latlng = 1;
					if(latlng)searchParcelObject(query,latlng);
					return;
				}else{
					objectType = getCadastreObjectTypeById(featureSet.value);
				}
				if(cadNumbers!==undefined){
					var info = '<b>Кадастровый номер: </b>' + cadNumbers;
					$('#info_reestr').html(info);
					//ObjInfoWrite(info,2,false,'Информация из Росреестр');
					//TblContent.showtbl();
					endUpdReq();
				}
    		};
    	//}else{alert('Что то пошло не так. Попробуйте перезагрузить страницу.')}
		endUpdReq();
	};
	request.onerror = function() {
		endUpdReq();
		alert('Что то пошло не так. Попробуйте перезагрузить страницу.')
	};
	startUpdReq();
	request.send();
}

function searchParcelObject(query,latlng) {
	var request = getCrHmlHttp();
	request.open('GET', query.getUrl(), true);
	request.onload = function(event) {
		//alert('ok');
  		//if (request.status >= 200 && request.status < 400){
  			data = JSON.parse(request.responseText);
  			if (data.features.length > 0) {
  				var featureSet = data.features[0];
				showParcelInfo(featureSet, latlng);
    		};
    	//}else{alert('Что то пошло не так. Попробуйте перезагрузить страницу.')}
		endUpdReq();
	};
	request.onerror = function() {
		endUpdReq();
		alert('Что то пошло не так. Попробуйте перезагрузить страницу.')
	};	
	startUpdReq();
	request.send();
}

var infoMarker;

function showParcelInfo(featureSet, latlng){
	//if(infoMarker)map.removeLayer(infoMarker);
	var info='';
	var NL = '<br/>';
	info += '<b>Кадастровый номер:</b> ' + featureSet.attributes['CAD_NUM'] + NL;
	info += '<b>Адрес:</b> ' + featureSet.attributes['OBJECT_ADDRESS'] + NL;
	info += '<b>Оценочная стоимость:</b> ' + featureSet.attributes['CAD_COST']+ ' р.' + NL;
	info += '<b>Площадь:</b> ' + featureSet.attributes['AREA_VALUE'] + ' кв. м.' + NL;
	info += '<b>Назначение:</b> ' + featureSet.attributes['UTIL_BY_DOC'] + NL;
	$('#info_reestr_detail').html(info);
	//ObjInfoWrite(info,2,false,'Информация из РосРеестра');
	//TblContent.showtbl();
	//infoMarker = new L.marker(latlng).addTo(map);
	//infoMarker.bindPopup(info).openPopup();
}