
L.Control.MyLayers = L.Control.extend({
	options: {
		collapsed: true,
		position: 'topright',
		autoZIndex: false
	},

	initialize: function (baseLayers, overlays, options) {
		L.setOptions(this, options);

		this._layers = {};
		this._lastZIndex = 0;
		this._handlingClick = true;

		for (var i in baseLayers) {
			this._addLayer(baseLayers[i], i);
		}

		for (i in overlays) {
			this._addLayer(overlays[i], i, true);
		}
	},

	onAdd: function (map) {
		this._initLayout();
		this._update();

		map
		    .on('layeradd', this._onLayerChange, this)
		    .on('layerremove', this._onLayerChange, this);

		return this._container;
	},

	onRemove: function (map) {
		map
		    .off('layeradd', this._onLayerChange)
		    .off('layerremove', this._onLayerChange);
	},

	addBaseLayer: function (layer, name) {
		this._addLayer(layer, name);
		this._update();
		return this;
	},

	addOverlay: function (layer, name) {
		this._addLayer(layer, name, true);
		this._update();
		return this;
	},

	removeLayer: function (layer) {
		var id = L.stamp(layer);
		delete this._layers[id];
		this._update();
		return this;
	},

	_initLayout: function () {
		var className = 'leaflet-control-layers',
		    container = this._container = L.DomUtil.create('div', className);

		container.setAttribute('aria-haspopup', false);

		if (!L.Browser.touch) {
			L.DomEvent
				.disableClickPropagation(container)
				.disableScrollPropagation(container);
		} else {
			L.DomEvent.on(container, 'click', L.DomEvent.stopPropagation);
		}

		var form = this._form = L.DomUtil.create('form', className + '-list');
		var header = this._header =  L.DomUtil.create('div','tbl-header');
		form.style.maxHeight = '95%';
		//form.style.overflow = 'auto';
		form.style.overflowX = 'hidden';
		if (this.options.collapsed) {
			/*if (!L.Browser.android) {
				L.DomEvent
				    .on(container, 'mouseover', this._expand, this)
				    .on(container, 'mouseout', this._collapse, this);
			}*/
			var link = this._layersLink = L.DomUtil.create('a', className + '-toggle', container);
			link.href = '#';
			link.title = 'Слои';
			//link.style.border = 'solid';

			//if (L.Browser.touch) {
				L.DomEvent
				    .on(link, 'click', L.DomEvent.stop)
				    .on(link, 'click', this._expand, this)
			//}
			//else {
			//L.DomEvent.on(link, 'focus', this._expand, this);
			//}
			L.DomEvent.on(form, 'click', function () {
				setTimeout(L.bind(this._onInputClick, this), 0);
			}, this);

			this._map.on('click', this._collapse, this);
		} else {
			this._expand();
		}
		
		var maps = L.DomUtil.create('label','');
		maps.innerHTML = '<b>Легенда</b>';
		form.appendChild(maps);
		
		this._loadLayerList = L.DomUtil.create('div', className + '-loadbase', form);
		this._baseLayersList = L.DomUtil.create('div', className + '-base', form);
		this._separator = L.DomUtil.create('div', className + '-separator', form);
		this._overlaysList = L.DomUtil.create('div', className + '-overlays', form);
		
		

		var tTab = L.DomUtil.create('div','');
		
//<a class="btn" href="#" id="btnLayerAdd"><i class="icon-plus-sign"></i></a>
		var userDataString = $.cookie('userData') || localStorage.user || '{}';
		var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));	
		
		if (user.username){
			var ls = L.DomUtil.create('label','');
			ls.innerHTML = '<b>Layers(Слои) </b> <button id="btnLayerAdd" class="btn btn-small" type="button"><i class="fas fa-layer-group"></i></button>';
			form.appendChild(ls);
		}
		this._commandList = L.DomUtil.create('div', className + '-overlays', form);

//		var br = L.DomUtil.create('div','row-fluid');
		//br.innerHTML = '<div class="span1"><button id="btnLayerAdd" class="btn btn-small" type="button"><i class="icon-plus-sign"></i></button></div> ';
		//br = $('<div class="span1"><button id="btnLayerAdd" class="btn btn-small" type="button"><i class="icon-plus-sign"></i></button></div> ');
		//$(this._commandList).append(br);
		/*
		var ltree = L.DomUtil.create('div','');
		ltree.id = 'layer_tree';
		ltree.style.background = '#FFF';*/
		var closeBtn = this._closeBtn = L.DomUtil.create('a', 'ms-dlgCloseBtn');
		closeBtn.id = 'closeLayersButton';
		closeBtn.style.float = 'right';
		closeBtn.style.clear = 'both';
		var spanClose = this._sclose = L.DomUtil.create('span', 's4-clust sbox-close-btn');
		var Img = this._img = L.DomUtil.create('div', 'close box-close-btn'); 
		Img.innerHTML = '<a href="#" id="close_btn" role="button"><span class="ui-icon ui-icon-closethick">close</span></a>';
		spanClose.appendChild(Img);
		closeBtn.appendChild(spanClose);
		header.style.display = 'none';		
		header.appendChild(closeBtn);
		container.appendChild(header);	
		//L.DomEvent.disableClickPropagation(ltree);		
		//form.appendChild(ltree);
		var divLayers = this._sclose = L.DomUtil.create('div', '');
		divLayers.id = 'layer_list';			
		form.appendChild(divLayers);
		
		var tTab = L.DomUtil.create('div','');
		tTab.id = 'table_list';		
		/*var tGisContainer = L.DomUtil.create('ul','Container');
		tGisContainer.id = 'table_list';
		tTab.appendChild(tGisContainer);*/
/*		var tTabBr = L.DomUtil.create('label');
		tTabBr.innerHTML = '<br/><b>Open tables</b>';
		form.appendChild(tTabBr);
		form.appendChild(tTab);
*/		
		container.appendChild(form);
		
		L.DomEvent
			.on(closeBtn, 'click', this._collapse, this);
			
		L.DomEvent.disableClickPropagation(container);	
		return container;
	},

	_addLayer: function (layer, name, overlay) {
		var id = L.stamp(layer);

		this._layers[id] = {
			layer: layer,
			name: name,
			overlay: overlay
		};

		if (this.options.autoZIndex && layer.setZIndex) {
			this._lastZIndex++;
			layer.setZIndex(this._lastZIndex);
		}
	},

	_update: function () {
		if (!this._container) {
			return;
		}

		this._baseLayersList.innerHTML = '';
		this._overlaysList.innerHTML = '';

		var baseLayersPresent = false,
		    overlaysPresent = false,
		    i, obj;

		for (i in this._layers) {
			obj = this._layers[i];
			this._addItem(obj);
			overlaysPresent = overlaysPresent || obj.overlay;
			baseLayersPresent = baseLayersPresent || !obj.overlay;
		}

		this._separator.style.display = overlaysPresent && baseLayersPresent ? '' : 'none';
	},

	_onLayerChange: function (e) {
		var obj = this._layers[L.stamp(e.layer)];

		if (!obj) { return; }

		if (!this._handlingClick) {
			this._update();
		}

		var type = obj.overlay ?
			(e.type === 'layeradd' ? 'overlayadd' : 'overlayremove') :
			(e.type === 'layeradd' ? 'baselayerchange' : null);

		if (type) {
			this._map.fire(type, obj);
		}
	},

	// IE7 bugs out if you create a radio dynamically, so you have to do it this hacky way (see http://bit.ly/PqYLBe)
	_createRadioElement: function (name, checked) {

		var radioHtml = '<input type="radio" class="leaflet-control-layers-selector" name="' + name + '"';
		if (checked) {
			radioHtml += ' checked="checked"';
		}
		radioHtml += '/>';

		var radioFragment = document.createElement('div');
		radioFragment.innerHTML = radioHtml;

		return radioFragment.firstChild;
	},

	_addItem: function (obj) {
		var label = document.createElement('label'),
		    input,
		    checked = this._map.hasLayer(obj.layer);

		if (obj.overlay) {
			input = document.createElement('input');
			input.type = 'checkbox';
			input.className = 'leaflet-control-layers-selector';
			input.defaultChecked = checked;
			label.className='checkbox';
		} else {
			input = this._createRadioElement('leaflet-base-layers', checked);
			label.className='radio';
		}

		input.layerId = L.stamp(obj.layer);

		L.DomEvent.on(input, 'click', this._onInputClick, this);

		//var name = document.createElement('span');
		//name.innerHTML = ' ' + obj.name;

		label.appendChild(input);
		
		//name=document.createTextNode(obj.name);
		//label.innerHTML = label.innerHTML + obj.name;
		txt=document.createTextNode(obj.name);
		label.appendChild(txt);

		var container = obj.overlay ? this._overlaysList : this._baseLayersList;
		container.appendChild(label);

		return label;
	},
	
	_addLoadItem: function (sname, checked, overlay, callback) {
		var ctrl=this;
		if (ctrl.loadlist===undefined){
			ctrl.loadlist=[];
		}
		
		var label = document.createElement('label'),
		    input, layer;
		    //checked = this._map.hasLayer(obj.layer);

		if (overlay) {
			input = document.createElement('input');
			input.type = 'checkbox';
			input.className = 'leaflet-control-layers-selector';
			input.defaultChecked = checked;
			
		} else {
			input = this._createRadioElement('leaflet-base-layers', checked);
			label.className='radio';
			input.id=sname;
		}

		//input.layerId = L.stamp(obj.layer);
		function view_layer(l2){
			for (i = 0; i < ctrl.loadlist.length; i++) {				
				if (ctrl.loadlist[i]!=l2 && ctrl._map.hasLayer(ctrl.loadlist[i])) {
					ctrl._map.removeLayer(ctrl.loadlist[i]);
				}
			}
			if(l2)
				ctrl._map.addLayer(l2);
		}
		
		if(checked){
			callback(function(l){
				ctrl.loadlist.push(l);
				layer=l;
				if (input.checked) {
					view_layer(layer);
				}
			});
		}


		L.DomEvent.on(input, 'click', function(){
			if(layer){
				if (input.checked) {
					view_layer(layer);
				}
				else if(ctrl._map.hasLayer(layer)){
					ctrl._map.removeLayer(layer);
				}
			}
			else{
				callback(function(l){
					ctrl.loadlist.push(l);
					layer=l;
					if (input.checked) {
						view_layer(layer);
					}
				});
			}
		}, this);

		//var name = document.createElement('span');
		//name.innerHTML = ' ' + sname;		
		//label.innerHTML = label.innerHTML + sname;
		label.appendChild(input);
		txt=document.createTextNode(sname);
		label.appendChild(txt);

		
		//name=document.createTextNode(sname);
		//label.appendChild(name);
		
		var container = overlay ? this._overlaysList : this._loadLayerList;		
		container.appendChild(label);

		return label;
	},

	_onInputClick: function () {
		var i, input, obj,
		    inputs = this._form.getElementsByTagName('input'),
		    inputsLen = inputs.length;

		this._handlingClick = true;

		for (i = 0; i < inputsLen; i++) {
			input = inputs[i];
			obj = this._layers[input.layerId];
			
			if(obj){
				if (input.checked && !this._map.hasLayer(obj.layer)) {
					this._map.addLayer(obj.layer);

				} else if (!input.checked && this._map.hasLayer(obj.layer)) {
					this._map.removeLayer(obj.layer);
				}
			}
		}

		this._handlingClick = false;

		//this._refocusOnMap();
	},

	_expand: function () {
		L.DomUtil.addClass(this._container, 'leaflet-control-layers-expanded');
		//L.DomUtil.addClass(this._closeBtn, 'sbox-close-btn-expanded');
		this._header.style.display = 'block';
	},

	_collapse: function () {
		this._container.className = this._container.className.replace(' leaflet-control-layers-expanded', '');
		this._header.style.display = 'none';
	}
});

L.control.mlayers = function (baseLayers, overlays, options) {
	return new L.Control.MyLayers(baseLayers, overlays, options);
};