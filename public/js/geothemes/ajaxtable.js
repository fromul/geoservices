function ajaxtable(container, doc_frm, sAjaxSource){
	var dt_table=this;
	this.rowcount=0;
	this.row_on_page_count=10;
	this.curent_page=0;
	this.columncount=5;
	this.aSelected=[];
	
	this.sort=[];
	
	var output = '<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="' + doc_frm.doc_description.tablename + '" style="width: 100%"=><thead><tr id = "dt_olap" style="display: none"></tr><tr id = "dt_fltr" style="vertical-align: top;display: none"></tr><tr>';	
	for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
		if(doc_frm.doc_description.columns[i].visible===undefined || doc_frm.doc_description.columns[i].visible){
			var width='70px';
			if(doc_frm.doc_description.columns[i].widget_object.getDatatableProperties().sWidth!=undefined)
				width=doc_frm.doc_description.columns[i].widget_object.getDatatableProperties().sWidth;		
			output += '<th style="overflow:  hidden; width: '+width+'; max-height:23px;" class="ui-state-default" fieldname="' + doc_frm.doc_description.columns[i].fieldname+'">' + doc_frm.doc_description.columns[i].title + '<span class="DataTables_sort_icon css_right ui-icon ui-icon-carat-2-n-s"></span></th>';
		}
	}
	
	output += '</tr></thead><tfoot><tr></tr></tfoot><tbody id="tbody"></tbody></table><div class="dataTables_processing" style="visibility: hidden;">Подождите...</div>';	
	container.append(output);
	
	$('.DataTables_sort_icon', container).click(function(){
		icon=$(this);
		th=icon.parent('th');
		var fieldname=th.attr('fieldname');
		if(fieldname===undefined || fieldname=='')
			return;
		var found=false;
		for(var i=0; i< dt_table.sort.length; i++){
			if(dt_table.sort[i].fieldname==fieldname){
				found=true;
				if( dt_table.sort[i].dir){
					dt_table.sort[i].dir=false;
					icon.removeClass('ui-icon-triangle-1-s');
					icon.addClass('ui-icon-triangle-1-n');
				}
				else{
					dt_table.sort.splice(i, 1);
					icon.removeClass('ui-icon-triangle-1-n');
					icon.addClass('ui-icon-carat-2-n-s');
				}
				break;
			}
		}
		if(!found){
			f={fieldname:fieldname, dir:true};
			dt_table.sort.push(f);
			icon.addClass('ui-icon-triangle-1-s');
		}
		dt_table._fnReDraw();
	});
	
	this._fnReDraw = function(){
		iDisplayStart=this.curent_page * this.row_on_page_count;
		$('.dataTables_processing', container ).show();
		
		str_res=JSON.stringify({sort:dt_table.sort});
		
		
		$.ajax({
			url: sAjaxSource+'&iDisplayStart=' + iDisplayStart + '&iDisplayLength=' + this.row_on_page_count,
			dataType: "json",	
			contentType : 'application/json',
			data: str_res,
			method: "POST",
			type: "POST",
			success: function(data) {
				$('.dataTables_processing', container ).hide();
				var tbody=$('#tbody', container);
				tbody.empty();
				for (var row_id = 0; row_id < data.aaData.length; row_id++) {
					var trow=$('<tr></tr>');
					tbody.append(trow);
					var aData=data.aaData[row_id];
					trow.data('doc',aData);
					for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
						if(doc_frm.doc_description.columns[i].visible===undefined || doc_frm.doc_description.columns[i].visible){
							var vv='';
							if(doc_frm.doc_description.columns[i].fieldname in aData)
								vv=aData[doc_frm.doc_description.columns[i].fieldname];
							doc_frm.doc_description.columns[i].widget_object.cntr=aData.id;
							element = doc_frm.doc_description.columns[i].widget_object.getUserVal(vv);
				
							
							var ctd=$('<td/>');
							trow.append(ctd);
							ctd.html('<div style="overflow:  hidden; max-height:23px;">'+element+'</div>');
							vv=JSON.stringify(vv);
							ctd.attr('title',removeescapeHtml(element));
							ctd.attr('fieldname',doc_frm.doc_description.columns[i].fieldname);				
							ctd.dblclick( function (event) {
								editcell(dt_table, doc_frm.doc_description, aData.id, $(event.target));
							});
						}				
					}
				}
				dt_table.rowcount=data.iTotalRecords;
				dt_table.update_pagination();
				
				
				$("tr", tbody).click(function(e){ //live("click", function (e) {
					var Row=$(this);
					var id = this.id;
					
					var index = jQuery.inArray(id, dt_table.aSelected);
					if(e.ctrlKey){
						if (index === -1) {
							dt_table.aSelected.push(id);
						} else {
							if(Row.hasClass('geos_row_selected')){
								Row.removeClass('geos_row_selected');
							}
							dt_table.aSelected.splice(index, 1);						
						}							
						Row.toggleClass("geos_row_selected");
					}
					else{
						dt_table.aSelected=[];
						dt_table.aSelected.push(id);
						$('.geos_row_selected',container).removeClass("geos_row_selected");
						Row.toggleClass("geos_row_selected");
					}
					
					$('.g_layer',$('#layer_list')).each(function(index){
						ml=$(this).data("mlayer");
						ml.view_selection(dt_table.aSelected);					
					});
					setTimeout( function() {
						dt_table.updateButtons();
					}, 150 );						

				});
			}
		});
	};
	
	var pagination=$('<div class="fg-toolbar ui-toolbar ui-widget-header ui-corner-bl ui-corner-br ui-helper-clearfix">\
<div class="dataTables_paginate fg-buttonset ui-buttonset fg-buttonset-multi ui-buttonset-multi paging_full_numbers" id="gis_admin.a_scheda_paginate">\
<a page="first" tabindex="0" class="first ui-corner-tl ui-corner-bl fg-button ui-button ui-state-default ui-state-disabled" id="gis_admin.a_scheda_first">Первая</a>\
<a page="prev"  tabindex="0" class="previous fg-button ui-button ui-state-default ui-state-disabled" id="gis_admin.a_scheda_previous">&lt;</a>\
<span id="table_pages">\
</span>\
<a page="next" tabindex="0" class="next fg-button ui-button ui-state-default" id="gis_admin.a_scheda_next">&gt;</a>\
<a page="last" tabindex="0" class="last ui-corner-tr ui-corner-br fg-button ui-button ui-state-default" id="gis_admin.a_scheda_last">Последняя</a>\
<label class="table_info" id="page_str"></label>\
</div>\
</div>');

	container.append(pagination);
	$('.fg-button', container).click(function(){
		item=$(this);
		update_pagination_list(item);
	});
	
	function update_pagination_list(item){			
			v=item.attr('page');
			old=dt_table.curent_page;
			switch (v) {
			   case 'first':
				  dt_table.curent_page=0;
				  break;
			   case 'last':
				  dt_table.curent_page=Math.trunc(dt_table.rowcount / dt_table.row_on_page_count);
				  break;			   
			   case 'prev':
				  if(dt_table.curent_page==0)
					  return;
				  dt_table.curent_page--;
				  break;
			   case 'next':
				  if(dt_table.curent_page >=Math.trunc(dt_table.rowcount / dt_table.row_on_page_count))
					  return;			   
				  dt_table.curent_page++;
				  break;
			   default:				
				  dt_table.curent_page=parseInt(v);
				  break;
			}
			if(old==dt_table.curent_page)
				return;
			dt_table._fnReDraw();
		}
	
	this.update_pagination = function(){
		var table_pages=$('#table_pages', container);
		table_pages.empty();		
		var start_page=Math.max(0, this.curent_page - 2);
		var end_page=Math.min( Math.trunc(this.rowcount / this.row_on_page_count) , Math.max(4, this.curent_page + 2));
		for(var i = start_page ; i <= end_page; i++){
			var p;
			s_i=(i+1)+'';
			if(this.curent_page==i)
				p=$('<a page="' + i + '" tabindex="0" class="fg-button ui-button ui-state-default ui-state-disabled">' + s_i + '</a>');
			else
				p=$('<a page="' + i + '"  tabindex="0" class="fg-button ui-button ui-state-default">' + s_i + '</a>');
			table_pages.append(p);
		}
		$('a.fg-button', container).click( function(){
			item=$(this);
			update_pagination_list(item);
		});
		
		if(this.curent_page==0){
			$("a[page='first']", container).addClass('ui-state-disabled');
			$("a[page='prev']", container).addClass('ui-state-disabled');
		}
		else{
			$("a[page='first']", container).removeClass('ui-state-disabled');
			$("a[page='prev']", container).removeClass('ui-state-disabled');			
		}
		if(this.curent_page==Math.trunc(this.rowcount / this.row_on_page_count)){
			$("a[page='last']", container).addClass('ui-state-disabled');
			$("a[page='next']", container).addClass('ui-state-disabled');
		}
		else{
			$("a[page='last']", container).removeClass('ui-state-disabled');
			$("a[page='next']", container).removeClass('ui-state-disabled');			
		}
		iDisplayStart=this.curent_page * this.row_on_page_count+1;
		iDisplayEnd=Math.min(iDisplayStart + this.row_on_page_count-1, this.rowcount);
		$("#page_str", container).html('Записи с '+iDisplayStart+' до ' + iDisplayEnd + ' из ' + this.rowcount + ' записей');
	};
	
	
	this._fnReDraw();


}