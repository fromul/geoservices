function drawTable(obj) {
	var objjson = JSON.parse(getRemote('/geothemes/getdbjsonbyid?id=1000'));
	var container = $('#' + obj.nodeid);
	var metaid = 1000;
	var	ECOLUMNS = new Array(), EDITCOLUMNS = new Array(), WCOLUMNS = new Array(), aSelected = new Array(), widgets = new Array(), tablewidgets = new Array();
	for (var i = 0; i < objjson.columns.length; i++) {
		widget = $.widgets.get_widget(objjson.columns[i]);				
		tempobj = widget.getDatatableProperties();
		if ((objjson.columns[i].pk == 'true') || (objjson.columns[i].fieldname == 'id')){
			console.log('PK detected');
		} else {
			if (objjson.columns[i].readonly != 'true') {
				EDITCOLUMNS.push(tempobj);
			} else {
				EDITCOLUMNS.push(null);
			}
			widgets.push(widget);
			ECOLUMNS.push(widget);
			WCOLUMNS.push(tempobj);
			tablewidgets.push(widget);
		}
	}
	
	var unique = randomString(10);
	var output = '<table cellpadding="0" cellspacing="0" border="0" class="datatable_wide" id="example"><thead><tr>';
	for (var i = 0; i < WCOLUMNS.length; i++){
		output += '<th></th>';
	}
	output += '</tr></thead><tfoot><tr></tr></tfoot><tbody></tbody></table>';	
	container.append(output);
	if (jQuery.ctables === undefined){
		var ctables = new Array();
		jQuery.ctables = ctables;
	}
	var tab = {div: '#example', widgets: tablewidgets};
	jQuery.ctables.push(tab);
	ThemeDT = $('#example').dataTable({
		"bJQueryUI": true,
		"oTableTools": { "sSwfPath": "media/swf/copy_csv_xls_pdf.swf" },		
		"sDom": 'A<"clear"><"H"lf>tr<"F"ip>',
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "/geothemes/bldata?mode=load&f="+metaid+'&unique='+unique,
		"fnRowCallback": function(nRow, aData, iDisplayIndex) {
			$('#example').attr('style' , '');
			element = null;
			$(nRow).live("click", function () {
							var id = this.id;
							var index = jQuery.inArray(id, aSelected);
							if (index === -1) {
								aSelected.push(id);
							} else {
								aSelected.splice(index, 1);
							}
							$(nRow).toggleClass("geos_row_selected");
			});
			for (var i = 1; i < aData.length; i++) {
				widgets[i - 1].cntr = aData[0];
				element = widgets[i - 1].getUserVal(aData[i]);
				$('td:eq(' + (i - 1) + ')', nRow).html(element);
				$('td:eq(' + (i - 1) + ')', nRow).attr('dbval',aData[i]);
			}
		},
		"sPaginationType": "full_numbers",
		"aoColumns": WCOLUMNS,		
			"oLanguage": {
				"sProcessing":   "Подождите...",
				"sLengthMenu":   "Показать _MENU_ записей",
				"sZeroRecords":  "Записи отсутствуют.",
				"sInfo":         "Записи с _START_ до _END_ из _TOTAL_ записей",
				"sInfoEmpty":    "Записи с 0 до 0 из 0 записей",
				"sInfoFiltered": "(отфильтровано из _MAX_ записей)",
				"sInfoPostFix":  "",
				"sSearch":       "Поиск:",
				"sUrl":          "",
				"oPaginate": {
					"sFirst": "Первая",
					"sPrevious": "<",
					"sNext": ">",
					"sLast": "Последняя"
				}
			},
			"oFilterWidgets": {
				aiExclude: ECOLUMNS,
				sSeparator: ", ",
				bGroupTerms: false,
					AutocompleteSource: "/geothemes/bldata?mode=auto&f=" + metaid+'&unique='+unique
				},
			 }).makeEditable({
				sUpdateURL: "/geothemes/bldata?mode=update&f=" + metaid+'&unique='+unique,
				sDeleteURL: "/geothemes/bldata?mode=delete&f=" + metaid+'&unique='+unique,
				aoColumns: EDITCOLUMNS,
				fnOnDeleted: function() {
					ThemeDT._fnReDraw();
                },
				fnOnAdded: function() {
					ThemeDT._fnReDraw();
                },
			});

			$("#acc-filters").accordion({
				collapsible: true,
				active: false
			});
			$('[for="f_actions"]').remove();
			$('[id="f_actions"]').remove();
			
			$("#example tbody tr").live("hover", function () {
				var id = this.id;
				$(this).attr('title', 'ID: ' + id);
			});

			$(document).ready(function () {    
				$('select').live('change', function () {
					if ($(this).attr('id') == 'changeme') {
						$(this).parent().submit();
					}
				});
			});
}