function showdiagrams(dt_table){
	require(["d3", "c3"], function (d3, c3) {
		/**
		 * Форма настройки диаграммы
		 */
		var diagramSettingsStr = '<div><form id="diagram-settings" class="form-horizontal">\
			<div class="control-group">\
				<label class="title_label" for="abscissa">Абсцисса</label>\
				<div id="abscissa" class="label_control">\
				</div>\
			</div>\
			<div class="ordinate-list">\
			</div>\
			<a class="btn" href="#" id="addOrdinate" title="Add ordinate"><i class="icon-plus-sign"></i></a>\
			</form>\
			<div id="chartC3"></div></div>\
		';
		
		$diagramSettings = $(diagramSettingsStr);
		var options = [];

		for (var i = 0; i < dt_table.doc_frm.doc_description.columns.length; i++) {
			if (dt_table.doc_frm.doc_description.columns[i].title !== '') {
				options.push(dt_table.doc_frm.doc_description.columns[i].fieldname + ':' + dt_table.doc_frm.doc_description.columns[i].title);
			}
		}

		var metaAbscissa = {
			"fieldname": "abscissa",
			"title": "Абсцисса",
			"description": "Ось абсцисс",
			"visible": true,
			"widget": {
				"name": "select",
				properties: {
					options: options.join(';')
				}
			}
		};

		var metaOrdinate = {
			"fieldname": "ordinate",
			"title": "Ордината",
			"description": "Ось ординат",
			"visible": true,
			"widget": {
				"name": "select",
				properties: {
					options: options.join(';')
				}
			}
		};
		// var user_widget=$.widgets.get_widget_byname('user');
		// debugger;
		var widgetAbscissa = $.widgets.get_widget(metaAbscissa);
		widgetAbscissa.assign($('#abscissa', $diagramSettings), '');

		var widgetOrdinateList = [];
		// var widgetOrdinate = $.widgets.get_widget(metaOrdinate);
		// widgetOrdinate.assign($('.ordinate', $diagramSettings), '');

		$('#addOrdinate', $diagramSettings).click(function () {
			var wrapper = $('\
				<div class="control-group">\
					<label class="title_label" for="ordinate">Ордината</label>\
					<div class="label_control">\
				</div>\
			');
			$('.ordinate-list', $diagramSettings).append(wrapper);
			var tempOrdinate = $.widgets.get_widget(metaOrdinate);					
			tempOrdinate.assign($('.label_control',wrapper), '');	
			widgetOrdinateList.push(tempOrdinate);					
		}).trigger('click');

		$diagramSettings.dialog({
			minWidth: 900,
			minHeight: 500,
			Height: 500,
			title: 'Data diagram',
			position: ["center", "center"],
			close: function (event, ui) {
				$diagramSettings.remove();
			},
			buttons: {
				"Draw": function () {
					var xFieldName = widgetAbscissa.getVal();
					var xFieldTitle = widgetAbscissa.getUserVal(xFieldName);

					var fieldNameList = [];
					widgetOrdinateList.forEach(function(widget) {
						fieldNameList.push(widget.getVal());
					});

					// var yFieldName = widgetOrdinate.getVal();
					// var yFieldTitle = widgetAbscissa.getUserVal(yFieldName);
					// xTitle = ''; сюда нужно сохрнаить название по рурсски из метаинформации


					str_res = JSON.stringify(dt_table.dataquery);
					$.ajax({
						url: dt_table.sAjaxSource + '&count_rows=1&iDisplayStart=' + iDisplayStart + '&s_fields=' + xFieldName + ',' + fieldNameList.join(','),
						// url: sAjaxSource + '&count_rows=1&iDisplayStart=' + iDisplayStart + '&s_fields=' + xFieldName + ',' + yFieldName,
						dataType: "json",
						contentType: 'application/json',
						data: str_res,
						method: "POST",
						type: "POST",
						success: function (data) {

							if (data.aaData) {
								// debugger;
								var xData = [xFieldTitle];
								// var yData = [yFieldTitle];
								var columns = [xData];
								for (let i = 0; i < fieldNameList.length; i++) {
									var tempColumn=[widgetOrdinateList[i].getUserVal(fieldNameList[i])];											
									columns.push(tempColumn);
									//columns.push();
									
								}

								for (let i = 0; i < data.aaData.length; i++) {
									xData.push(data.aaData[i][xFieldName]);											
									// yData.push(data.aaData[i][yFieldName]);
									for (let j = 0; j < fieldNameList.length; j++) {
										columns[j + 1].push(data.aaData[i][fieldNameList[j]]);	
									}
									
								}

								// debugger;

								var chart = c3.generate({
									bindto: '#chartC3',
									data: {
										x:  xFieldTitle,
										columns: columns
									}
								});

							}
						}
					});


					// var chart = c3.generate({
					// 	bindto: '#chartC3',
					// 	data: {
					// 	  columns: [
					// 		['data', 30, 200, 100, 400, 150, 250]
					// 	  ]
					// 	}
					// });

				},
				"Cancel": function () {
					$diagramSettings.remove();
				}
			},
			modal: true
		});

		dt_table.updateButtons();
	});
}