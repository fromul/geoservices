﻿
function initForm(form_node, widgets, form_template) {
	for (var i = 0; i < widgets.length; i++) {
		widget=widgets[i];
		var form_element=$('#'+widget.object.fieldname, form_node);
		widget.assign(form_element);
	}
}