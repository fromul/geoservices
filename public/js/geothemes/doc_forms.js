


function logversion(){
	var N= navigator.appName, ua= navigator.userAgent, tem;
  	var M= ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
  	if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
  	M= M? [M[1], M[2]]: [N, navigator.appVersion,'-?'];
  	return M;
};

function serverlog(msg){
	
	res={v:logversion(), msg:msg}
	
	msg=JSON.stringify(res);
	var res={msg:msg};	
	str_res=JSON.stringify({document:res});

	$.ajax({
		type: "POST",
		url: "/dataset/add?f=2170",
		contentType : 'application/json',
		data: str_res,
		success: function(msg){
			console.log('ok');
		},
		error: function(){
			console.log('error');
		}
	});	
}

var comment_str='<div id="comments_list"></div>\
<a href="#" id="viewform">Add comment</a>\
<form class="form-horizontal comment_form">\
  <div class="control-group">\
    <label class="title_label" for="title">Title</label>\
    <div class="label_control">\
      <input type="text" class="title" placeholder="Title">\
    </div>\
  </div>\
  <div class="control-group">\
    <label class="title_label" for="">Text</label>\
    <div class="label_control">\
      <textarea  class="body" placeholder="Text"></textarea>\
    </div>\
  </div>\
  <div class="control-group">\
    <div class="title_label">      \
      <button  class="btn add_comment">Submit</button>\
    </div>\
  </div>\
</form>';



function loadcomments(metaid, row_id, list){
	list.empty();
	if(!row_id || !metaid)
		return;
	$.getJSON("/dataset/list?f=2154&iDisplayStart=0&iDisplayLength=100&f_datasetid=" + metaid+'&f_rowid='+ row_id, function(data) {			
		if(!data  || !data.aaData || data.aaData.length==0)
			return;
		var date_widget=$.widgets.get_widget_byname('date');
		var user_widget=$.widgets.get_widget_byname('user');
		
		for (var i = 0; i < data.aaData.length; i++) {				
				row_str = '<div class="comment" id="'+data.aaData[0].id+'"><div class="comment_user">' + user_widget.getUserVal(data.aaData[i].created_by)+'</div>';
				if(data.aaData[i])
					row_str+='<i title="Delete comment" id="delete_comment" class="commnet_close icon-remove"></i>';
				row_str += '<div class="i-comment-date">' + date_widget.getUserVal(data.aaData[i].created_on) + '</div>';
				row_str += '<p>' + data.aaData[i].title + '</p><p>' + data.aaData[i].body + '</p></div>';
				var row=$(row_str);				
				list.append(row);
				$('#delete_comment', row).click(function(){
					if(confirm("Delete the comment?")){						
						res={};						
						res.f_id=$(this).parents('.comment').attr('id');
						str_res=JSON.stringify({document:res});
						$.ajax({
							type: "POST",
							url: "/dataset/delete?f=2154",
							contentType : 'application/json',
							data: str_res,
							success: function(msg){
								if(msg=='ok'){
									loadcomments(metaid, row_id, list);
								}
								else{
									alert(msg);
								}
							}
						});
					}
				});
			}	
	});
}

function loadaddinfo(container, classname, master_id, askdataset_id, doc_id, view){
	var object_container=$('<div class="object_container"></div>');
	container.append(object_container);
	var _self=this;
	
	var btns=$('');
	container.append(btns);
	var doc_frmEdit;
	
	function AddItem(){		
		var AddDlg = $('<div class="image_form" title="Item">\
		</div>');
		if(doc_frmEdit){
			var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div><div class="btn-group"> <button class="btn add">Save</button>  <button class="btn cancel">Cancel</button> </div></div>');
			AddDlg.append(row);
			doc_frmEdit.show_form($('.fields',row), {}, '', false);
			btns.hide();
			$('.add',row).click(function(){
				var newobject=doc_frmEdit.save_form(function(res, msg){
					if (msg.status != 'ok') {
						var error_box = $('.text-error', container);
						if (error_box.length > 0) {
							error_box.html(msg);
						}
						else
							alert(msg);
						return;
					}
					else{
						btns.show();
						newobject.id=msg.data;
						newobject.add=true;
						newobject.action='add';
						_self.objectlist.push(newobject);
						drawobjects();
						row.remove();
						AddDlg.dialog("close");
						AddDlg.remove();
					}
						
				}, true);
			})	
			$('.cancel',row).click(function(){
				btns.show();
				row.remove();
				AddDlg.dialog("close");
				AddDlg.remove();
			})					
		}
		
		AddDlg.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {
			}
		});
				
	}
	
	function SelectItem(){		
		var AddDlg = $('<div class="image_form" title="Item">\
		</div>');

		var metadataset_select = {
			"fieldname": "row",
			"title": "Row select",
			"description": "",
			"visible": true,
			"widget": {
				"name": "theme_select",
				properties: {
					dataset_id: askdataset_id
				}
			}
		};
		
		var dataset_select = $.widgets.get_widget(metadataset_select);
		

		var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div></div>');
		AddDlg.append(row);
		dataset_select.assign($('.fields', row), '');	

		
		AddDlg.dialog({
			resizable: true,
			height: 'auto',
			width: 'auto',
			modal: false,
			buttons: {
				"Select": function () {	
					var row_id=dataset_select.getVal();
					if(row_id==''){
						alert('Select row.');
						return;
					}					
					newobject=dataset_select.doc;
					newobject.add=true;
					newobject.action='select';
					_self.objectlist.push(newobject);
					drawobjects();				
					$(this).dialog("close");
					AddDlg.remove();
				},
				"Cancel": function () {				
					$(this).dialog("close");
					AddDlg.remove();
				}
			}
		});
		
		

				
	}
	
	items = $('<ul class="thumbnails"/>');
	container.append(items);
	if(!view){
		pluscardvar=$('<li class="span2 addcard"> \
	<a href="#" class="thumbnail add"><i class="icon-plus"></i>Add</a>\
	<a href="#" class="thumbnail select"><i class="icon-hand-up"></i>Select</a>\
	</li>');
		items.append(pluscardvar);
		$('.add',pluscardvar).click(function(){
			AddItem();
		});
		$('.select',pluscardvar).click(function(){
			SelectItem();
		});
	}
		
	this.objectlist=[];
	function drawobjects(){
		object_container.empty()
		for (var i = 0; i < _self.objectlist.length; i++) {
			if(_self.objectlist[i].del === true)
				continue;
			if(!view){
				if(_self.objectlist[i].action=='add')
					var row=$('<div class="thumbnail"><i title="Edit" class="icon-pencil"></i><i title="Delete" class="icon-remove close"></i><div class="object_text"></div></div>');
				else
					var row=$('<div class="thumbnail"><i title="Delete" class="icon-remove close"></i><div class="object_text"></div></div>');
			}
			else
				var row=$('<div class="thumbnail"><div class="object_text"></div></div>');
			row.data('doc',_self.objectlist[i])
			object_container.append(row);
			doc_frmEdit.show_form($('.object_text',row), _self.objectlist[i], '', true);
		}
		$('.icon-remove', container).click(function(){
			if(confirm("Delete?")){						
				var doc=$(this).parents('.thumbnail').data('doc');
				res={};			
				res.f_id=doc.link_id;
				doc.del=true;
				drawobjects();
			}
		});
		$('.icon-pencil', container).click(function(){
			
			var doc=$(this).parents('.thumbnail').data('doc');
			var AddDlg = $('<div class="image_form" title="Item">\
			</div>');
			if(doc_frmEdit){
				var row=$('<div class="form'+askdataset_id+'"><div class="fields"></div><div class="btn-group"> <button class="btn add">Save</button>  <button class="btn cancel">Cancel</button> </div></div>');
				AddDlg.append(row);
				doc_frmEdit.show_form($('.fields',row), doc, '', false);
				btns.hide();
				$('.add',row).click(function(){
					var newobject=doc_frmEdit.save_form(function(res, msg){
						if (msg.status != 'ok') {
							var error_box = $('.text-error', container);
							if (error_box.length > 0) {
								error_box.html(msg);
							}
							else
								alert(msg);
							return;
						}
						else{
							
							for (var i = 0; i < _self.objectlist.length; i++) {
								if(_self.objectlist[i].id.toString() == newobject.f_id.toString()){
									_id=newobject.f_id;
									_self.objectlist[i]=newobject;
									_self.objectlist[i].id=_id;
									_self.objectlist[i].action='add';
									break;
								}
								
							}
							
							
							btns.show();							
							drawobjects();
							row.remove();
							AddDlg.dialog("close");
							AddDlg.remove();
						}
							
					}, true);
				})	
				$('.cancel',row).click(function(){
					btns.show();
					row.remove();
					AddDlg.dialog("close");
					AddDlg.remove();
				})					
			}
			
			AddDlg.dialog({
				resizable: true,
				height: 'auto',
				width: 'auto',
				modal: false,
				buttons: {
				}
			});
						
			
		});		
	}
	$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + askdataset_id, function (data) {
		if (data.aaData.length == 0)
			return;
		var table_json = JSON.parse(data.aaData[0].JSON);
		table_json.metaid = askdataset_id;
		doc_frmEdit = new doc_template(table_json);
		doc_frmEdit.init_form(function () {
			if(doc_id){
				$.getJSON("/dataset/list?f=6&iDisplayStart=0&iDisplayLength=100&f_classname=" + classname+'&f_docid='+ doc_id, function(data) {
					if(!data  || !data.aaData || data.aaData.length==0)
						return;
					var id_list='';
					for (var i = 0; i < data.aaData.length; i++) {
						if(data.aaData[i].rowid){
							id_list+=data.aaData[i].rowid+'-PropertyIsEqualTo;';
						}
					}
					
					$.getJSON("/dataset/list?f="+askdataset_id+"&iDisplayStart=0&iDisplayLength=100&f_id="+ id_list, function(refdata) {
						if(!refdata  || !refdata.aaData || refdata.aaData.length==0)
							return;
						
						for (var i = 0; i < refdata.aaData.length; i++) {				
							var row=$('<div class="object_card"><i title="Delete comment" class="icon-remove"></i></div>');
							for (var j = 0; j < data.aaData.length; j++) {
								if(data.aaData[j].rowid==refdata.aaData[i].id){
									refdata.aaData[i].link_id=data.aaData[j].id;
									refdata.aaData[i].action=data.aaData[j].action;
									break;
								}									
							}
						}
						_self.objectlist=refdata.aaData;
						drawobjects();
					});
				});
			}			
		});		
	});

	
	

	this.save=function(doc_id){
		var doclist=[];
		var dellist=[];
		for (var i = 0; i < this.objectlist.length; i++) {
			if(this.objectlist[i].add===true){
				var newobj={docid:doc_id, datasetid:master_id, classname:classname, rowid:this.objectlist[i].id, rowdatasetid:askdataset_id, action:'add'};
				doclist.push(newobj);
			}
			else if(this.objectlist[i].del===true){
				var delobj={f_id: this.objectlist[i].link_id};
				dellist.push(delobj);
			}
			//master_id, askdataset_id, doc_id			
		}			
		
		str_res=JSON.stringify({document:doclist});
		console.log(str_res);
		$.ajax({
			type: "POST",
			url: "/dataset/add?f=6",
			contentType : 'application/json',
			data: str_res,
			success: function(msg){
				console.log('Ok in loadaddinfo.');
			},
			error: function(){
				console.log('Error in loadaddinfo.');
			}
		});
		str_res=JSON.stringify({document:dellist});
		$.ajax({
			type: "POST",
			url: "/dataset/delete?f=6",
			contentType : 'application/json',
			data: str_res,
			success: function(msg){
				console.log('Ok in loadaddinfo.');
			},
			error: function(){
				console.log('Error in loadaddinfo.');
			}
		});
	}
	return this;
}

function doc_template(doc_description){	
	var geom_field_cnt=0;
	var geom_field_index=-1;
	this.doc_description=doc_description;
	this.form_template={};
	this.print_template={};
	this.view_template={};
	this.executing=false;
	var userDataString = $.cookie('userData') || localStorage.user || '{}';
	var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));
	this.addons=[];
	var _self=this;
	
	this.show_form=function(container, doc_data, form_name, view, rowtemplate){
		this.container=container;
		if(view && rowtemplate){
			row=$(rowtemplate);
			container.append(row);
			container=row;
			row.data('doc',doc_data);
		}
		else
			container.empty();
		if(doc_data===undefined)
			doc_data={};
		fieldcalc.calculatefields(doc_description, doc_data, 'default', undefined, function(error, chdoc){
			doc_data=chdoc;
			if(view && rowtemplate){
					
			}
			else if(view && doc_description.view_template){
				container.append($(_self.view_template.main));	
			}
			else{
				if(form_name!=undefined && form_name!='')
					container.append($(_self.form_template[form_name]));	
				else
					container.append($(_self.form_template.main));	
			}
			//console.log(getpath(doc_description.form_template))
			for (var i = 0; i < doc_description.columns.length; i++) {
				if(doc_description.columns[i].widget_object===undefined)
						continue;
				doc_description.columns[i].widget_object.doc=doc_data;
				if(doc_description.metaid==5 && doc_description.columns[i].fieldname=='isadmin' && (user.isAdmin===undefined || user.isAdmin!=true)){
					var widget_container=$('#'+doc_description.columns[i].fieldname, container);
					widget_container.hide();
					continue;
				}
					
				if(doc_description.columns[i].tablename!='main' && doc_description.columns[i].tablename!='' && doc_description.columns[i].tablename!=undefined)
						continue;
				var widget_container=$('#'+doc_description.columns[i].fieldname, container);
				var value='';
				if (doc_data!=undefined && doc_description.columns[i].fieldname in doc_data)
					value=doc_data[doc_description.columns[i].fieldname];
				if(view)
					widget_container.html(doc_description.columns[i].widget_object.getUserVal(value, view));
				else
					doc_description.columns[i].widget_object.assign(widget_container, value);
					
				if(doc_description.columns[i].fieldname=='id')
					$('input',widget_container).prop('disabled', true);
				if(doc_description.columns[i].widget_object.geometry_type!=undefined){
					geom_field_cnt++;
					geom_field_index=i;
				}
			}
			
			mapcont=$('#inplacemap', container);
			if(mapcont.length>0 && typeof map !=="undefined"){			
				_self.mapwidth = $('#map').width();
				_self.mapheight = $('#map').height();
			
				$('#map').appendTo(mapcont);
				setTimeout(function(){
					//$('#mapcontainer').show();
					
					$('#map').width("850px");					
					$('#map').height("300px");				
					map.invalidateSize(true);				
				});
				g_rect={max_x:-1000, min_x: 1000, max_y: -1000, min_y:1000};
				for (var i = 0; i < doc_description.columns.length; i++) {
					if(doc_description.columns[i].widget_object!=undefined && doc_description.columns[i].widget_object.getrect!=undefined){
						c_rect=doc_description.columns[i].widget_object.getrect(doc_data[doc_description.columns[i].fieldname]);	
						g_rect.min_x=Math.min(c_rect.min_x, g_rect.min_x);
						g_rect.max_x=Math.max(c_rect.max_x, g_rect.max_x);
						g_rect.min_y=Math.min(c_rect.min_y, g_rect.min_y);
						g_rect.max_y=Math.max(c_rect.max_y, g_rect.max_y);					
					}
				}
				if(g_rect.min_x!=1000){
					map.setView([(g_rect.min_y + g_rect.max_y)/2%90, (g_rect.min_x + g_rect.max_x)/2%180], 10/*map.getZoom()*/);
				}
			}
			if(view){
				var comments_container=$('#comments', container);
				comments_container.html(comment_str);		
				$('.add_comment',comments_container).click(function(){
					var res={};
					res.title = $('.title',comments_container).val();
					res.body = $('.body',comments_container).val();
					res.datasetid = doc_description.metaid;
					res.rowid = doc_data.id;
					if(res.id && res.id!='NULL'){
						method='update';
						res.f_id=res.id;				
					}
					else
						method='add';
					$('.title',comments_container).val('');
					$('.body',comments_container).val('');
					
					str_res=JSON.stringify({document:res});
					this.executing=true;
					$.ajax({
						type: "POST",
						url: "/dataset/"+method+"?f=2154",
						contentType : 'application/json',
						data: str_res,
						success: function(msg){
							loadcomments(doc_description.metaid, doc_data.id, $('#comments_list',comments_container))
							$('form',comments_container).hide();
							$('#viewform',comments_container).html('Add comment')
						},
						error: function(){					
							alert('error');
						}
					});			
				});
				$('#viewform',comments_container).click(function(){
					var com_form=$('form',comments_container);
					if(com_form.css('display') == 'none'){
						$('form',comments_container).show();
						$('#viewform',comments_container).html('Hide the comment form')
					}
					else{
						$('form',comments_container).hide();
						$('#viewform',comments_container).html('Add comment')
					}
					
				});
			}
			
			// Loading comments only if the document data is provided
			if (doc_data && 'id' in doc_data) {
				loadcomments(doc_description.metaid, doc_data.id, $('#comments_list',comments_container));
			}
			
			/*
			$('.tablelink' ,container).each(function( index, cont ) {
				contjs=$(cont);
				var data_id=contjs.html();
				var add=new loadaddinfo(contjs, cont.id,doc_description.metaid, data_id, doc_data.id, view);
				cont.id="tablelink_"+data_id;
				_self.addons.push(add);
			});*/
			if(geom_field_cnt==1){
				doc_description.columns[geom_field_index].widget_object.activateDrawing();
			}
			for (var i = 0; i < doc_description.columns.length; i++) {
				if(doc_description.columns[i].widget_object===undefined)
						continue;
				if(doc_description.columns[i].filtervalue)
					doc_description.columns[i].widget_object.sendmessage(doc_description.columns[i].filtervalue);
			}
			if(changelang)
				changelang();
			
		});
		
		
		
		
	};
	
	this.getnodebyname=function (fieldname){
		for (var i = 0; i < doc_description.columns.length; i++) {
			if( doc_description.columns[i].fieldname == fieldname){
				return doc_description.columns[i];				
			}
		}
	};
	
	
	this.init_form=function (callback_ready){
		var dform=this;
			
			
		for (var i = 0; i < doc_description.columns.length; i++) {
			if( doc_description.columns[i].widget_object ===undefined && doc_description.columns[i].widget!==undefined){
				doc_description.columns[i].widget_object=$.widgets.get_widget(doc_description.columns[i]);
				doc_description.columns[i].widget_object.metaid = doc_description.metaid;
				doc_description.columns[i].filter_widget=$.widgets.get_widget(doc_description.columns[i]);
				doc_description.columns[i].filter_widget.metaid = doc_description.metaid;
				doc_description.columns[i].widget_object.doc_template=this;
			}
		}
		for (var i = 0; i < doc_description.columns.length; i++) {
			if(doc_description.columns[i].widget===undefined || doc_description.columns[i].widget.properties===undefined )
				continue;
			if(doc_description.columns[i].widget.properties.filter!=undefined){
				for(fname in doc_description.columns[i].widget.properties.filter){
					v=doc_description.columns[i].widget.properties.filter[fname];
					for (var j = 0; j < doc_description.columns.length; j++) {
						if(v.indexOf('#'+doc_description.columns[j].fieldname)!=-1){
							doc_description.columns[j].widget_object.listnerlist.push(doc_description.columns[i].widget_object);
							doc_description.columns[i].widget_object.relationlist.push(doc_description.columns[j].widget_object);
						}
						
					}
				}
			}
			if(doc_description.columns[i].condition===undefined || doc_description.columns[i].condition=='')
				continue;			
			for (var j = 0; j < doc_description.columns.length; j++) {				
				if(i!=j && doc_description.columns[i].condition.indexOf(doc_description.columns[j].fieldname)!=-1)	{
					doc_description.columns[i].widget_object.relationlist.push(doc_description.columns[j].widget_object);					
					doc_description.columns[j].widget_object.listnerlist.push(doc_description.columns[i].widget_object);
				}		
			}
		}
		
		var form_template='';	
		
		if(doc_description.form_template && doc_description.form_template!='' && doc_description.form_template.length>20){
			form_template=gettemplate(doc_description.form_template);
			
			for (var i = 0; i < doc_description.columns.length; i++) {				
				
					
				if(doc_description.columns[i].tablename!='main' && doc_description.columns[i].tablename!='' && doc_description.columns[i].tablename!=undefined)
					continue;
				if(doc_description.columns[i].widget!==undefined){
					doc_description.columns[i].widget_object.lvis=false;
				}					
				
				//if(doc_description.columns[i].visible)
					form_template=form_template.replace(new RegExp('#'+doc_description.columns[i].fieldname+'#','g'), '<div class="" id="'+doc_description.columns[i].fieldname+'"></div>');
				//else
					//form_template=form_template.replace(new RegExp('#'+doc_description.columns[i].fieldname+'#','g'), '<div style="display: None;" id="'+doc_description.columns[i].fieldname+'"></div>');					
			}			
			form_template=form_template.replace(new RegExp('#comments#','g'), '<div id="comments"></div>');					
		}
		else{
			var form_template='<div class="text-error"/>';
			for (var i = 0; i < doc_description.columns.length; i++) {
				if(doc_description.columns[i].widget===undefined || !doc_description.columns[i].visible)
					continue;
				form_template+='<div class="form-group" id="'+doc_description.columns[i].fieldname+'"><label>'+doc_description.columns[i].title+'</label></div>';
			}
		}
		dform.form_template.main=form_template;
		
		
		var view_template = '';		
		if(doc_description.view_template && doc_description.view_template!='' && doc_description.view_template.length>5){
			view_template=gettemplate(doc_description.view_template);
			for (var i = 0; i < doc_description.columns.length; i++) {				
				if(doc_description.columns[i].tablename!='main' && doc_description.columns[i].tablename!='' && doc_description.columns[i].tablename!=undefined)
					continue;
				if(doc_description.columns[i].widget!==undefined){
					doc_description.columns[i].widget_object.lvis=false;
				}
				view_template=view_template.replace(new RegExp('#'+doc_description.columns[i].fieldname+'#','g'), '<div style="display: inline-block;" id="'+doc_description.columns[i].fieldname+'"></div>');
			}			
			view_template=view_template.replace(new RegExp('#comments#','g'), '<div id="comments"></div>');					
		}
		else{
			var view_template='<div class="text-error"/>';
			for (var i = 0; i < doc_description.columns.length; i++) {
				if(doc_description.columns[i].widget===undefined || !doc_description.columns[i].visible)
					continue;
				view_template+='<div style="display: block; margin-right:20px" id="'+doc_description.columns[i].fieldname+'"><label>'+doc_description.columns[i].title+'</label></div>';
			}
		}
		if(doc_description.metaid==100){
			view_template='<div style="display: inline-block;" id="name"></div>';
			doc_description.view_template = view_template;
		}
		dform.view_template.main=view_template;
		
		if(typeof doc_description.print_template == 'string')
			doc_print_template=gettemplate(doc_description.print_template);
		if(doc_description.print_template){
			if($.isArray(doc_description.print_template)){
				for(var i=0; i<doc_description.print_template.length; i++){
					dform.print_template[doc_description.print_template[i].name]=gettemplate(doc_description.print_template[i].body);
				}
				dform.print_template.main = gettemplate(doc_description.print_template[0].body);
			}
			else if (gettemplate(doc_description.print_template)!='')
				dform.print_template.main = gettemplate(doc_description.print_template);
		}
		
		if(dform.print_template.main===undefined || dform.print_template.main=='') {
			var print_template='';
			for (var i = 0; i < doc_description.columns.length; i++) {				
				if(!doc_description.columns[i].visible)
					continue;				
				print_template+='<div>#' + doc_description.columns[i].fieldname+'#</div>';
			}
			dform.print_template.main = print_template;
		}
		if(callback_ready!=undefined)
			callback_ready();
	};
	
	this.close=function(){
		var formmap=$('#map', this.container);
		if(formmap.length>0){			
			formmap.appendTo($('#mapcontainer'));
			setTimeout(function(){
				
				
				$('#map').width(_self.mapwidth+'px');				
				$('#map').height(_self.mapheight+'px');				
				map.invalidateSize(true);				
			});
		}
	}

	this.getJSON=function(doc, meta){

	}


	this.save_form=function(complite, commit){
		var dform=this;
		if (this.executing){
			alert('saving in progress...');
			return;
		}
		var res={};
		res.dataset_id=doc_description.metaid;		
		res.columns=new Array();
		for (var i = 0; i < doc_description.columns.length; i++) {
			if(doc_description.columns[i].widget_object===undefined)
					continue;
			if(doc_description.columns[i].tablename!='main' && doc_description.columns[i].tablename!='' && doc_description.columns[i].tablename!=undefined)
					continue;			
			try{
				v=doc_description.columns[i].widget_object.getVal();
			}
			catch (e) {
				serverlog(e);
			    complite(null, {status:'error', data:e});
			    return;			   
			}
			if(doc_description.columns[i].fieldname=='authors'){
				serverlog({authors:v});
			}
			if(doc_description.columns[i].required==true && (doc_description.columns[i].widget_object.visible || doc_description.columns[i].widget_object.visible===undefined) && (v===undefined || v==='' || v==='NULL' || (typeof v == "object" && v.length==0)) ){
				serverlog('Введите поле '+doc_description.columns[i].title);
				serverlog(res)
				complite(null,{status:'error', data:'Введите поле '+doc_description.columns[i].title});
				return;
			}
			if(v===undefined || v==='' || v==null)
				v='NULL';
			res[doc_description.columns[i].fieldname]=v;
			if(res[doc_description.columns[i].fieldname]===undefined)
				res[doc_description.columns[i].fieldname]='';			
		}
		//fieldcalc.calculatefields(doc_description, res, 'default', undefined, function(error, chdoc){
		//	res=chdoc;
			if(commit){
				var doc_id;
				var method;
				if(res.id && res.id!='NULL'){
					doc_id=res.id;
					method='update';
					res.f_id=res.id;
					res.id=undefined;
				}
				else
					method='add';
				str_res=JSON.stringify({document:res});
				console.log(str_res);
				//serverlog({commiting:res});
				this.executing=true;
				$.ajax({
					type: "POST",
					url: "/dataset/"+method+"?f=" + doc_description.metaid,
					contentType : 'application/json',
					data: str_res,
					success: function(msg){
						dform.executing=false;					
						response=JSON.parse(msg);
						if(method=='add')
							doc_id=response.data;
						for(var i=0; i<_self.addons.length; i++){
							
							_self.addons[i].save(doc_id);
						}
						complite(res, response);
		/*					ThemeDT.fnDraw();
						updateButtons();*/
					},
					error: function(){
						dform.executing=false;
						complite(res, {status:'error'});
					}
				});
			}
			else
				complite(res, {status:'ok'});
		//});
		
		
		return res;
	};
	
	

	
	this.print_document = function(doc_data, name){
		var str_template='';
		if(name!=undefined && name!='' && name!='simple list')
			str_template=this.print_template[name];

		if(!str_template)
			str_template=this.print_template.main;
			
		for (var i = 0; i < doc_description.columns.length; i++) {				
			if(doc_description.columns[i].widget_object===undefined)
				continue;
			var value='';
			if(doc_description.columns[i].fieldname in doc_data)
				value=doc_data[doc_description.columns[i].fieldname];
			var user_val='';
			if(doc_description.columns[i].fieldname=='doublet'){
				if(value=='true' || value==true)
					user_val='Дублет';
				else
					user_val='';
			}
			else
				user_val=doc_description.columns[i].widget_object.getUserVal(value);
			str_template=str_template.replace(new RegExp('#'+doc_description.columns[i].fieldname+'#','g'), user_val);		
		}
		return str_template;
	};
}


function editdocument(container, dataset_id, doc, mode, callback){
	getdatasetmeta(dataset_id  , function(error, table_json){
		if(error!=null)
			return;
		table_json.metaid=dataset_id;
		var doc_frm = new doc_template(table_json);
		doc_frm.init_form(function () {
			if(typeof doc === 'object'){
				doc_frm.show_form( container, doc, '', mode);
				callback('',doc_frm);
			}
			else if(typeof doc === 'number'){
				$.getJSON("/dataset/list?f="+dataset_id+"&iDisplayStart=0&iDisplayLength=1&f_id="+doc, function (data) {
					if (data.aaData.length == 0){
						callback('Object not found','');
						return;
					}
					doc_frm.show_form( container, data.aaData[0], '', mode);
					callback('',doc_frm);						
				});
			}
			else {
				doc_frm.show_form( container, {}, '', mode);
				callback('',doc_frm);
			}
		});
	});

}