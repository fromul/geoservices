var alreadyqueued = false;
$('#ask_for_access_button').click(function() {
	var id            = getQueryVariable('id');
	var queryisstatus = sendAccessQuery(id);

	if (queryisstatus === 0)
	{
		$('#message_field').html('<div class="alert alert-error">Error occured, please, try later</div>');
	}
	else if (queryisstatus === 1)
	{
		alreadyqueued = true;
		$('#message_field').html('<div class="alert alert-success">Access query was made</div>');
	}
	else if (queryisstatus === 2)
	{
		$('#message_field').html('<div class="alert">You have already send the access query</div>');
	}
});