var load_methods = [{ title: 'Copy', name: 'copy', params: [{ name: 'field', type: 'fieldlist' }] }, { title: 'Const value', name: 'const', params: [{ name: 'value', type: 'edit' }] }, { title: 'Copy Point', name: 'copy_pnt', params: [{ name: 'lon', type: 'fieldlist' }, { name: 'lat', type: 'fieldlist' }] }, { title: 'Copy Date', name: 'copy_date', params: [{ name: 'field', type: 'fieldlist' }, { name: 'format', type: 'edit' }] }];
		//$('#data_load', command_panel).click(function () {



function createFileTable(metaid, container, alloweddisplaylayers, callback, filtervalues, simple) {
	url="/datafile/meta?first_is_head=true&path=" + metaid;
	
	$.getJSON(url, function (data) {
		if (data.status==="error" || data.JSON == ''){
			callback('missing dataset meta');
			return;
		}
		var table_json = data.JSON;
		
		var sAjaxSource = "/datafile/list?first_is_head=true&path=" + metaid + '&count_rows=true';
		
		ThemeDT = new gdataset(table_json, metaid, container, sAjaxSource, undefined, simple);
		ThemeDT.drawtablehead();
		ThemeDT._fnReDraw();
		container.ThemeDT = ThemeDT;

		if (callback)
			callback(ThemeDT);
	});
}


function importdata(input_dataset){
			var options = {};
			var metaid=input_dataset.dataset_id;
			//options.filter='shp,tab,css';
			OpenFileDlg(function (path) {
				var container = $('<div class="edittab"><div><a class="lang btn" id="btnStart" style="">Start loading</a>Delimeter<select id="delimeter"><option value="t">Tab</option><option value=";">;</option><option value=",">,</option></select>Encoding<select id="encoding"><option value="CP1251">CP1251</option><option value="UTF-8">UTF-8</option><option value="KOI8-RU">KOI8-RU</option><option value="CP866">CP866</option></select></div>Field mapping<div id="fields"></div>Loading data<div id="tab" style="position: relative; height:300px;"></div></div>');
				container.appendTo("body");
				var tab_cont = $('#tab', container);
				var loadingtable;
				var load_id = gethash(path) + '&delimeter=t';
				var dlmtr = ',';
				var encoding = 'CP1251';
				var delimeter = $('#delimeter', container).change(function () {
					dlmtr = delimeter.val();
					load_id = gethash(path) + '&delimeter=' + dlmtr + '&encoding=' + encoding;
					tab_cont.empty();
					createFileTable(load_id, tab_cont, false, function (gdataset) {
						setTimeout(function() {
							$('.method_list', container).trigger('change');						
						}, 500);
						
						loadingtable = gdataset;
					});
				});
				dlmtr = delimeter.val();

				var encoding_input = $('#encoding', container).change(function () {
					encoding = encoding_input.val();
					load_id = gethash(path) + '&delimeter=' + dlmtr + '&encoding=' + encoding;
					tab_cont.empty();
					createFileTable(load_id, tab_cont, false, function (gdataset) {
						loadingtable = gdataset;
						$('.method_list', container).trigger('change');
						//$('.method_list').trigger('change')
						
						
						/////////////////////////
					});
				});
				encoding = encoding_input.val();

				var btnStart = $('#btnStart', container).click(function () {
					var data = { delimeter: dlmtr, first_is_head: true, encoding: encoding };

					$('.paramfield', container).each(function (index) {
						fld_cont = $(this);
						mthd = $('#mthd', fld_cont);
						if (mthd.val() == '-1')
							return;
						var fld = { name: mthd.val() };
						var prm = [];
						$('.param_fld_list', fld_cont).each(function (index) {
							input = $(this);
							prm.push({ name: this.id, value: input.val() });
						});
						$('.param', fld_cont).each(function (index) {
							input = $(this);
							prm.push({ name: this.id, value: input.val() });
						});
						fld.prm = prm;
						data[this.id] = fld;
					});

					str_res = JSON.stringify(data);
					$.ajax({
						url: '/datafile/import?f=' + metaid + '&path=' + gethash(path),
						dataType: "json",
						contentType: 'application/json',
						data: str_res,
						method: "POST",
						type: "POST",
						success: function (data) {
							input_dataset._fnReDraw();
						}
					});
				});

				createFileTable(load_id, tab_cont, false, function (gdataset) {
					loadingtable = gdataset;
					doc_frm=input_dataset.doc_frm;

					var field_map = $('#fields', container);
					for (var i = 0; i < input_dataset.doc_frm.doc_description.columns.length; i++) {
						if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
							var div_el = $('<div class="paramfield"  id="' + doc_frm.doc_description.columns[i].fieldname + '" style="display: inline-block; margin-right:20px; vertical-align: top;" ><label>' + doc_frm.doc_description.columns[i].title + '</label><select class="form-control method_list" id="mthd" type="text"><option value="-1">--</option></select><div id="params"></div></div>');
							field_map.append(div_el);
							var mthd = $('#mthd', div_el).change(function () {
								var params = $('#params', $(this).parent());
								var paramfield = $(this).parent('.paramfield');

								params.empty();
								for (var j = 0; j < load_methods.length; j++) {
									if (load_methods[j].name == $(this).val()) {
										for (var m = 0; m < load_methods[j].params.length; m++) {
											if (load_methods[j].params[m].type == 'fieldlist') {
												params.append($("<div>" + load_methods[j].params[m].name + "</div>"));
												var fldlist = $('<select class="param_fld_list" id="' + load_methods[j].params[m].name + '"></select>');
												params.append(fldlist);
												for (var s = 0; s < loadingtable.doc_frm.doc_description.columns.length; s++) {
													if (paramfield.attr('id') == loadingtable.doc_frm.doc_description.columns[s].fieldname || paramfield.attr('id') == getcorrectobjname(loadingtable.doc_frm.doc_description.columns[s].fieldname))
														fldlist.append('<option selected value="' + loadingtable.doc_frm.doc_description.columns[s].fieldname + '">' + loadingtable.doc_frm.doc_description.columns[s].title + '</option>');
													else
														fldlist.append('<option value="' + loadingtable.doc_frm.doc_description.columns[s].fieldname + '">' + loadingtable.doc_frm.doc_description.columns[s].title + '</option>');
												}
											}
											else
												params.append($('<input class="param" type="text" id="' + load_methods[j].params[m].name + '"/>'));
										}
										break;
									}
								}
								$('.param_fld_list', params).change(function () {
									cpy_field = $(this).val();
									var mthd = $('#mthd', $(this).parent().parent());
									mthd_name = mthd.val();
									var formats = ['d.M.y', 'M/d/y', 'y-M-ddTHH:mm:ssZ', 'y/M/d'];
									if (mthd_name == 'copy_date') {
										maxcnt = 0;
										var indexOfmax = -1;
										var c_rows = $('tbody tr', container);
										for (var i_frm = 0; i_frm < formats.length; i_frm++) {
											frm = formats[i_frm]
											var cnt = 0;
											for (var j_Row = 0; j_Row < c_rows.length; j_Row++) {
												var geos_row_selected = $(c_rows[j_Row]);
												var doc_data_base = geos_row_selected.data('doc');
												if (cpy_field in doc_data_base && doc_data_base[cpy_field] != '' && isDate(doc_data_base[cpy_field], frm)) {
													d = getDateFromFormat(doc_data_base[cpy_field], frm);
													if (d != 0) {
														dv = new Date(d);
														s = formatDate(dv, 'yyyy-MM-dd HH:mm:ss');
														console.log(s);
														cnt++;
													}
													//d=parseDate(doc_data_base[cpy_field], frm);												
												}
											}
											if (cnt > maxcnt) {
												maxcnt = cnt;
												indexOfmax = i_frm;
											}
										}
										var format = $('#format', $(this).parent());
										if (indexOfmax != -1) {
											format.val(formats[indexOfmax]);
										}
										else
											format.val('');
									}
								});
								$('.param_fld_list', params).change();

							});
							for (var j = 0; j < load_methods.length; j++) {
								if (load_methods[j].name == 'copy')
									mthd.append($('<option selected value="' + load_methods[j].name + '">' + load_methods[j].title + '</option>'));
								else
									mthd.append($('<option value="' + load_methods[j].name + '">' + load_methods[j].title + '</option>'));
							}
							mthd.change();
						}
					}
				});
				container.dialog({
					minWidth: 900,
					minHeight: 500,
					Height: 500,
					title: 'Data loading',
					position: ["center", "center"],
					close: function (event, ui) {
						container.remove();
					},
					buttons: {
						"Open": function () {
							if ($('#filepath').val() == '' || $('#filename').val() == '') {
								alert('Select a file!');
								return;
							}
							callback($('#filepath').val());
							$(this).dialog("close");
							$('#fm_OpenFileDlg').remove();
						},
					},
					modal: true
				});
			}, options);
//		});
}