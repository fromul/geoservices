function ToDec2(string) {
	arr = string.split(/[\D]/);
	var lon_sec = arr[2]/3600;
	var lon_mn = arr[1]/60;
	var lon = parseInt(arr[0],10)+lon_mn+lon_sec;
return lon;
};

function FromDec2(string) {

	var lan_gr = addzero(Math.floor(string),3);
	var lan_mn = addzero(Math.floor((string-lan_gr)*60),2);
	var lan_sec = addzero(Math.round(((string-lan_gr)*60-lan_mn)*60),2);
	var value =lan_gr+"°"+lan_mn+"'"+lan_sec+"''";
return value;
};

(function($) {
	var pasteEventName = ($.browser.msie ? 'paste' : 'input') + ".coordinates";
	var iPhone = (window.orientation != undefined);
	var geos_init=false;
	
	$.fn.extend({
		//Helper Function for Caret positioning
		caret: function(begin, end) {
			if (this.length == 0) return;
			if (typeof begin == 'number') {
				end = (typeof end == 'number') ? end : begin;
				return this.each(function() {
					if (this.setSelectionRange) {
						this.setSelectionRange(begin, end);
					} else if (this.createTextRange) {
						var range = this.createTextRange();
						range.collapse(true);
						range.moveEnd('character', end);
						range.moveStart('character', begin);
						range.select();
					}
				});
			} else {
				if (this[0].setSelectionRange) {
					begin = this[0].selectionStart;
					end = this[0].selectionEnd;
				} else if (document.selection && document.selection.createRange) {
					var range = document.selection.createRange();
					begin = 0 - range.duplicate().moveStart('character', -100000);
					end = begin + range.text.length;
				}
				return { begin: begin, end: end };
			}
		},
		coordinates2: function(coordinates, settings) {
			geos_init=true;
			var initval=false;
			
			if (!coordinates && this.length > 0) {
				var input = $(this[0]);
				return input.data($.coordinates.dataName)();
			}
			settings = $.extend({
				editpointmode: "default",
				placeholder: "_",
				degree_template:"999°99'99''",
				completed: null
			}, settings);
	
			// Создание дополнительных элементов управления
			var input = $(this[0]);
			input.addClass("coordinate");
			var buffer=settings.degree_template.replace(/9/g,"0");
			string=input.val();
					
			
			
			function clearBuffer(sub_input, start, end) {
				for (var i = start; i < end && i < len; i++) {
					if (tests[i])
						buffer[i] = settings.placeholder;						
				}
			};
			function writeBuffer(sub_input) {
				var s='';
				sub_input.val(buffer);
				/*
				input=sub_input[0].main_input;
				if(!initval){
					test=input[0].lan.val()+' '+input[0].lon.val();
					try { input[0].subdecimal.val(ToDec(test)); } catch (e) { }
					test='MULTIPOINT('+ToDec(test)+')';
					input.val(test);
				}*/
				return s;
			};			
		
			function testCoordinate(vl){
				var numbers='0123456789';
				if (vl.length<3) return false;
				for (var j=0; j < settings.degree_template.length; j++){
					if(settings.degree_template.charAt(j)=='9'){
						if ((numbers.indexOf(vl.charAt(j))==-1) )
							return false;
					}
					else{
						if (vl.charAt(j)!=settings.degree_template.charAt(j))
							return false;					
					}					
				}
				var parts = vl.split(/\D/,3);					
				if (parts[0]<0 || parts[0]>360)  return false;
				if (parts[1]<0 || parts[1]>60)  return false;
				if (parts[2]<0 || parts[2]>60)  return false;
				return true;
			};
		
///////////////////////
			var defs = $.coordinates.definitions;
			var tests = [];
			var partialPosition = coordinates.length;
			var firstNonMaskPos = null;
			var len = coordinates.length;
			function keydownEvent(e) {
				var trg=$('#'+e.target.id, this.form);
				var k=e.which;				
				//console.log('key down');				
					//backspace, delete, and escape get special treatment
				if(k == 8 || k == 46 || (iPhone && k == 127)){
					var pos = trg.caret(),
						begin = pos.begin,
						end = pos.end;
					
					clearBuffer(trg, begin, end);
					writeBuffer(trg);

					return false;
				} else if (k == 27) {//escape
					trg.val(trg[0].firstValue);
					//trg.caret(0, checkVal(trg));
					return false;
				}
				else{
					//console.log('write buffer');					
					//writeBuffer(trg);
				}
				
				return true;
			};
			
			function seekNext(pos) {
				while (++pos <= settings.degree_template.length && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function seekPrev(pos) {
				while (--pos >= 0 && settings.degree_template[pos]!='9');
				return pos;
			};
			
			function keypressEvent(e) {
				var trg=$('#'+e.target.id, this.form);
				//console.log('keypress');		
				var k = e.which,
					pos = trg.caret();
				
				if (e.ctrlKey || e.altKey || e.metaKey || k<32) {//Ignore
					return true;
				} else if (k) {
					if(pos.end-pos.begin!=0){
						clearBuffer(trg, pos.begin, pos.end);
					}
					var p = seekNext(pos.begin - 1);
					if (p < len) {
						var c = String.fromCharCode(k);
						var s_val= trg.val();
						var future_val=s_val.substring(0,p)+c+s_val.substring(p+1,s_val.length);
						if (testCoordinate(future_val)/* tests[p].test(c)*/) {							
							buffer=future_val;
							writeBuffer(trg);
							var next = seekNext(p);
							trg.caret(next);
							if (settings.completed && next >= len)
								settings.completed.call(input);
						}
					}
					return false;
				}
			};
			input.bind("setvalue.coordinate", function (e) {
					var input=$('#'+e.target.id, this.form);
					val=input.val();				
					buffer=val;
				}
			);

			input.bind("keydown.coordinates", keydownEvent);
			input.bind("keypress.coordinates", keypressEvent);			
			writeBuffer(input);
			initval=false;			
		}
		
	});
	
	
})(jQuery);
