/*
 * Autocomplete for Jeditable
 *
 * Copyright (c) 2012
 *
 * Depends on jQuery UI Autocomplete
 *
 */

$.editable.addInputType('autocomplete', {
     element : $.editable.types.text.element,
     plugin : function(settings, original) {
      $('input', this).autocomplete(settings.autocomplete, 
          {                                                 
          dataType:'json',
          parse : function(data) {                                                                                                                    
              return $.map(data, function(item)
              {
                  return {
                          data : item,
                          value : item.Key,
                          result: item.value                                                                                     
                         }
              })
             },
          formatItem: function(row, i, n) {                                                        
                  return row.value;
              },
          });                                        
      }
 });
