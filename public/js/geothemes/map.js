//OpenLayers.ProxyHost = "/proxy?type=regular&url=";
var lon = 105;
var lat = 52;
var zoom = 4;
var TILE_MODE_SINGLE = true;
var TILE_MODE_MULTYTILE_WIDTH = 2000;
var TILE_MODE_MULTYTILE_HEIGHT = 500;
var mapheight = 1024;
var mapwidth = $(window).width() - 20;
var farr = ['point', 'polygon', 'line'];//, 'rect', 'rectangle','baikalcity','political_division'];

var MAPFILESTORE = 'F:/DATA/GISSTORAGE/mapfiles_tmp/';

var mapPanel, legendPanel, layerList;
var listingFeatures, drawingFeature;

var drawnItems;

var map;
var _lastZIndex=1000;

var GETFEATURE_STATUS        = false;
var GETFEATURE_RASTER_STATUS = false;
var RADIUS                   = 2; // Accuracy in PIXELS
var LayerContainer           = $('#l_container');	

var map_edit_listners = [];
var map_select_flag   = false;
var drawControl;

function map_edit_handle(e, mode){
	for(var i=0; i<map_edit_listners.length; i++){
		f=map_edit_listners[i];
		f[0](e, mode);
	}
}

function add_map_handle(f, gtype, widget){
	if(drawControl!=undefined){
		map.removeControl(drawControl);
		drawControl=undefined;
	}
	if(gtype=='line'){		
		drawControl = new L.Control.Draw({
			position: 'topright',
			draw: {
				polyline: {
					metric: true
				},
				polygon: false,
				rectangle: false,
				circle: false,
				marker: false
			},
			edit: {
				featureGroup: drawnItems,
				remove: false
			}
		});	
		
	}
	if(gtype=='polygon'){
		drawControl = new L.Control.Draw({
			position: 'topright',
			draw: {
				polygon: {
					allowIntersection: false,
					showArea: true,
					drawError: {
						color: '#b00b00',
						timeout: 1000
					},
					shapeOptions: {
						color: '#bada55'
					}
				},				
				rectangle: false,
				polyline: false,
				circle: false,
				marker: false
			},
			edit: {
				featureGroup: drawnItems,
				remove: false
			}
		});	
		
	}

	if(gtype=='rectangle'){		
		drawControl = new L.Control.Draw({
			position: 'topright',
			draw: {
				rectangle: {
					metric: true
				},
				polygon: false,				
				polyline: false,
				circle: false,
				marker: false
			},
			edit: {
				featureGroup: drawnItems,
				remove: false
			}
		});	
		
	}	
	if(gtype=='point'){
		drawControl = new L.Control.Draw({
			position: 'topright',
			draw: {
				polygon: false,
				rectangle: false,
				polyline: false,
				circle: false,
				marker: true
			}
		});	
		drawnItems.addTo(map);
	}
	
	map.addControl(drawControl);
	
	map.on('leaflet-draw-draw-marker:created', function (e) {
		alert('lf');
	});
	
	map_edit_listners=[];		
	map_edit_listners[0]=[f,widget];	
}


function stop_map_edit(widget){
	if(map_edit_listners.length==0 || map_edit_listners[0][1]!=widget)
		return;
	map_edit_listners=[];
	if(drawControl!=undefined)
		drawControl.remove();
	drawControl=undefined;
	if(drawnItems!=undefined){
		drawnItems.clearLayers();
		drawnItems.removeFrom(map);
	}
}


function timeshift(div){
	var progressbar=$('<table><tr><td align="right"><button class="btn" id="downButton">&lt;</button></td><td><div id="progressbar"></div></td><td><button class="btn" id="upButton">&gt;</button></td></tr><tr><td><input type="text" id="setShowTime" style="width: 40px;"></td><td><button id="setButton" class="btn">move to</button></td></tr></table><hr>');	

	div.append(progressbar);
	var progressbar = $('#progressbar', div);
	progressbar.progressbar({
		value : 1,
		max : 50
	});

	$( "button", div ).on( "click", function( event ) {
		var target = $( event.target );			
		var progressbarValue = progressbar.progressbar( "option", "value" );
		if ( target.is( "#downButton" ) ) {
			progressbarValue=progressbarValue-1;
			progressbar.progressbar( "option", {
			  value: progressbarValue 
			});
		} else if ( target.is( "#upButton" ) ) {
			progressbarValue=progressbarValue + 1;
			progressbar.progressbar( "option", {
			  value: progressbarValue
			});
		} else if ( target.is( "#setButton" ) ) {
			progressbarValue = parseInt($('#setShowTime').val());
			progressbar.progressbar( "option", {
			  value: progressbarValue
			});
		}
/*		for(var i=0; i < MLayers.length; i++){
			MLayers[i].timechange(progressbarValue);
		}*/
		progressbarValue;
	});
}


function reorder_layers(){
	_lastZIndex=1000;
	$('.g_layer',$('#layer_list')).each(function(index){
		ml=$(this).data("mlayer");
		if (ml.layer===undefined)
			return;
		_lastZIndex--;		
		ml.layer.setZIndex(_lastZIndex);		
//		map.removeLayer(ml.layer);
/*
		if(ml.selection_layer!=undefined){
			//map.removeLayer(ml.selection_layer);
			_lastZIndex--;
			ml.selection_layer.setZIndex(_lastZIndex);		
			
		}*/
	});

} //end reorder_layers()


/**
 * Creates the layer control for specific theme.
 *
 * @param string  title         Title of the theme
 * @param boolean visible       Specifies if the theme should be visible
 * @param DOMNode tab_container Node that will be populated with the control
 *
 * @return void
 */
function create_layer_control(title, visible, dataset_id){
	var layer_list = $('#layer_list');
	//list_item      = $('<div class="layer_item"/>');
	
	//layer_list.prepend(list_item);
	var layer_control = $( '<div class="g_layer">\
		<button type="button" id="close_btn" class="close" aria-label="Close">  <span aria-hidden="true">×</span></button>\
	  	<form></form><div class="row">\
			<div class="form-group col-4">\
				<button type="button" class="text-left"><i class="fas fa-table glyphicon"></i></button>\
			</div>\
			<div class="form-group col-8">\
				<input type="checkbox" id="layer_switch" class="form-check-input">\
				<label class="form-check-label" for="exampleCheck1" id="legend_layer_title">' + title + '</label>\
			</div>\
		</div></form>\
		<div class="row g_layer_content" ><div class="col-12" id="rules"></div>\
		</div>\
	</div>' );
	
	layer_control.attr("datasetid", dataset_id);
	
	//var tab_data      = $('<div class="btn tab_data" href="#" id="tab_data"><i class="icon-align-justify"></i></div>');
	//var check_box     = $('<div class="fleft" style="width: 254px;"><input id="layer_switch" class="fleft" type="checkbox"></input><div id="layer_title" class="fleft"/><div style="clear: both;" id="rules"/>');

	if (visible) {
		$('#layer_switch', layer_control).attr('checked', true);
	}

	$('#layer_switch', layer_control).change(function() {
		ml = $(this).parents('.g_layer').data("mlayer");
		if(ml.layer){
			if($('#layer_switch', layer_control)[0].checked)
				map.addLayer(ml.layer);
			else
				map.removeLayer(ml.layer);
			reorder_layers();
		}
	});

	$('#close_btn', layer_control).click(function() {
		var dataset_id = layer_control.attr("datasetid");
		openedthemes.pop(dataset_id, title);

		ml = $(this).parents('.g_layer').data("mlayer");
		if(ml.layer)
			map.removeLayer(ml.layer);
		$(this).parents('.g_layer').remove();
		//$(this).parent().parent().remove();
	});

/*	layer_control.append(check_box);
	layer_control.append(close_btn);
	layer_control.append(tab_data);*/

	layer_list.prepend(layer_control);	
	layer_control.click(function(){
		$('.g_layer', layer_list).removeClass('g_active_layer');	
		$(this).addClass('g_active_layer');	
	});

	if ($('.g_layer',layer_list).length == 2) {
		layer_list.sortable({
			revert: true
		});

		layer_list.on( "sortstop", function( event, ui ) {
			reorder_layers();			
		});
	} else if ($('.g_layer', layer_list).length > 2) {
		layer_list.sortable( "refresh" );		
	}

	
	return layer_control;
} //end create_layer_control()


function MLayer(title, visible, gdataset, dataset_id){
	var o_layer=this;
	this.Mtype='';	
	this.fieldtype='';
	this.name='';
	var layer_control = create_layer_control(title, visible, dataset_id);	
	this.layer_id='';
	this.title=title;
	this.static_map=false;
	this.dataset_id=dataset_id;
	this.style_object=new map_style();
	if(gdataset)
		gdataset.layers.push(this);
	this.destroy=function(){		
		openedthemes.pop(dataset_id, title);
		map.removeLayer(this.layer);
		layer_control.remove();		
	};
	this.assign_tab_field=function(obj, fieldindex){
		this.meta=obj;
		this.style_object.layer_json=obj;
		this.fieldindex=fieldindex;
		this.Mtype='field';	
		this.name=obj.columns[fieldindex].fieldname;
		if(this.title=='')
			this.title=obj.columns[fieldindex].title;
		if(this.title=='')
			this.title=obj.title;
		if(this.title=='')
			this.title=obj.columns[fieldindex].fieldname;
		if(obj.columns[fieldindex].widget.properties.gtype===undefined || obj.columns[fieldindex].widget.properties.gtype==''){
			this.fieldtype=obj.columns[fieldindex].widget.name;
		}
		else
			this.fieldtype=obj.columns[fieldindex].widget.properties.gtype;
		
		
		
		
		this.selected_array=new Array();
		if(obj.unique===undefined || obj.unique=='')	
			obj.unique = randomString(10);
		
		this.unique=obj.unique;
		this.layer_id=obj.metaid;
		
		//this.layer_sld=obj.columns[fieldindex].widget.sld;
		
		
		if(obj.columns[fieldindex].widget.style)
			this.style_object.readFromJSON(obj.columns[fieldindex].widget.style);
		else if(obj.columns[fieldindex].widget.sld)
			this.style_object.readFromSLD(obj.columns[fieldindex].widget.sld);
		
		this.style_object.geometry_type=this.fieldtype;
		if(this.style_object.title=='')
			this.style_object.title=obj.title;
		
		
		var mapstyle=this.style_object.save2Map(obj);
		//console.log(style_object.save2JSON());
		//style_object2=new map_style();
		//style_object2.readFromJSON(style_object.save2JSON());
/*
		sld_object=new geos_sld();
		sld_object.assign(this.layer_sld, this.fieldtype);
		var mapstyle=sld_object.get_map_style(obj);*/
		//var gdataset.dataquery;
		var dataquery = {f: encodeURI(this.layer_id), unique: obj.unique+this.name, fieldname: this.name, mapstyle: mapstyle, selected_array: this.selected_array };
		if(gdataset)
			var fullquery = $.extend(dataquery, gdataset.dataquery);
		else
			var fullquery = dataquery;
		var tempobj =$.post("/map/createmap", fullquery) .done(function( data ) {	
			var result=JSON.parse(data);
			if(result.status=='success'){
				o_layer.wms_link=result.wms_link;
				o_layer.static_map=result.static_map;
				minZoom=result.minZoom;
				maxZoom=result.maxZoom;
				var opacity=result.opacity;
				if(opacity===undefined)
					opacity=1;
				layername=result.wms_layer_name;
				if(!result.static_map)
					layername+=',selection'
				var wmslayer = L.tileLayer.wms(result.wms_link , {
					layers: layername,					
					format: 'image/png',
					transparent: true,
					tileSize: mapheight,
					minZoom:minZoom,
					maxZoom:maxZoom,
					opacity:opacity
				});
				o_layer.layer=wmslayer;
				o_layer.wmslayer=wmslayer;
				_lastZIndex++;		
				wmslayer.setZIndex(_lastZIndex);		

				if (visible){
					map.addLayer(wmslayer);
				}
/*
				if (result.selection_wms_link!=undefined){
					o_layer.selection_wms_link=result.selection_wms_link;
					var selection_wmslayer = L.tileLayer.wms(result.selection_wms_link , {
						layers: 'selection',					
						format: 'image/png',
						transparent: true,
						tileSize: mapheight
					});
					o_layer.selection_layer=selection_wmslayer;
					_lastZIndex++;		
					selection_wmslayer.setZIndex(_lastZIndex);		
					
					if (visible)
						map.addLayer(selection_wmslayer);
				}
	*/			
			}
		});

		o_layer.drawsign(this.layer_sld);
		var userDataString = $.cookie('userData') || localStorage.user || '{}';
		var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));	
		
		if (user.username){
			layer_control.dblclick(function (){		
				s = new layer_sld_form(o_layer.style_object, obj, function(mapstyleobject){
					o_layer.style_object=mapstyleobject;
					if(isNaN(obj.metaid)){
						url="/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_guid=" + obj.metaid;
					}
					else{
						url="/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + obj.metaid;
					}
					$.getJSON(url, function(data) {			
						var table_json    = JSON.parse(data.aaData[0].JSON);			
						style=mapstyleobject.save2JSON();
						table_json.columns[o_layer.fieldindex].widget.style=style;
						//o_layer.layer_sld=sld;
						o_layer.drawsign();
						var table_json_data=JSON.stringify(table_json);
						obj.layer=o_layer;
						$.post("/dataset/savestructure", { tablejson: table_json_data } );			
						o_layer.update_map();			
					});
				});
			});
		}
	}
	
	this.assign_file=function(file_path, mapstyle){
		this.Mtype='file';
		this.layer_sld='';
		var hash=gethash(file_path);
		this.unique = randomString(10);
		this.layer_id=hash;	
		this.epsg=4326;
		
		function createlayer(){
			mapstyle=o_layer.style_object.save2Map()
			var tempobj =$.post("/map/createfilemap",{source: hash, unique: o_layer.unique, fieldname: o_layer.name, mapstyle: mapstyle, selected_array: [], epsg: o_layer.epsg, layermeta: JSON.stringify( o_layer.meta), jsonstyle: o_layer.style_object.save2JSON() }) .done(function( data ) {	
				var result=JSON.parse(data);
				if(result.status=='success'){
					o_layer.wms_link=result.wms_link;
					
					var wmslayer = L.tileLayer.wms(result.wms_link , {
						layers: filenameclean,
						format: 'image/png',
						transparent: true,
						tileSize: mapheight,
						minZoom:0,
						maxZoom:18
					});

					o_layer.layer=wmslayer;
					//o_layer.layer_sld=get_default_sld( result.type.toLowerCase(), filename);
					//o_layer.update_map();				
					o_layer.drawsign(o_layer.layer_sld);
					
					if (visible)
						map.addLayer(wmslayer);
				}
			});
		}

		var filename=file_path.substring(file_path.lastIndexOf('/')+1,file_path.length);
		var filename_ext=file_path.substring(file_path.lastIndexOf('.')+1,file_path.length).toLowerCase();
		if(filename_ext=='shp')
			this.Mtype='file';
		else if(filename_ext=='tif' || filename_ext=='tiff')
			this.Mtype='tiff';
		else
			this.Mtype='MTiff';
		var filepathstyle=file_path.substring(0, file_path.lastIndexOf('.'))+'.style';		
		
		var filenameclean = filename.substring(0, filename.lastIndexOf('.'));		
		

		
		
		$.getJSON("/map/info?source=" + hash, function(rastrmeta) {			
			if(rastrmeta.status!='ok'){
				return;
			}
			o_layer.Mtype = rastrmeta.data.layer_type.toLowerCase();
			o_layer.style_object.setgeometrytype(o_layer.Mtype);
			o_layer.meta={columns:[]};
			o_layer.epsg = rastrmeta.epsg;
			o_layer.meta.epsg = rastrmeta.epsg;
			$.extend( o_layer.meta, rastrmeta.data );
			for(var i=0; i<rastrmeta.data.band_count; i++){
				var c={fieldname:'pixel', title:'band'+i, type:'number'};
				// $.extend( c, rastrmeta.data );
				o_layer.meta.columns.push(c);
			}
		

			if(mapstyle){
				this.style_object.readFromJSON(mapstyle);
				createlayer();
			}
			else{
				s = new layer_sld_form(o_layer.style_object, o_layer.meta, function(mapstyleobject){
					o_layer.style_object=mapstyleobject;
					style=o_layer.style_object.save2JSON();				
					saveFile(filepathstyle, style);
					createlayer();
				});
			}							
		});

		
		
		
		
		layer_control.dblclick(function (){
			s = new layer_sld_form(o_layer.style_object, o_layer.meta, function(mapstyleobject){
				o_layer.style_object=mapstyleobject;
				style=o_layer.style_object.save2JSON();				
				saveFile(filepathstyle, style);
				o_layer.drawsign();
				o_layer.update_map();
			});
		});
	}

	this.update_map=function(onlyselection) {
		var o_layer=this;
		if(o_layer.layer==undefined)
			return;		
		
//		style_object=new map_style();		
//		style_object.geometry_type = this.fieldtype;
//		style_object.readFromSLD(this.layer_sld);
		var mapstyle=this.style_object.save2Map();
		
		var dataquery = {unique: this.unique+this.name, fieldname: this.name, mapstyle: mapstyle, selected_array: this.selected_array, epsg: o_layer.epsg, jsonstyle: o_layer.style_object.save2JSON()};
		
		if(this.Mtype=='field'){
			dataquery.f = encodeURI(this.layer_id); 
			if(gdataset)
				var fullquery = Object.assign(dataquery, gdataset.dataquery);			
		}
		else{
			dataquery.source = encodeURI(this.layer_id);
			var fullquery = dataquery;
			fullquery.layermeta = JSON.stringify( o_layer.meta);
		}
					
		if(this.Mtype=='field')
			url = "/map/createmap";
		else
			url = "/map/createfilemap";
		var tempobj =$.post(url,fullquery).done(function() {
			var d = new Date();
			var n = d.toLocaleString();
			if(!o_layer.static_map /*&& onlyselection===undefined*/){
				url=o_layer.wms_link + "&t=" + n;
				o_layer.layer.setUrl(url);
			}
			/*
			if(o_layer.selection_layer!=undefined){
				url=o_layer.selection_wms_link + "&t=" + n;
				o_layer.selection_layer.setUrl(url);
			}*/
		});	
	};

	this.redraw_map=function() {
		var o_layer=this;
		if(o_layer.layer==undefined)
			return;
				
		var d = new Date();
		var n = d.toLocaleString();
		if(!o_layer.static_map ){			
			url=o_layer.wms_link + "&t=" + n;
			o_layer.layer.setUrl(url);
		}
		/*
		if(o_layer.selection_layer!=undefined){
			url=o_layer.selection_wms_link + "&t=" + n;
			o_layer.selection_layer.setUrl(url);
		}*/	
	};
	
	this.view_selection=function(selected_array){	
		this.selected_array=selected_array;
		this.update_map(true);	
	};	
	
	
	this.drawsign=function (sld_text){		
		$('#layer_title',layer_control).text(title);	
		style_object=this.style_object;// new map_style();		
		//style_object.geometry_type = this.fieldtype;
		//style_object.readFromSLD(sld_text);
		$('#legend_layer_title', layer_control).html(style_object.title);
		var rules=$('#rules', layer_control);
		rules.empty();
		if(style_object.style_type=='rules'){
			for(var i=0; i<style_object.rules.length; i++){
				symbolizer=$('<div style="clear: both;" class="row"><div class="canvas_div"></div></div>');

				cnvs=$('<canvas class="fleft clearb" id="ld_sign" width="80px" height="45px"></canvas>');
				$('.canvas_div',symbolizer).append(cnvs);
				if(style_object.rules.length>1 && style_object.rules[i].mark!=''){
					label=$('<div class="class_mark">'+style_object.rules[i].mark+'</div>');
					symbolizer.append(label);
				}
				// else
				// 	label=$('<div class="class_mark"></div>');				
				
				
				rules.append(symbolizer);
				style_object.rules[i].Draw(cnvs);			
			}			
		}
		else if(style_object.style_type=='wizard'){
			for(var i=0; i<style_object.flds.length; i++){
				if(style_object.flds[i].title!=''){
					label=$('<div class="class_mark">'+style_object.flds[i].title+'</div>');
					rules.append(label);
				}				
				for(var j=0; j<style_object.flds[i].classes.length; j++){
					symbolizer=$('<div style="clear: both;" class="row"><div class="canvas_div"></div></div>');										
					cnvs=$('<canvas class="fleft clearb" id="ld_sign" width="80px" height="45px"></canvas>');
					$('.canvas_div',symbolizer).append(cnvs);
					if(style_object.flds[i].classes[j].title!=''){
						label=$('<div class="class_mark">'+style_object.flds[i].classes[j].title+'</div>');
						symbolizer.append(label);
					}					
					rules.append(symbolizer);
					style_object.flds[i].classes[j].Draw(cnvs);
				}
			}
		}
		else if(style_object.style_type=='chart'){
			for(var i=0; i<style_object.chartfields.length; i++){
				label=$('<div class="row-fluid"><div class="span4"><canvas class="fleft clearb" id="ld_sign" width="80px" height="45px"></canvas></div><div class="span8">'+style_object.chartfields[i].title+'</div></div>');
				rules.append(label);
				rule_canvas=$('canvas', label);				
				cnvs=rule_canvas[0];
				ctx=cnvs.getContext('2d');
				ctx.stroke();
				ctx.clearRect(0, 0, cnvs.width, cnvs.height);
				ctx.fillStyle=style_object.chartfields[i].color;
				ctx.strokeStyle='#000000';
				if(style_object.chart_type=='pie'){
					boundary=cloneArray(map_symbols['pie']);
					draw_object(ctx, boundary, 20, 20, 40, 0);
				}
				else{
					boundary=cloneArray(map_symbols['rect2']);
					draw_object(ctx, boundary, 0, 0, -1, 0);
				}
				
			}
			if(style_object.style_type=='chart' && style_object.chart_type=='pie' && style_object.chart_size!='static'){
				style_object.pie_maxsize=parseFloat(style_object.pie_maxsize);
				style_object.pie_minsize=parseFloat(style_object.pie_minsize);
				style_object.pie_maxval =parseFloat(style_object.pie_maxval);
				style_object.pie_minval=parseFloat(style_object.pie_minval);
				
				cns_size=style_object.pie_maxsize+20;
				label=$('<div class="row-fluid"><div class="span4"><canvas class="fleft clearb" id="ld_sign" width="200px" height="'+cns_size+'px"></canvas></div></div>');
				rules.append(label);
				rule_canvas=$('canvas', label);				
				cnvs=rule_canvas[0];
				ctx=cnvs.getContext('2d');
				ctx.beginPath();
				var dr=(style_object.pie_maxsize - style_object.pie_minsize)/6;
				var dv=(style_object.pie_maxval - style_object.pie_minval)/6;
				var cur=style_object.pie_minval;
				for(var i=0; i<7; i++){
					rad=(style_object.pie_minsize + dr*i)/2;
					ctx.beginPath();
					ctx.arc(80,cns_size-rad,rad,Math.PI/2,Math.PI*(3/2));
					ctx.stroke();
					cur=Math.round((style_object.pie_minval + dv*i)*1000)/1000;
					ncur=Math.round((cur+dv)*1000)/1000;
					if(i==0)
						ctx.fillText("Менее "+cur,100,cns_size-rad*2);
					else if(i==6)
						ctx.fillText("Более "+cur,100,cns_size-rad*2);
					else
						ctx.fillText(cur +" - "+ncur,100,cns_size-rad*2);
					cur=ncur;
				}
				
			}
		}
		else if(style_object.style_type=='initial'){
			var icon=$('<i class="fas fa-camera fa-3x"></i>');
			rules.append(icon);
		}
		else{
			symbolizer=$('<div style="clear: both;" class="row"><div class="canvas_div"></div></div>');
			//label=$('<div class="class_mark"></div>');
			cnvs=$('<canvas class="fleft clearb" id="ld_sign" width="80px" height="45px"></canvas>');
			$('.canvas_div',symbolizer).append(cnvs);
			//symbolizer.append(label);			
			rules.append(symbolizer);
			style_object.base_rule.Draw(cnvs, style_object);
		}

		/*
		for(var i=0; i<sld_object.rules.length; i++){
			symbolizer=$('<div></div>');
			if(sld_object.rules.length>1)
				label=$('<div>'+sld_object.rules[i].mark+'</div>');
			else
				label=$('<div></div>');
			cnvs=$('<canvas class="fleft clearb" id="ld_sign" width="80px" height="25px"></canvas>');
			symbolizer.append(label);
			symbolizer.append(cnvs);
			rules.append(symbolizer);
			sld_object.rules[i].Draw(cnvs, 0, 2, false, sld_object.rules.length>1);
		}*/				
	}
	
	this.redraw = function (){
		this.update_map();	
		//this.layer.redraw()		
	}

	this.timechange=function(t){
		if (this.Mtype == 'MTIF') {			
			layer.url = "http://geos.icc.ru:8800/fcgi-bin/mapserv.fcgi?map=" + this.mapfilesarray[t].mapfile;
			layer.redraw(true);
		}
	}

	layer_control.data("mlayer", this);	
	return this;
}


function FileLayer(path, visible){
	
	console.log("FileLayer");
	console.log(path);
	
	var o_layer=this;
	var unique = randomString(10);

	var fname=path.slice(path.lastIndexOf(dir_separator)+1);
	var filenameclean=fname.substring(0, fname.lastIndexOf('.'));
	
	
	hesh=gethash(path);
	var tempobj = JSON.parse(getRemote("/createmap?filehesh=" + hesh + "&unique=" + unique + "&from_color=000000&to_color=FFFFFF" ));
	var mapfile=str_replace('\\\\', '/',MAPFILESTORE) +unique +".map";

	if ("mapfilesobject" in tempobj && "data" in tempobj.mapfilesobject && tempobj.mapfilesobject.data.length>0) {
		mapfile = str_replace('/', '\\',tempobj.mapfilesobject.data[0].mapfile);
		mapfile = str_replace('\\', '\\\\',mapfile);
	}			
	
	
	var wmslayer = L.tileLayer.wms("http://geos.icc.ru:8800/fcgi-bin/mapserv.fcgi?&map=" + mapfile, {
		    layers: filenameclean,
		    format: 'image/png',
		    transparent: true,
			tileSize: mapheight
		});
	
	this.layer=wmslayer;
	map.addLayer(wmslayer);		
	
	var layer_control=create_layer_control(filenameclean, true);
	
	layer_control.dblclick(function (){
		var sldtxt=''
		if(obj.object.columns[fieldindex].widget.sld===undefined)
			sldtxt=sld_text;
		else
			sldtxt=obj.object.columns[fieldindex].widget.sld;
			s=new layer_sld_form(obj.object, sldtxt, fieldindex, function(sld, mapstyle){
				obj.object.columns[fieldindex].widget.sld=sld;
				obj.object.columns[fieldindex].widget.mapstyle=mapstyle;
				
				o_layer.drawsign(sld);			
				f=obj.object;
				var table_json_data=JSON.stringify(f);

				$.post("/dataset/savestructure", { tablejson: table_json_data } );			
				
				o_layer.view_selection(aSelected);		
							
				//wmslayer.setUrl("http://geos.icc.ru/fcgi-bin/mapserv.fcgi?SLD_BODY="+encodeURIComponent(sld)+"&map=" + mapfile);
		});
	});
	
	this.drawsign=function (sld_text){
		sld_object=new geos_sld();
		sld_object.assign(sld_text, this.fieldtype);
		h=sld_object.rules.length*15;
		layer_control.height(h+2);	
		$('#ld_sign', layer_control).remove();
		cnvs=$('<canvas class="fleft clearb" id="ld_sign" width="250" height="'+h+'"></canvas>')
		layer_control.append(cnvs);	
		//cnvs=$('#ld_sign', layer_control);
		cnvs.height(h);	
		sld_object.Draw(cnvs, obj.object.columns[fieldindex].title);
	}
	
	this.redraw = function (){
		this.layer.redraw()		
	}
	
	this.timechange=function(t){
		if(this.Mtype=='MTIF'){			
			layer.url = "http://geos.icc.ru:8800/fcgi-bin/mapserv.fcgi?map=" + this.mapfilesarray[t].mapfile;
			layer.redraw(true);
		}
	}
	
	this.view_selection=function(selected_array){
		sldtxt=obj.object.columns[fieldindex].widget.sld;			
		if (sldtxt===undefined || sldtxt=='') return;
		sld_object=new geos_sld();			
		sld_object.assign(sldtxt, this.fieldtype );
		sld_object.view_selection(selected_array);
		sldtxt=sld_object.getSLD();			
		wmslayer.setUrl("http://geos.icc.ru:8800/fcgi-bin/mapserv.fcgi?map=" + mapfile);			
	};
	
	layer_control.data("mlayer", this);
	//o_layer.drawsign(layer_sld);
	return this;
}


// HardCore precision http://en.wikipedia.org/wiki/Decimal_degrees
function metersToDegrees(obj) {
	var number = obj.meters;
	if (Math.abs(obj.lat) < 12) {
		number = number / 102470;
	} else if (Math.abs(obj.lat) < 32) {
		number = number / 78710;
	} else if (Math.abs(obj.lat) < 55) {
		number = number / 43496;
	} else {
		number = number / 20000;
	}
	return number;
}


function getPropsByName(obj) {
	for (var i = 0; i < obj.tjs.columns.length; i++) {
		if (obj.tjs.columns[i].fieldname == obj.pn) {
			switch (obj.mode) {
				case 'attrjson':
					return obj.tjs.columns[i];
					break;
				case 'title':
					return obj.tjs.columns[i].title;
					break;
			}
		}
	}
	return null;
}


function getMapJSON() {
	var retobj = {
		tablename: THEME_JSON.tablename,
		schema: THEME_JSON.schema,
		layers: []
	}
	for (var i = 0; i < map.layers.length; i++) {
		if(isset(map.layers[i].params) && !(map.getLayersByName(map.layers[i].name)[0].isBaseLayer)) {
			var tempo = {
				mapfile: str_replace('\\', '/', str_replace('\\\\', '/', map.layers[i].url)),
				sldfile: str_replace('\\', '/', str_replace('\\\\', '/', map.layers[i].params.SLD)),
				layer: map.layers[i].params.LAYERS
			}
			retobj.layers.push(tempo);
		}
	}
	return retobj;
}

/**
* wmsDraw - drawing WMS layers for the table
*/
function wmsDraw(object, dataset, alloweddisplaylayers) {
	for (var i = 0; i < object.columns.length; i++) {
		if(object.columns[i].widget===undefined)
			continue;
		if (farr.indexOf(object.columns[i].widget.name) != -1 ) {
			if (alloweddisplaylayers !== undefined) {
				if (alloweddisplaylayers.indexOf(object.columns[i].fieldname) !== -1) {
					openedthemes.push(object.metaid, object.columns[i].title);
					var l = new MLayer(object.columns[i].title, true, dataset, object.metaid);
					object.layer = l;
					l.assign_tab_field(object,i);
				}
			} else {
				openedthemes.push(object.metaid, object.columns[i].title);
				var l = new MLayer(object.columns[i].title, true, dataset, object.metaid);
				object.layer = l;
				l.assign_tab_field(object,i);
			}
		}
	}
}

function AddWMSLayer(wms) {
	
	var wmslayer = L.tileLayer.wms(wms.wms_link , {
		layers: wms.layername,					
		format: 'image/png',
		transparent: true,
		tileSize: mapheight,
	});
	_lastZIndex++;		
	wmslayer.setZIndex(_lastZIndex);		
	map.addLayer(wmslayer);
}

/* GetLocation Utilities */
function getLocationInit() {
	if ($('#dialog_findcity').length === 0) {
		$('body').append('<!-- Dialog Lookup the city --> \
<div id="dialog_findcity" title="Поиск населенного пункта" style="display: none;"> \
	<div style="" id="dialog_findcity_name"> \
		<label>Введите название искомого населенного пункта</label><br><input type="text" id="dialog_findcity_name_value" placeholder="Dallas" value="">  <button type="button" id="dialog_findcity_lookup">Найти</button> \
	</div> \
	<hr> \
	<div id="dialog_findcity_caption" style="max-height: 400px; overflow: scroll;"></div> \
</div>');
	}
}

function getLocation(jData) {
	if (jData == null) { return; }
	$('#dialog_findcity_caption').html('');
	var html = '';
	var geonames = jData.geonames;
	for (i=0;i< geonames.length;i++) {
		var name = geonames[i];
		var htmlstr = '<div style="font-size: 10px;"><strong>' + name.name + '</strong>    <button type="button" style="margin: 2px; padding: 2px;" onclick="centerMap(' + name.lng + ', ' + name.lat + '); return false;">Показать на карте</button><br>' + name.adminName1 + ', ' + name.countryName + '</div>';
		$('#dialog_findcity_caption').append(htmlstr);
	}
}

/**
 * Storing curent map position in cookies
 */
function storeCurrentPositionInCookies(currenttableid) {
	var currentcenter = map.getCenter();
	var cookiestring  = map._zoom + ':' + currentcenter.lat + ':' + currentcenter.lng;
	$.cookie('map_' + currenttableid, cookiestring);
} //end storeCurrentPositionInCookies()


/**
*	drawMap - drawing map with base layers in given container
*
*	@param nodeid - given ID  of container
*/
function drawMap(nodeid, baselayers){
	var defaultlan  = 52;
	var defaultlng  = 105;
	var defaultzoom = 3;

	var currenttableid = getQueryVariable('id');
	if (currenttableid === null) {
		currenttableid = 'notspecified';
	} //end if

	if ($.cookie('map_' + currenttableid) !== null) {
		var rememberedpositionarray = explode(':', $.cookie('map_' + currenttableid));
		if (rememberedpositionarray.length !== 3) {
			c('Broken remembered map position');
		} else {
			defaultzoom = rememberedpositionarray[0];
			defaultlan  = rememberedpositionarray[1];
			defaultlng  = rememberedpositionarray[2];
		}
	}
	
	
	console.log('before map');
	map = L.map(nodeid ).setView([defaultlan, defaultlng], defaultzoom);
	console.log('after map');
	
	
	setTimeout(function(){
		map.invalidateSize(true);
		}, 500);
	
	
	L.Control.measureControl().addTo(map);	
/*	
	L.control.scale({imperial: false, position: 'bottomright'}).addTo(map);
	L.control.coordinates({}).addTo(map);
	
*/
	var ggl;
	var tgis;
	var yam;	
	var gglrmap;
	var HYBRID;
	var bing;


	var layer_control=new L.Control.MyLayers({}, {
	});

	map.addControl(layer_control);	
/*
	if(baselayers===undefined || baselayers.google_satellite){
		layer_control._addLoadItem('Google satellite', true, false, function(viewlayer){			
			require(['l_google'], function(u) {			
				HYBRID  = new L.Google('HYBRID');
				viewlayer(HYBRID);			
			});		
		})
	}*/

	if(baselayers===undefined || baselayers.google_roadmap){
		layer_control._addLoadItem('Google road map', false, false, function(viewlayer){
			require(['l_google'], function(u) {			
				gglrmap = new L.Google('ROADMAP');
				viewlayer(gglrmap);			
			});		
		})
	}
	if(baselayers===undefined || baselayers.yandex){
		layer_control._addLoadItem('Yandex', false, false, function(viewlayer){
			/*
			var yam=L.tileLayer(
				'http://vec{s}.maps.yandex.net/tiles?l=map&v=4.55.2&z={z}&x={x}&y={y}&scale=2&lang=ru', {
					subdomains: ['01', '02', '03', '04'],
					reuseTiles: true,
					updateWhenIdle: false,
					//crs: L.CRS.EPSG3395
					crs: L.CRS.EPSG3857
				}
			);
			viewlayer(yam);
			*/
			//require(['l_yandex'], function(u) {	
				yam     = new L.Yandex();
				
				viewlayer(yam);			
			//});	
		})
	}
	
	if(baselayers===undefined || baselayers.gis2){
		layer_control._addLoadItem('2GIS', false, false, function(viewlayer){
			//require(['2gis', 'l_2gis', 'api-2gis'], function(u) {	
				tgis    = new L.DGis();
				viewlayer(tgis);			
			//});		
		})
	}
	if(baselayers===undefined || baselayers.bing){
		layer_control._addLoadItem('Bing', false, false, function(viewlayer){
			require(['l_Bing'], function(u) {	
				bing = new L.BingLayer("AhFsQpvPbKvlztg94VL_ttGy_vAIvFjfHd9p6fw-sgdS-zZqB7noNKvLQiQV8v0h" , {type: 'AerialWithLabels'});		
				bing.setZIndex(0);
				viewlayer(bing);
			});		
		})
	}
	
	if(baselayers===undefined || baselayers.osm){
		layer_control._addLoadItem('OSM', false, false, function(viewlayer){
			osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
			viewlayer(osm);		
		})
	}

	if(baselayers===undefined || baselayers.cadastr){
		var cad = L.tileLayer.wms("http://pkk5.rosreestr.ru/arcgis/services/Cadastre/CadastreWMS/MapServer/WMSServer", {
			layers: '8,9,10,11,12,14,15,16,18,19,20',
			format: 'image/png',
			transparent: true,
			attribution: "ISDCT SB RAS Map Server"
		});
		cad.setZIndex(1);
		layer_control.addOverlay(cad,'Cadastr');
	}
	
	
	if(baselayers===undefined || baselayers.osm){
		layer_control._addLoadItem('None', false, false, function(viewlayer){
			//osm = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
			viewlayer(null);		
		})
	}


/*
	if(baselayers===undefined || baselayers.cadastr){
		var cad = L.tileLayer.wms("http://maps.rosreestr.ru/arcgis/services/Cadastre/CadastreWMS/MapServer/WMSServer", {
			layers: '22,21,20,19,18,16,15,14,13,11,10,9,7,6,4,3,2,1',
			format: 'image/png',
			transparent: true,
			attribution: "ISDCT SB RAS Map Server"
		});
		cad.setZIndex(1);
		layer_control.addOverlay(cad,'Cadastr');
	}
*/
	drawnItems = new L.FeatureGroup();	
	
	
	

//	var selObj = L.control.objsel();
//	map.addControl(selObj);	

	map.on('dragend', function (e) {		
		storeCurrentPositionInCookies(currenttableid);
	});	

	map.on('zoomend', function() {
		storeCurrentPositionInCookies(currenttableid);
	});

	

	map.on('draw:created', function (e) {
		var type = e.layerType,
			layer = e.layer;
		map_edit_handle(layer, 'create');
	});

	map.on('draw:edited', function (e) {
		var layers = e.layers;				
		layers.eachLayer(function(layer) {
			map_edit_handle(layer, 'edit');					
		});				
	});
	
	map.getblayername=function(){
		if(map.hasLayer(ggl) || map.hasLayer(gglrmap) || map.hasLayer(HYBRID)){
			return 'google';
		}else if(map.hasLayer(tgis)){ 
			return 'tgis'; 
		}else if(map.hasLayer(yam)){ 
			return 'yam';
		}else if(map.hasLayer(osm)){ 
			return 'osm';
		}else if(map.hasLayer(cad)){ 
			return 'cad';
		}
		return '';
	}
		
	$('input[type=radio]#OSM').click();
//	initiateTour();
/*
	var layer_tree = $("#layer_tree");
	//$.jstree.defaults.core.themes.variant = "large";
	
		layer_tree.jstree({
		//'core': {
            'themes': {
                'name': 'proton',
                'responsive': true,				
            },			
        //},
		"plugins" : [ 
			"themes","json_data","ui","crrm","dnd","search","types"//,"contextmenu" 
		],		
		"json_data" : { 
			"ajax" : {
				"cache": "false",
				"url" : "/geothemes/layers",
				"data" : function (n) { 
					// the result is fed to the AJAX request `data` option
					//if(n=='-1')
					//	alert('You should log in.');
					return { 
						"mode" : "2", 
						"id" : n.attr ? n.attr("id") : 0 
					}; 
				}
			}
		},
		"types" : {
			"max_depth" : -2,
			"max_children" : -2,
//			"valid_children" : [ "drive" ],
			"types" : {
				"default" : {
					"valid_children" : "none",
					"icon" : {
						"image" : "/js/file.png"
					}
				},				
				"map" : {
					"valid_children" : "none",
					"icon" : {
					//	"image" : "/img/media/zoom.png"
						"image" : "/images/layers.png"
						//"image" : "/js/folder.png"
					},
					"start_drag" : false,
					"move_node" : false,
					"delete_node" : false,
					"remove" : false

				},
				"folder" : {
					"valid_children" : [ "folder", "map" ],
					"icon" : {
						"image" : "/img/media/folder-2.png"
						//"image" : "/images/layers.png"
					},
					"start_drag" : false,
					"move_node" : false,
					"delete_node" : false,
					"remove" : false

				}
			}
		}
	})
	.bind("select_node.jstree", function (event, data) {
		if(data.rslt.obj[0].attributes.dataset_id.value!=-1){
			
			var f = true;
			
			$('.g_layer',$('#layer_list')).each(function(index){
				ml=$(this).data("mlayer");
				if(data.rslt.obj[0].attributes.dataset_id.value == ml.layer_id){
					f = false;
					return;
				}
			});	
			
			if(f){
				addlayer(data.rslt.obj[0].attributes.dataset_id.value);
			}

		}
		
	});
*/
	var restoredlayers = {};//openedthemes.get();
	var drawnthemes    = new Array();
	for (var key in restoredlayers) {
		var layer = restoredlayers[key].split(":");
		if (layer[0] in drawnthemes) {
			drawnthemes[layer[0]].push(layer[1]);
		} else {
			drawnthemes[layer[0]] = new Array();
			drawnthemes[layer[0]].push(layer[1]);
		}
	}

	for (var key in drawnthemes) {
		if(key!=100 || key!='100')
			createDataTable(key, null, drawnthemes[key]);
	}
	

	var btn = $("#btnLayerAdd");		
	btn.click(function() {
		OpenFileDlg(function(path){
			if(path[path.length-1]=='/')
				path=path.substring(0,path.length-1);		
			var l = new MLayer(path, true);
			l.assign_file(path);
			return;			
		}, {filter:'tif,tiff,shp'});
	});
} //end drawMap()


function drawBaseLayers(){
}


function addlayer(dataset_id){
	if(dataset_id===undefined)
		return;
	
	var obj = JSON.parse(getRemote("/geothemes/getdbjsonbyid?id=" + dataset_id));
	obj.metaid = dataset_id;
	if(obj.columns===undefined){
		console.log('Error JSON of table meta.');
		return;
	}	
	for (var j = 0; j < obj.columns.length; j++) {
		if (farr.indexOf(obj.columns[j].widget.name) != -1) {
			openedthemes.push(dataset_id, obj.columns[j].title);
			b=new MLayer(obj.columns[j].title, true);			
			b.assign_tab_field(obj, j);
		}
	}
}


/**
*	Drawing single raster on the map (with "redraw")
*/
function drawSingleRaster(curval, mapstyle) {
	var path = curval;
	if(path[path.length-1]=='/') path=path.substring(0,path.length-1);
	var filename=path.substring(path.lastIndexOf('/')+1,path.length);

	var l = new MLayer(path, true);
	l.assign_file(path);
}





// Define the `MapListApp` module


function viewmap(mapobject, center, scale, overlay){
	function view_all(){
		for(var i=mapobject.layerlist.length - 1; i>=0; i--){
			if(mapobject.layerlist[i].wms_link)
				AddWMSLayer(mapobject.layerlist[i])
			else if(mapobject.layerlist[i].viewtab)
				createDataTable(mapobject.layerlist[i].id, undefined, undefined, undefined, mapobject.layerlist[i].filter);
			else
				wmsDraw(mapobject.layerlist[i].table_json);
		}
		if(center){
			c=wkt2decimal(center);
			map.setView([c.lat, c.lon], scale);
		}
		if(overlay=='None')
			$('input[type=radio]#None').click();
		else if(overlay=='Yandex')
			$('input[type=radio]#Yandex').click();
		else if(overlay=='Google satellite')
			$('input[type=radio]#Google satellite').click();
		else if(overlay=='2GIS')
			$('input[type=radio]#2GIS').click();
		else if(overlay=='Bing')
			$('input[type=radio]#Bing').click();
		else if(overlay=='OSM')
			$('input[type=radio]#OSM').click();
		reorder_layers();
	};
	function loadlayerjson(index){
		if(index>=mapobject.layerlist.length){
			view_all();
			return;
		}
		$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + mapobject.layerlist[index].id, function(data) {			
			if(data.aaData.length==0)
				return;
			var table_json    = JSON.parse(data.aaData[0].JSON);
			table_json.metaid = table_json.dataset_id;
			mapobject.layerlist[index].table_json=table_json;
			loadlayerjson(index+1);			
		});		
	};
	if(mapobject.layerlist[0].table_json!==undefined || mapobject.wms)
		view_all()
	else
		loadlayerjson(0);
};

function hidemap(map){
	if(map && map.layerlist){
		for(var i=0; i<map.layerlist.length; i++){
			if(map.layerlist[i].table_json && map.layerlist[i].table_json.layer)
				map.layerlist[i].table_json.layer.destroy();		
		}
	}
};


function ViewMapList(container,section){
	$.ajax({
	url: '/dataset/list?f=2&iDisplayStart=0&iDisplayLength=1200&iSortingCols=2&sort=[{"fieldname":"section","dir":true},{"fieldname":"weight","dir":true}]&f_hostname='+location.hostname,
	dataType: "json",					
	method: "POST",
	success: function(data) {
			var sectionname='';
			for (var i = 0; i < data.aaData.length; i++) {
				if(sectionname!=data.aaData[i].section){
					sectionname=data.aaData[i].section;
					if(sectionname==null){
						var mapitem=$('<div class="mapsection"><h5>Другие</h5></div>');
						container.append(mapitem);					
					}
					else{
						var mapitem=$('<div class="mapsection"><h5>'+sectionname+'</h5></div>');
						container.append(mapitem);
					}
				}
				var mapitem=$('<div class="mapitem"></div>');
				var mapwidget=$.widgets.get_widget_byname('Map');
				mapitem.data('doc', data.aaData[i]);
				mapitem.html(mapwidget.getUserVal(data.aaData[i].json));
				container.append(mapitem);
				if(data.aaData[i].defaultmap){
					$('input[type=checkbox]',mapitem).prop( "checked", true );
					viewmapclick($('input[type=checkbox]',mapitem));
					//$('input[type=checkbox]',mapitem).trigger('click');
				}
			}
		}
	});
	
	
	
}