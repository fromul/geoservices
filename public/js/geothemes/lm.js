function LM (obj) {
	this.nodeid = obj.nodeid;
	this.nodeid_b = this.nodeid + '_base';
	this.nodeid_t = this.nodeid + '_table';
	this.nodeid_a = this.nodeid + '_additional';
	this.layersarr = new Array();
	var map = obj.map;
	
	function init() {
		$('[name="base_layers"]').live('change', function() {
			var arry = map.getLayersByName($(this).val());
			map.setBaseLayer(arry[0]);
		});
	}
	init();
	function getRemote(url) {
		$.ajax({url: url, method: 'GET', async: false, success: function(result) {
			return result;
        }});
	}
	var format = new OpenLayers.Format.SLD();
	
	this.addBaseLayer = function(OLinst) {
		this.layersarr.push(OLinst);
		$('#' + this.nodeid_b).append('<div><span id="layer_name">' + OLinst.name + '</span><input type="radio" name="base_layers" value="' + OLinst.name + '"></div>');
		map.addLayer(OLinst);
	}
	this.addBaseLayers = function(OLinstArr) {
		for (var i = 0; i < OLinstArr.length; i++) {
			this.addBaseLayer(OLinstArr[i]);
		}
	}
	
	this.addLayer = function(OLinst) {
		this.layersarr.push(OLinst);
		console.log(OLinst);
		/*
		if (OLinst.params.SLD.length > 0) {
			var temposld = getRemote(OLinst.params.SLD);
			console.log(temposld);
			obj = format.read(temposld);
			
		}
		*/
		$('#' + this.nodeid_t).append('<div><span id="layer_name">' + OLinst.name + '</span><input type="checkbox" name="table_layers" value="' + OLinst.name + '"></div>');
		map.addLayer(OLinst);
	}
	
	this.addAddLayer = function(OLinst) {
		
		this.layersarr.push(OLinst);
		console.log(OLinst);
		/*
		if (OLinst.params.SLD.length > 0) {
			var temposld = getRemote(OLinst.params.SLD);
			console.log(temposld);
		}
		*/
	}
}
