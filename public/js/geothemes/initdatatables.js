﻿/*
/*
	Basic permission constants
*/
var ableToAddEditViewOwn = 0;
var ableToViewOthers = 1;
var ableToEditOthers = 2;
var isOwner = 3;
var isModerator = 4;

var datasets = [];
var decimalcoordinates = false;


var tabs4tables;

var tabCounter = 0;



/* <div style="display: block; margin-right:20px" id="regionid"><label>regionid</label><input type="hidden" id="regionid"></div> */




/**
 * Setups new tab.
 *
 * @param Number metaid Metaid of the theme of false if only container needs to be returned 
 * @param String title  Tab title
 *
 * @return DOMNode Content node
 */
function create_tab_item(metaid, title) {
	var table_tab = this;
	$('.bodycontainer').show();

	if(!tabs4tables){
		tabs4tables= $("#tabs-lc").tabs();
		tabs4tables.delegate("span.ui-icon-close", "click", function () {
			var panelId = $(this).closest("li").remove().attr("aria-controls");
			$("#" + panelId).remove();
			tabs4tables.tabs("refresh");
		});
		
	}
		
	var tabNameExists = false;
	var scnt = 0;
	$('#tabs-lc ul li a').each(function (i) {
		scnt++;
		if (this.text == title) {
			tabNameExists = true;
			return this;
		}
	});

	//addNewTab(title, tab_container);
	tabCounter++;
	var divTabs = tabs4tables;//$("#tabs-lc");
	var ul = $("#map-tabs");
	var contentDivId = "divTab" + tabCounter;
//	divTabs.tabs("destroy");
	var li = $("<li><a id='a_" + contentDivId + "' href='#" + contentDivId + "'>" + title + "</a>" + '<span id="del_tab" metaid="' + metaid + '" class="ui-icon ui-icon-close"></span>' + "</li>");
	ul.append(li);
	var contentDiv = $('<div class="edittab datatable" id="' + contentDivId + '"></div>');
	divTabs.append(contentDiv);
	tabs4tables.tabs("refresh");
	li.data('content',contentDiv);

	//		contentDiv.append(tab_container);
	//		tab_container.show();
	divTabs.tabs({ active: (tabCounter - 1) });
	$('#del_tab', li).click(function () {
		content=$(this).parents('li').data('content');
		ind = datasets.indexOf(content.ThemeDT);
		if (ind == -1)
			return;
		var curgdataset=datasets[ind];
		datasets.splice(ind, 1);
		divTabs.tabs("destroy");
		tabCounter--;
		openedthemes.pop($(this).attr("metaid"));

		contentDiv.remove();
		li.remove();
		divTabs.tabs({ active: (tabCounter - 1) });
		if (curgdataset)
			curgdataset.layers_destroy();

		if (tabCounter == 0) {
			$('.bodycontainer').hide(500);
			$('#map').height($(window).height() + "px");
			$.removeCookie("bodycontainerheight");
			setTimeout(function () {
				updateMapstate();
			}, 600);

		}
	});

	$('#a_' + contentDivId, divTabs).trigger('click');
	if (scnt == 0)
		$('#medium').trigger('click');
	updateMapstate();

	return contentDiv;
}


/**
 * Stores open layers using cookies.
 */
var openedthemes = {
	push: function (themeid, columnname) {
		var storeditem = themeid + ":" + columnname;
		var openedthemessplitted = this.get();
		if (openedthemessplitted.indexOf(storeditem) == -1) {
			openedthemessplitted.push(storeditem);
		}

		$.cookie("openedthemes", openedthemessplitted.join(","));
	},
	pop: function (themeid, columnname) {
		if (columnname === undefined) {
			var storeditem = themeid + ":" + columnname;
			var filteredopenedthemes = new Array();
			var openedthemessplitted = this.get();
			var numberofopenedthemes = openedthemessplitted.length;
			for (var i = 0; i < numberofopenedthemes; i++) {
				var layersplitted = openedthemessplitted[i].split(":");
				if (layersplitted[0] !== themeid) {
					filteredopenedthemes.push(openedthemessplitted[i]);
				}
			}
			$.cookie("openedthemes", filteredopenedthemes.join(","));
		} else {
			var storeditem = themeid + ":" + columnname;
			var filteredopenedthemes = new Array();
			var openedthemessplitted = this.get();
			var numberofopenedthemes = openedthemessplitted.length;
			for (var i = 0; i < numberofopenedthemes; i++) {
				if (openedthemessplitted[i] !== storeditem) {
					filteredopenedthemes.push(openedthemessplitted[i]);
				}
			}
			$.cookie("openedthemes", filteredopenedthemes.join(","));
		}
	},
	get: function () {
		if ($.cookie("openedthemes") == null || $.cookie("openedthemes") === "") {
			return new Array();
		} else {
			return $.cookie("openedthemes").split(",");
		}
	}
}




function createDataTable(metaid, container, alloweddisplaylayers, callback, filtervalues, simple) {

	getdatasetmeta(metaid  , function(error, table_json){
			if(error!=null)
				return;
			
		table_json.metaid = metaid;
		if (!(table_json.rights && table_json.rights != '' && table_json.rights.length == 3 && table_json.rights.charAt(2) != '0')) {
			for (var i = 0; i < table_json.columns.length; i++) {
				if (table_json.columns[i].fieldname == 'published') {
					table_json.columns.splice(i, 1);
					break;
				}
			}
		}
		if (filtervalues) {
			$.each(filtervalues, function (index, value) {
				for (var i = 0; i < table_json.columns.length; i++) {
					if (table_json.columns[i].fieldname == index)
						table_json.columns[i].filtervalue = value;
				}
			});

		}
		var sAjaxSource = "/dataset/list?f=" + metaid + '&count_rows=true';
		if (container == null) {
			if (metaid != 100) {
				container = create_tab_item(metaid, table_json.title);
			}
			else
				container = $('<div style="display: none; height: 100%;"/>');
		}
		ThemeDT = new gdataset(table_json, metaid, container, sAjaxSource, undefined, simple);
		ThemeDT.comandpanel();
		ThemeDT.drawtablehead();
		ThemeDT._fnReDraw();
		container.ThemeDT = ThemeDT;
		datasets.push(ThemeDT);
		wmsDraw(table_json, ThemeDT, alloweddisplaylayers);
		if (callback)
			callback(ThemeDT);
	});
	
}


function createFileTable(metaid, container, alloweddisplaylayers, callback, filtervalues, simple) {
	
	$.getJSON("/datafile/meta?f=" + metaid + '&first_is_head=true', function (data) {
		var table_json = data;
		var sAjaxSource = "/datafile/list?f=" + metaid + '&first_is_head=true'
		if (container === undefined)
			container = create_tab_item(metaid, table_json.title);
		ThemeDT = new gdataset(table_json, metaid, container, sAjaxSource, undefined, simple);
		ThemeDT.comandpanel();
		ThemeDT.drawtablehead();
		ThemeDT._fnReDraw();			
		datasets.push(ThemeDT);
		if (callback)
			callback(ThemeDT);
	});
	
}

function removeHtmlSymbols(text) {
	return text
		.replace(/&amp;/g, "&")
		.replace(/&lt;/g, "<")
		.replace(/&gt;/g, ">")
		.replace(/&quot;/g, '')
		.replace(/&#039;/g, '');
}

function gdataset(themeobj, metaid, container, sAjaxSource, tabledata, simple) {
	this.dataset_id = metaid;
	themeobj.metaid=metaid;
	this.metadata=clone(themeobj);
	this.maxrowcount = 0;
	this.mode='table';
	this.dataquery = {};
	this.tabledata = tabledata;
	this.deleteddata = [];
	this.aSelected = [];
	this.layers = [];
	var dt_table = this;
	this.rowcount = 0;
	this.rowselect=[];
	this.row_on_page_count = 10;
	this.curent_page = 0;
	this.columncount = 5;
	this.FilterValues = {};
	this.filterobjects={};
	this.groupdata = {};
	this.aSelected = [];
	this.sort = [];
	var doc_frm = new doc_template(this.metadata);
	this.doc_frm = doc_frm;
	var user=new gp_user();
	if (this.metadata.unique === undefined || this.metadata.unique == '')
		this.metadata.unique = randomString(10);


	this.fnFilter = function (filter_values, fieldname) {
		filter_values = removeHtmlSymbols(filter_values)
		this.FilterValues[fieldname] = filter_values;
		this.curent_page = 0;
		this._fnReDraw();
		this.update_pagination();
	};

	this.updateButtons = function () {
		var geos_row_selected = $('.geos_row_selected', container);
		if (geos_row_selected.length > 0) {
			$('#btnCustomBaseAddRow', container).removeAttr('disabled');
			$('#btnCustomEditRow', container).removeAttr('disabled');
			$('#btnDeleteRow', container).removeAttr('disabled');
			$('#btnCustomView', container).removeAttr('disabled');
			$('.storybutton', container).removeAttr('disabled');

		}
		else {
			$('#btnCustomBaseAddRow', container).attr("disabled", "disabled");
			$('#btnCustomEditRow', container).attr("disabled", "disabled");
			$('#btnDeleteRow', container).attr("disabled", "disabled");
			$('#btnCustomView', container).attr("disabled", "disabled");
			$('.storybutton', container).attr("disabled", "disabled");
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	this._fnReDraw = function (rowtemplate) {
		iDisplayStart = this.curent_page * this.row_on_page_count;
		$('.dataTables_processing', container).show();
		var data = { sort: dt_table.sort };


		$.each(this.groupdata, function (index, value) {
			if (value != '' && value !== undefined){
				if(value=='none')
					data['g_' + index] = 'count';
				else
					data['g_' + index] = value;
			}
		});

		$.each(this.FilterValues, function (index, value) {
			if (value != '' && value !== undefined)
				data['f_' + index] = value;
		});

		function filldata(tabledata, row_b, row_cnt) {
			$('.dataTables_processing', container).hide();
			var tbody = $('#tbody', container);
			
			if(dt_table.mode!='table'){
				if(rowtemplate===undefined){	
					var cardlist=$('<ul class="thumbnails object_container"></ul>');
					container.append(cardlist);
				}
			}
			else{
				dt_table.tbody=tbody;
				tbody.empty();
			}
			for (var row_id = row_b; row_id < row_cnt; row_id++) {
				if (tabledata[row_id] === undefined)
					break;
				
				
				var aData = tabledata[row_id];
				var trow = $('<tr></tr>');
				tbody.append(trow);
				trow.data('doc', aData);
				aData.trow=trow;
				function drawrow(doc){
					aData=doc;
					if(dt_table.mode=='table'){
						
						for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
							if (metaid == 5 && (doc_frm.doc_description.columns[i].fieldname == 'password' || doc_frm.doc_description.columns[i].fieldname == 'password2'))
								continue;
							if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
								var vv = '';
								if (doc_frm.doc_description.columns[i].fieldname in aData)
									vv = aData[doc_frm.doc_description.columns[i].fieldname];
								doc_frm.doc_description.columns[i].widget_object.cntr = aData.id;
								doc_frm.doc_description.columns[i].widget_object.doc = aData;
								element = doc_frm.doc_description.columns[i].widget_object.getUserVal(vv);


								var ctd = $('<td class="editrow"/>');
								doc.trow.append(ctd);
								ctd.html('<div>' + element + '</div>');
								//ctd.html(element);
								//vv=JSON.stringify(vv);
								ctd.attr('title', removeescapeHtml(element));
								ctd.attr('fieldname', doc_frm.doc_description.columns[i].fieldname);
								ctd.dblclick(function (event) {
									editcell(dt_table, doc_frm.doc_description, $(event.target));
								});
							}
						}
						doc.trow.click(function (e) { //live("click", function (e) {
							var Row = $(this);
							doc_data_base = Row.data('doc');
							var id = doc_data_base.id;

							var index = jQuery.inArray(id, dt_table.aSelected);
							if (e.ctrlKey) {
								if (index === -1) {
									dt_table.aSelected.push(id);
								} else {
									if (Row.hasClass('geos_row_selected')) {
										Row.removeClass('geos_row_selected');
									}
									dt_table.aSelected.splice(index, 1);
								}
								Row.toggleClass("geos_row_selected");
							}
							else {
								dt_table.aSelected = [];
								dt_table.aSelected.push(id);
								$('.geos_row_selected', container).removeClass("geos_row_selected");
								Row.toggleClass("geos_row_selected");
								
								for(var is=0; is<dt_table.rowselect.length; is++){
									dt_table.rowselect[is](Row);
								}
							}

							$('.g_layer', $('#layer_list')).each(function (index) {
								ml = $(this).data("mlayer");
								if (ml.dataset_id == metaid)
									ml.view_selection(dt_table.aSelected);
							});
							setTimeout(function () {
								dt_table.updateButtons();
							}, 150);

						});
					}
					else{
						if(rowtemplate!=undefined){							
							doc_frm.show_form(container, aData, '', true, rowtemplate);							
						}
						else{
							var row=$('<li class="span4 linkitem"><div class="thumbnail"><i title="Edit" class="icon-pencil"></i><i title="Delete" class="icon-remove close"></i><div class="object_text"></div></div></li>');
							row.data('doc',aData);
							cardlist.append(row);						
							doc_frm.show_form($('.object_text',row), aData, '', true);
						}
					}	
				}
				if(dt_table.groupdata.grouping){
					drawrow(aData);
				}
				else{
					fieldcalc.calculatefields(doc_frm.doc_description, aData, 'view', undefined, function(error, doc){						
						drawrow(doc);
					});
				}
				
			}

			$('.detailstab', tbody).hide();
			$(".detailslink").html('View details');

			
			dt_table.updateButtons();
			dt_table.aSelected = [];
		}


		if (this.tabledata !== undefined) {
			filldata(this.tabledata, iDisplayStart, this.row_on_page_count);
			dt_table.rowcount = this.tabledata.length;
			dt_table.update_pagination();
		}
		else {
			str_res = JSON.stringify(data);
			$.ajax({
				url: sAjaxSource + '&count_rows=1&iDisplayStart=' + iDisplayStart + '&iDisplayLength=' + this.row_on_page_count,
				dataType: "json",
				contentType: 'application/json',
				data: str_res,
				method: "POST",
				type: "POST",
				success: function (data) {
					if (data.aaData) {
						filldata(data.aaData, 0, data.aaData.length);
						dt_table.rowcount = data.iTotalRecords;
					}
					else {
						filldata([], 0, 0);
						dt_table.rowcount = 0;
					}
					dt_table.update_pagination();
				}
			});
		}
		this.dataquery = data;
	};

	this.update_pagination_list=function(item) {
		v = item.attr('page');
		old = dt_table.curent_page;
		switch (v) {
			case 'first':
				dt_table.curent_page = 0;
				break;
			case 'last':
				dt_table.curent_page = Math.floor(dt_table.rowcount / dt_table.row_on_page_count);
				break;
			case 'prev':
				if (dt_table.curent_page == 0)
					return;
				dt_table.curent_page--;
				break;
			case 'next':
				if (dt_table.curent_page >= Math.floor(dt_table.rowcount / dt_table.row_on_page_count))
					return;
				dt_table.curent_page++;
				break;
			default:
				dt_table.curent_page = parseInt(v);
				break;
		}
		if (old == dt_table.curent_page)
			return;
		dt_table._fnReDraw();
	}
	this.groupingdata = function () {
		fields = '';
		dt_table.groupdata = {};
		dt_table.groupdata.grouping = true;
		for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
			if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
				doc_frm.doc_description.columns[i].widget_object.gr_fn = doc_frm.doc_description.columns[i].widget_object.group.val();
				dt_table.groupdata[doc_frm.doc_description.columns[i].fieldname] = doc_frm.doc_description.columns[i].widget_object.group.val();
			}
		}
		this._fnReDraw();
	}
	this.update_pagination = function () {
		var table_pages = $('#table_pages', container);
		table_pages.empty();
		var start_page = Math.max(0, this.curent_page - 2);
		var end_page = Math.min(Math.floor(this.rowcount / this.row_on_page_count), Math.max(4, this.curent_page + 2));
		for (var i = start_page; i <= end_page; i++) {
			var p;
			s_i = (i + 1) + '';
			if (this.curent_page == i)
				p = $('<a page="' + i + '" tabindex="0" class="fg-button ui-button ui-state-default ui-state-disabled">' + s_i + '</a>');
			else
				p = $('<a page="' + i + '"  tabindex="0" class="fg-button ui-button ui-state-default">' + s_i + '</a>');
			table_pages.append(p);
		}
		$('a.fg-button', container).click(function () {
			item = $(this);
			dt_table.update_pagination_list(item);
		});

		if (this.curent_page == 0) {
			$("a[page='first']", container).addClass('ui-state-disabled');
			$("a[page='prev']", container).addClass('ui-state-disabled');
		}
		else {
			$("a[page='first']", container).removeClass('ui-state-disabled');
			$("a[page='prev']", container).removeClass('ui-state-disabled');
		}
		if (this.curent_page == Math.floor(this.rowcount / this.row_on_page_count)) {
			$("a[page='last']", container).addClass('ui-state-disabled');
			$("a[page='next']", container).addClass('ui-state-disabled');
		}
		else {
			$("a[page='last']", container).removeClass('ui-state-disabled');
			$("a[page='next']", container).removeClass('ui-state-disabled');
		}
		iDisplayStart = this.curent_page * this.row_on_page_count + 1;
		iDisplayEnd = Math.min(iDisplayStart + this.row_on_page_count - 1, this.rowcount);
		$("#page_str", container).html('Записи с ' + iDisplayStart + ' до ' + iDisplayEnd + ' из ' + this.rowcount + ' записей');
	};

	this.add_crud_panel=function (panel){
		if(themeobj.rights.charAt(1)=='0' && !user.isAdmin)
			return;
		var command_panel = $('<a class="lang btn btnCustomAddRow" role="button" href="#" title="Add"><i class="fas fa-plus"></i></a> \
			<a class="lang btn btnCustomEditRow" href="#" title="Edit" disabled="disabled"><i class="fas fa-edit"></i></a> \
			<a class="lang btn btnCustomBaseAddRow" href="#" title="Duplicate" disabled="disabled"><i class="far fa-copy"></i></a> \
			<a class="lang btn btnDeleteRow" role="button" href="#" title="Delete" disabled="disabled"><i class="far fa-trash-alt"></i></a> \
			');
		panel.append(command_panel);
		
		$('.btnCustomAddRow', panel).click(function () {
			if(doc_frm.doc_description.addlink){
				var win = window.open(doc_frm.doc_description.addlink, '_blank');
 				win.focus();
			}
			else
				dt_table.edit_form('add');
		});

		$('.btnCustomBaseAddRow', panel).click(function () {
			dt_table.edit_form('baseadd');
		});
			
		$('.btnCustomEditRow', panel).click(function () {
			var geos_row_selected = $(this).parents('tr');
			if (geos_row_selected.length == 0)
				geos_row_selected = $('.geos_row_selected', dt_table.tbody);
			if (geos_row_selected.length == 1){
				if(doc_frm.doc_description.editlink){
					doc_data = $(geos_row_selected).data('doc');
					editlink=doc_frm.doc_description.editlink.replace('#ID', doc_data.id);	
					var win = window.open(editlink, '_blank');
					win.focus();
				}
				else
					dt_table.edit_form('edit', geos_row_selected);
			}
		});
		
		$('.btnDeleteRow', panel).click(function(){ 
			dt_table.delete();	
		});	

		if(themeobj.objectstory){			
			var story = $('<a class="lang btn storybutton" href="#"  title="Object history" disabled="disabled"><i class="fas fa-history"></i></a> ');
			panel.append(story);
			story.click(function () {
				var geos_row_selected = $(this).parents('tr');
				if (geos_row_selected.length == 0)
					geos_row_selected = $('.geos_row_selected', dt_table.tbody);
				if (geos_row_selected.length == 1){
					doc = geos_row_selected.data('doc');
					
					$.getJSON("/dataset/list?f=101&iDisplayStart=0&iDisplayLength=100&f_datasetid=" + metaid+'&f_docid='+doc.id, function (data) {
						//if (data.aaData.length == 0)
						//	return;
						
						
						var meta_date = {
							"fieldname": "date",
							"title": "data",
							"description": "",
							"visible": true,
							"widget": {
								"name": "date",
								properties: {									
								}
							}
						};
		
						var date_widget = $.widgets.get_widget(meta_date);
						
						var meta_user = {
							"fieldname": "user",
							"title": "data",
							"description": "",
							"visible": true,
							"widget": {
								"name": "user",
								properties: {									
								}
							}
						};
		
						var user_widget = $.widgets.get_widget(meta_user);
						
						var AddDlg = $('<div class="image_form" title="Object story"><div class="objectview"></div><hr/><table><thead><tr><th>N</th><th>Date</th><th>User</th><th>View</th></thead> <tbody></tbody> </table></div>');
						var table_body=$('tbody',AddDlg);
						for(var i=0; i<data.aaData.length; i++){
							var row=$("<tr><td>"+(i+1)+"</td><td class='date'></td><td class='user'></td><td><i class='icon-zoom-in'></i></td></tr>");
							row.data('doc', JSON.parse(data.aaData[i].data))
							cont=$('.date',row);
							cont.html(date_widget.getUserVal( data.aaData[i].edited_on));
							cont=$('.user',row);
							cont.html(user_widget.getUserVal( data.aaData[i].created_by));							
							table_body.append(row);
							
						}
						
						$('.icon-zoom-in',table_body).click(function(){
							var geos_row_selected = $(this).parents('tr');
							if (geos_row_selected.length == 0)
								geos_row_selected = $('.geos_row_selected', dt_table.tbody);
							if (geos_row_selected.length == 1){
								var hdoc = geos_row_selected.data('doc');
								var doc_frmEdit = new doc_template(themeobj);
								doc_frmEdit.init_form(function () {
									doc_frmEdit.show_form($('.objectview',AddDlg), hdoc, '', true);
									var btn=$('<button class="btn add">Recovery state</button>');
									$('.objectview',AddDlg).append(btn);
									btn.click(function(){
										if(!hdoc.f_id){																			
											var doc_id=hdoc.id;											
											hdoc.f_id=doc.id;
											hdoc.id=undefined;										
										}
										str_res=JSON.stringify({document:hdoc});
										console.log(str_res);
										//serverlog({commiting:res});
									
										$.ajax({
											type: "POST",
											url: "/dataset/update?f=" + metaid,
											contentType : 'application/json',
											data: str_res,
											success: function(msg){
												msg=JSON.parse(msg);
												if (msg.status != 'ok') {
													var error_box = $('.text-error', AddDlg);
													if (error_box.length > 0) {
														if(typeof msg==='string')
															error_box.html(msg);
														else
															error_box.html(msg.status);
													}
													else
														alert(msg.data);
													return;
												}
												dt_table._fnReDraw();
												AddDlg.dialog("close");
												AddDlg.remove();
												dt_table.updateButtons();
											},
											error: function(){
												alert('error');
											}
										});
									});
								});
							}
						});
						AddDlg.dialog({
							resizable: true,
							height: 'auto',
							width: 'auto',
							modal: false,
							buttons: {
							}
						});
					});
				}
				
			});
		}
	}
	
	this.add_olap_panel=function(panel){
		var command_panel = $('<label class="btn btn-outline-secondary lang">	<input class="tcmd_check generalization" type="checkbox" autocomplete="off"> Generalization \
					<a href="#" class="btn btnApply olapApply" style="display: none;"><i class="far fa-check-circle"></i></a>\
					<a href="#" class="lang btn btnApply olapReset" style="display: none;"><i class="fas fa-ban"></i></a></label>');
		panel.append(command_panel);
		
		$('.generalization', command_panel).click(function () {
			var tr = $('#dt_olap', container);
			if (this.checked) {
				tr.show("slow");
				$('.olapApply', container).show("slow");
				$('.olapReset', container).show("slow");
			} else {
				tr.hide(500);
				$('.olapApply', container).hide(500);
				$('.olapReset', container).hide(500);
			}
		});	
		
		$('.olapApply', container).click(function () {
			dt_table.groupingdata();
		});	
		
		$('.olapReset', container).click(function () {
			dt_table.olapreset();
		});		
	}
	
	this.add_config_panel=function(panel){
		var command_panel = $('<a class="lang btn" href="#" title="Settings"><i class="fas fa-wrench"></i></a>');
		panel.append(command_panel);
		command_panel.click(function(){
			var eSettings = $('<div class="settings_form" title="Table settings">\
			<div class="btn-toolbar mb-3" role="toolbar" aria-label="Toolbar with button groups">\
				<div class="btn-group mr-2" role="group" aria-label="First group">\
					<a class="lang btn" href="/dataset/tabstructure?id='+ metaid + '" target="blank" title="Structure"><i class="fas fa-ellipsis-h"></i></a>\
					<a class="lang btn" href="/dataset/users?id='+ metaid + '" target="blank" title="Users"><i class="fas fa-users"></a></i>\
					<a class="lang btn data_load" href="#" title="Import"><i class="fas fa-file-import"></i></a>\
					<a class="lang btn metadataload" href="#" title="Copy metadata url"><i class="fas fa-receipt"></i></i></a>\
					<input type="hidden" id="metadatatmp2019"/>\
				</div>\
				<div class="input-group">\
					<label  class="lang btn">rows on page</label>\
					<select class="form-control tcmd_select rowcount" size="1" ><option value="10" selected="selected">10</option><option value="25">25</option><option value="50">50</option><option\ value="100">100</option></select>\
				</div>\
				<div class="input-group form-check">\
					<input  class="form-control decimal form-check-input" type="checkbox" />\
					<label  class="lang btn form-check-label">decimal coordinates</label>\
				</div>\
			</div>\
			</div>');
			
			$('.decimal', eSettings).prop('checked', decimalcoordinates);
			

			eSettings.dialog({
				resizable: true,
				height: 'auto',
				width: 'auto',
				modal: false,
				buttons: {
					"Close": function () {				
						$(this).dialog("close");
						eSettings.remove();
					}
				}
			});
			$('.data_load', eSettings).click(function () {
				eSettings.dialog("close");
				eSettings.remove();
				importdata(dt_table);
				
			});
			$('.metadataload', eSettings).click(function () {
				const el = document.createElement('textarea');
				el.value = "http://"+window.location.hostname+"/dataset/list?f="+geodatasetguid+"&f_guid="+dt_table.dataset_id;
				document.body.appendChild(el);
				el.select();
				document.execCommand('copy');
				document.body.removeChild(el);
				eSettings.append('<div class="alert alert-success" role="alert"> The metadata url is copied!  </div>');

				// $('#metadatatmp2019').val();
				// var copyText = document.getElementById("metadatatmp2019");
				// copyText.select();
				// document.execCommand("copy");
				// alert("Copied the text: " + copyText.value);
				
			});
			
			$('.rowcount', eSettings).change(function () {
				dt_table.row_on_page_count = parseInt($(this).val());
				dt_table.curent_page = 0;
				dt_table._fnReDraw();
			});	
			$('.decimal', eSettings).click(function () {
				if (this.checked) {
					$('input').trigger('decimal.coordinates');
					decimalcoordinates = true;
				} else {
					$('input').trigger('minute_sec.coordinates');
					decimalcoordinates = false;
				}
				dt_table._fnReDraw();
			});		
			
	
		});
	}	
		
	this.add_filter_panel=function(panel){
		var command_panel = $('<label class="btn btn-outline-secondary">\
    <input class="tcmd_check filterswitch" type="checkbox" id="filters"  autocomplete="off"> Filters  \
			<a href="#" class="lang btn btnApply" id="filterReset" style="display: none;"><i class="fas fa-eraser"></i></a></label>');
		panel.append(command_panel);
		$('#filters', command_panel).click(function () {
			var tr = $('#dt_fltr', container);
			if (this.checked) {
				tr.show("slow");
				$('#filterReset', container).show("slow");
			} else {
				tr.hide(500);
				$('#filterReset', container).hide(500);
			}
		});
		
		$('#filterReset', container).click(function () {
			$('.filter-term', container).trigger('click');
		});		
	}
	
	this.add_tabmanage_panel=function(panel){
		var command_panel = $('<a class="lang btn" href="#" id="btn2Map" title="Show on map"><i class="fas fa-globe-europe"></i></a> \
		<a class="lang btn" href="#" id="btnShowDiagram" title="Diagram"><i class="fas fa-chart-pie"></i></a>\
		<a class="lang btn" href="#" id="btnTableSave" title="Export"><i class="fas fa-file-download"></i></a>');
		panel.append(command_panel);
		
		$('#btnTableSave', container).click(function(){
			dt_table.table_export();	
		});	
		
		$('#btn2Map', panel).click(function () {
			dt_table.movemap2object();
		});			
		
		$('#btnShowDiagram', container).click(function () {
			showdiagrams(dt_table);
		});		
	}
	
	this.add_print_panel=function(panel){
		var print_str = '';
		if ($.isArray(themeobj.print_template)) {
			for (var i = 0; i < themeobj.print_template.length; i++) {
				print_str += '<a class="btn btnPrint" href="#" title ="' + themeobj.print_template[i].name + '" id="btnPrint"><i class="fas fa-print"></i></a>';
			}
		}
		else {
			print_str = '<a class="btn btnPrint" href="#" placeholder="" id="btnPrint" title="Print the rows"><i class="fas fa-print"></i></a>';
		}
		var command_panel = $(print_str);
		panel.append(command_panel);
		('.btnPrint', command_panel).click(function () {
			dt_table.print($(this).attr('title'));
		});		
	}
	
	this.comandpanel=function(){
		var panel = $('<div id="command_panel" class="tabcomands"><div class="cmdpanel"></div></div>');
		container.append(panel);
		panel=$('.cmdpanel', panel);
		if (metaid == 100) {
			this.add_filter_panel(panel);			
		}
		else {
			
			if (typeof simple === 'object') {
				this.add_crud_panel(panel);
				this.add_filter_panel(panel);
				this.add_olap_panel(panel);
			}
			else if (simple == true) {
				this.add_crud_panel(panel);
			}
			else {
				this.add_crud_panel(panel);
				this.add_filter_panel(panel);
				this.add_olap_panel(panel);
				this.add_print_panel(panel);
				this.add_tabmanage_panel(panel);
				this.add_config_panel(panel);
			}
		}
	}
	
	this.drawtablefilters=function(){
		var filtersfields = $('#dt_fltr', container);
		for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
			if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
				var div_el = $('<th class="ui-state-default dt_th"></th>');
				filtersfields.append(div_el);
				if (doc_frm.doc_description.columns[i].widget_object.hasFilter()) {
					w_properties = doc_frm.doc_description.columns[i].widget_object.getDatatableProperties();
					doc_frm.doc_description.columns[i].$AutocompleteSource = "/dataset/list?f=" + metaid + '&unique=' + themeobj.unique;
					//console.log(doc_frm.doc_description.columns[i].fieldname);
					//console.log(w_properties.sFilterName);
					if (w_properties.sFilterName && $[w_properties.sFilterName]) {
						if (!doc_frm.doc_description.columns[i].filtervalue)
							doc_frm.doc_description.columns[i].filter_object = new $[w_properties.sFilterName](div_el, doc_frm.doc_description.columns[i], dt_table, doc_frm.doc_description.columns[i].filtervalue);
						if (doc_frm.doc_description.columns[i].filtervalue)
							dt_table.FilterValues[doc_frm.doc_description.columns[i].fieldname] = doc_frm.doc_description.columns[i].filtervalue;
					}
				}
			}
		}	
	}
	
	this.drawolapfields=function(){
		var olapfields = $('#dt_olap', container);
		for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
			if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
				var div_el = $('<th class="ui-state-default dt_th"></th>');
				olapfields.append(div_el);
				doc_frm.doc_description.columns[i].widget_object.olap(div_el, '', themeobj);
			}
		}
	}
	
	this.pagination=function(){
		var pagination = $('<div class="tabpagination pagination">\
		<div>\
		<a page="first" tabindex="0" class="fg-button">Первая</a>\
		<a page="prev"  tabindex="0" class="fg-button" >&lt;</a>\
		<span id="table_pages">\
		</span>\
		<a page="next" tabindex="0" class="fg-button" >&gt;</a>\
		<a page="last" tabindex="0" class="fg-button" >Последняя</a>\
		<label class="table_info" id="page_str"></label>\
		</div>\
		</div>');
		
		container.append(pagination);
				$('.fg-button', container).click(function () {
			item = $(this);
			dt_table.update_pagination_list(item);
		});
	}
	
	this.drawtablehead=function(){
		
		var tab_container = '';// '<th class="action_th"></th><th class="action_th"></th>';
		var sumwidth = 0;
		for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
			if (metaid == 5 && (doc_frm.doc_description.columns[i].fieldname == 'password' || doc_frm.doc_description.columns[i].fieldname == 'password2'))
				continue;
			if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
				var width = '70px';
				if (doc_frm.doc_description.columns[i].size !== undefined) {
					v = parseInt(doc_frm.doc_description.columns[i].size);
					if (isNaN(v))
						v = 70;
					width = v + 'px';
					sumwidth += v;
				}
				else {
					width = '70px';
					sumwidth += 70;
				}
				sumwidth += 40;
				tab_container += '<th class="lang fixed_th"  title="' + doc_frm.doc_description.columns[i].description + '" fieldname="' + doc_frm.doc_description.columns[i].fieldname + '">' + doc_frm.doc_description.columns[i].title + '</th>';
			}
		}

		tab_container = '<div class="tabcontainer"><div class="tabscroll"><table class="table '+doc_frm.doc_description.unique+'" id="' + doc_frm.doc_description.tablename + '" ><thead><tr>' + tab_container;

		tab_container += '</tr><tr id = "dt_olap" style="display: none"></tr><tr id = "dt_fltr" style="vertical-align: top;display: none"></tr></thead><tfoot><tr></tr></tfoot><tbody id="tbody"></tbody></table></div><div class="dataTables_processing" style="visibility: hidden;">Подождите...</div></div>';
		var tab_container_node = $(tab_container);
		container.append(tab_container_node);

		table1=$('table', tab_container_node);
		table1.data('table', this);
		$('.fixed_th', container).click(function () {
			icon = $(this);
			th = icon;
			var fieldname = th.attr('fieldname');
			if (fieldname === undefined || fieldname == '')
				return;
			var found = false;
			for (var i = 0; i < dt_table.sort.length; i++) {
				if (dt_table.sort[i].fieldname == fieldname) {
					found = true;
					if (dt_table.sort[i].dir) {
						dt_table.sort[i].dir = false;
						icon.removeClass('headSortUp');
						icon.addClass('headSortDown');
					}
					else {
						dt_table.sort.splice(i, 1);
						icon.removeClass('headSortDown');
					}
					break;
				}
			}
			if (!found) {
				f = { fieldname: fieldname, dir: true };
				dt_table.sort.push(f);
				icon.addClass('headSortUp');
			}
			dt_table._fnReDraw();
		});

		// var pagination = $('<div class="pagination">\
		// <div>\
		// <a page="first" tabindex="0" class="fg-button">Первая</a>\
		// <a page="prev"  tabindex="0" class="fg-button" >&lt;</a>\
		// <span id="table_pages">\
		// </span>\
		// <a page="next" tabindex="0" class="fg-button" >&gt;</a>\
		// <a page="last" tabindex="0" class="fg-button" >Последняя</a>\
		// <label class="table_info" id="page_str"></label>\
		// </div>\
		// </div>');
		

		dt_table.drawtablefilters();		
		dt_table.drawolapfields();
		if (simple == true) {
			console.log('pagination is closed');
		}
		else
			this.pagination();
	}

	sAjaxSource = sAjaxSource + '&unique=' + themeobj.unique;
	this.sAjaxSource = sAjaxSource;
	
	doc_frm.init_form(function () {
		for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
			if (doc_frm.doc_description.columns[i].widget_object != undefined) {
				doc_frm.doc_description.columns[i].widget_object.table=dt_table;
			}
		}
		$("tbody tr", container).delegate("hover", function () {
			var id = this.id;
			$(this).attr('title', 'Идентификатор: ' + id);
		});

	});
	
	
	this.movemap2object=function(){
		var c_rows = $('tr.geos_row_selected', container);
		if (c_rows.length == 0)
			return;
		g_rect = { max_x: -1000, min_x: 1000, max_y: -1000, min_y: 1000 };

		for (var j_Row = 0; j_Row < c_rows.length; j_Row++) {
			var geos_row_selected = $(c_rows[j_Row]);
			var doc_data_base = geos_row_selected.data('doc');
			for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
				if (doc_frm.doc_description.columns[i].widget_object != undefined && doc_frm.doc_description.columns[i].widget_object.getrect != undefined) {
					c_rect = doc_frm.doc_description.columns[i].widget_object.getrect(doc_data_base[doc_frm.doc_description.columns[i].fieldname]);
					g_rect.min_x = Math.min(c_rect.min_x, g_rect.min_x);
					g_rect.max_x = Math.max(c_rect.max_x, g_rect.max_x);
					g_rect.min_y = Math.min(c_rect.min_y, g_rect.min_y);
					g_rect.max_y = Math.max(c_rect.max_y, g_rect.max_y);
				}
			}
		}
		if (g_rect.min_x != 1000) {
			map.setView([(g_rect.min_y + g_rect.max_y) / 2, (g_rect.min_x + g_rect.max_x) / 2], 10/*map.getZoom()*/);
		}
	}
	
	this.olapreset=function(){
		fields = '';
		dt_table.groupdata = {};
		for (var i = 0; i < doc_frm.doc_description.columns.length; i++) {
			if (doc_frm.doc_description.columns[i].visible === undefined || doc_frm.doc_description.columns[i].visible) {
				doc_frm.doc_description.columns[i].widget_object.group.val("none");
				doc_frm.doc_description.columns[i].widget_object.gr_fn = null;
			}
		}
		dt_table._fnReDraw();
	}
	
	
	this.print=function(template_name){
		var a = $(this);
		var PopupPage = "<html><head><style>.card{ display: inline-block;}</style><link href='/css/normalize.css' rel='stylesheet' type='text/css' /><link href='/css/print_cards.css' rel='stylesheet' type='text/css' /></head><body class='alla'>";
		var DataArray = [];
		var c_rows = $('tr.geos_row_selected', container);
		if (c_rows.length > 0) {
			for (var j_Row = 0; j_Row < c_rows.length; j_Row++) {
				doc_data = $(c_rows[j_Row]).data('doc');
				DataArray.push(doc_data);
			}
		}
		else {
			var c_rows = $('tbody tr', container);
			for (var j_Row = 0; j_Row < c_rows.length; j_Row++) {
				doc_data = $(c_rows[j_Row]).data('doc');
				DataArray.push(doc_data);
			}
		}

		for (var j_Row = 0; j_Row < DataArray.length; j_Row++) {
			style = "display: inline-block; border-style: dashed; border-color:#cccccc; border-width: 1px;";
			page_num = j_Row % 6;
			if (page_num == 0 || page_num == 1)
				style += "border-top:white;"
			if (page_num == 4 || page_num == 5)
				style += "border-bottom:white;"
			if (page_num % 2 == 0)
				style += "border-left:white;"
			else
				style += "border-right:white;"
			if ((metaid == 1225 || metaid == 7) && template_name == 'Cards')
				var template_row = '<div style="' + style + '">' + doc_frm.print_document(DataArray[j_Row], template_name);
			else
				var template_row = doc_frm.print_document(DataArray[j_Row], template_name);
			if ((metaid == 1225 || metaid == 7) && template_name == 'Cards')
				template_row += '</div>'
			PopupPage += template_row;
		}
		PopupPage += '</body></html>';
		var printWin = window.open('', '', 'toolbar,width=800,height=600,scrollbars,resizable,menubar');
		printWin.document.write(PopupPage);
		printWin.focus();
	}
	
	
	
	this.table_export=function(){
		v_sAjaxSource=sAjaxSource;
		$.each(dt_table.groupdata, function(index, value) {
			if(value!='' && value!==undefined){
				v_sAjaxSource+='&g_'+index+'='+value;
			}
		});			
		$.each(dt_table.FilterValues, function(index, value) {
			if(value!='' && value!==undefined)
				v_sAjaxSource+='&f_'+index+'='+value;
		});			
		v_sAjaxSource+='&to_file=true&format=kml';
		window.location.href =v_sAjaxSource;
	}
	
		
	this.delete=function(){
		var geos_row_selected=$('.geos_row_selected', container);
		if(geos_row_selected.length!=1) return;
		var doc_data_base=geos_row_selected.data('doc');			
		
		if(confirm("Delete the record?")){
			if(dt_table.tabledata===undefined){
				res={};
				res.dataset_id=doc_frm.doc_description.metaid;		
				res.f_id=doc_data_base.id;
				str_res=JSON.stringify({document:res});
				$.ajax({
					type: "POST",
					url: "/dataset/delete?f=" + doc_frm.doc_description.metaid,
					contentType : 'application/json',
					data: str_res,
					success: function(msg){
						if(msg=='ok'){
							dt_table._fnReDraw();
							dt_table.updateButtons();
						}
						else{
							alert(msg);
						}
					}
				});
			}
			else{
				dt_table.deleteddata.push(doc_data_base);
				doc_data_base.operation='delete';
				dt_table.tabledata.splice (dt_table.tabledata.indexOf(doc_data_base) , 1);
				dt_table._fnReDraw();
				dt_table.updateButtons();							
			}
		}
	}
	
	

	this.layers_destroy = function () {
		for (var i = 0; i < this.layers.length; i++) {
			this.layers[i].destroy();
		}
	}

	this.edit_form = function (mode, geos_row_selected) {
		if(dt_table.maxrowcount!=0 && (mode == 'add' || mode == 'baseadd') && dt_table.tabledata.length>=dt_table.maxrowcount){
			alert('Можно ввести не более '+dt_table.maxrowcount + ' записей.');
			return;
		}
		if (dt_table.groupdata.grouping) {
			alert('Вы находитесь в режиме обобщения данных (aggregation). Для редактирования нужно выйти из него.');
			return;
		}
		//tab_container_node.hide();
		var doc_data;
		if (mode == 'add') {
			doc_data = {};
			viewform();
		}
		else if (mode == 'baseadd') {
			if (!geos_row_selected)
				var geos_row_selected = $('tr.geos_row_selected', container);
			doc_data_base = $(geos_row_selected).data('doc');
			doc_data = clone(doc_data_base);			
			fieldcalc.calculatefields(doc_frm.doc_description, doc_data, 'copy', undefined, function(error, doc){
				doc_data=doc;
				doc_data.id = undefined;
				viewform();
			});			
		}
		else {
			if (!geos_row_selected)
				var geos_row_selected = $('tr.geos_row_selected', container);
			doc_data = $(geos_row_selected).data('doc');
			viewform();
		}
		function viewform(){
			var eRowform = $('<div id="g_edit_form" class="g_edit_form" title="Edit form"></div>');
			//var eRowform=$("#g_edit_form",container );
			eRowform.empty();
			eRowform.html('<div class="text-error"></div>')


			var doc_frmEdit = new doc_template(themeobj);
			dt_table.doc_frmEdit=doc_frmEdit;
			doc_frmEdit.init_form(function () {
				doc_frmEdit.show_form(eRowform, doc_data, '', mode == 'view');
				var btn_view = $('#btnCustomView', container);
				var btn_save = $('#btnAdd', container);
				var btn_cancel = $('#btnCancel', container);
				if (mode == 'view') {
					btn_save.hide();
					btn_cancel.show();
					btn_cancel.html('Close');
				}
				else {
					btn_save.show("slow");
					btn_cancel.show("slow");
					btn_cancel.html('Cancel');
				}
				if (mode == 'add' || mode == 'baseadd') {
					eRowform.attr('title', 'Adding form')
				}

				eRowform.dialog({
					resizable: true,
					height: '600',
					width: 'auto',
					modal: false,
					buttons: {
						"Save": function () {
							if (dt_table.tabledata === undefined) {
								res = doc_frmEdit.save_form(function (res, msg) {
									if (msg.status != 'ok') {
										var error_box = $('.text-error', eRowform);
										if (error_box.length > 0) {
											error_box.html(msg.status);
											eRowform.animate({
												scrollTop: 0
											}, 500);
										}
										else
											alert(msg.data);
										return;
									}
									dt_table._fnReDraw();
									dt_table.updateButtons();
									//tab_container_node.show();
									btn_view.trigger('click');
									setTimeout(function () {
										$('.g_layer', $('#layer_list')).each(function (index) {
											ml = $(this).data("mlayer");
											ml.redraw();
										});

									}, 150);
									doc_frmEdit.close();
									eRowform.dialog("destroy");
									
									// eRowform.remove();
								}, true);
							}
							else {
								res = doc_frmEdit.save_form(function (res, msg) {
									if (msg.status != 'ok') {
										var error_box = $('.text-error', eRowform);
										if (error_box.length > 0) {
											error_box.html(msg.data);
											eRowform.animate({
												scrollTop: 0
											}, 500);
										}
										else
											alert(msg.data);
										return;
									}
									if (mode == 'add' || mode == 'baseadd') {
										dt_table.tabledata.push(res);
									}
									else {
										for (var index in doc_data) {
											doc_data[index] = res[index];
										}
									}
									dt_table._fnReDraw();
									dt_table.updateButtons();
									//if(tab_container_node)
									//	tab_container_node.show();
									doc_frmEdit.close();
									
									eRowform.dialog("destroy");
									// eRowform.remove();
								}, false);
							}

						},
						"Cancel": function () {
							dt_table.updateButtons();
							doc_frmEdit.close();
							eRowform.dialog("destroy");
							// eRowform.remove();
						}
					}
				});
				changelang();
				translate();

			});
			
			dt_table.updateButtons();
			translate();
		}
	};

	this.layers_destroy = function () {
		for (var i = 0; i < this.layers.length; i++) {
			this.layers[i].destroy();
		}
	}
	

	return this;
}
