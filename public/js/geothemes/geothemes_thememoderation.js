function moderationDecideOnSpecificRecord(themeid, recordid, accept) {
	$.ajax({
		url: '/geothemes/ajaxhub',
		dataType: 'json',
		async: true,
		data: {action: 'moderationsetnewrecordstate', themeid: themeid, recordid: recordid, accept: accept},
		success: function(data) {
			if (data.status === 'error') {
				alert('Error while setting new record state (moderation)');
			} else {
				ThemeDT.fnDraw();
			}
		}
	});
} //end moderationDecideOnSpecificRecord()

$(document).ready(function() {
	if (typeof THEME_JSON !== 'undefined' && THEME_JSON.act_list.indexOf(3) === -1 && THEME_JSON.act_list.indexOf(4) === -1) {
		if (THEME_JSON.moderated === true) {
			$('#edit-tab').prepend('<div class="alert"><strong>Notice</strong> After submission added record will be on moderation, <strong>do not</strong> submit it again</div>');
		}
	} //end if
});