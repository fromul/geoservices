/*
	Globals
*/
var id, schema, tablename,ontology_table;

$(document).ready(function() {
	id        = parseInt(getQueryVariable('id'));
	schema    = getQueryVariable('schema');
	tablename = getQueryVariable('tablename');
	ontology = getQueryVariable('ontology');
	if (ontology == 'create') {
		
	}
	else if (ontology > 0) {
		$.getJSON("/ontology/getField",{ontology: ontology, ajax: 'true'}, function(data){
			var fields = $("#tablefields");
			ontology_table = ontology;
			data.forEach(function(field){
				var field_json = {fieldname: field.fieldname, title: field.title, description: field.description, widget: {name: field.widget, properties: JSON.parse(field.option) } };
				createfield(field_json, fields);

			});
		});
	}

	if (id > 0) {
		$.getJSON("/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id="+id,{ajax: 'true'}, function(data){
			var table_json    = JSON.parse(data.aaData[0].JSON);
			frm = createform(table_json, id);
			$("#mj_container_general").empty().append(frm);	
			$('#fade_overlay').remove();
		});

/*		$.getJSON("/geothemes/ajaxhub", {action: 'checkifthemeisavailableformoderation', id: id}, function(data){
			if (data.status === 'success') {
				if (data.data === false) {
					$('#collapseOneAndHalf').removeClass('in');
					$('#moderation_settings').html('<p>Current theme does not support moderation. Please, recreate this theme from scratch.</p>');
				} //end if
			} else {
				$('#collapseOneAndHalf').removeClass('in');
				$('#moderation_settings').html('<p class="muted">Error occured</p>');
			} //end if
		});
*/		
	} 
	else if(tablename!='' && tablename!=null) {
		$.getJSON("/geothemes/genjson",{tablename: tablename}, function(data){
			frm = createform(data, id);
			$("#mj_container_general").empty().append(frm);	
			$('#fade_overlay').remove();
		});	
	}

	else {
		frm = createform(null);
		$("#mj_container_general").empty().append(frm);
		$('#fade_overlay').remove();
	}

});
