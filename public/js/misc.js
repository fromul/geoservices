/*
*	### Server and Client shared script
*/

/**
*	getQueryVariable
*/
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
	return null;
}

/**
*	Pausing browser
*/
function pausecomp(millis) {
	var date = new Date();
	var curDate = null;

	do { curDate = new Date(); } 
	while(curDate-date < millis);
} 

function changeVectorProjectionLine(initepsg, desiredepsg, feature) {
	initepsg = new OpenLayers.Projection(initepsg);
	desiredepsg = new OpenLayers.Projection(desiredepsg);
	var totalvectors = feature.geometry.components.length;
	for (var j = 0; j < totalvectors; j++) {
		feature.geometry.components[j] = feature.geometry.components[j].transform(initepsg, desiredepsg);
	}
	return feature;
}

function changeVectorProjection(initepsg, desiredepsg, feature) {
	initepsg = new OpenLayers.Projection(initepsg);
	desiredepsg = new OpenLayers.Projection(desiredepsg);
	var totalvectors = feature.geometry.components.length;
	var actualvectors = feature.geometry.components;
	for (var i = 0; i < totalvectors; i++) {
		var totalpoints = actualvectors[i].components.length;
		var actualpoints = actualvectors[i].components;
		for (var j = 0; j < totalpoints - 1; j++) {
			feature.geometry.components[i].components[j] = feature.geometry.components[i].components[j].transform(initepsg, desiredepsg);
		}
	}
	return feature;
}

function hexToRgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}
function functionApplyTags(funcbody) {
	return str_replace('\'', 'XXQUOTEXX', str_replace('"', '\'', str_replace('\n', 'XXNEWLINEXX', funcbody)));
}
function functionReverseTags(funcbody, foreval) {
	if (foreval) return str_replace('XXQUOTEXX', '\'', str_replace('XXNEWLINEXX', '\n', funcbody));
	return str_replace('XXQUOTEXX', '\'', str_replace('XXNEWLINEXX', '\n', funcbody));
}
function functionGetName(funcbody) {
	var body = functionReverseTags(funcbody);
	var beg = body.indexOf(' ');
	var end = body.indexOf('(');
	return trim(substr(body, beg, (end - beg)));
}


/**
*	JS implementation of PHP str_replace function
*/
var str_replace = function (search, replace, subject, count) {
  var i = 0,
    j = 0,
    temp = '',
    repl = '',
    sl = 0,
    fl = 0,
    f = [].concat(search),
    r = [].concat(replace),
    s = subject,
    ra = Object.prototype.toString.call(r) === '[object Array]',
    sa = Object.prototype.toString.call(s) === '[object Array]';
  s = [].concat(s);
  if (count) {
    this.window[count] = 0;
  }

  for (i = 0, sl = s.length; i < sl; i++) {
    if (s[i] === '') {
      continue;
    }
    for (j = 0, fl = f.length; j < fl; j++) {
      temp = s[i] + '';
      repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
      s[i] = (temp).split(f[j]).join(repl);
      if (count && s[i] !== temp) {
        this.window[count] += (temp.length - s[i].length) / f[j].length;
      }
    }
  }
  return sa ? s : s[0];
}

/**
* tranlsit - trnforming cyrillic to latin string
*/
var translit = function (str) {
	if (str === undefined) {
		str = 'notdef';
	};
	if (str == undefined) {
		str = 'notdef';
	};
	var transl = new Array();
    transl['А']='A';     transl['а']='a';
    transl['Б']='B';     transl['б']='b';
    transl['В']='V';     transl['в']='v';
    transl['Г']='G';     transl['г']='g';
    transl['Д']='D';     transl['д']='d';
    transl['Е']='E';     transl['е']='e';
    transl['Ё']='Yo';    transl['ё']='yo';
    transl['Ж']='Zh';    transl['ж']='zh';
    transl['З']='Z';     transl['з']='z';
    transl['И']='I';     transl['и']='i';
    transl['Й']='J';     transl['й']='j';
    transl['К']='K';     transl['к']='k';
    transl['Л']='L';     transl['л']='l';
    transl['М']='M';     transl['м']='m';
    transl['Н']='N';     transl['н']='n';
    transl['О']='O';     transl['о']='o';
    transl['П']='P';     transl['п']='p';
    transl['Р']='R';     transl['р']='r';
    transl['С']='S';     transl['с']='s';
    transl['Т']='T';     transl['т']='t';
    transl['У']='U';     transl['у']='u';
    transl['Ф']='F';     transl['ф']='f';
    transl['Х']='X';     transl['х']='x';
    transl['Ц']='C';     transl['ц']='c';
    transl['Ч']='Ch';    transl['ч']='ch';
    transl['Ш']='Sh';    transl['ш']='sh';
    transl['Щ']='Shh';    transl['щ']='shh';
    transl['Ъ']='';     transl['ъ']='';
    transl['Ы']='Y';    transl['ы']='y';
    transl['Ь']='';    transl['ь']='';
    transl['Э']='E';    transl['э']='e';
    transl['Ю']='Yu';    transl['ю']='yu';
    transl['Я']='Ya';    transl['я']='ya';
	transl['_']='_';  transl[' ']='_';  
    var result='';
    for(i = 0; i < str.length; i++) {
        if (transl[str[i]] != undefined) {
			result+=transl[str[i]]; 
		} else {
			result+=str[i];
		}
    }
    return result;
}

/**
* trim - getting rid of spaces
*/
var trim = function(str) {
	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

/**
* randomString - generating random string
*/
var randomString = function (length, numerical) {
	if (numerical)
	{
		var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
	}
	else
	{
		var chars = '1234567980'.split('');
	}

    if (! length) { length = Math.floor(Math.random() * chars.length); }
    var str = '';
    for (var i = 0; i < length; i++) { str += chars[Math.floor(Math.random() * chars.length)];}
    return str;
}

/**
*	JS implementation of PHP isset() function
*
*	@param obj - input object we are trying to examine
*	@return bool
*/
var isset = function (obj) {
	if (typeof obj != 'undefined') return true;
	else return false;
}


/**
*	It is an util function for obtaining correct object name
*
*	@param name - input name to make correct
*	@return correct name
*/
var getcorrectobjname = function (name) {
	name=name.toLowerCase();
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	var digits='0123456789';
	var res=''
	name=translit(name);
	for(var i=0; i<name.length; i++){
		if (res.length==0 && digits.indexOf(name.charAt(i))!=-1 ) continue;
		if(chars.indexOf(name.charAt(i))!=-1)
			res=res+name.charAt(i);
		if (res.length>15) break;
	}
	return res;	
}

/**
*	JS implementation of PHP explode() function
*/
var explode = function (delimiter, string, limit) {
  if ( arguments.length < 2 || typeof delimiter == 'undefined' || typeof string == 'undefined' ) return null;
  if ( delimiter === '' || delimiter === false || delimiter === null) return false;
  if ( typeof delimiter == 'function' || typeof delimiter == 'object' || typeof string == 'function' || typeof string == 'object'){
    return { 0: '' };
  }
  if ( delimiter === true ) delimiter = '1';

  // Here we go...
  delimiter += '';
  string += '';

  var s = string.split( delimiter );


  if ( typeof limit === 'undefined' ) return s;

  // Support for limit
  if ( limit === 0 ) limit = 1;

  // Positive limit
  if ( limit > 0 ){
    if ( limit >= s.length ) return s;
    return s.slice( 0, limit - 1 ).concat( [ s.slice( limit - 1 ).join( delimiter ) ] );
  }

  // Negative limit
  if ( -limit >= s.length ) return [];

  s.splice( s.length + limit );
  return s;
}
 
/**
*	JS implementation of PHP implode() function
*/
var implode = function (glue, pieces) {
  // http://kevin.vanzonneveld.net
  // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   improved by: Waldo Malqui Silva
  // +   improved by: Itsacon (http://www.itsacon.net/)
  // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
  // *     example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
  // *     returns 1: 'Kevin van Zonneveld'
  // *     example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
  // *     returns 2: 'Kevin van Zonneveld'
  var i = '',
    retVal = '',
    tGlue = '';
  if (arguments.length === 1) {
    pieces = glue;
    glue = '';
  }
  if (typeof(pieces) === 'object') {
    if (Object.prototype.toString.call(pieces) === '[object Array]') {
      return pieces.join(glue);
    }
    for (i in pieces) {
      retVal += tGlue + pieces[i];
      tGlue = glue;
    }
    return retVal;
  }
  return pieces;
}

/**
*	JS implementation of PHP substr function
*/
var substr = function (str, start, len) {
  var i = 0,
    allBMP = true,
    es = 0,
    el = 0,
    se = 0,
    ret = '';
  str += '';
  var end = str.length;

  // BEGIN REDUNDANT
  this.php_js = this.php_js || {};
  this.php_js.ini = this.php_js.ini || {};
  // END REDUNDANT
  switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
  case 'on':
    // Full-blown Unicode including non-Basic-Multilingual-Plane characters
    // strlen()
    for (i = 0; i < str.length; i++) {
      if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
        allBMP = false;
        break;
      }
    }

    if (!allBMP) {
      if (start < 0) {
        for (i = end - 1, es = (start += end); i >= es; i--) {
          if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
            start--;
            es--;
          }
        }
      } else {
        var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
        while ((surrogatePairs.exec(str)) != null) {
          var li = surrogatePairs.lastIndex;
          if (li - 2 < start) {
            start++;
          } else {
            break;
          }
        }
      }

      if (start >= end || start < 0) {
        return false;
      }
      if (len < 0) {
        for (i = end - 1, el = (end += len); i >= el; i--) {
          if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
            end--;
            el--;
          }
        }
        if (start > end) {
          return false;
        }
        return str.slice(start, end);
      } else {
        se = start + len;
        for (i = start; i < se; i++) {
          ret += str.charAt(i);
          if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
            se++; // Go one further, since one of the "characters" is part of a surrogate pair
          }
        }
        return ret;
      }
      break;
    }
    // Fall-through
  case 'off':
    // assumes there are no non-BMP characters;
    //    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
  default:
    if (start < 0) {
      start += end;
    }
    end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
    // PHP returns false if start does not fall within the string.
    // PHP returns false if the calculated end comes before the calculated start.
    // PHP returns an empty string if start and end are the same.
    // Otherwise, PHP returns the portion of the string from start to end.
    return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
  }
  return undefined; // Please Netbeans
}

/**
*	JS implementation of PHP substr_replace function
*/
var substr_replace = function (str, replace, start, length) {
  if (start < 0) { // start position in str
    start = start + str.length;
  }
  length = length !== undefined ? length : str.length;
  if (length < 0) {
    length = length + str.length - start;
  }
  return str.slice(0, start) + replace.substr(0, length) + replace.slice(length) + str.slice(start + length);
}


var is_number = function (str) {
	return !isNaN(str);
}

var substr = function (str, start, len) {
  // Returns part of a string
  //
  // version: 909.322
  // discuss at: http://phpjs.org/functions/substr
  // +     original by: Martijn Wieringa
  // +     bugfixed by: T.Wild
  // +      tweaked by: Onno Marsman
  // +      revised by: Theriault
  // +      improved by: Brett Zamir (http://brett-zamir.me)
  // %    note 1: Handles rare Unicode characters if 'unicode.semantics' ini (PHP6) is set to 'on'
  // *       example 1: substr('abcdef', 0, -1);
  // *       returns 1: 'abcde'
  // *       example 2: substr(2, 0, -6);
  // *       returns 2: false
  // *       example 3: ini_set('unicode.semantics',  'on');
  // *       example 3: substr('a\uD801\uDC00', 0, -1);
  // *       returns 3: 'a'
  // *       example 4: ini_set('unicode.semantics',  'on');
  // *       example 4: substr('a\uD801\uDC00', 0, 2);
  // *       returns 4: 'a\uD801\uDC00'
  // *       example 5: ini_set('unicode.semantics',  'on');
  // *       example 5: substr('a\uD801\uDC00', -1, 1);
  // *       returns 5: '\uD801\uDC00'
  // *       example 6: ini_set('unicode.semantics',  'on');
  // *       example 6: substr('a\uD801\uDC00z\uD801\uDC00', -3, 2);
  // *       returns 6: '\uD801\uDC00z'
  // *       example 7: ini_set('unicode.semantics',  'on');
  // *       example 7: substr('a\uD801\uDC00z\uD801\uDC00', -3, -1)
  // *       returns 7: '\uD801\uDC00z'
  // Add: (?) Use unicode.runtime_encoding (e.g., with string wrapped in "binary" or "Binary" class) to
  // allow access of binary (see file_get_contents()) by: charCodeAt(x) & 0xFF (see https://developer.mozilla.org/En/Using_XMLHttpRequest ) or require conversion first?
  var i = 0,
    allBMP = true,
    es = 0,
    el = 0,
    se = 0,
    ret = '';
  str += '';
  var end = str.length;

  // BEGIN REDUNDANT
  this.php_js = this.php_js || {};
  this.php_js.ini = this.php_js.ini || {};
  // END REDUNDANT
  switch ((this.php_js.ini['unicode.semantics'] && this.php_js.ini['unicode.semantics'].local_value.toLowerCase())) {
  case 'on':
    // Full-blown Unicode including non-Basic-Multilingual-Plane characters
    // strlen()
    for (i = 0; i < str.length; i++) {
      if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
        allBMP = false;
        break;
      }
    }

    if (!allBMP) {
      if (start < 0) {
        for (i = end - 1, es = (start += end); i >= es; i--) {
          if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
            start--;
            es--;
          }
        }
      } else {
        var surrogatePairs = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
        while ((surrogatePairs.exec(str)) != null) {
          var li = surrogatePairs.lastIndex;
          if (li - 2 < start) {
            start++;
          } else {
            break;
          }
        }
      }

      if (start >= end || start < 0) {
        return false;
      }
      if (len < 0) {
        for (i = end - 1, el = (end += len); i >= el; i--) {
          if (/[\uDC00-\uDFFF]/.test(str.charAt(i)) && /[\uD800-\uDBFF]/.test(str.charAt(i - 1))) {
            end--;
            el--;
          }
        }
        if (start > end) {
          return false;
        }
        return str.slice(start, end);
      } else {
        se = start + len;
        for (i = start; i < se; i++) {
          ret += str.charAt(i);
          if (/[\uD800-\uDBFF]/.test(str.charAt(i)) && /[\uDC00-\uDFFF]/.test(str.charAt(i + 1))) {
            se++; // Go one further, since one of the "characters" is part of a surrogate pair
          }
        }
        return ret;
      }
      break;
    }
    // Fall-through
  case 'off':
    // assumes there are no non-BMP characters;
    //    if there may be such characters, then it is best to turn it on (critical in true XHTML/XML)
  default:
    if (start < 0) {
      start += end;
    }
    end = typeof len === 'undefined' ? end : (len < 0 ? len + end : len + start);
    // PHP returns false if start does not fall within the string.
    // PHP returns false if the calculated end comes before the calculated start.
    // PHP returns an empty string if start and end are the same.
    // Otherwise, PHP returns the portion of the string from start to end.
    return start >= str.length || start < 0 || start > end ? !1 : str.slice(start, end);
  }
  return undefined; // Please Netbeans
}

// ---------------------- Only Client Scripts -----------------------

/**
* Synchronous AJAX requests
*/
var getRemote = function (url) {
    return $.ajax({
        type: "GET",
        url: url,
        async: false,
    }).responseText;
}

var postRemote = function (url, data) {
    return $.ajax({
        type: "POST",
		data: data,
        url: url,
        async: false,
    }).responseText;
}

var c = function(msg) {
	console.log(msg);
}

var getWidgetsHTML = function () {
	var output = '<option value="null" selected>Выберите виджет</option>';
	for (var k = 0; k < $.widgets.widgetlist.length; k++) { output += '<option value="' +  $.widgets.widgetlist[k][0] + '">' + $.widgets.widgetlist[k][0] + '</option>';}
	return output;
}


function cleanNewLines(text)
{
	text = text.replace(/(\r\n|\n|\r)/gm,"");
	return text;
}

if (typeof module !== "undefined") {
	module.exports = {
		translit: translit,
		randomString: randomString,
		isset: isset,
		explode: explode,
		implode: implode,
		substr: substr,
		substr_replace: substr_replace,
		str_replace: str_replace,
		trim: trim,
		is_number: is_number,
		getcorrectobjname:getcorrectobjname,
		functionReverseTags: functionReverseTags,
		functionApplyTags: functionApplyTags,
		clone: clone,
		hexToRgb: hexToRgb,
		functionGetName: functionGetName,
		cleanNewLines: cleanNewLines,
		c: c,
		/* Add more */
	};
}

function getUrlVars() {
    var i,
        tmp     = [],
        tmp2    = [],
        objRes   = {},
		strQuery = window.location.search;
    if (strQuery != '') {
        tmp = (strQuery.substr(1)).split('&');
        for (i = 0; i < tmp.length; i += 1) {
            tmp2 = tmp[i].split('=');
            if (tmp2[0]) {
                objRes[tmp2[0]] = tmp2[1];
            }
        }
    }
    return objRes;
}

