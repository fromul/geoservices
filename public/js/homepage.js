var metacashe=[];
var metacashelistners={};

function getdatasetmeta(dataset_id , callback){
	if(dataset_id in metacashe){		
		callback(null, clone(metacashe[dataset_id]));
		return;
	}
	else{
		if (dataset_id in metacashelistners){
			metacashelistners[dataset_id].push(callback);
		}
		else{
			metacashelistners[dataset_id] = [callback];
			if (isNaN(dataset_id)){
				url="/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_guid=" + dataset_id;
			}
			else{
				url="/dataset/list?f=100&iDisplayStart=0&iDisplayLength=100&s_fields=JSON&f_id=" + dataset_id;
			}
			$.getJSON(url, function (data) {
				if (data.status==="error" || data.aaData.length == 0){
					callback('missing dataset meta');
					return;
				}
				var table_json = JSON.parse(data.aaData[0].JSON);
				metacashe[dataset_id]=table_json;
				table_json.metaid = table_json;
				for (var i=0; i<metacashelistners[dataset_id].length; i++){
					metacashelistners[dataset_id][i](null, table_json);
				}
				delete metacashelistners[dataset_id];
			});
		}
	}
	
}

function getfilemeta(dataset_id , callback){

	url="/datafile/meta?first_is_head=true&path=" + dataset_id;
		
	$.getJSON(url, function (data) {
		if (data.status==="error" || data.aaData.length == 0){
			callback('missing dataset meta');
			return;
		}
		var table_json = JSON.parse(data.aaData[0].JSON);
		metacashe[dataset_id]=table_json;
		table_json.metaid = table_json;
		for (var i=0; i<metacashelistners[dataset_id].length; i++){
			metacashelistners[dataset_id][i](null, table_json);
		}
		delete metacashelistners[dataset_id];
	});


	
}

function t(str){
	if(str=='profile')
		return 'Профиль'
	return str;
}

var dragging = false;

function updateMapstate(){
	if($('.bodycontainer').is(':visible') && $('.datatable ').length>0){
		$('#map').height($(window).height() - $('.bodycontainer').height() + "px");	
		if(map)		
			map.invalidateSize(true);
	}		
	else{
		$('#map').height("100%");
		if(map!==undefined)	
			map.invalidateSize(true);
	}
}

function changelang(lang){
	
}

function DrawMenu(container, filter){
	$.ajax({
		url: '/dataset/list?f=4&iDisplayStart=0&iDisplayLength=100&sort=[{"fieldname":"weight"}]'+filter,
		dataType: "json",									
		success: function(data) {
			container.empty();			
			//var menu=$('<ul id="admin-menu" class="nav nav-pills admin"/>');
			var menu=$('<ul class="nav"/>');
			container.append(menu);
			var isAdmin = false;
			var islogged = false;
			var userDataString = $.cookie('userData') || localStorage.user || '{}';
			var user = JSON.parse(decodeURIComponent(userDataString == 'undefined' ? '{}' : userDataString));				
			var userWelcomeOrLoginBox = $('#user-welcome-or-login');
			if (user.username){	// logged in
				islogged=true;
				isAdmin=user.isAdmin;				
			}
			$.map(data.aaData, function(item) {
				if(item.hostname && item.hostname!='' && item.hostname!=location.hostname)
					return;
				if(item.mtype==1 || isAdmin){
					var nodeitem=$('<li><a id="'+item.name+'" href="'+item.link+'">'+item.caption+'</a></li>');
					nodeitem.click(function(event){
						router.route($(event.target).attr('href'));
						return false;
					});				
					menu.append(nodeitem);
				}
			});
		}		
	});
}

function showdiv(link, div){	
		link.bind( "click", function( event ) {
			if($(event.target).parent().hasClass('active')){
				$('.nav li').removeClass('active');
				div.hide();
			}
			else{
				$('.nav li').removeClass('active');
				$(event.target).parent().addClass('active');
				$('.content_div').hide();
				div.show();
			}
		});
	}

var langwords={
	Filters:'Фильтр',
	Generalization:'Обобщение',
	name:'Название',
	"rows on page":"Строк на странице",
	"Start loading":'Начать загрузку',
	"Add":"Добавить",
	"Edit": "Редактировать",
	"Delete":"Удалить",
	"Duplicate" : "Дублировать",
	"Show on map": "Показать на карте",
	"Apply":"Применить",
	"Reset":"Сбросить",
	"Import":"Импорт",
	"Export":"Экспорт",
	"Structure":"Структура",
	"Count":"Количество",
	"Group by": "Группировать",
	"Username": "Имя пользователя",
	"Password": "Пароль",
	"Open File Dialog":"Открытие файла",
	"Login":"Войти",
	"Register":"Регистрация",
}	
	function translate(lang){
		$('.lang').contents().filter(function(){
			return this.nodeType !== 1;
		}).each( function( key, value ) {
			word=value.nodeValue.trim();
			if(word in langwords){
				value.nodeValue=langwords[word];
			}
		});
		
		$('.lang').each( function( key, value ) {
			word=$(value).attr("title");
			if(word){
				word=word.trim();
				if(word in langwords){
					$(value).attr("title", langwords[word]);
				}
			}
			word=$(value).attr("value");
			if(word){
				word=word.trim();
				if(word in langwords){
					$(value).attr("value", langwords[word]);
				}
			}
		});
		
	}
	
function changelang(lang){
	if(lang=='rus'){
		$('.lang_e').hide();
		$('.lang_r').show();
		$('.li_rus').addClass('active');
		$('.li_eng').removeClass('active');
	}
	else{
		$('.lang_e').show();
		$('.lang_r').hide();	
		$('.li_rus').removeClass('active');
		$('.li_eng').addClass('active');		
	}
	if($.collapse)
		$('.topmenu').collapse('toggle');
}

	
$(document).ready(function() {
	// cleanslate.init();
	
	$('#fullsize').click(function(e){
		$('#mapcontainer').height("0px");
		$('#map').height("0px");
		$('.bodycontainer').height(($(window).height() - 38) + "px");
		$.cookie("bodycontainerheight", $(window).height() + "px");
		map.invalidateSize(true);
	});
	$('#minimize').click(function(e){
		$('#mapcontainer').height(($(window).height() - 38) + "px");
		$('#map').height(($(window).height() - 38) + "px");
		$('.bodycontainer').height("38px");
		$.cookie("bodycontainerheight", "38px");
		map.invalidateSize(true);
	});
	$('#medium').click(function(e){
		$('.bodycontainer').height("50%");	
		$('#mapcontainer').height("50%");
		$('#map').height("100%");
		$.cookie("bodycontainerheight", "50%");
		map.invalidateSize(true);
	});


	$('#changebodycontainerheight').mousedown(function(e){
		e.preventDefault();
		dragging = true;
		var main = $('.mapcontaier');
		$(document).mousemove(function(e){
			if($(window).height() - e.pageY < 30 )
				return;			
			$('#mapcontainer').height(e.pageY + "px");
			$('#map').height(e.pageY + "px");
			$('.bodycontainer').height( ($(window).height() - e.pageY) + "px");
			$.cookie("bodycontainerheight", ($(window).height() - e.pageY) + "px");
			map.invalidateSize(true);
		});		
	});	
		
	$(document).mouseup(function(e){
		if (dragging) {
			$(document).unbind('mousemove');
			dragging = false;
		}
	});
});