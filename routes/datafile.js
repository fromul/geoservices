var express = require('express');
var router = express.Router();
var datafile = require('../controllers/datafile');





/* GET document listing. */
router.get('/list', datafile.list  );
router.post('/list', datafile.list);


/* Get file meta. */
router.get('/meta', datafile.meta);


/* import file. */
router.post('/import', datafile.import);


module.exports = router;