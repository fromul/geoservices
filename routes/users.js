var config = require("../conf.json");
var express = require('express');
var router = express.Router();
var LocalStrategy  = require('passport-local').Strategy;
var crypto = require('crypto');
var passport       = require('passport');

const JwtStrategy = require('passport-jwt').Strategy;
ExtractJwt = require('passport-jwt').ExtractJwt;

var geo_users=require("../controllers/middleware/geo_users");


const localStrategy=new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password'
}, function(username, password,done){
  console.log(username, password);
  var sql="SELECT ID, isadmin, locked, username, foldername, hash FROM public.users WHERE USERNAME='"+username+"'";
  console.log(sql);
  global.pgPool.query(sql, function(error, sql_res, unofields) {
    // console.log('I am here 1');    
    if (error != null){      
        console.log(error);      
        done(error, null);
    }
    else{
      // console.log('I am here 2.', sql_res.rows.length);    
      if(sql_res.rows.length==1){
        var hashedpswd    = geo_users.passwordhash(password);
        console.log('db hash', hashedpswd, sql_res.rows[0].hash)
        if(hashedpswd==sql_res.rows[0].hash){
          // console.log(sql_res.rows[0]);
          // console.log('password is ok');
          done(null, sql_res.rows[0]);
        }
        else{
          done(null, false, { message: 'Incorrect password.' }) 
        }
        
      }
      else{
        done(null, false, { message: 'Incorrect username.' });       //done(null, false, { message: 'Incorrect password.' }) 
      }
      
    }
  });  
});

passport.use('localStr', localStrategy);

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


const authenticate = passport.authenticate('localStr', {session: true});


function authenticationMiddleware () {
  return function (req, res, next) {
      if (req.isAuthenticated()) {
          return next()
      }
      res.redirect('/')
  }
}

router.post('/login', authenticate, (req, res) => {
  var userstr=JSON.stringify(req.user);
  res.cookie('userData', userstr, { maxAge: 9000000, httpOnly: false });
  res.send({status:'ok', 'userData': req.user}) ;
});


router.get('/test', (req, res) => {  
  if (req.isAuthenticated()){
    console.log('isAuthenticated');
  }
  else{
    console.log('is not Authenticated');
  }
  if (req.meta){
    console.log('has meta');
  }
  else{
    console.log('has not meta');
  }
  res.send({status:'ok', data:req.user}) ;
});


router.get('/logout', (req, res) => {
  console.log('logout');
  req.logout();
  res.cookie('userData', '', { maxAge: 900000, httpOnly: false });
  
  res.send({status:'ok'}) ;
});



passport.serializeUser(function(user, done) {
  console.log('serializeUser');
  done(null, user.id);
});


passport.deserializeUser(function(id, done) {
  // console.log('id',id);
  var sql="SELECT ID, isadmin, locked, username, foldername FROM public.users WHERE ID='"+id+"'";
  
  global.pgPool.query(sql, function(error, sql_res, unofields) {
  
    if (error != null){        
        done(error);        
    }
    else{
      // console.log('I am here 2', sql_res.rows.length);    
      if(sql_res.rows.length==1){
        // console.log(sql_res.rows[0]);
        if(sql_res.rows[0].foldername==null)
          sql_res.rows[0].foldername=sql_res.rows[0].username;
        done(null, sql_res.rows[0]);
      }
      else{
        done('Incorrect username.');
      }
      
    }
  });
 
});

module.exports = router;
