var meta_manager;

var rfm = require("../rfm/rfm");
var fs = require('fs');
var iconv            = require('iconv-lite');
var path            = require('path');
var mime            = require('mime');
var rootpath = process.cwd() + '/';
var config = require(rootpath+"/conf.json");
var dir_separator='/';

// 	var rootpath     = process.cwd() + '/',
// 	path             = require('path'),
// //	iconv            = require('iconv'),	
// 	queryString      = require('querystring'),

// 	urlman           = require('url');
	// common           = require(rootpath + 'routes/common/lib/common');
	// var	misc             = require(rootpath + 'themes/core/cleanslate/public/js/misc'),
	// rfm              = require(rootpath + 'modules/community/rfm/rfm'),
	// mime = require('mime'),
	// exec             = require('child_process').exec,
	// expressValidator = require('express-validator');
	// check            = require('validator').check,
	// sanitize         = require('validator').sanitize;
	
	// postgres         = require(rootpath + 'modules/community/postgres/postgres');
var geo_meta_table = require("./geo_meta_table");
	// var client = postgres.connect();
var meta_manager = new geo_meta_table(100);
	// meta_manager.init();
var geo_table = require("./geo_table");
	// var m_date = require("./lib/date");
	// var m_geom = require("./lib/geom");
var geo_map = require("./geo_map");
	
	// mail = require("mailer");
	// var request = require('request');



module.exports.init=function(module, objects) {
	meta_manager=objects.meta_manager;
}

function getqueryparams(req){
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.queryString(attrname);
	for (var attrname in req.body)
		qparams[attrname] = req.bodyString(attrname);
	return qparams;
}


/**
*	getdbjsonbyid
*/
function get_table_json(req, id, callback){	
	//calipso.modules.postgres.fn.init_gis_user(req, function() {	
		if('session' in req && 'user' in req.session && 'user' in req.session && req.session.user!=null && 'id' in req.session.user)
			meta_manager.curent_user_id=req.session.user.id;
		else
			meta_manager.curent_user_id='';
		meta_manager.get_table_json({}, id, function (meta){
			callback(meta);
		});
	//});
}



/**
* getting file from dataset
*/
exports.file = function (req, res, next) {
	var qparams = getqueryparams(req);

	var dataset_id      = _qv(qparams, 'dataset_id');
	var row_id 		    = _qv(qparams, 'row_id');
	var fieldname 		    = _qv(qparams, 'fieldname');
	var filehesh 		    = _qv(qparams, 'filehesh');
	var userid 		    = _qv(qparams, 'userid');

	console.log('filehesh=',filehesh );
	if('l1_'==filehesh.substr(0,3))
		filehesh = filehesh.slice(3);
	console.log('filehesh=',filehesh	);
	
	var file_path=rfm.getpath(filehesh);
	
	console.log('d=',file_path	);
	var filequery={};
	filequery['f_'+fieldname]=filehesh;

	function senderror(){
		error={status:'ok',message:'The file does not exist in the dataset.'};
		res.end(JSON.stringify(error),"UTF-8");
	}
	// testing correct filepath
	if (!fs.existsSync(file_path)) {
		console.log('point 0');
		if(userid!=''){
			var sql="SELECT username, foldername FROM public.users WHERE ID='"+userid+"'";
			console.log(sql);
			global.pgPool.query(sql, function(error, sql_res, unofields) {
				if (error != undefined){        
					console.log('point 1');
					senderror();       
				}
				else{
					if(sql_res.rows.length==1){
						console.log('point 2');
						if(sql_res.rows[0].foldername==null)
							sql_res.rows[0].foldername=sql_res.rows[0].username;
						var root = config.modules.rfm.config.mainrootdir;
						

						pos=file_path.indexOf(root);
						if (pos==0){
							cpath=file_path.slice(root.length);
							file_path = root + dir_separator + sql_res.rows[0].foldername + dir_separator + cpath;
						}
						accesssend();
					}
					else{
						console.log('point 3', sql_res.rows.length);
						senderror();
					}			
				}
			});
		}
		else{
			senderror();
		}  
	}
	else{
		accesssend();
	}

	function sendfile(){
		
		console.log('file_path='+file_path);
		
		
		if (fs.existsSync(file_path)) {
			var filename = path.basename(file_path);
			var mimetype = mime.lookup(file_path);
			res.setHeader('Content-disposition', 'attachment; filename=' + filename);
			res.setHeader('Content-type', mimetype);
			var filestream = fs.createReadStream(file_path);
			filestream.on("data", function (chunk) {
				res.write(chunk, "binary");
			});
			filestream.on("end", function () {
				//console.log("Served file " + file_path);
				res.end();
			});
			filestream.on("error", function(err){
				console.log(err);
				res.write(err + "\n");
				res.end();
			});
		}
		else{
			senderror()
			console.log(error);
		}
	}
	
	
	function accesssend(){
		console.log(req.user);
		console.log('row_id', row_id);
		if(row_id=='' || row_id == null || row_id=='NULL' || row_id=='null'){
			if(username !='' && rfm.getaccess(req, filehesh)){
				sendfile();
				return;
			}
			else{
				error={status:'error', message:'You do not have access to the file.'};
				res.end(JSON.stringify(error),"UTF-8");
				console.log(error);
			}
		}
			
		
		
		from_list=req.g_table.buildfrom();
		where_list=req.g_table.buildwhere(req, filequery, true, false);
		sSQL='SELECT main.created_by, main.edited_by, main.'+fieldname + ' FROM ' + from_list + ' WHERE ' + where_list + ' LIMIT 1';
		console.log(sSQL);
		global.pgPool.query(sSQL, function(error, query_result, unofields2) {
			console.log('query_result.rows.length ',query_result.rows.length);
			if (error === undefined && query_result.rows.length==1)	{					
				//console.log('user_id='+user_id);
				sendfile();
			}
			else if(error ==! undefined){			
				console.log(error);
				res.end(JSON.stringify(error),"UTF-8");
			}
			else{
				console.log('Access denied');
				res.end(JSON.stringify({status:'error', message:'Access denied'}),"UTF-8");
			}
		});
		
	}
	
}

/**
* getting file from dataset
*/
function get_icon(req, res, options, next) {
	var qparams = req.query;
	for (var attrname in req.body) {
		qparams[attrname] = req.body[attrname];
	}	
	var filename 		    = _qv(qparams, 'filename');
	var map_file_dir=calipso.config.getModuleConfig('geothemes', 'mapfiledir');
	var mainrootdir = calipso.config.getModuleConfig("rfm", "mainrootdir");
	file_path=calipso.modules.rfm.fn.getpath(filename);
	ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
	file_path=map_file_dir + '/images/' + filename + '.' + ext;	
	
	if (fs.existsSync(file_path)) {		
		var mimetype = mime.lookup(file_path);
		res.setHeader('Content-disposition', 'attachment; filename=' + filename);
		res.setHeader('Content-type', mimetype);
		var filestream = fs.createReadStream(file_path);
		filestream.on("data", function (chunk) {
			res.write(chunk, "binary");
		});
		filestream.on("end", function () {
		   console.log("Served file " + file_path);
		   res.end();
		});
		filestream.on("error", function(err){
			console.log(err);
			res.write(err + "\n");
			res.end();
		});
	}
	else{
		error={message:'The file does not exist in the dataset.'};
		res.end(JSON.stringify(error),"UTF-8");
		console.log(error);
	}
}





/**
 * object list method
 */
exports.list = function(req, res, next) {
	// console.log(req.user)
	var dataset_id = req.queryString('f');
	var user_id='';
	var settings={};
	// console.log(req.session);
	if(req.user){
		user_id=req.user.id;
		settings.user=req.user;
	}

	var qparams = getqueryparams(req);
	
	if(req.g_table==undefined){
		res.end(JSON.stringify({status: "error", data: 'empty metadata12'}), "UTF-8");
		return;
	}
	// var g_table = new geo_table(user_id, dataset_id, meta_manager, mdobj, settings);
	var g_map = new geo_map(user_id, dataset_id, meta_manager, req.meta);

	var sLimit='';
	iDisplayStart= _qv(qparams, 'iDisplayStart');
	iDisplayLength=_qv(qparams, 'iDisplayLength');
	if (iDisplayStart!='' && iDisplayLength!='')	{
		sLimit = 'LIMIT ' + iDisplayLength + ' OFFSET ' + iDisplayStart;
	} //end if
	else{
			sLimit = 'LIMIT 10000 OFFSET 0';
	}
	s_fields=_qv(qparams,'s_fields');
	includefields=[];
	if(s_fields!='')
		includefields=s_fields.split(',');
	var sSQL='';
	if(_qv(qparams, 'to_file')=='true'){// сохранение в файл
		if(_qv(qparams, 'format')=='kml'){// kml
			sSQL=req.g_table.documentquery(req, includefields,qparams, '', function(error, result){
				if (error === null)	{							
					var kml     = '<?xml version="1.0" standalone="yes"?>\
<kml xmlns="http://earth.google.com/kml/2.1">\
<Document>';							
					
					for (var r = 0; r < result.aaData.length; r++){	
						row = result.aaData[r];
						name='';
						desc=row.id;
						coords='';	
						if(row){													
							for (fieldname in result.aaData[r]){
								v=row[fieldname];
								
								if(v==null || v===undefined || !(typeof v ==='string'))
									continue;
								v=v.replace(/&/g,"");
								if(fieldname =='binom')
									name+=v;
								else if (v.indexOf('MULTIPOINT')!=-1){								
									var re = /\d+.\d+/g;
									var nms = v.match(re);
									coords=nms[0]+', '+nms[1];											
									
								}
								else if (fieldname=='site1' || fieldname=='site2'){
									desc+=', '+v;
								}									
							}
							kml+='<Placemark><name>'+name+'</name><description>'+desc+'</description><Point><coordinates>'+coords+'</coordinates></Point></Placemark>\n';
						}
					}
					kml+='</Document>\
</kml>';
					res.setHeader('Content-disposition', 'attachment; filename=table.kml');
					res.setHeader('Content-type', 'application/vnd.google-earth.kml+xml');
					res.end(kml,"UTF-8");
				}
				else{
					console.log(error);
					res.end(JSON.stringify(error),"UTF-8");
				}
			});
		}
		else{
			sSQL=req.g_table.documentquery(req, includefields,qparams, '', function(error, result){
				if (error === null)	{
					var aaData     = '';
					delimeter='';
					if(result.aaData.length>0){
						delimeter='';
						for (fieldname in result.aaData[0]){
							aaData+=delimeter+fieldname;
							delimeter='\t';
						}
						aaData+='\n';
					}
					for (var r = 0; r < result.aaData.length; r++){
						delimeter='';
						for (fieldname in result.aaData[r]){
							v=result.aaData[r][fieldname];
							if(typeof v ==='object'){
								str=''
								for (sname in v){
									if(typeof v[sname]!='object')
										str+=v[sname];
								}
								aaData+=delimeter+str;
							}
							else
								aaData+=delimeter+v;
							delimeter='\t';
						}
						aaData+='\n';
					}
					res.setHeader('Content-disposition', 'attachment; filename=table.txt');
					res.setHeader('Content-type', 'text/plain');
					res.end(aaData,"UTF-8");
				}
				else{
					console.log(error);
					res.end(JSON.stringify(error),"UTF-8");
				}
			});
		}
	}
	else{
		sSQL=req.g_table.documentquery(req, includefields,qparams, sLimit, function(error, result){
			//console.log('I am here3');
			if (error === null)	{
				res.format = "json";
				res.end(JSON.stringify(result),"UTF-8");
			}
			else{
				console.log(error);
				res.end(JSON.stringify(error),"UTF-8");
			}
		});
	}
}

/**
 * delete method
 */
exports.delete = function(req, res, next) {
	var qparams = getqueryparams(req);

	qparams.document=req.body.document;
	
	if(req.g_table==undefined){
		res.end(JSON.stringify({status: "error", data: 'empty metadata'}), "UTF-8");
		return;
	}
	// console.log(qparams.document);
	if(typeof qparams.document=='string'){
		console.log('string');
		qparams.document=JSON.parse(qparams.document);
	}

	if(Array.isArray(qparams.document)){
		var ids=[];
		function docdel(object_number){
			if(qparams.document.length == object_number){
				resp={status:'ok'}
				res.end(JSON.stringify(resp),"UTF-8");
				return;
			}
			req.g_table.delete(req,qparams.document[object_number], function(result){
				//ids.push(result.data);
				docdel(object_number+1);
			});
		}
		docdel(0);
	}
	else{
		req.g_table.delete(req,qparams.document, function(result){
			res.end(result,"UTF-8");
			return;
		});
	}
			
		
}

/**
 * update method
 */
exports.update = function(req, res, next) {
	//console.log('bldata_update');
	var qparams = getqueryparams(req);

	qparams.document=req.body.document;
	
	if(req.g_table==undefined){
		res.end(JSON.stringify({status: "error", data: 'empty metadata'}), "UTF-8");
		return;
	}
	// console.log(qparams.document);
	if(typeof qparams.document=='string'){
		console.log('string');
		qparams.document=JSON.parse(qparams.document);
	}
	
	if(req.meta.dataset_id==5 && _qv(qparams, 'f_confirmhash' )!='' ){
		qparams.document={f_confirmhash: _qv(qparams, 'f_confirmhash' ) };
	}
	if(Array.isArray(qparams.document)){
		var ids=[];
		function upd(object_number){
			if(qparams.document.length == object_number){
				resp={status:'ok'}
				res.end(JSON.stringify(resp),"UTF-8");
				return;
			}
			req.g_table.update(req, qparams.document[object_number], function(result){
				//ids.push(result.data);
				upd(object_number+1);
			});
		}
		upd(0);
	}
	else{
		req.g_table.update(req, qparams.document, function(result){
			if(result==='userredirect'){
				res.redirect('/?action=confirmuser');
				return;
			}
			res.end(JSON.stringify(result),"UTF-8");
			return;
		});

	}
	

}

/**
 * Add method
 */
exports.add=function(req, res, next) {
	
	var qparams = getqueryparams(req);
	console.log('bldata_add', qparams);
	qparams.document=req.body.document;
	
	if(req.g_table==undefined){
		res.end(JSON.stringify({status: "error", data: 'empty metadata'}), "UTF-8");
		return;
	}
	// console.log(qparams.document);
	if(typeof qparams.document=='string'){
		console.log('string');
		qparams.document=JSON.parse(qparams.document);
	}

	if(Array.isArray(qparams.document)){
		var ids=[];
		function ins(object_number){
			if(qparams.document.length == object_number){
				resp={status:'ok', data:ids}
				res.end(JSON.stringify(resp),"UTF-8");
				return;
			}
			req.g_table.insert(req, qparams.document[object_number], function(result){
				ids.push(result.data);
				ins(object_number+1);
			});
		}
		ins(0);
	}
	else{
		req.g_table.insert(req, qparams.document, function(result){
			res.end(JSON.stringify(result),"UTF-8");
			return;
		});
	}
	
}



/**
 * Defining basic access constants for current user, trying to access dataset
 */

// function user_dataset_access (req, dataset_id, successcallback, errorcallback) {
// 	req.act_list = new Array();
// 	var l_list   = new Array();

// 	var sql      = '';

// 	if ('session' in  req && 'user' in req.session && req.session.user!=undefined) {
// 		sql = 'SELECT public.dataset_access('+dataset_id+', \''+req.session.user.id+'\') as g_access;';
// 	} else {
// 		sql = 'SELECT public.dataset_access('+dataset_id+', \'\') as g_access';
// 	} 
// 	var query = client.query(sql, function(error, unorows, unofields) {
// 		if (error === null)	{
// 			var accessdatarray = misc.explode(':', unorows.rows[0].g_access);
// 			st_parse = accessdatarray[0].split(',');

// 			for (var i = 0; i < st_parse.length; i++) {
// 				l_list.push(parseInt(st_parse[i]));
// 			}

// 			req.act_list = l_list;
// 			successcallback(l_list);
// 		} else {
// 			errorcallback();
// 		} //end if
// 	});
// }


function _qv(query, valuename){
	if (query===undefined)
		return '';
	if(valuename in query) {
		return query[valuename];
	} else {
		return '';
	}
} //end _qv()





/**
*	genJSON - generate JSON from relational
*/
function genJSON(req, res, options, next) {
	var table  = sanitize(req.query.tablename).escape();
	calipso.modules.postgres.fn.init_gis_user(req, function() {
		meta_manager.curent_user_id=req.session.user.id;
		meta_manager.getTableJSON(table, function(tableobject){
			res.format  = "json";
			res.end(JSON.stringify(tableobject),"UTF-8");
		});
	});
}



exports.drop=function(req, res, next) {	
	meta_manager.dropTable( req, function(error){
		if(error)
			reply = {status: 'error', data: error};
		else
			reply = {status: 'ok', data: 'The table was deleted'};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
	});
}

exports.users=function(req, res, next) {	
	// console.log('wwwwwwwwwwwwwwwwwwwwwwwwwwwww');
	meta_manager.users( req, function(error, data){
		if(error)
			reply = {status: 'error', data: error};
		else
			reply = {status: 'ok', data : data};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
	});
}

exports.useradd=function(req, res, next) {	
	var qparams = getqueryparams(req);
	meta_manager.useradd( req, qparams, function(error, data){
		if(error)
			reply = {status: 'error', data: error};
		else
			reply = {status: 'ok', data : data};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
	});
}

exports.userdelete=function(req, res, next) {	
	var qparams = getqueryparams(req);
	meta_manager.userdelete( req, qparams, function(error, data){
		if(error)
			reply = {status: 'error', data: error};
		else
			reply = {status: 'ok', data : data};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
	});
}
exports.clear=function(req, res, next) {
	meta_manager.clearTable(req, function(error){
		if(error)
			reply = {status: 'error', data: error};
		else
			reply = {status: 'ok', data: 'The table is empty'};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
	});
}




/**
 * import method
 */
function bldata_import(req, res, options, next) {
	var dataset_id = req.sanitize('f').escape();
	var dataset_import_id = req.sanitize('import_f').escape();
	var user_id='';
	if('session' in req && 'user' in req.session && req.session && req.session.user && 'id' in req.session.user && req.session.user.id!='' )
		user_id=req.session.user.id;
	var qparams = req.query;
	for (var attrname in req.body)
		qparams[attrname] = req.body[attrname];
	var first_is_head='first_is_head' in qparams;
	var delimeter=_qv(qparams, 'delimeter');	
	if(delimeter=='' || delimeter=='t')
		delimeter="\t";
	var encoding=_qv(qparams, 'encoding');	
	if(encoding=='')
		encoding='CP1251';

	calipso.modules.postgres.fn.gis_user_dir(user_id, function(user_dir){
	
		var mainrootdir = calipso.config.getModuleConfig("rfm", "mainrootdir");	
		var file_path=calipso.modules.rfm.fn.getpath(dataset_import_id);
		
		if (misc.is_number(dataset_id)) {
			user_dataset_access(req, dataset_id,
				function (alist) {
					get_table_json(req, dataset_id, function (mdobj){
						if(mdobj==null){
							res.end(JSON.stringify({status: "error", data: 'empty metadata'}), "UTF-8");
							return;
						}
						console.log('mdobj.dataset_id='+mdobj.dataset_id);
						var g_table = new geo_table(user_id, mdobj.dataset_id, meta_manager, mdobj);
						for (var i = 0; i < mdobj.columns.length; i++){
							if(!(mdobj.columns[i].fieldname in qparams))
								continue;
							if(mdobj.columns[i].widget.name=='classify'){
								//mdobj.columns[i].g_table = new geo_table(user_id, mdobj.columns[i].widget.properties.dataset_id, meta_manager, mdobj.columns[i].widget.properties.ref_meta);
								mdobj.columns[i].g_table = new geo_table(user_id, mdobj.columns[i].widget.properties.dataset_id, meta_manager, mdobj.usingmeta[mdobj.columns[i].widget.properties.dataset_id]);
							}
						}
						function load_obj(loadobj, last){
							var newobj={};
							console.log('loadobj',loadobj);
							if(loadobj!=null){
								function handling_params(paramnum, obj, newobj){
									if(paramnum == mdobj.columns.length){
										console.log('INSERT LOAD');
										console.log(newobj);
										g_table.insert(newobj, function(result){								
											return;
										});
										return;
									}
									console.log('target fieldname=', mdobj.columns[paramnum].fieldname);
									//console.log('qparams', qparams);
									if((mdobj.columns[paramnum].fieldname in qparams)){
										var method=qparams[mdobj.columns[paramnum].fieldname];
										console.log('method name',method.name);										
										switch (method.name){											
											case 'copy':
												if(method.prm!==undefined && method.prm.length==1 && method.prm[0].value!=undefined && method.prm[0].value!=''){
													cpy_fld=method.prm[0].value;
													console.log('source field = ',cpy_fld);
													if(cpy_fld in obj && obj[cpy_fld]!==undefined && obj[cpy_fld]!=''){
														console.log('widget name=',mdobj.columns[paramnum].widget.name);
														if(mdobj.columns[paramnum].widget.name=='classify'){
															subqparams={};
															var props = mdobj.columns[paramnum].widget.properties;
															if(props!=undefined && props.db_field!=null && props.db_field!=undefined){
																subqparams['f_'+props.db_field[0]]=obj[cpy_fld];
																sub_sql=mdobj.columns[paramnum].g_table.buildquery(['id'], subqparams);
																console.log(sub_sql);
																var subquery = client.query(sub_sql, function(error, query_result, unofields2) {
																	console.log('run sql end');
																	if (error === null)	{
																		if(query_result.rows.length==1){
																			newobj[mdobj.columns[paramnum].fieldname]=query_result.rows[0]['id'];
																		}
																		else
																			console.log('ambiguous decision for value='+obj[cpy_fld]);
																		handling_params(paramnum+1, obj, newobj);
																		return;
																		
																	} else {
																		console.log(sub_sql);
																		console.log(error);
																		return;
																	}
																});
																return;
															}
														}
														else if(mdobj.columns[paramnum].widget.name=='number'){
															
															v=obj[cpy_fld];
															console.log('v=');
															console.log(v);
															console.log('name='+mdobj.columns[paramnum].fieldname);
															if(v!==undefined && typeof v === 'string'){
																v=v.replace(/,/g, '.');
																if(!isNaN(parseFloat(v)) && isFinite(v))
																	newobj[mdobj.columns[paramnum].fieldname]=v;
															}
															else if(v!==undefined && typeof v === 'number'){
																newobj[mdobj.columns[paramnum].fieldname]=v;
															}
														}
														else{
															newobj[mdobj.columns[paramnum].fieldname]=obj[cpy_fld];
														}
													}
												}
												break;
											case 'copy_pnt':
												if(method.prm!==undefined && method.prm.length==2 && method.prm[0].value!=undefined && method.prm[1].value!=undefined && method.prm[0].value!='' && method.prm[1].value!='' ){
													cpy_fld=method.prm[0].value;
													cpy_fld2=method.prm[1].value;
													var lat='', lon='';
													if(cpy_fld in obj && obj[cpy_fld]!==undefined && obj[cpy_fld]!=''){
														lat=m_geom.parseCoordinate(obj[cpy_fld]);
													}
													if(cpy_fld2 in obj && obj[cpy_fld2]!==undefined && obj[cpy_fld2]!=''){
														lon=m_geom.parseCoordinate(obj[cpy_fld2]);
													}
													if(lat!='' && lon!=''){
														newobj[mdobj.columns[paramnum].fieldname]='MULTIPOINT('+lat+' '+lon+')';
													}
												}
												break;
											case 'copy_date':
												if(method.prm!==undefined && method.prm.length==2 && method.prm[0].value!=undefined && method.prm[1].value!=undefined && method.prm[0].value!='' && method.prm[1].value!='' ){
													cpy_fld=method.prm[0].value;
													frm=method.prm[1].value;										
													d=m_date.getDateFromFormat(obj[cpy_fld], frm);
													if (d!=0) {
														dv = new Date(d); 
														newobj[mdobj.columns[paramnum].fieldname]=m_date.formatDate(dv, 'yyyy-MM-dd HH:mm:ss');
													}
												}
												break;
											case 'const':
												if(method.prm!==undefined && method.prm.length==1 && method.prm[0].value!=undefined && method.prm[0].value!='' ){
													v=method.prm[0].value;												
													if (v!='') {													
														newobj[mdobj.columns[paramnum].fieldname]=v;
													}
												}
												break;												
										}
									}
									handling_params(paramnum+1, obj, newobj);								
								}
								handling_params(0, loadobj, newobj);
							}
							else if (!last){
								res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
							}
							if(last){
								output = {
									status      : 'ok',
									data		: 'ok'
								}
								res.end(JSON.stringify(output),"UTF-8");				
								return false;
							}
							return true;
						}
						ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
						if(ext=='csv' || ext=='txt'){
							filedata_load(file_path, encoding, first_is_head, delimeter, load_obj);		
						}
						else if(support_exts.indexOf(ext)!=-1){
							new_file_path = file_path.substr(0, file_path.lastIndexOf(".")) + ".json";
							jsondata_load(new_file_path, encoding, load_obj);
						}
					});
				},
				function (message)	{
					if (typeof message === 'undefined')
						message = "No access";
					res.end(JSON.stringify({status: "error", data: message}), "UTF-8");
				}
			);
		}
		else
			res.end(JSON.stringify({status: "error", data: 'The paramenter f is missing.'}), "UTF-8");
	});
}


exports.savestructure = function(req, res, next) {
	if (req.body.tablejson === undefined){
		reply = {status: 'error: json is not defined!'};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
		return;
	}

	if(!req.user){
		console.log('req.user=',req.user)
		reply = {status: "error", data: 'You should log in.'};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
		return;
	}

	var strtablejson = req.body.tablejson;
	var tablejson = JSON.parse(strtablejson);
	var dataset_id=-1;	
	if(tablejson.dataset_id!=undefined)
		dataset_id=tablejson.dataset_id;
	
	meta_manager.processTableJson(req, tablejson, dataset_id, function(error, new_dataset_id){
		var reply;
		// console.log('processTableJson',error, new_dataset_id);
		// var stack = new Error().stack
		// console.log( stack )
		if(error=='')
			reply = {status: 'ok', data: new_dataset_id};
		else
			reply = {status: 'error', data: error};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
	});
	
}

