var rootpath     = process.cwd() + '/';
var common = require(rootpath+"/controllers/utils/common");
var misc = require(rootpath+"/public/js/misc");
var config = require(rootpath+"/conf.json");
var geo_access = require("./geo_access");

var service_fields = [
//	{name : 'id',           type : 'integer'},
	{name : 'is_deleted',           type : 'bool', default_value: 'false'},
//	{name : 'symbol',           type : 'string'},//for symbol filepath
	{name : 'classname',           type : 'string'},
	{name : 'created_by',           type : 'string'},
	{name : 'edited_by',            type : 'string'},
	{name : 'edited_on',            type : 'date', indexed: 'true', default_value: 'current_timestamp'},
	{name : 'created_on',           type : 'string'},
	{name : 'published',           type : 'bool', default_value: 'false', indexed: 'true' },
//	{name : 'waiting_for_approval', type : 'integer', default_value: '1'},
];

var field_types = {
	integer     : 'integer',
	text        : 'text',
	string      : 'varchar(1024)',
	longstring  : 'varchar(4096)',
	number      : 'double precision',
	date        : 'timestamp',
	serial      : 'serial',
	point       : 'geometry',
	line        : 'geometry',
	polygon     : 'geometry',
	bool        : 'boolean',
	region      : 'integer',
	tableaccess : 'integer',
};

function existsColumn(columns, name) {
	name = name.toLowerCase();
	for (var i = 0; i < columns.length; i++) {
		if (columns[i].fieldname.toLowerCase() == name) {
			return i;	
		} //end if
	} //end for

	return -1;
} 


function AddServiceFields(tableJSON) {
	for (var i = 0; i < service_fields.length; i++) {
		if (existsColumn(tableJSON.columns, service_fields[i].name) ==- 1){
			var field = {
				fieldname : service_fields[i].name,
				visible   : false,
				type      : service_fields[i].type,
				default_value: service_fields[i].default_value,
				indexed: service_fields[i].indexed,
			};
			tableJSON.columns[tableJSON.columns.length]=field;
		}
	}

	return tableJSON;
} //end AddServiceFields()

function RemoveServiceFields(tableJSON) {
	for (var i = 0; i < service_fields.length; i++){
		if(service_fields[i].name=='published' || service_fields[i].name=='classname')
			continue;
		var fieldnumber = existsColumn(tableJSON.columns, service_fields[i].name);
		if (fieldnumber !== -1) {
			tableJSON.columns.splice(fieldnumber,1);
		}
	}
	return tableJSON;
}

/*	Basic permission constants
	var ableToAddEditViewOwn = 0;
	var ableToViewOthers     = 1;
	var ableToEditOthers     = 2;
	var isOwner              = 3;
	*/


function geo_meta_table(main_dataset_id, install) {	
	this.service_tables={};
	// this.client=pg_client;	
	this.curent_user_id='';
	
	this.database_schema=config.modules.postgres.config.user_schema;
	this.main_dataset_id = main_dataset_id;
	

	/**
	 * �������� �������� ������ � �� ����������
	 */
	this.init = function(){
		var geo_table = this;

		var createrolesqlpath = process.cwd() + '/modules/community/geothemes/lib/preparedatabase/role.sql';
		var content           = fs.readFileSync(createrolesqlpath, 'utf8');;
		global.pgPool.query(content, function(error, sql_res, unofields) {
			if (error != undefined){
				if (error.code !== "42710") {
					console.log(error);
					console.log("Error processing the database role");
				}
			}
		});

		var datasetaccessqlpath = process.cwd() + '/modules/community/geothemes/lib/preparedatabase/dataset_access.sql';
		var content           = fs.readFileSync(datasetaccessqlpath, 'utf8');;
		global.pgPool.query(content, function(error, sql_res, unofields) {
			if (error != undefined){
				if (error.code !== "42710") {
					console.log(error);
					console.log("Error processing the dataset_access function");
				}
			}
		});

		var metadatakeeper        = [];
		var tablestructuresfolder = process.cwd() + '/modules/community/geothemes/lib/tablestructures';
		var tablestructureslist   = fs.readdirSync(tablestructuresfolder);
		for (key in tablestructureslist) {			
			var correctfile = tablestructureslist[key].match(/.meta/i);
			if (correctfile) {
				var metafilename = tablestructuresfolder + "/" + tablestructureslist[key];
				var content      = fs.readFileSync(metafilename, 'utf8');				
				metadatakeeper.push(JSON.parse(content));
			}
		}
		
		//console.log(metadatakeeper);

		var sqlkeeper       = [];
		var tabledatafolder = process.cwd() + '/modules/community/geothemes/lib/tabledata';
		var tabledatalist   = fs.readdirSync(tabledatafolder);
		for (key in tabledatalist) {
			var correctfile = tabledatalist[key].match(/.sql/i);
			if (correctfile) {
				var sqlfilename = tabledatafolder + "/" + tabledatalist[key];
				var content     = fs.readFileSync(sqlfilename, 'utf8');
				sqlkeeper.push(content);
			}
		} 

		/*
		for (key in metadatakeeper) {			
			if (parseInt(metadatakeeper[key].dataset_id) === this.main_dataset_id ) {				
				//calipso.info("metadatakeeper[key].dataset_id"+this.main_dataset_id+" "+metadatakeeper[key].dataset_id);
				var meta = metadatakeeper[key];
				delete metadatakeeper[key];
				geo_table.processTableJson(meta, meta.dataset_id, function(error, create){
					//calipso.info(tablename + " (main dataset) table was processed");
					processNextMetaTable();
				});
			}
		} */

		/**
		 * Sequential meta processing - popping items form local array and
		 * processing SQL when no meta items left (all tables are created).
		 */
	
		function processNextMetaTable() {
			if (metadatakeeper.length === 0) {
				processNextSQL();
			} else {				
				var meta = metadatakeeper.pop();
				if (meta !== undefined) {
					var dataset_id = -1;
					if ("dataset_id" in meta === true && meta.dataset_id != undefined) {
						dataset_id = meta.dataset_id;
					}
					//calipso.log("dataset_id = " + dataset_id);
					//if(dataset_id==5)
					//	calipso.log("dataset_id = " + dataset_id);;

					geo_table.processTableJson({},meta, dataset_id, function(create){
						//calipso.info(tablename + " table was processed");
						processNextMetaTable();
					});
				} else {
					processNextMetaTable();
				}
			}
		} //end processNextMetaTable();
		
		if(install)
			processNextMetaTable();


		/**
		 * Sequential SQL processing.
		 */

		function processNextSQL() {
			if (sqlkeeper.length !== 0) {
				var content = sqlkeeper.pop();
				var query   = global.pgPool.query(content, function(error, sql_res, unofields) {
					if (error != undefined){
						console.log(error);
						console.log("Some error may be not wrong.");								
					}
				});
			}
		} //end processNextSQL()
	};
	
	/**
	* Removing table
	*/
	this.dropTable=function (req, callback) {
		var geo_table=this;
		if(req.meta){
			console.log('req.user', req.user);
			if(req.user===undefined){
				callback('You should log in.');
				return;
			}
			if(req.meta.owner==req.user.id || req.user.isadmin){						
				tablename = req.meta.tablename;							
				var sub_sql='DROP TABLE '+tablename+'; ';						
				sub_sql+="DELETE FROM public.geo_dataset WHERE id=" + req.meta.dataset_id+";";
				var query = global.pgPool.query(sub_sql, function(error, sql_res2, unofields) {
					callback(error);								
				});	
			}
			else
				callback('You should be the owner.');
		}
		else{
			callback('The f parametor is not number or table was deleted.');
		}
	};
	
	
	
	/**
	* Clearing table data
	*/
	this.clearTable=function (req, callback) {
		var geo_table=this;
		if(req.meta){
			console.log('req.user', req.user);
			if(req.user===undefined){
				callback('You should log in.');
				return;
			}
			
			if(req.meta.owner==req.user.id || req.user.isadmin){						
				tablename = req.meta.tablename;
				var sub_sql='DELETE FROM '+tablename+'; ';							
				var query = global.pgPool.query(sub_sql, function(error, sql_res2, unofields) {
					callback(error);								
				});
			}
			else
				callback('You should be the owner.');
		}
		else{
			callback('The f parametor is not number.');
		}
	};	
	

	this.users=function(req, callback) {
		console.log('ww11111');
		if(req.meta){
			console.log('ww222');
			if(req.user===undefined){
				callback('You should log in.');
				return;
			}
			if(req.meta.owner==req.user.id || req.user.isadmin){
				var checktablequery      = "SELECT a.user_id, a.access_mode, u.username, u.fullname   FROM public.table_access_rules a LEFT OUTER JOIN USERS u ON a.user_id = u.id WHERE (a.table_id = '" + req.meta.dataset_id + "' AND NOT a.is_deleted AND NOT waiting_for_approval)";
				console.log(checktablequery);
				var cquery = global.pgPool.query(checktablequery, function(error, rows, fields) {
					if (error === undefined)
					{
						callback(error, rows.rows);								
					}
					else{
						callback(error);
					}
				});
			}
			else{
				callback('You should be owner.');
			}

			
		}
		else{
			callback('The f parametor is not correct.');
		}
		

		

		// var cleanupquery     = 'DELETE FROM public.table_access_rules WHERE table_id = ' + tableid + ' AND waiting_for_approval = FALSE;';
		// var insertrulesquery = '';
		// if(tablejson.userlist){
		// 	userlist=tablejson.userlist;
		// 	for (var i = 0; i < userlist.length; i++){
		// 		insertrulesquery += "INSERT INTO public.table_access_rules (table_id, user_id, access_mode, is_deleted, waiting_for_approval) VALUES ('" + tableid + "', '" + userlist[i].userid + "', '"+userlist[i].accesstype+"', FALSE, FALSE);";
		// 		//console.log(insertrulesquery);
		// 	}
		// }	

	}
	

	this.useradd=function(req, qparams, callback) {
		if(req.meta){
			if(req.user===undefined){
				callback('You should log in.');
				return;
			}
			if(req.meta.owner==req.user.id || req.user.isadmin){
				var checktablequery = "INSERT INTO public.table_access_rules (table_id, user_id, access_mode, is_deleted, waiting_for_approval) VALUES ('" + req.meta.dataset_id + "', '" + qparams.userid + "', '"+qparams.accesstype+"', FALSE, FALSE);";
				console.log(checktablequery);
				var cquery = global.pgPool.query(checktablequery, function(error, rows, fields) {
					if (error === undefined)
					{
						callback(error, rows.rows);								
					}
					else{
						callback(error);
					}
				});
			}

			
		}
		else{
			callback('The f parametor is not number.');
		}
		
	}

	this.userdelete=function(req, qparams, callback) {
		if(req.meta){
			if(req.user===undefined){
				callback('You should log in.');
				return;
			}
			if(req.meta.owner==req.user.id || req.user.isadmin){
				var cleanupquery     = "DELETE FROM public.table_access_rules WHERE table_id = " + req.meta.dataset_id +  " AND user_id ='" + qparams.userid+"'";
				
				console.log(cleanupquery);
				var cquery = global.pgPool.query(cleanupquery, function(error, rows, fields) {
					if (error === undefined)
					{
						callback(error, 'deleted');								
					}
					else{
						callback(error);
					}
				});
			}

			
		}
		else{
			callback('The f parametor is not number.');
		}

	}
	/**
	* AJAX theme update
	*/
	this.processTableJson=function (req, tablejson, dataset_id, callback) {
		if(tablejson.tablename.indexOf('.')==-1){
			tablejson.tablename=this.database_schema+'.'+tablejson.tablename;
			//console.log(tablejson.tablename);
		}
		for (var i = 0; i < tablejson.columns.length; i++){
			if(tablejson.columns[i].tablename===undefined || tablejson.columns[i].tablename=='')
				tablejson.columns[i].tablename='main';
			if(tablejson.columns[i].sqltablename===undefined || tablejson.columns[i].sqltablename=='')
				tablejson.columns[i].sqltablename=tablejson.tablename;
		}
		if(isNaN(dataset_id))
			dataset_id=-1;		

		var geo_table=this;
		tablejson = RemoveServiceFields(tablejson);
		
		
		
		
		function register(error, newtablejson){
		// console.log("error=",error);
			if(error!=''){
				callback(error,-1);
				return;
			}
			if(tablejson.dataset_id==-1){
				geo_table.register_dataset(req, newtablejson, newtablejson.dataset_id, function(new_dataset_id){
					// processUserList(new_dataset_id);
					callback('',new_dataset_id);
				});
			}
			else{
				var sub_sql="SELECT id FROM public.geo_dataset WHERE id="+tablejson.dataset_id;
				var query = global.pgPool.query(sub_sql, function(error, sql_res, unofields) {
					if (error === undefined){
						if (sql_res.rows.length === 0)	{
							geo_table.register_dataset(req, newtablejson, newtablejson.dataset_id, function(new_dataset_id){
								// processUserList(new_dataset_id);
								callback('',new_dataset_id);
							});
						} else {
							geo_table.update_register_dataset(req, newtablejson, newtablejson.dataset_id, function(new_dataset_id){
								//console.log("update_register_dataset new_dataset_id="+dataset_id);
								// processUserList(dataset_id);
								callback('',tablejson.dataset_id);
							});
						}
					}
					else {
						console.log(error);						
						console.log(sub_sql);
						callback(error,-1);
					}
				});
			}		
		};
		strtablejson = JSON.stringify(tablejson);
		tablename = tablejson.tablename;
		schema     = tablejson.schema;
		if (tablejson.schema == undefined || tablejson.schema == ''){
			tablejson.schema = this.database_schema;
		}
		if(tablename.indexOf('.')!=-1){
			tparts=tablename.split('.');
			schema=tparts[0];
			tablename=tparts[1];
		}

		if(dataset_id==-1){
			geo_table.createTable(tablejson, register);
		}
		else{
			if(req.user && req.user.isadmin){
				geo_table.updateTable(tablejson, register);
			}
			else{
				//console.log(geo_table.user);
				geo_table.is_author(dataset_id, geo_table.curent_user_id, function(error, author){
					if (error === undefined){
						if(author){
							geo_table.updateTable(tablejson, register);
							callback('',dataset_id);
						}
						else
							callback('You are not owner',-1);
					}
					else{
						console.log(error);
						callback(error, -1);
					}
				});
			}
		}
	
		
		
		// var sql="SELECT table_name FROM information_schema.tables WHERE table_schema='"+schema+"' and table_name='"+tablename+"'";		
		// var query = global.pgPool.query(sql, function(error, sql_res, unofields) {
		// 	if (error === undefined){
		// 		if (sql_res.rows.length == 0)	{
		// 			//console.log("createTable 1 "+sql);
		// 			geo_table.createTable(tablejson, register);
		// 			return;
		// 		} else {
		// 			//console.log("updateTable 2 "+sql +sql_res.rows.length);
		// 			if(dataset_id==-1){
		// 				var sql2="SELECT created_by, id FROM public.geo_dataset WHERE not is_deleted and d_table='"+tablejson.tablename+"'";
		// 				var query = global.pgPool.query(sql2, function(error, sql_res2, unofields) {
		// 					if(error !== undefined){
		// 						callback(error,-1);
		// 						return;
		// 					}
		// 					if(sql_res2.rows.length == 0 ){
		// 						geo_table.updateTable(tablejson, register);
		// 						//callback(dataset_id);
		// 					}
		// 					else if (sql_res2.rows.length == 1 && sql_res2.rows[0].created_by==geo_table.curent_user_id){
		// 						dataset_id=sql_res2.rows[0].id;
		// 						geo_table.updateTable(tablejson, register);
		// 						//callback(dataset_id);
		// 					}
		// 					else 
		// 						callback('The table with such name already exists.',-1);
							
		// 				});
		// 			}
		// 			else{
		// 				if(geo_table.user && geo_table.user.isAdmin){
		// 					geo_table.updateTable(tablejson, register);
		// 				}
		// 				else{
		// 					//console.log(geo_table.user);
		// 					geo_table.is_author(dataset_id, geo_table.curent_user_id, function(error, author){
		// 						if (error === undefined){
		// 							if(author){
		// 								geo_table.updateTable(tablejson, register);
		// 								callback('',dataset_id);
		// 							}
		// 							else
		// 								callback('You are not owner',-1);
		// 						}
		// 						else{
		// 							console.log(error);
		// 							callback(error, -1);
		// 						}
		// 					});
		// 				}
		// 			}
										
		// 		}
		// 	}
		// 	else {
		// 		console.log(error);
		// 		callback(error,-1)
		// 	}
		// });
	}
	
	
	this.is_author=function(dataset_id, user_id, callback){		
		var sql2="SELECT created_by FROM public.geo_dataset WHERE not is_deleted and id="+dataset_id;		
		var query = global.pgPool.query(sql2, function(error, sql_res2, unofields) {
			if (error === undefined && sql_res2.rows.length == 1 && sql_res2.rows[0].created_by==user_id)							
				callback(error,true);
			else if(error === undefined && sql_res2.rows.length == 0)
				callback('The table is not registered.',false);
			else{
				callback(error,false);
			}
		});
	}
	/**
	* Creating table
	*/
	this.createTable=function(json_desc_obj, callback) {
		var geo_table=this;
		var parent_json;
		var dataset_id=-1;
		var outputobject;
		var dataset_id_sql="SELECT nextval('public.geotable_seq_id'::regclass) id";
		var cquery = global.pgPool.query(dataset_id_sql, function(error, result, fields) {	
			if(error!==undefined){
				console.log(error);
				console.log(dataset_id_sql);
				callback(error);
			}
			else{
				dataset_id=result.rows[0].id;
				
				if(json_desc_obj.inherits){		
					outputobject = RemoveServiceFields(json_desc_obj);
					this.get_table_json({}, json_desc_obj.inherit_table, function (meta){
						parent_json=meta;
						outputobject.dataset_id=dataset_id;
						createtab();
					});
				}
				else{
					outputobject = AddServiceFields(json_desc_obj);
					outputobject.dataset_id=dataset_id;
					createtab();
				}
			}
		});

		

		
		function createtab(){
			console.log('createtab');
			var reqobj       = new Object();			
			var accessmodevalue;
			if(outputobject.accessmode!==undefined)
				accessmodevalue = outputobject.accessmode.access_mode;

			strtablejson	 = JSON.stringify(outputobject);
			var tparts=outputobject.tablename.split('.');
			var scheme=tparts[0];
			var tablename=tparts[1];
			if(tablename.length>10){
				tablename=tablename.substring(0,9);				
			}
			tablename+="_"+dataset_id;
			outputobject.tablename = scheme + '.' + tablename;
			

			var sqlstr    = 'CREATE TABLE ' + scheme + '.' + tablename + '( ';
			var strfields ='';
			var field_indexes='';			
			for (var i = 0; i < outputobject.columns.length; i++){
				if(outputobject.columns[i].tablename!='main' && outputobject.columns[i].tablename!==undefined)
					continue;
				if(outputobject.columns[i].inherit_table )
					continue;
				if(json_desc_obj.inherits && outputobject.columns[i].fieldname=='id')
					continue;
				if ((outputobject.columns[i].type !== 'line') && (outputobject.columns[i].type !== 'point') && (outputobject.columns[i].type !== 'polygon')){
					if(strfields != '')	{
						strfields = strfields + ', ';
					}
					if(outputobject.columns[i].default_value===undefined || outputobject.columns[i].default_value==''){
						strfields = strfields + '"' + outputobject.columns[i].fieldname + '" ' + field_types[outputobject.columns[i].type];
						if(!json_desc_obj.inherits && outputobject.columns[i].fieldname=='id'){
							//if(outputobject.sequencename=='' || outputobject.sequencename===undefined)
							outputobject.sequencename = outputobject.tablename+'_seq';							
							strfields+=' DEFAULT '+ "nextval('"+outputobject.sequencename+"'::regclass)";							
						}	
					}
					else
						strfields = strfields + '"' + outputobject.columns[i].fieldname + '" ' + field_types[outputobject.columns[i].type] + ' DEFAULT '+ outputobject.columns[i].default_value;
					if(outputobject.columns[i].indexed=='true'){
						index_name='index_'+tablename+'_'+outputobject.columns[i].fieldname;
						if(index_name.length>30)
							index_name=index_name.substring(0,29);
						field_indexes+=' CREATE INDEX '+index_name+'  ON ' + outputobject.tablename + '  ("'+outputobject.columns[i].fieldname+'");';
					}
					if (outputobject.columns[i].pk!=undefined && outputobject.columns[i].pk) {
						strfields=strfields + ' CONSTRAINT ' + misc.getcorrectobjname(common.randomString(10)) + '_constr PRIMARY KEY ';
					}			
				}
			}
			console.log('CREATE SEQUENCE BEFORE')
			if(json_desc_obj.inherits){
				sqlstr = sqlstr + strfields + ') INHERITS ('+parent_json.tablename+');';
			}
			else{
				sqlstr = sqlstr + strfields + ');';
				sqlstr="CREATE SEQUENCE "+outputobject.sequencename + "; " + sqlstr;
				console.log('CREATE SEQUENCE ',outputobject.sequencename);
			}
			
			
			
	 
			// <AddGeometryColumn> query
			console.log(sqlstr);
			var geomstr = '';
			for (var i = 0; i < outputobject.columns.length; i++){
				if(outputobject.columns[i].tablename!='main' || outputobject.columns[i].inherit_table!=='')
					continue;			
				switch(outputobject.columns[i].type) {
					case 'point':
					  geomstr = geomstr + 'SELECT AddGeometryColumn(\'' + scheme + '\',\'' + tablename + '\',\'' + outputobject.columns[i].fieldname + '\', 4326,\'MULTIPOINT\',2);';
					  break;
					case 'line':
					  geomstr = geomstr + 'SELECT AddGeometryColumn(\'' + scheme + '\',\'' + tablename + '\',\'' + outputobject.columns[i].fieldname + '\', 4326,\'MULTILINESTRING\',2);';
					  break;
					case 'polygon':
					  geomstr = geomstr + 'SELECT AddGeometryColumn(\'' + scheme + '\',\'' + tablename + '\',\'' + outputobject.columns[i].fieldname + '\', 4326,\'MULTIPOLYGON\',2);';
					  break;
				}
			}
				

			// var tableownerquery = "ALTER TABLE " + schema + "." + tablename + " OWNER TO storage_users;"
			
			// Executing all queries at once
			var createtablequery = sqlstr + geomstr + field_indexes;// + populateservicefields;
			console.log(createtablequery);
			if(strfields==''){
				console.log('strfields is empty');
				callback('strfields is empty');	
			}
			else{
				// console.log(createtablequery);
				var cquery = global.pgPool.query(createtablequery, function(error, rows, fields) {	
					
					if(error!==undefined){
						console.log(error);
						console.log(createtablequery);
						callback(error);
					}
					else{
						callback('', outputobject);				
					}
				});
			}
		}
	}
	
	
	this.register_dataset=function(req, json_desc_obj, dataset_id, callback){		
		var strtablejson = JSON.stringify(json_desc_obj);
		console.log("this.register_dataset=", strtablejson);
		var accessmodevalue='';
		if(json_desc_obj.accessmode!==undefined)
			accessmodevalue = json_desc_obj.accessmode.access_mode;
		var is_moderated    = json_desc_obj.moderated;
		if(is_moderated===undefined)
			is_moderated='false';
		if(dataset_id!=-1)
			sql = "INSERT INTO geo_dataset(id, name, created_by, \"JSON\", status, d_schema, d_table, access_mode, description, guid) VALUES ("+dataset_id+",'"+json_desc_obj.title+"', '" + req.user.id+"','"+strtablejson+"', 1, NULL,'"+json_desc_obj.tablename+"', 0, '"+json_desc_obj.description+"', uuid_generate_v4())";
		else
			sql = "INSERT INTO geo_dataset(name, created_by, \"JSON\", status, d_schema, d_table, access_mode, description, guid) VALUES ('"+json_desc_obj.title+"', '" + req.user.id+"',$$"+strtablejson+"$$, 1, NULL,'"+json_desc_obj.tablename+"', 0, '"+json_desc_obj.description+"', uuid_generate_v4()) RETURNING id;";

		var fquery = global.pgPool.query(sql, function(error, sql_res, fields) {
			if (error === undefined)	{					
				if(dataset_id==-1)
					callback(sql_res.rows[0].id);
				else
					callback(dataset_id);
				return;
			}
			else{
				console.log(error);
				callback(-1);
				return;
			}						
		});	
	};
	
	this.update_register_dataset=function(req, json_desc_obj, dataset_id, callback){
		// console.log("json_desc_obj",json_desc_obj);
		var strtablejson = JSON.stringify(json_desc_obj);
		var accessmodevalue='';
		if(json_desc_obj.accessmode!==undefined)
			accessmodevalue = json_desc_obj.accessmode.access_mode;
		var is_moderated    = json_desc_obj.moderated;
		if(is_moderated===undefined)
			is_moderated='false';
		sql = "UPDATE geo_dataset SET is_deleted=false, name='" + json_desc_obj.title + "', \"JSON\"=$$" + strtablejson + "$$, description='"+json_desc_obj.description+"'";
		if(is_moderated!=undefined)
			sql+=", is_moderated = " + is_moderated;
		sql+=" WHERE id=" + dataset_id;		
		//console.log('registering table:'+json_desc_obj.title);
		var fquery = global.pgPool.query(sql, function(error, sql_res, fields) {
			if (error === undefined)	{					
				callback(true);
				return;
			}
			else{
				console.log(error);
				callback(false);
				return;
			}						
		});	
	}
	
	/**
	*	Table update
	*/
	this.updateTable=function(json_desc_obj, callback) {
		var geo_table=this;
		//console.log('inner of updateTable');
		var parent_json;
		var newobj;
		if(json_desc_obj.inherits){		
			newobj = RemoveServiceFields(json_desc_obj);
			this.get_table_json({}, json_desc_obj.inherit_table, function (meta){
				parent_json=meta;
				updatetab();
			});
		}
		else{
			newobj = AddServiceFields(json_desc_obj);
			updatetab();
		}
					
		function updatetab(){
			var strtablejson = JSON.stringify(json_desc_obj);
			var accessmodevalue;
			if(newobj.accessmode!==undefined)
				accessmodevalue = newobj.accessmode.access_mode;
			var is_moderated    = newobj.moderated;
			if(is_moderated===undefined)
				is_moderated='false';


			// Original version
			geo_table.getTableJSON(newobj.tablename, function (oldobj){			
				// Accumulating queries
				var queryCollector = '';
				tparts=newobj.tablename.split('.');
				var schema=tparts[0];
				var tablename=tparts[1];
				// Checking every column
				for (var i = 0; i < newobj.columns.length; i++) {
					if(newobj.columns[i].tablename!='main' && newobj.columns[i].tablename!==undefined)
						continue;
					if(newobj.columns[i].inherit_table )
						continue;
					var new_element = true;
					for (var j = 0; j < oldobj.columns.length; j++) {					
						if (newobj.columns[i].fieldname.toLowerCase() == oldobj.columns[j].fieldname.toLowerCase()) {
							// Exact same columns - it is not required to change
							new_element = false;						
							break;
						}
						
					}
					if ((new_element) && (newobj.columns[i].fieldname !== 'id')) {					
						//console.log('field does not exist '+newobj.columns[i].fieldname);
						var geomstr = 'SELECT AddGeometryColumn(\'' + schema + '\', \'' + tablename + '\',\'' + newobj.columns[i].fieldname + '\', 4326,\'HEREGOESEPSG\',2);';
						switch(newobj.columns[i].type) {
							case 'point':
								var geomstr = geomstr.replace(/HEREGOESEPSG/g, 'MULTIPOINT');
								queryCollector = queryCollector + geomstr;
								break;
							case 'line':
								var geomstr = geomstr.replace(/HEREGOESEPSG/g, 'MULTILINESTRING');
								queryCollector = queryCollector + geomstr;
								break;
							case 'polygon':
								var geomstr = geomstr.replace(/HEREGOESEPSG/g, 'MULTIPOLYGON');
								queryCollector = queryCollector + geomstr;
								break;
							case 'calculated':
								break;		
							case 'tableaccess':
								break;		
								
							default:
								if(newobj.columns[i].default_value===undefined || newobj.columns[i].default_value=='')
									queryCollector = queryCollector + 'ALTER TABLE ' + schema + '."' + tablename + '" ADD COLUMN ' + newobj.columns[i].fieldname + ' ' + field_types[newobj.columns[i].type] + ';';						
								else						
									queryCollector = queryCollector + 'ALTER TABLE ' + schema + '."' + tablename + '" ADD COLUMN ' + newobj.columns[i].fieldname + ' ' + field_types[newobj.columns[i].type] + ' DEFAULT '+newobj.columns[i].default_value+';';
								break;
						}
					}				
				}			
				//console.log('queryCollector='+queryCollector);
				if(queryCollector!=''){								
					var cquery = global.pgPool.query(queryCollector, function(error, sql_res, fields) {					
						if (error == undefined) {									
							callback('', newobj);
						}
						else{
							console.log('updating - error');
							console.log(error);
							callback(error);
						}
					});
					
				}
				else
					callback('', newobj);
			});	
		}
	};
	
	this.getTableJSON=function (table, callback) {
		var geo_table=this;
		tparts=table.split('.');
		var schema=tparts[0];
		var table=tparts[1];
		//console.log('getTableJSON '+schema + ' '+ table);
	
		var tableobject = new Object();
		var params      = new Array();
		
		var query = "SELECT i.data_type, i.column_name,g.type FROM information_schema.columns i left outer join public.geometry_columns g on i.column_name=g.f_geometry_column and i.table_schema=g.f_table_schema and i.table_name=g.f_table_name WHERE i.table_name='"+table+"' and i.table_schema = '" + schema +"'";
		
		var cquery = global.pgPool.query(query, function(error, sql_res, fields) {			
			if (error === undefined) {
				//console.log('sql_res.rows.length='+sql_res.rows.length);
				for (var i = 0; i < sql_res.rows.length; i++) {
					var curcol = sql_res.rows[i];
					var tempo;					
					if ((curcol.column_name=='fid' || curcol.column_name=='id' || curcol.column_name=='gid') && (curcol.data_type.match(/integer/i) || curcol.data_type.match(/serial/i))) {
						tempo = {
							fieldname: curcol.column_name,
							title: curcol.column_name,
							type: 'integer',
							visible: 'false',								
							pk: 'true',
							widget: {
								name: 'hidden'
							}
						}
						params.push(tempo);
					} 
					else if (curcol.data_type.match(/integer/i) || curcol.data_type.match(/serial/i) || curcol.data_type.match(/double/i) ) {
						tempo = {
							fieldname: curcol.column_name,
							title: curcol.column_name,
							type: 'number',	
							widget: {
								name: 'number'
							}
						}
						params.push(tempo);
					}
					else if (curcol.data_type.match(/timestamp/i) || curcol.data_type.match(/date/i)) {
						tempo = {
							fieldname: curcol.column_name,
							title: curcol.column_name,
							type: 'date',								
							widget: {
								name: 'date'
							}
						}
						params.push(tempo);
					} else if ((curcol.data_type.match(/character/i)) || (curcol.data_type !== 'USER-DEFINED')) {
						tempo = {
							fieldname: curcol.column_name,
							title: curcol.column_name,
							type: 'string',
							widget: {
								name: 'edit',
								properties: {
									size: 16,
								}
							}
						}
						params.push(tempo);
					} else if (curcol.data_type == 'USER-DEFINED' && curcol.type!=''){
						var cursign = '';
						switch (curcol.type) {
							case 'POINT':
								cursign = 'point'; 
								break;						
							case 'MULTIPOINT':
								cursign = 'point'; 
								break;
							case 'LINESTRING':
								cursign = 'line';
								break;
							case 'MULTILINESTRING':
								cursign = 'line';
								break;
							case 'POLYGON':
								cursign = 'polygon';
								break;
							case 'MULTIPOLYGON':
								cursign = 'polygon';
								break;
							default:
								cursign = '';
								break;
						} //end switch
						
						if (cursign!='') {							
							var tempo = {
								fieldname: curcol.column_name,
								title: curcol.f_geometry_column ,
								type: 'geometry',
								visible: 'true',							
								widget: {
									name: cursign
								}
							}
							params.push(tempo);
						} //end if
					}						
					
				}				
				
				tableobject = {
					tablename: schema+'.'+table,					
					title: schema+'.'+table,
					input: '',
					print: '',
					moderated: false,
					accessmode: {access_mode: 4},
					columns: params				
				}				
				//console.log(tableobject);
				callback(tableobject);
				return;
			}
			else {
				console.log('error in getTableJSON');
				console.log(error);
				console.log(query);
				callback(null);				
				return;
			}
		});
	}
	
	
	/**
	*	getdbjsonbyid


		
	*/
	
	this.get_table_json=function (usingmeta,dataset_id, callback,req){	
		
		var geo_table=this;
		var meta;
		// console.log('-------------------------');
		
		if(usingmeta[dataset_id]!==undefined){
			callback(null);
			return;
		}

		// console.log('-------------------------2');
				
   
		function req_load_meta(column_id){
			if(column_id==meta.columns.length || meta.columns[column_id].widget===undefined){
				meta.usingmeta=usingmeta;
				
				if(req){
					geo_access.getaccess(req, meta, function(meta){
						callback(meta);
					});
				}
				else{
					callback(meta);
				}
				
				return;
			}
			// console.log(meta.columns[column_id].widget.name);
			// console.log('-ok');
			if((meta.columns[column_id].widget.name=='classify' || meta.columns[column_id].widget.name=='details' || meta.columns[column_id].widget.name=='links')/* && (meta.columns[column_id].widget.properties.dataset_id % 1 === 0)*/ && usingmeta[meta.columns[column_id].widget.properties.dataset_id]===undefined){
				// console.log(' using meta dataset_id=',meta.columns[column_id].widget.properties.dataset_id);
				geo_table.get_table_json(usingmeta,meta.columns[column_id].widget.properties.dataset_id, function (mdobj){
					if(mdobj!=null)
						usingmeta[meta.columns[column_id].widget.properties.dataset_id]=mdobj;
					//meta.columns[column_id].widget.properties.ref_meta=mdobj;
					req_load_meta(column_id+1);
				});
			}
			else{
				req_load_meta(column_id+1);
				// console.log('there is meta ');
			}
		}
		
		var client = this.client;
		var sql='';
		if (isNaN(dataset_id)){
			// console.log('is nan');
			if (this.curent_user_id!='') {
				sql = 'SELECT "JSON", created_by, wms_link, wms_layer_name, opacity, is_moderated, wms_zoom_max, wms_zoom_min, public.dataset_access(id, \''+this.curent_user_id+'\') as g_access  FROM public.geo_dataset WHERE guid = \'' + dataset_id + '\';';
			} else {
				sql = 'SELECT "JSON", created_by, wms_link, wms_layer_name, opacity, is_moderated, wms_zoom_max, wms_zoom_min, public.dataset_access(id, \'\') as g_access  FROM public.geo_dataset WHERE guid = \'' + dataset_id + '\';';		
			}
		}
		else{
			if (this.curent_user_id!='') {
				sql = 'SELECT "JSON", created_by, wms_link, wms_layer_name, opacity, is_moderated, wms_zoom_max, wms_zoom_min, public.dataset_access(id, \''+this.curent_user_id+'\') as g_access  FROM public.geo_dataset WHERE id = ' + dataset_id + ';';
			} else {
				sql = 'SELECT "JSON", created_by, wms_link, wms_layer_name, opacity, is_moderated, wms_zoom_max, wms_zoom_min, public.dataset_access(id, \'\') as g_access  FROM public.geo_dataset WHERE id = ' + dataset_id + ';';		
			}
		}
	
		// console.log(sql);
		// console.log(n);
		// console.log('------------------------------------------');
		
		var query = global.pgPool.query(sql, function(error, unorows, unofields) {			
			
			// console.log('unorows.rows[0]');
			// console.log(error);
			if (error===undefined) {
				// console.log('unorows.rows[1]');
				if (unorows.rows.length === 1) {
					// console.log('unorows.rows[2]');
					// console.log(unorows.rows[0]);
					meta = JSON.parse(unorows.rows[0].JSON);
					meta.dataset_id=dataset_id;
					if(meta.tablename.indexOf('.')==-1 && meta.schema!=''){
						meta.tablename=meta.schema + '.' + meta.tablename;
					}
					
					if(meta==null){
						callback(null);
						return;					
					}
					meta.rights=unorows.rows[0].g_access;
					meta.wms_link=unorows.rows[0].wms_link;
					meta.wms_layer_name=unorows.rows[0].wms_layer_name;
					meta.dataset_id=dataset_id;
					meta.opacity=unorows.rows[0].opacity;
					meta.owner=unorows.rows[0].created_by;
					if(!(meta.rights && meta.rights!='' && meta.rights.length==3 && meta.rights.charAt(2)!='0')){
						for (var i = 0; i < meta.columns.length; i++){
							if(meta.columns[i].fieldname=='published'){
								meta.columns.splice(i,1);
								break;
							}
						}
					}						
					if(unorows.rows[0].wms_zoom_max==null || unorows.rows[0].wms_zoom_max==undefined)
						meta.maxZoom=18;
					else
						meta.maxZoom=unorows.rows[0].wms_zoom_max;
					if(unorows.rows[0].wms_zoom_max==null || unorows.rows[0].wms_zoom_max==undefined)
						meta.minZoom=0;
					else
						meta.minZoom=unorows.rows[0].wms_zoom_min; 
					usingmeta[dataset_id]=meta;
					req_load_meta(0);
				} else {
					console.log(unorows);
					callback(null);
				} //end if
			} else {
				console.log(error);				
				callback(null);
			}
		});
	}

	
	return this;
}


module.exports = geo_meta_table;