var rootpath     = process.cwd() + '/';
//misc             = require(rootpath + 'themes/core/cleanslate/public/js/misc'),
//common           = require(rootpath + 'modules/community/common/lib/common'),
//sanitize         = require('validator').sanitize;
var geo_table = require("./geo_table");
var config = require("../../conf.json");

/**
 * Converting Windows UNC style to the Linux one (assuming
 * that target format starts with /s root folder.
 *
 * @param {String} filepath Converted filepath
 *
 * @return {String}
 */
function toUNIXUNC(filepath) {
	filepath = filepath.replace(new RegExp("[a-zA-Z]\:(\\\\|\\/)", "i"), "/s/");
	return filepath;
}


/**
 * Converting locally processed filepath to the
 * filepath at the remote system.
 *
 * @param {String} filepath Converted filepath
 *
 * @return {String}
 */
function toMapserverPath(filepath) {
	if(!filepath)
		return '';
	var searchedRegExp = calipso.config.getModuleConfig('geothemes', 'mapfiledir').replace(new RegExp("\\\\{1,2}", "g"), "(\\\\{1,2}|\\/{1,2})");
	var modifiedFilepath = filepath.replace(new RegExp(searchedRegExp), calipso.config.getModuleConfig('geothemes', 'remoteHostMapFilesPath'));
	return modifiedFilepath;
}

var font_symbols=[{
	name:'MapInfoSymbols',
	startsymbol: 34,
	endsymbol: 68
}];

function geo_map(mapfilename, map_file_dir, user_id, dbclient, meta_manager) {
/*
	this.map_file_dir=calipso.config.getModuleConfig('geothemes', 'mapfiledir');
	if(!fs.existsSync(this.map_file_dir)){
		console.log("You should set up mapfiledir in geothemes config!");
		console.log("current state of map_file_dir=" + this.map_file_dir);
		if(callback)
			callback("error");
	}*/

	
	/*
	*	Creates map file for table's geometry column
	*/
	var gmap=this;
	
	var filepath = map_file_dir + '/' + mapfilename + '.map';	
	//console.log(filepath);
	var mapserver_path = config.modules.geothemes.config.mapserver_path;
				
	this.createMapForDB=function (fieldname, mapstyle, mdobj, selection_array, query, callback) {
		var sql='';
				
		var column=common.getcolumn(fieldname, mdobj);
		var pfld=common.getcolumn(column.parentfield, mdobj);
		
		
		var metadata = {pg_user: '', pg_password: '', pg_db_name: '', pg_host: '', tablename: mdobj.tablename, indexcolumn: mdobj.columns[0].fieldname };
		metadata.pg_user=config.modules.postgres.config.db_user;
		metadata.pg_password=config.modules.postgres.config.db_passwd;
		metadata.pg_db_name=config.modules.postgres.config.db_name;
		metadata.pg_host=config.modules.postgres.config.db_host;
		metadata.pg_port=config.modules.postgres.config.db_port;

				
		var map_options_path = map_file_dir + '/' + mapfilename + '.options';
		var map_symbols_path = map_file_dir + '/' + mapfilename + '.sym';
		var tag_symbols = mapfilename + '.sym';
		
		
		var map_options={ mapstyle:'',  selection:'', query:''};
		if(fs.existsSync(map_options_path)){
			map_options_str=fs.readFileSync(map_options_path);
			map_options = JSON.parse(map_options_str);
		}

		var data = '';
		var style_default;
		switch (column.type) {
			case 'point':
				data=fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/point_template.map', 'utf8');
				style_default=fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/point_template.style', 'utf8');
				break;
			case 'line':
				data=fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/line_template.map', 'utf8');
				style_default=fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/line_template.style', 'utf8');
				break;
			case 'polygon':
				data=fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/polygon_template.map', 'utf8');
				style_default=fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/polygon_template.style', 'utf8');
				break;		
			default:
				if(callback)
					callback("error");
				return;			
		}
		style_default=style_default.replace(/#fieldname/g, column.fieldname);
		if(pfld!=null && column.parentfield!='id' && mapstyle==''){
			mapstyle=map_options.mapstyle;
		}
		if(mapstyle=='' && map_options.mapstyle!=''){
			mapstyle=map_options.mapstyle;
		}	
		else if(mapstyle=='')
			mapstyle=style_default;	

		
		var geom_fieldname='';
		
		function write_mapfile(){		
			map_options.mapstyle=mapstyle;
			if(selection_array=='-' && map_options.selection!=''){
				selection_array=map_options.selection;
			}
			map_options.selection=selection_array;
			//console.log('write_mapfile');
			//console.log(mapstyle);

			data=data.replace(/tag_style/g, mapstyle);
			if(selection_array!=''){
				ids=selection_array.split(',');
				if(ids.length==0)
					str_filter = 'main.ID=-1';
				else{
					str_filter = '';
					for(var i=0; i<ids.length; i++){
						if(str_filter!='')
							str_filter+=' OR ';
						str_filter+='main.ID='+ids[i];
					}

				}
			}
			else{
				str_filter='main.id=-1';
			}

			str_filter=' '+str_filter;
			data=data.replace(/tag_selection_filter/g, str_filter);

			//data=data.replace(/&quot;/g, '"');
			data=data.replace(/tag_filepath/g, filepath);
			data=data.replace(/tag_fieldname/g, column.fieldname);
			data=data.replace(/tag_user/g, metadata.pg_user );
			data=data.replace(/tag_password/g, metadata.pg_password );
			data=data.replace(/tag_db_name/g, metadata.pg_db_name);
			data=data.replace(/tag_host/g, metadata.pg_host);
			data=data.replace(/tag_table/g, metadata.tablename);
			data=data.replace(/tag_host/g, metadata.pg_host);
			data=data.replace(/tag_port/g, metadata.pg_port);

			data=data.replace(/tag_mapserver_path/g, mapserver_path);
			data=data.replace(/tag_map_file_dir/g, map_file_dir);
			data=data.replace(/tag_symbols/g, tag_symbols);
			

			sql=sql.replace(/"/g, '\\"');
	//		if(sql.indexOf("ORDER BY")!=-1)
	//			sql=sql.substring(0,sql.indexOf("ORDER BY"));
			
			data=data.replace(/tag_query/g, sql);


			if(column.widget.properties.label!=undefined && column.widget.properties.label!='')
				data=data.replace(/tag_indexcolumn/g, 'LABELITEM "'+column.widget.properties.label+'"');
			else
				data=data.replace(/tag_indexcolumn/g, '');


			//tag_geom
			data=data.replace(/tag_geom/g, column.fieldname);
			/*
			if(mdobj.columns[column_index].sql_fields!=undefined){
				for(var i=0; i<mdobj.columns[column_index].sql_fields.length; i++){
					if(mdobj.columns[column_index].sql_fields[i].map){
						data=data.replace(/tag_geom/g, mdobj.columns[column_index].sql_fields[i].alias);
						break;
					}
				}
			}
			else{
				data=data.replace(/tag_geom/g, column.fieldname);
			}*/


			if (data != '') {
				fs.writeFileSync(filepath, data, 'utf-8', function (err) {
					if (err) {
						console.log(err);
					}
				});
			} else {
				if(callback)
					callback("error");
			}
			map_options_str=JSON.stringify(map_options);
			fs.writeFileSync(map_options_path, map_options_str, 'utf-8', function (err) {
				if (err) {
					console.log(err);
				}
			});

			var mod_filepath = filepath.replace(/\\/g, '\\\\');
			if(callback)
				callback(null, mapserver_path+"?map="+mod_filepath);
			gmap.update_symbols(map_symbols_path, fieldname, mdobj);
		}
		
		sql=query;		
		map_options.query=sql;		
		write_mapfile();		
		/*
		if((!query) && map_options.query!=''){
			sql=map_options.query;		
			map_options.query=sql;
			geom_fieldname=column.fieldname;
			write_mapfile();
		}
		else {		
		
			
			
			console.log(sql);
		}*/
	}
	
	
	this.createMapForFile=function(sourcepath, mapstyle, from_color, to_color, req_date, callback) {
		if(callback===undefined)
			return "error";
		if (from_color == null) {
			var from_color = {
				r: Math.floor((Math.random()*120)+128),
				g: Math.floor((Math.random()*120)+128),
				b: Math.floor((Math.random()*120)+128)
			}
		}
		if (to_color == null) {
			var to_color = {
				r: Math.floor((Math.random()*120)+128),
				g: Math.floor((Math.random()*120)+128),
				b: Math.floor((Math.random()*120)+128)
			}
		}

		var rngmin=0;
		var rngmax=0;
		console.log('sourcepath=');
		console.log(sourcepath);
		if(sourcepath[sourcepath.length-1]=='/')
			sourcepath=sourcepath.substring(0,sourcepath.length-1);

		var dirpath = toUNIXUNC(sourcepath.substring(0, sourcepath.lastIndexOf('/')));
		//var dirpath = sourcepath.substring(0, sourcepath.lastIndexOf('/'));

		var filename=sourcepath.substring(sourcepath.lastIndexOf('/')+1,sourcepath.length);

		var filenameclean=filename.substring(0, filename.lastIndexOf('.'));
		//var subdata = {filepath: filepath, dirpath: dirpath, filename: filename};
		var ext=filename.substring(filename.lastIndexOf('.')+1,filename.length).toUpperCase();
		var min_raster_scale = 0;
		var max_raster_scale = 0;
		var topaste = new Object();
		var ret_style='';
		topaste.tag_style='';
		// if(mapstyle)
			topaste.tag_style=mapstyle;
		// else {
		// 	var filepathstyle=sourcepath.substring(0, sourcepath.lastIndexOf('.'))+'.style';				
		// 	if(fs.existsSync(filepathstyle)){
		// 		topaste.tag_style='';
		// 		topaste.tag_style=fs.readFileSync(filepathstyle);
		// 		ret_style=topaste.tag_style;
		// 	}
		// }

					function createMapForMTiff(sourcepath, ext_type){
							var filename2=sourcepath.substring(sourcepath.lastIndexOf('/')+1,sourcepath.length);
							var filenameclean2=filename2.substring(0, filename2.lastIndexOf('.'));
							var terminal = require('child_process').spawn('rastr_info', ['-d',sourcepath,'-stat']);
							terminal.stdout.on('data', function (rdata) {
								topaste.tag_imagetype = 'PNG';
								topaste.tag_shapepath = dirpath;
								topaste.tag_mappath = filepath.replace('/', '\\');
								topaste.tag_layer_name = filenameclean;
								topaste.tag_layer_data = filenameclean2;
								var arr = misc.explode('\r\n', '' + rdata);
								for (var i = 0; i < arr.length; i++) {
									// Detecting projection
									if (arr[i].indexOf('PROJCS[') != -1) {
										if (arr[i].indexOf("WGS 84 / UTM zone 48N") > 0) {
											topaste.tag_projection = "\"proj=utm\"\
											\"zone=48\"\
											\"ellps=WGS84\"\
											\"datum=WGS84\"\
											\"units=m\"\
											\"no_defs\"";
										}
									} else if (arr[i].indexOf('GEOGCS[') != -1) {
										if (arr[i].indexOf("WGS 84 / UTM zone 48N") > 0) {
											topaste.tag_projection = "\"proj=utm\"\
											\"zone=48\"\
											\"ellps=WGS84\"\
											\"datum=WGS84\"\
											\"units=m\"\
											\"no_defs\"";
										}
									}

									if (!topaste.tag_projection) {
										topaste.tag_projection = "\"init=epsg:4326\"";
									}

									// Detecting lower left
									if (arr[i].indexOf('Lower Left  (') != -1) {
										var tempo = arr[i].replace('Lower Left  ( ', '');
										tempo = misc.substr(tempo, 0, tempo.indexOf(')'));
										var tempoarr = misc.explode(',', tempo);
										topaste.tag_extent_minx = parseFloat(tempoarr[0].trim());
										topaste.tag_extent_miny = parseFloat(tempoarr[1].trim());
									}
									
									// Detecting upper right
									if (arr[i].indexOf('Upper Right (') != -1) {
										var tempo = arr[i].replace('Upper Right (', '');
										tempo = misc.substr(tempo, 0, tempo.indexOf(')'));
										var tempoarr = misc.explode(',', tempo);
										topaste.tag_extent_maxx = parseFloat(tempoarr[0].trim());
										topaste.tag_extent_maxy = parseFloat(tempoarr[1].trim());
									}
									
									if (topaste.tag_extent_minx > topaste.tag_extent_maxx) {
										var temporaryValue = topaste.tag_extent_maxx;
										topaste.tag_extent_maxx = topaste.tag_extent_minx;
										topaste.tag_extent_minx = temporaryValue;
									}

									if (topaste.tag_extent_miny > topaste.tag_extent_maxy) {
										var temporaryValue = topaste.tag_extent_maxy;
										topaste.tag_extent_maxy = topaste.tag_extent_miny;
										topaste.tag_extent_miny = temporaryValue;
									}
									
									// Detecting size
									if (arr[i].indexOf('Size is ') != -1) {
										var tempo = arr[i].replace('Size is ', '');
										var tempoarr = misc.explode(', ', tempo);
										topaste.tag_size_x = tempoarr[0];
										topaste.tag_size_y = tempoarr[1];
									}
									// Detecting stmax
									if (arr[i].indexOf('    STATISTICS_MAXIMUM') != -1) {
										if(ext_type=='MTIF')
											var tempo = rngmax;
										else{
											var tempo = arr[i].replace('    STATISTICS_MAXIMUM=', '');
										}
										topaste.tag_stmax = tempo;
										max_raster_scale = tempo;
										//console.log('STATISTICS_MAX');
										//console.log(tempo);
									}
									// Detecting stmin
									if (arr[i].indexOf('    STATISTICS_MINIMUM') != -1) {
										if(ext_type=='MTIF'){
											var tempo = rngmin;
										}
										else{
											var tempo = arr[i].replace('    STATISTICS_MINIMUM=', '');
										}
										topaste.tag_stmin = tempo;
										min_raster_scale = tempo;
										//console.log('STATISTICS_MINIMUM');
										//console.log(tempo);
									}
									// Detecting type
									if (arr[i].indexOf('Geometry: ') != -1) {
										if (arr[i].indexOf('Polygon') != -1) {
											topaste.tag_layer_type = 'POLYGON';
											topaste.tag_layer_symbol = 'line1';
											topaste.tag_layer_symsize = '5';
										} else if (arr[i].indexOf('Point') != -1) {
											topaste.tag_layer_type = 'POINT';
											topaste.tag_layer_symbol = 'circle';
											topaste.tag_layer_symsize = '10';
										} else if (arr[i].indexOf('Line') != -1) {
											topaste.tag_layer_type = 'LINE';
											topaste.tag_layer_symbol = 'line1';
											topaste.tag_layer_symsize = '2';
										}
									}
								}
								topaste.tag_layer_color_min_r = from_color.r;
								topaste.tag_layer_color_min_g = from_color.g;
								topaste.tag_layer_color_min_b = from_color.b;

								topaste.tag_layer_color_max_r = to_color.r;
								topaste.tag_layer_color_max_g = to_color.g;
								topaste.tag_layer_color_max_b = to_color.b;
							});
							terminal.stdout.on('end', function (rdata) {
								var data = fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/rastr_template.map', 'utf8');
								data = populateTemplate(data, topaste);
								fileCreationBasedOnData(data);
							});
						}

						function createMapForTiff(sourcepath, ext_type){
							var filename2=sourcepath.substring(sourcepath.lastIndexOf('/')+1,sourcepath.length);
							var filenameclean2=filename2.substring(0, filename2.lastIndexOf('.'));
							console.log('222222222');
							console.log('sourcepath=',sourcepath);
							//var terminal = require('child_process').execFile('gdalinfo', ['-stats', '-hist', sourcepath]);
							var terminal = require('child_process').spawn('rastr_info', ['-stat', '-d',sourcepath]);
							topaste.tag_layer_type = 'tiff';
							var fileinfostr='';
							terminal.stdout.on('data', function (rdata) {
								fileinfostr=fileinfostr+rdata.toString();
							});

							terminal.stdout.on('end', function (rdata) {
								if(rdata)
									fileinfostr=fileinfostr+rdata.toString();
								fileinfo=JSON.parse(fileinfostr);
								epsg="4326";
								searchstr="AUTHORITY['EPSG','";
								ind=fileinfo.projection.lastIndexOf("AUTHORITY['EPSG','");
								if(ind!=-1){
									
									ind=ind+searchstr.length;
									end=fileinfo.projection.indexOf("'", ind+1);
									
									if(end!=-1){
										
										epsg=fileinfo.projection.substr(ind,end-ind);
										console.log('epsg', epsg)
									}
								}

								console.log('fileinfostr=');
								console.log(fileinfostr);
																
								topaste.tag_imagetype = 'PNG';
								topaste.tag_shapepath = dirpath;
								topaste.tag_mappath = filepath.replace('/', '\\');
								topaste.tag_layer_name = filenameclean;
								topaste.tag_layer_data = filename2;																
								
								topaste.tag_size_x = parseFloat(fileinfo.cell_size_x);
								topaste.tag_size_y = parseFloat(fileinfo.cell_size_y);

								topaste.tag_extent_minx = parseFloat(fileinfo.left);
								topaste.tag_extent_miny = parseFloat(fileinfo.top);
								topaste.tag_projection = "\"init=epsg:"+epsg+"\"";
								topaste.tag_extent_maxx = topaste.tag_extent_minx + parseFloat(fileinfo.cell_size_x) * parseFloat(fileinfo.column_count);
								topaste.tag_extent_maxy = topaste.tag_extent_miny + parseFloat(fileinfo.cell_size_y) * parseFloat(fileinfo.row_count);
								topaste.tag_stmax = parseFloat(fileinfo.max);
								topaste.tag_stmin = parseFloat(fileinfo.min);
								if(mapstyle!=undefined && mapstyle.indexOf('STYLE')!=-1)
									topaste.tag_processing =' 		PROCESSING   "BANDS=1"';
								else
									topaste.tag_processing = '';
								console.log(topaste.tag_style);
								if(topaste.tag_style=='1'){
								topaste.tag_style='CLASS\n\
		STYLE\n\
		  COLORRANGE 0 0 0   255 255 255 \n\
			  DATARANGE '+topaste.tag_stmin+' '+topaste.tag_stmax+'\n\
		  RANGEITEM "foobar"\n\
		END\n\
             END\n';
								}
								else{
									topaste.tag_style=topaste.tag_style.replace('#min', topaste.tag_stmin);
									topaste.tag_style=topaste.tag_style.replace('#max', topaste.tag_stmax);									
								}
																
								
								var data = fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/rastr_template.map', 'utf8');
								data = populateTemplate(data, topaste);
								fileCreationBasedOnData(data);
							});

						}

		console.log('0000000');				
		console.log('ext=');				
		console.log(ext);				

		if(ext=='SHP')
		{
			// Shapefile case


			var terminal = require('child_process').execFile('ogrinfo', ['-so', sourcepath, filenameclean]);
			terminal.stdout.on('data', function (rdata) {
				//console.log(rdata);
				topaste.tag_shapepath = dirpath;
				topaste.tag_mappath = filepath.replace('/', '\\');
				topaste.tag_layer_name = filenameclean;
				topaste.tag_layer_data = filenameclean;

				var arr = misc.explode('\r\n', '' + rdata);
				for (var i = 0; i < arr.length; i++) {
					// Detecting extent
					if (arr[i].indexOf('Extent: ') != -1) {
						var tempo = arr[i].replace('Extent: ', '');
						tempo = tempo.replace(') - (', ', ');
						tempo = tempo.replace('(', '');
						tempo = tempo.replace(')', '');
						var tempoarr = misc.explode(', ', tempo);
						topaste.tag_extent_minx = tempoarr[0];
						topaste.tag_extent_miny = tempoarr[1];
						topaste.tag_extent_maxx = tempoarr[2];
						topaste.tag_extent_maxy = tempoarr[3];
					}
					// Detecting type
					if (arr[i].indexOf('Geometry: ') != -1) {
						if (arr[i].indexOf('Polygon') != -1) {
							topaste.tag_layer_type = 'POLYGON';
							topaste.tag_layer_symbol = 'line1';
							topaste.tag_layer_symsize = '5';
						} else if (arr[i].indexOf('Point') != -1) {
							topaste.tag_layer_type = 'POINT';
							topaste.tag_layer_symbol = 'circle';
							topaste.tag_layer_symsize = '10';
						} else if (arr[i].indexOf('Line') != -1) {
							topaste.tag_layer_type = 'LINE';
							topaste.tag_layer_symbol = 'line1';
							topaste.tag_layer_symsize = '2';
						}
						// Needs revising
						topaste.tag_layer_color_r = from_color.r;
						topaste.tag_layer_color_g = from_color.g;
						topaste.tag_layer_color_b = from_color.b;
					}
				}
			});
			terminal.stdout.on('end', function (rdata) {
				//console.log('end');
				//console.log(rdata);
				var data = fs.readFileSync(rootpath+'/modules/community/geothemes/maptemplates/vector_template.map', 'utf8');
				data = populateTemplate(data, topaste);
				fileCreationBasedOnData(data);
			});
			terminal.on('exit', function(code) {
			if (code != 0) {
				console.log('Failed: ' + code);
			}
		});
		}
		else if (ext=='MTIF')
		{

			if(req_date !=undefined && !misc.is_number(req_date) )
				var req_date = Date.parse(req_date);
			var data = fs.readFileSync(sourcepath, 'utf8');
			var dir_separator='/';
			var dir=sourcepath.slice(0,sourcepath.lastIndexOf(dir_separator)+1);
			var rngfile=sourcepath.slice(0,sourcepath.lastIndexOf('.')+1)+'rng';

			var rngtxt = fs.readFileSync(rngfile, 'utf8');
			var row = misc.explode(' ', rngtxt);
			rngmin=row[0];
			rngmax=row[1];

			var outputobj = new Object();
			outputobj.data = new Array();
			var arr = misc.explode('\r\n', '' + data);
			var idealfilepath = filepath;
			for (var i = 0; i < arr.length; i++) {
				var row = misc.explode(' ', arr[i]);
				var c_date = Date.parse(row[0]);
				//filepath = idealfilepath.replace('UNIQUE_DUMMY', common.randomString(10));
				if(req_date === undefined){
					createMapForMTiff(dir+row[1], ext);
					outputobj.data.push({mapfile: filepath, time: c_date});
				}
				else if (misc.is_number(req_date)){
					if(req_date== row[0]){
						createMapForMTiff(dir+row[1], ext);
						outputobj.data.push({mapfile: filepath, time: c_date});
					}
				}
				else{
					if(req_date.getTime() > c_date.getTime()){
						createMapForMTiff(dir+row[1], ext);
						outputobj.data.push({mapfile: filepath, time: c_date});
					}
				}
			}
			return outputobj;
		}
		else if(ext=='TIF' || ext=='TIFF'){
			console.log('11111111111111111111');
			createMapForTiff(sourcepath, ext);
		}
		else{
			console.log('The ext is not defined.');
			callback({status:'error', message:''});
			return;
		}

		function populateTemplate(data, topaste) {
			for(var key in topaste){
				data = data.replace(key, topaste[key]);
			}
			return data;
		}

		function fileCreationBasedOnData(data) {
			if (data != '') {
				fs.writeFileSync(filepath, data, 'utf-8', function (err) {
					if (err) {
						console.log(err);
					}
				});

				callback({status:'success', wms_link:mapserver_path+"?map="+ toMapserverPath(filepath), type:topaste.tag_layer_type, map_style: ret_style});
				return 'error';
			};
			callback({status:'error'});
		}
	}

	this.update_symbols=function(symbols_path, fieldname, mdobj){
		var select_query='select name, symbol, filled from public.mapsymbols';
		var def_symbols='';
		var def_symbols_path=map_file_dir + '/symbols/def_symbols.sym';
		if(fs.existsSync(def_symbols_path)){
			def_symbols=fs.readFileSync(def_symbols_path);			
			//console.log("def_symbols="+def_symbols);
		}
		
		//var symbols_path=this.map_file_dir + '\\' + mapfilename + '.sym';
		//console.log("symbols_path="+symbols_path);
		var mainrootdir = calipso.config.getModuleConfig("rfm", "mainrootdir");
		
		var symbol_content='';
		var column=common.getcolumn(fieldname, mdobj);
		if(column && column.widget && column.widget.style){
			var style=JSON.parse(column.widget.style);
			if(style.style_type=='single' && style.base_rule){
				for(var j=0; j<style.base_rule.symbolizers.length; j++){						
					if(style.base_rule.symbolizers[j].image && style.base_rule.symbolizers[j].image!=''){
						file_path=calipso.modules.rfm.fn.getpath(style.base_rule.symbolizers[j].image);
						ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
						new_path=map_file_dir + '/images/' + style.base_rule.symbolizers[j].image + '.' + ext;
						filename='images/'+style.base_rule.symbolizers[j].image + '.' + ext;
						console.log(file_path);
						console.log(new_path);
						calipso.modules.rfm.fn.copyFile(file_path,new_path, function(error){
							if(error)
								console.log(error);								
						} );
						
						symbol_content+='SYMBOL\n\
						NAME "' + style.base_rule.symbolizers[j].image + '"\n\
						TYPE pixmap\n\
						IMAGE "'+filename+'"\n\
						END\n';
					}
				}
			}
			else{
				if(style.rules){
					for(var i=0; i<style.rules.length; i++){
						if(!style.rules[i].symbolizers)
							continue;
						for(var j=0; j<style.rules[i].symbolizers.length; j++){						
							if(style.rules[i].symbolizers[j].image && style.rules[i].symbolizers[j].image!=''){
								file_path=calipso.modules.rfm.fn.getpath(style.rules[i].symbolizers[j].image);
								ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
								new_path=map_file_dir + '/images/' + style.rules[i].symbolizers[j].image + '.' + ext;
								filename='images/'+style.rules[i].symbolizers[j].image + '.' + ext;
								console.log(file_path);
								console.log(new_path);
								calipso.modules.rfm.fn.copyFile(file_path,new_path, function(error){
									if(error)
										console.log(error);								
								} );
								
								symbol_content+='SYMBOL\n\
	  NAME "' + style.rules[i].symbolizers[j].image + '"\n\
	  TYPE pixmap\n\
	  IMAGE "'+filename+'"\n\
	END\n';
							}
						}
					}				
				}
			}
		}
		for(var i=0; i<font_symbols.length; i++){
			for(var j=font_symbols[i].startsymbol; j<=font_symbols[i].endsymbol; j++){
							symbol_content+='SYMBOL\n\
	NAME "' + font_symbols[i].name + j +'"\n\
	TYPE truetype\n\
	FONT "'+font_symbols[i].name+'"\n\
	CHARACTER "&#'+j+';"\n\
END\n';				
			}
		}
		
	
		//console.log(symbol_content);
		var query = dbclient.query(select_query, function(error, query_result, unofields2) {
			if (error === null)	{
				
				for(var i=0; i<query_result.rows.length; i++){
					if(!query_result.rows[i].symbol || query_result.rows[i].symbol=='' || query_result.rows[i].name=='image' || query_result.rows[i].symbol=='font symbol')
						continue;
					var sfld='';
					if(query_result.rows[i].filled)
						sfld='Filled TRUE\n';
					symbol_content+='\
Symbol\n\
Name \''+query_result.rows[i].name+'\'\n\
Type VECTOR\n\
'+sfld+'\
Points\n\
'+query_result.rows[i].symbol+'\n\
END\n\
END\n';

//Filled FALSE\n\
				}
				//console.log("symbol_content="+symbol_content);
				symbol_content=def_symbols+'\n'+symbol_content;
				if (symbol_content != '\n') {
					fs.writeFileSync(symbols_path, symbol_content, 'utf-8', function (err) {
						if (err) {
							console.log(err);
						}
					});
				}
			} else {
				console.log(select_query);
				console.log(error.message);
				callback(error.message);
			}
		});
	}
	return this;
}

module.exports = geo_map;