var rootpath = process.cwd() + '/';
var config = require("../../conf.json");


//	misc = require(rootpath + 'themes/core/cleanslate/public/js/misc'),
// 	common = require(rootpath + 'modules/community/common/lib/common'),
// 	sanitize = require('validator').sanitize;
// //var email 	= require(rootpath + 'node_modules/emailjs/email');
//var email 	= require("emailjs/email");

var mail = require("mailer");
var fieldcalc = require("../../public/js/geothemes/fieldcalc");
var geo_access  = require('./geo_access');
/*
var server 	= email.server.connect({
   user:    "bit", 
   password:"tA%_thitsh", 
   host:    "mail.icc.ru", 
   ssl:     true
});

*/

var service_flds = ['is_deleted',  'created_by','edited_by', 'edited_on', 'created_on','id'];


//var poolConfig = 'smtps://username:password@smtp.example.com/?pool=true';

/*
	Basic permission constants
*/
var ableToAddEditViewOwn = 0;
var ableToViewOthers = 1;
var ableToEditOthers = 2;
var isOwner = 3;
var isModerator = 4;

function mysql_real_escape_string(str) {
	return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
		switch (char) {
			case "\0":
				return "\\0";
			case "\x08":
				return "\\b";
			case "\x09":
				return "\\t";
			case "\x1a":
				return "\\z";
			case "\n":
				return "\\n";
			case "\r":
				return "\\r";
			case "\"":
			case "'":
			case "\\":
			case "%":
				return "\\" + char; // prepends a backslash to backslash, percent,
			// and double/single quotes
		}
	});
}

// Inheritance functions
function extend(Child, Parent) {
	var F = function () { }
	F.prototype = Parent.prototype
	Child.prototype = new F()
	Child.prototype.constructor = Child
	Child.superclass = Parent.prototype
	mixin(Child.prototype, Parent)
}

function _qv(query, valuename) {
	if (query === undefined)
		return '';
	if (typeof query === 'object' && valuename in query) {
		return query[valuename];
	} else {
		return '';
	}
} //end _qv()


function randomString(length) {
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
	if (!length) {
		length = Math.floor(Math.random() * chars.length);
	}
	var str = '';
	for (var i = 0; i < length; i++) {
		str += chars[Math.floor(Math.random() * chars.length)];
	}
	return str;
}

function mixin(dst, src) {
	var tobj = {}
	for (var x in src) {
		if ((typeof tobj[x] == "undefined") || (tobj[x] != src[x])) {
			dst[x] = src[x];
		}
	}
	/*
	if(document.all && !document.isOpera){
		var p = src.toString;
		if(typeof p == "function" && p != dst.toString && p != tobj.toString &&
		 p != "\nfunction toString() {\n    [native code]\n}\n"){
			dst.toString = src.toString;
		}
	}*/
}

function sql_field(object) {
	this.object = object;

	return this;
}

sql_field.prototype.select_expr = function (fn, settings) {
	if (fn == 'max' || fn == 'min' || fn == 'avg' || fn == 'count')
		return fn + '(' + this.object.tablename + '."' + this.object.sqlname + '")';
	else
		return this.object.tablename + '."' + this.object.sqlname + '"';
}

sql_field.prototype.where = function (tab_alias, conditions) {
	return '';
}


sql_field.prototype.sql_value = function (value) {
	return value;
}


function sql_boolean(object) {
	this.fieldname = object.fieldname;
	this.object = object;
	//this.tab_alias=tab_alias;
	if (!object.sqlname)
		object.sqlname = this.fieldname;
	this.select_expr = function (fn, settings) {
		if (fn == 'count')
			return fn + '(' + object.tablename + '."' + object.sqlname + '")';
		else
			return object.tablename + '."' + object.sqlname + '"';
	}
	this.where = function (tab_alias, conditions) {
		// console.log('conditions=',conditions);
		if (tab_alias)
			tab_alias = object.tablename + '.';
		else
			tab_alias = '';
			conditions=conditions+'';
		v = conditions.toLowerCase().trim();
		if(v=='true'){
			return tab_alias + '"' + object.sqlname + '" = TRUE ';
		}
		else if(v=='false'){
			return tab_alias + '"' + object.sqlname + '" = FALSE ';
		}
		else
			return " FALSE ";		
	}
	this.sql_value = function (value) {
		console.log('boolean value=',value, typeof value)
		if ((typeof value == "boolean" && value) || value==='true')
			return "true";
		else if ((typeof value === "boolean" && !value) || value==='false') {			
			return "false";
		}
		else
			return 'NULL';
	}
	return this;
}
extend(sql_boolean, sql_field);

function sql_date(object) {
	this.fieldname = object.fieldname;
	this.object = object;
	if (!object.sqlname)
		object.sqlname = this.fieldname;
	//this.tab_alias=tab_alias;
	this.select_expr = function (fn, settings) {
		if (fn == 'max' || fn == 'min' || fn == 'avg')
			return 'to_char(' + fn + '(' + object.tablename + '."' + object.sqlname + '"), \'YYYY-MM-DD HH24:MI:SS\')';
		else if (fn == 'count') {
			return fn + '(' + object.tablename + '."' + object.sqlname + '")';
		}
		else if (fn.indexOf('groupby') != -1) {
			gr_part = fn.split('_');
			period = gr_part[1];
			return 'to_char(date_trunc(\'' + period + '\',' + object.tablename + '."' + object.sqlname + '"), \'YYYY-MM-DD HH24:MI:SS\')';
		}
		else
			return 'to_char(' + object.tablename + '."' + object.sqlname + '", \'YYYY-MM-DD HH24:MI:SS\')';
	}
	this.where = function (tab_alias, conditions) {
		if (tab_alias)
			tab_alias = object.tablename + '.';
		else
			tab_alias = '';

		var sWhere = '';
		var tempoarr = misc.explode(' - ', conditions, null);
		if (tempoarr[0] != '∞') {
			sWhere = tab_alias + '"' + object.sqlname + '" >= to_timestamp(\'' + tempoarr[0] + '\', \'YYYY-MM-DD HH24:MI:SS\')';
		}
		if (tempoarr[1] != '∞') {
			if (tempoarr[0] != '∞') {
				sWhere += ' AND ';
			}
			sWhere += tab_alias + '"' + object.sqlname + '" <= to_timestamp(\'' + tempoarr[1] + '\', \'YYYY-MM-DD HH24:MI:SS\') ';
		}
		return sWhere;
	}
	this.sql_value = function (value) {
		value = value.replace(/'/gi, "");
		if(value=='' || value===undefined || value==null)
			return ' NULL ';
		return 'to_timestamp(\'' + value + '\', \'YYYY-MM-DD HH24:MI:SS\')';
	}
	return this;
}
extend(sql_date, sql_field);
/*
field.tablename=$("#tablename", fieldcontainer);
field.sqltablename=$("#sqltablename", fieldcontainer);
field.sqlname=$("#sqlname", fieldcontainer);
*/

function sql_string(object) {
	this.fieldname = object.fieldname;
	this.object = object;
	//this.tab_alias=tab_alias;
	if (!object.sqlname)
		object.sqlname = this.fieldname;
	this.select_expr = function (fn, settings) {
		if (fn == 'count')
			return fn + '(' + object.tablename + '."' + object.sqlname + '")';
		else
			return object.tablename + '."' + object.sqlname + '"';
	}
	this.where = function (tab_alias, conditions) {
		if (tab_alias)
			tab_alias = object.tablename + '.';
		else
			tab_alias = '';
		v = conditions.toLowerCase();
		vsl = v.split(' ');
		vs = [];
		for (var i = 0; i < vsl.length; i++) {
			v = vsl[i].trim();
			if (v != '')
				vs.push(v);
		}

		termWhere = '';
		if (object.widget.properties && object.widget.properties.tsvector != '' && object.widget.properties.tsvector != undefined) {
			tsvector = object.widget.properties.tsvector;
			var res_st = '';
			for (var i = 0; i < vs.length; i++) {
				if (vs[i] != '') {
					if (res_st != '')
						res_st += ' & ';
					res_st += vs[i] + ':*';
				}
			}
			termWhere = tab_alias + '"' + tsvector + '" @@ to_tsquery( \'' + res_st + '\')';
		}
		else {
			for (l = 0; l < vs.length; l++) {
				if (termWhere != '')
					termWhere += ' AND ';
				termWhere += 'lower(' + tab_alias + '"' + object.sqlname + '")' + " LIKE '%" + vs[l] + "%'";
			}
		}
		return termWhere;
	}
	this.sql_value = function (value) {
		if (value === undefined)
			return 'NULL';
		if (typeof value == "object")
			return "'" + JSON.stringify(value) + "'";
		else if (typeof value === "string") {
			value = value.replace(/'/g, '&quot;');
			return "'" + value + "'";
		}
		else
			return 'NULL';
	}
	return this;
}
extend(sql_string, sql_field);

function sql_geometry(object, convertGeometries) {
	this.cname = 'sql_geometry';
	this.fieldname = object.fieldname;
	this.object = object;
	if (!object.sqlname)
		object.sqlname = this.fieldname;
	//this.tab_alias=tab_alias;	
	this.select_expr = function (fn, period) {
		if (fn == 'count')
			return fn + '(' + object.tablename + '."' + object.sqlname + '")';
		else if (fn != '' && fn.indexOf('distance') == 0) {
			ar = fn.split('--');
			return "round(cast(ST_Distance(ST_Transform(" + object.tablename + '."' + object.sqlname + '"' + ", 28418), ST_Transform(ST_GeomFromText('" + ar[1] + "',4326),28418)) As numeric)/1000,2)";
		}
		else if (fn === 'regions') {
			return "public.adm4_region_f.id region_id, public.adm4_region_f.name"
		}
		else if (fn === 'districts') {
			return "public.adm6_district_f.id region_id, public.adm6_district_f.name"
		}
		else {
			if (convertGeometries)
				return 'ST_AsText(' + object.tablename + '."' + object.sqlname + '")';
			else
				return object.tablename + '."' + object.sqlname + '"';
		}
	}

	this.where = function (tab_alias, conditions) {
		if (tab_alias)
			tab_alias = object.tablename + '.';
		else
			tab_alias = '';

		if (conditions === false) {
			return tab_alias + '"' + object.sqlname + '" IS NOT NULL';
		} 
		else if (conditions.indexOf('geotable') == 0) {
			parts = conditions.split(':');
			var geotable = '';
			var tableId = parts[1];
			var recordId = parts[2];
			var field = parts[3];
			switch (tableId) {
				case '2171':
					geotable = 'public.adm4_region_f'
					break;
				case '2172':
					geotable = 'public.adm6_district_f'
					break;
			
				default:
					throw new Error('Unsupported table ID');
					break;
			}

			return geotable + ".id = " + recordId;
			// where ST_Contains(r.geom, s.pnt) and r.id = 1067
			// return "ST_Distance( ST_GeomFromText('" + coordinates + "',4326)," + tab_alias + '"' + object.sqlname + '")<' + dist;
		}
		else if (conditions.indexOf('--') != -1) {
			parts = conditions.split('--');
			coordinates = parts[1];
			dist = parts[0];
			proj = '4326';
			if (dist.indexOf(':') != -1) {
				distparts = dist.split(':');
				proj = distparts[0];
				dist = distparts[1];
			}
			if (dist == '' || isNaN(parseFloat(dist)) || !isFinite(dist) || proj == '')
				return '';
			if (proj == '4326')
				return "ST_Distance( ST_GeomFromText('" + coordinates + "',4326)," + tab_alias + '"' + object.sqlname + '")<' + dist;
			else
				return "ST_Distance( ST_Transform(ST_GeomFromText('" + coordinates + "',4326), " + proj + "),ST_Transform(" + tab_alias + '"' + object.sqlname + '", ' + proj + '))<' + dist;
		}
		else
			return 'ST_Contains( ST_GeomFromText( \'' + conditions + '\', 4326), ' + tab_alias + '"' + object.sqlname + '" )';
	}
	this.sql_value = function (value) {
		value = value.replace(/'/gi, "");
		if (value && value.indexOf && value.indexOf('(')!=-1) {
			if (value.indexOf('MULTILINESTRING(((') == 0) {
				value = value.replace('MULTILINESTRING(((', 'MULTILINESTRING((');
				value = value.replace(')))', '))');
			}
			return 'ST_GeomFromText(\'' + value + '\',4326)';
		}
		else if(value.length>0 && value.indexOf('(')==-1 ){
			return '\'' + value + '\'';
		}
		else
			return 'NULL';
	}
	return this;
}
extend(sql_geometry, sql_field);

var number_operators = {
	PropertyIsEqualTo: '=',
	PropertyIsNotEqualTo: '!=',
	PropertyIsLessThan: '<',
	PropertyIsLessThanOrEqualTo: '<=',
	PropertyIsGreaterThan: '>',
	PropertyIsGreaterThanOrEqualTo: '>='
}


function sql_number(object) {
	this.fieldname = object.fieldname;
	this.object = object;
	if (!object.sqlname)
		object.sqlname = this.fieldname;
	//	this.tab_alias=tab_alias;
	this.select_expr = function (fn, period) {
		if (fn != '' && fn != 'groupby')
			return fn + '(' + object.tablename + '."' + object.sqlname + '")';
		else
			return object.tablename + '."' + object.sqlname + '"';
	}

	this.where = function (tab_alias, conditions) {
		if (tab_alias)
			tab_alias = object.tablename + '.';
		else
			tab_alias = '';
		var sWhere = '';
		var tempoarr = conditions.split('-');//misc.explode('-', , null);
		if (tempoarr.length == 2 || tempoarr.length == 4) {
			if (tempoarr.length > 0) {
				sWhere = tab_alias + '"' + object.sqlname + '"' + number_operators[tempoarr[1]] + tempoarr[0];
			}
			if (tempoarr.length == 4) {
				sWhere += ' AND ';
				sWhere += tab_alias + '"' + object.sqlname + '" ' + number_operators[tempoarr[3]] + tempoarr[2];
			}
		}
		else if (tempoarr.length == 1) {
			sWhere += tab_alias + '"' + object.sqlname + '"=' + tempoarr[0];
		}
		return sWhere;
	}
	this.sql_value = function (value) {
		//console.log('sql_value, value=',value,this.fieldname);
		if(typeof value == 'number')
			return value;
		else if(typeof value === 'object' && value.id && typeof value.id == 'number')
			return value.id;
		else{
			try {
				value=JSON.parse(value);
				if(typeof value == 'number')
					return value;
				else if(typeof value === 'object' && value.id && typeof value.id === 'object')
					return value.id;
				else{
					console.log('sql_value, value=',value,this.fieldname);
					return 'NULL';
				}
			} catch(e) {
				return 'NULL';
			}
			//console.log('sql_value, value=',value,this.fieldname);
			return 'NULL';
		}
	}
	return this;
}
extend(sql_number, sql_field);

function sql_tabaccess(object, user_id) {
	this.fieldname = object.fieldname;
	this.object = object;
	if (!object.sqlname)
		object.sqlname = this.fieldname;
	//	this.tab_alias=tab_alias;	

	this.select_expr = function (fn, period) {
		if (fn == 'count')
			return fn + '(' + object.tablename + '."id")';
		else
			return "public.dataset_access(" + object.tablename + ".id, '" + user_id + "')";
	}

	this.where = function (tab_alias, conditions) {
		var sWhere = '';
		return sWhere;
	}
	this.sql_value = function (value) {
		return value;
	}
	return this;
}
extend(sql_tabaccess, sql_field);


/**
 * @param Object  settings                   SQL generation settings
 * @param Boolean settings.convertGeometries Specifies if geometries should be fetched as WKT
 */
function geo_table(user_id, dataset_id, meta, settings) {
	console.log("geo_table". meta);
	this.regionField = '';
	this.dataset_id=dataset_id;
	this.districtField = '';
	this.isUseTableDistrict = false;
	this.isUseTableRegion = false;
	// this.

	if (meta == null || meta===undefined) {
		console.log('Error in geo_table creating, meta is null dataset_id=', dataset_id);
		var stack = new Error().stack
		console.log( stack )
		return;
	}
	this.curent_user_id = '';
	this.fields = [];
	this.tabs = [];
	this.details = [];
	

	if (!settings) settings = {};
	if ("convertGeometries" in settings === false) {
		settings.convertGeometries = true;
	}

	var g_tab = this;
	this.publishing = false;
	this.grouping = false;
	this.document_template = {};
	this.build_template_tree = function (m, tab_alias, includefields, level, doc_part, main_field) {
		if (level > 4) return;
		for (var i = 0; i < m.columns.length; i++) {
			if (level==0 && m.columns[i].fieldname == 'id' && dataset_id!=5)
				m.columns[i].widget = { name: 'number' };
			if (level>0 && m.columns[i].fieldname == 'id')
				continue;
			if (level == 0 && m.columns[i].fieldname == 'published')
				this.publishing = true;
			if (includefields != undefined && includefields.length > 0 && includefields.indexOf(m.columns[i].fieldname) == -1)
				continue;
			if (m.columns[i].tablename === undefined)
				m.columns[i].tablename = tab_alias;
			if (m.columns[i].sqltablename === undefined)
				m.columns[i].sqltablename = meta.tablename;
			if (m.columns[i].sqlname === undefined)
				m.columns[i].sqlname = m.columns[i].fieldname;
			if (m.columns[i].widget === undefined)
				m.columns[i].widget = { name: 'edit' };

			switch (m.columns[i].widget.name) {
				case 'date':
					doc_part[m.columns[i].fieldname] = new sql_date(m.columns[i]);
					break;
				case 'point':
					doc_part[m.columns[i].fieldname] = new sql_geometry(m.columns[i], settings.convertGeometries);
					break;
				case 'line':
					doc_part[m.columns[i].fieldname] = new sql_geometry(m.columns[i], settings.convertGeometries);
					break;
				case 'polygon':
					doc_part[m.columns[i].fieldname] = new sql_geometry(m.columns[i], settings.convertGeometries);
					break;
				case 'string':
					doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
					break;
				case 'edit':
					doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
					break;
				case 'textarea':
					doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
					break;
				case 'number':
					doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
					break;
				//case 'tableaccess':
				//	doc_part[m.columns[i].fieldname]=new sql_tabaccess(m.columns[i]);
				//	break;
				case 'details':
					var ref_meta = meta.usingmeta[m.columns[i].widget.properties.dataset_id];
					var det_table = new geo_table(user_id, m.columns[i].widget.properties.dataset_id, ref_meta);
					this.details.push({ tab: det_table, ref_meta: ref_meta, col_meta: m.columns[i], reftype: 'details' });
					break;
				case 'links':
					var ref_meta = meta.usingmeta[m.columns[i].widget.properties.dataset_id];
					var det_table = new geo_table(user_id, m.columns[i].widget.properties.dataset_id, ref_meta);
					this.details.push({ tab: det_table, ref_meta: ref_meta, col_meta: m.columns[i], reftype: 'links' });
					break;
				case 'classify':
					doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
					doc_part[m.columns[i].fieldname].subfields = [];
					if (m.columns[i].widget.properties.db_field !== undefined && m.columns[i].widget.properties.db_field != '')
						doc_part[m.columns[i].fieldname].displayfields = m.columns[i].widget.properties.db_field;


					//ref_meta=m.columns[i].widget.properties.ref_meta;
					ref_meta = meta.usingmeta[m.columns[i].widget.properties.dataset_id];
					if (ref_meta && doc_part[m.columns[i].fieldname].displayfields) {
						tab = ref_meta.tablename;
						join_tab_alias = m.columns[i].widget.properties.reftablename;
						doc_part[m.columns[i].fieldname].join_tab_alias = join_tab_alias;
						cond = m.columns[i].tablename + '."' + m.columns[i].sqlname + '" = ' + join_tab_alias + '.id AND NOT ' + join_tab_alias + '.is_deleted';
						this.tabs.push({ table: tab, alias: join_tab_alias, join_cond: cond });
						for (var h = 0; h < doc_part[m.columns[i].fieldname].displayfields.length; h++) {
							reffieldname = doc_part[m.columns[i].fieldname].displayfields[h];
							for (var d = 0; d < ref_meta.columns.length; d++) {
								if (reffieldname == ref_meta.columns[d].fieldname) {
									var clmn = JSON.parse(JSON.stringify(ref_meta.columns[d]));
									clmn.tablename = join_tab_alias;
									newrefname = 'ref_' + reffieldname + randomString(4);
									doc_part[newrefname] = new sql_string(clmn);
									doc_part[newrefname].fieldname = newrefname;
									this.fields.push(doc_part[newrefname]);
									doc_part[m.columns[i].fieldname].subfields.push({ name: reffieldname, refname: newrefname });
									break;
								}
							}
						}
					}
					break;
				default:
					switch (m.columns[i].type) {
						case 'integer':
							doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
							break;
						case 'serial':
							doc_part[m.columns[i].fieldname] = new sql_number(m.columns[i]);
							break;
						case 'bool':
							doc_part[m.columns[i].fieldname] = new sql_boolean(m.columns[i]);
							break;
							
						default:
							doc_part[m.columns[i].fieldname] = new sql_string(m.columns[i]);
							break;
					}
					break;
			}
			if (doc_part[m.columns[i].fieldname]) {
				this.fields.push(doc_part[m.columns[i].fieldname]);
				doc_part[m.columns[i].fieldname].main_field = main_field;
			}
			/*
			if(tab_alias=='main.')
				doc_part[m.columns[i].fieldname].field_alias = doc_part[m.columns[i].fieldname].fieldname;
			else
				doc_part[m.columns[i].fieldname].field_alias = doc_part[m.columns[i].fieldname].tab_alias.slice(0,-1)+'_' + doc_part[m.columns[i].fieldname].fieldname;	
			*/
		}
	}
	tab = meta.tablename;
	tab_alias = 'main';
	this.tabs.push({ table: tab, alias: tab_alias, join_cond: '' });
	includefields = [];
	if (settings.includefields)
		includefields = settings.includefields;
	this.build_template_tree(meta, tab_alias, includefields, 0, this.document_template);


	this.buildselect = function (req, include, qparams, groupby) {
		sfields = [];
		this.grouping = false;

		for (var i = 0; i < this.fields.length; i++) {
			//if( include!=undefined && include.length>0 && include.indexOf(this.fields[i].fieldname)==-1)
			//	continue;
			fn = _qv(qparams, 'g_' + this.fields[i].fieldname);
			if (fn != '') {
				this.grouping = true;
				break;
			}
		}
		var id_filter_value = _qv(qparams, 'f_id');
		for (var i = 0; i < this.fields.length; i++) {
			fn = _qv(qparams, 'g_' + this.fields[i].fieldname);
			if (this.grouping && fn == '')
				fn = 'count';
			if (fn == 'regions') {				
	
				this.isUseTableRegion = true;
				
				this.isgroupbyregion = true;
				this.groupbyregionfield = this.fields[i].object.tablename + '."' + this.fields[i].object.sqlname + '"';
				this.regionField = 	this.fields[i].object.tablename + '."' + this.fields[i].object.sqlname +'"';
				this.fields[i].fn = fn;
			}
			if (fn == 'districts') {
				this.isUseTableDistrict = true;
				this.isGroupByDistrict = true;
				this.groupByDistrictField = this.fields[i].object.tablename + '."' + this.fields[i].object.sqlname + '"';
				this.districtField = this.fields[i].object.tablename + '."' + this.fields[i].object.sqlname +'"';
				this.fields[i].fn = fn;
			}
			if (fn.indexOf('groupby') != -1)
				groupby.push(this.fields[i].select_expr(fn));
			if (include != undefined && include.length > 0 && include.indexOf(this.fields[i].fieldname) == -1)
				continue;
			if (this.fields[i].displayfields != undefined) {
				;
			}
			/*			if(this.fields[i].cname)
							sfields.push(this.fields[i].select_expr(fn) + ' AS ' + this.fields[i].fieldname );
						else*/
			sfields.push(this.fields[i].select_expr(fn) + ' AS "' + this.fields[i].fieldname + '"');
			if(this.fields[i].subfields && include != undefined && include.length > 0 ){
				for(var sf=0; sf<this.fields[i].subfields.length; sf++){
					if(include.indexOf(this.fields[i].subfields[sf].refname)==-1 ){
						var fld=this.document_template[this.fields[i].subfields[sf].refname];
						sfields.push(fld.select_expr(fn) + ' AS "' + fld.fieldname + '"');
					}
				}
			}



			// console.log('I am here.', dataset_id);
			if (this.fields[i].fieldname == 'JSON' && (dataset_id == 100 || dataset_id =='2ba9414e-f63b-469d-b971-74e74c74bba8') ) {
				if(req.user && req.user.isadmin){
					sfields.push("'222' AS a_access");
				}
				else{
					sfields.push("public.dataset_access(id, '" + user_id + "') AS a_access");
				}
				
				sfields.push("description AS dataset_description");
				sfields.push("id AS dataset_id");
			}
			//adding calculating fields
			cf = _qv(qparams, 'calc_' + this.fields[i].fieldname);
			if (cf != '') {
				sfields.push(this.fields[i].select_expr(cf) + ' AS "calc_' + this.fields[i].fieldname + '"');
			}
		}
		s = sfields.join(', ');
		return s;
	};

	this.buildfrom = function () {
		var from = '';

		for (var i = 0; i < this.tabs.length; i++) {
			var tableName = this.tabs[i].table;

			if (this.tabs[i].table.indexOf(" ") !== -1) {
				var splittedTableName = this.tabs[i].table.split(".");
				if (splittedTableName.length === 2) {
					var schema = splittedTableName[0];
					var table = splittedTableName[1];
					tableName = schema + ".\"" + table + "\"";
				}
			}

			if (from == '')
				from = tableName + ' ' + this.tabs[i].alias;
			else if(this.tabs[i].join_cond!=='')
				from += ' LEFT OUTER JOIN ' + tableName + ' ' + this.tabs[i].alias + ' ON ' + this.tabs[i].join_cond;				
			else
				from += ' LEFT OUTER JOIN ' + tableName + ' ' + this.tabs[i].alias;
		}
		if (this.isUseTableRegion) {
			from += ' JOIN public.adm4_region_f ON ST_Contains(public.adm4_region_f.geom, ' + this.regionField + ') ';
		}
		if (this.isUseTableDistrict) {
			from += ' JOIN public.adm6_district_f ON ST_Contains(public.adm6_district_f.geom, ' + this.districtField + ') ';
		}

		return from;
	};


	function removeHtmlSymbols(txt) {
		if(txt==null || txt===undefined || txt.replace===undefined)
			return '';
		return txt
			.replace(/&amp;/g, "&")
			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">")
			.replace(/&quot;/g, '')
			.replace(/&#039;/g, '')
			.replace(/'/g, '');;
	}
	this.buildwhere = function (req, qparams, use_alias, updating) {
		
		if (this.tabs.length == 0)
			return 'false';
		sWhere = '';
		var f_id = false;
		for (var i = 0; i < this.fields.length; i++) {
			field_filter_value = _qv(qparams, 'f_' + this.fields[i].fieldname);
			if (field_filter_value == '' && ["point", "line", "polygon"].indexOf(this.fields[i].object.type) !== -1 && settings.convertGeometries === false) {
				if (sWhere != '') {
					sWhere += ' AND ';
				}

				sWhere += "(" + this.fields[i].where(use_alias, false) + ") ";
			} else if (field_filter_value == '') {
				continue;
			} else {
				// console.log('buildwhere1=', this.fields[i].fieldname, field_filter_value);
				if (this.fields[i].fieldname == 'id'){
					// console.log(this.fields[i]);
					f_id = true;
				}
				field_filter_value = removeHtmlSymbols(field_filter_value+'');
				// console.log('buildwhere1=', this.fields[i].fieldname, field_filter_value);
				if (sWhere != '')
					sWhere += ' AND ';
				// console.log('field_filter_value=', field_filter_value);
				var gtempoarr = field_filter_value.split(';');//misc.explode(';', field_filter_value, null);
				if (gtempoarr.length > 1)
					sWhere += ' (';
				for (var j = 0; j < gtempoarr.length; j++) {
					gtempoarr[j] = gtempoarr[j].trim();					
					if (gtempoarr[j] == '')
						continue;
					if (j != 0) {
						sWhere += ' OR ';
					}
					var tab_alias='';
					if (use_alias)
						tab_alias = this.fields[i].object.tablename + '.';
					else
						tab_alias = '';
					if(gtempoarr[j]=='isnull'){
						sWhere +='(' + tab_alias + '"' + this.fields[i].object.sqlname + '" IS NULL' + ')';
						// sWhere +='(' + this.fields[i].where(use_alias, gtempoarr[j]) + ')';
					}
					else if(gtempoarr[j]=='isnotnull'){
						sWhere +='(' + tab_alias + '"' + this.fields[i].object.sqlname + '" IS NOT NULL' + ')';
					}
					else{
						if (dataset_id == 100 && this.fields[i].fieldname == 'id' && typeof gtempoarr[j]=='string' && isNaN(gtempoarr[j])){
							
							sWhere += "( guid='"+ gtempoarr[j] + "')";
							
						}
						else						
							sWhere += '(' + this.fields[i].where(use_alias, gtempoarr[j]) + ')';
						if (gtempoarr[j].indexOf('geotable') == 0) {
							parts = gtempoarr[j].split(':');
							var geotable = '';
							var tableId = parts[1];
							var recordId = parts[2];
							var field = parts[3];
							switch (tableId) {
								case '2171':
									// geotable = 'public.adm4_region_f';
									this.isUseTableRegion = true;
									this.regionField = 	this.fields[i].object.tablename + '."' + this.fields[i].object.sqlname +'"';
									break;
								case '2172':
									// geotable = 'public.adm6_district_f';
									this.isUseTableDistrict = true;
									this.districtField = this.fields[i].object.tablename + '."' + this.fields[i].object.sqlname +'"';
									break;
							
								default:
									throw new Error('Unsupported table ID');
									break;
							}	
						}
					}
				}
				if (gtempoarr.length > 1)
					sWhere += ') ';
			}
		}
		
		// console.log('---------', sWhere);
		// console.log('settings', meta.rights);
		if(req.user)
			user_id=req.user.id;

		if (updating && (meta.rights.charAt(1) == '0' || meta.rights.charAt(1) == '1') && (!(req.user && req.user.isadmin)) && dataset_id!=5 ) {
			// console.log('888');
			if (sWhere != '')
				sWhere = '(' + sWhere + ')' + ' AND ';
			if (use_alias) {
				if (user_id != undefined && user_id != null && user_id != '' && meta.rights.charAt(1) == '1')
					sWhere += this.tabs[0].alias + ".created_by='" + user_id + "'";
				else if (meta.rights.charAt(1) == '0')
					sWhere += ' ' + this.tabs[0].alias + '.published ';
				else
					sWhere += ' false ';
			}
			else {
				if (user_id != undefined && user_id != null && user_id != '' && meta.rights.charAt(1) == '1')
					sWhere += " created_by='" + user_id + "'";
				else if (meta.rights.charAt(1) == '0')
					sWhere += ' published ';
				else
					sWhere += ' false ';
			}
		}

		if ((!(req.user && req.user.isadmin)) && !updating && (meta.rights.charAt(0) == '0' || meta.rights.charAt(0) == '1')) {
			/*console.log('user_id=');
			console.log(user_id);
			console.log('meta.rights=');
			console.log(meta.rights);*/

			var subWhere = ''

			if (dataset_id == 100) {
				if (use_alias) {
					if (user_id != undefined && user_id != null && user_id != '' && meta.rights.charAt(0) == '1')
						subWhere += ' (' + this.tabs[0].alias + ".created_by='" + user_id + "' OR " + this.tabs[0].alias + ".published OR SUBSTRING(public.dataset_access(main.id, '" + user_id + "'), 1,1)<>'0') ";
					else
						subWhere += ' (' + this.tabs[0].alias + ".published OR SUBSTRING(public.dataset_access(main.id, '" + user_id + "'), 1,1)<>'0') ";
				}
				else {
					if (user_id != undefined && user_id != null && user_id != '' && meta.rights.charAt(0) == '1')
						subWhere += " (created_by='" + user_id + "' OR published OR SUBSTRING(public.dataset_access(main.id, '" + user_id + "'), 1,1)<>'0') ";
					else
						subWhere += " (published OR SUBSTRING(public.dataset_access(main.id, '" + user_id + "'), 1,1)<>'0') ";
				}

			}
			else if(dataset_id!=5 && !(settings.user && settings.user.isAdmin) ){
				if (use_alias) {
					if (user_id != undefined && user_id != null && user_id != '' && meta.rights.charAt(0) == '1')
						subWhere += '(' + this.tabs[0].alias + ".created_by='" + user_id + "' OR " + this.tabs[0].alias + ".published)";
					if ((user_id == undefined || user_id == null || user_id == '') && meta.rights.charAt(0) == '1')
						subWhere += this.tabs[0].alias + ".published ";
					else if (meta.rights.charAt(0) == '0')
						subWhere += ' ' + this.tabs[0].alias + '.published ';
				}
				else {
					if (user_id != undefined && user_id != null && user_id != '' && meta.rights.charAt(0) == '1')
						subWhere += " (created_by='" + user_id + "' OR " + "published )";
					if ((user_id == undefined || user_id == null || user_id == '') && meta.rights.charAt(0) == '1')
						subWhere += " published ";
					else if (meta.rights.charAt(0) == '0')
						subWhere += ' published ';
				}

			}

			if (sWhere != '' && subWhere != '')
				sWhere += ' AND ';
			sWhere += subWhere;

		}
		if (sWhere != '') {
			if (use_alias)
				sWhere += ' AND NOT ' + this.tabs[0].alias + '.is_deleted';
			else
				sWhere += ' AND NOT is_deleted';
		}
		else {
			if (use_alias)
				sWhere += ' NOT ' + this.tabs[0].alias + '.is_deleted';
			else
				sWhere += ' NOT is_deleted';
		}

		return sWhere;
	};

	this.buildquery = function (req, include, qparams) {
		groupby = [];
		select_list = this.buildselect(req, include, qparams, groupby);

		where_list = this.buildwhere(req, qparams, true, false);
		from_list = this.buildfrom();

		var distinct = '';
		if (_qv(qparams, 'distinct') != '')
			distinct = ' distinct ';
		result = 'SELECT ' + distinct + select_list + ' FROM ' + from_list;
		if (where_list != '')
			result += ' WHERE ' + where_list;
		groupbystr = '';
		if (groupby.length > 0)
			groupbystr = groupby.join(', ');
		if (this.isgroupbyregion) {
			if (groupbystr != '')
				groupbystr += ', ';
			groupbystr += ' public.adm4_region_f.id, public.adm4_region_f.name ';
		}
		if (this.isGroupByDistrict) {
			if (groupbystr != '')
				groupbystr += ', ';
			groupbystr += ' public.adm6_district_f.id, public.adm6_district_f.name ';
		}
		if (groupbystr != '')
			result += ' GROUP BY ' + groupbystr;


		return result;
	};

	this.build_order = function (qparams) {
		sOrder = '';
		if (typeof qparams=='object' && 'sort' in qparams && qparams.sort !== undefined) {
			var sort = qparams.sort;
			if (typeof sort === "string") {
				//console.log(sort);
				try {
					sort = JSON.parse(sort);
				} catch (ex) {
					return '';
				}
			}
			if (!Array.isArray(sort))
				return '';
			//console.log("sort.length", sort.length);
			for (var i = 0; i < sort.length; i++) {
				//console.log("sort[i].fieldname", sort[i].fieldname);
				if (sort[i].fieldname === undefined || sort[i].fieldname == '')
					continue;
				var sortcolumn;
				for (var j = 0; j < this.fields.length; j++) {
					if(this.fields[j].fieldname==sort[i].fieldname){
						sortcolumn=this.fields[j];
						break;
					}
				}
				if (sort[i].dir)
					dir = 'ASC';
				else
					dir = 'DESC'
				if(sortcolumn && sortcolumn.displayfields){

					// console.log('sssssssssssssssssssssoooooooooooooooorrrrrrrrrrrrrtttttttttttttttttttt');
					// console.log(sortcolumn.subfields);
					for (var j = 0; j < sortcolumn.subfields.length; j++) {
						if (sOrder != '')
							sOrder += ', '
						sOrder += '"' + sortcolumn.subfields[j].refname + '" ' + dir;						
					}	
				}
				else{
					
					if (sOrder != '')
						sOrder += ', '
					sOrder += '"' + sort[i].fieldname + '" ' + dir;
				}
				//}
			}
			//console.log(sOrder);
			return sOrder;
		}
		for (var i = 0; i < this.fields.length; i++) {
			col = _qv(qparams, 'iSortCol_' + i);
			dir = _qv(qparams, 'sSortDir_' + i);
			if (col != '') {
				fieldname = _qv(qparams, 'mDataProp_' + col);
				if (fieldname == '')
					continue;
				if (sOrder != '')
					sOrder += ', '
				sOrder += '"' + this.fields[i], fieldname + '" ' + dir;
			}
		}
		return sOrder;
	};

	this.build_document_tree = function (includefields, document_template, row) {
		var result = {};
		result.id = '';
		if('id' in row)
			result.id = row.id;
		for (var fieldname in document_template) {
			if (includefields != undefined && includefields.length > 0 && includefields.indexOf(fieldname) == -1)
				continue;

			if (dataset_id==5 && (fieldname =='password' || fieldname == 'password2' || fieldname == 'hash'))
				continue;


			if (document_template[fieldname].displayfields) {
				res = { id: row[fieldname] };
				for (var i = 0; i < document_template[fieldname].subfields.length; i++) {
					nmfld = document_template[fieldname].subfields[i].refname;
					if (nmfld in row) {
						res[document_template[fieldname].subfields[i].name] = row[nmfld];
						continue;
					}
					/*
					newld=document_template[fieldname].join_tab_alias+'_'+nmfld;
					if(newld in row){
						res[nmfld]=row[newld];
						continue;
					}*/
				}
				/*	
				for(var i=0; i<document_template[fieldname].displayfields.length; i++){
					nmfld=document_template[fieldname].displayfields[i];
					if('ref_'+nmfld in row){
						res[nmfld]=row['ref_'+nmfld];
						continue;
					}
					newld=document_template[fieldname].join_tab_alias+'_'+nmfld;
					if(newld in row){
						res[nmfld]=row[newld];
						continue;
					}
				}*/
				result[fieldname] = res;
			}
			else if (this.isgroupbyregion && document_template[fieldname].fn == 'regions') {
				result[fieldname] = { id: row['region_id'], name: row[fieldname] };//public.adm4_region_f.id, public.adm4_region_f.name )
			}
			else if (this.isGroupByDistrict && document_template[fieldname].fn == 'districts') {
				result[fieldname] = { id: row['district_id'], name: row[fieldname] };
			}
			else
				result[fieldname] = row[fieldname];

			if ('calc_' + fieldname in row) {
				result['calc_' + fieldname] = row['calc_' + fieldname];
			}

			// if (fieldname == 'JSON' && dataset_id == 100) {
			// 	JSON_value = JSON.parse(row[fieldname]);
			// 	JSON_value.dataset_id = row['dataset_id'];
			// 	JSON_value.description = row['dataset_description'];
			// 	JSON_value.rights = row['a_access'];
			// 	result[fieldname] = JSON.stringify(JSON_value);
			// }
		}
		return result;
	}
	this.documentquery = function (req, includefields, qparams, sLimit, callback, output) {
		var g_table = this;
		var sSQL = this.buildquery(req, includefields, qparams);
		var distinct = '';
		if (_qv(qparams, 'distinct') != '')
			distinct = ' distinct ';
		var order_list = this.build_order(qparams);
		if (order_list == '' && this.tabs.length > 0 && distinct == '' && !this.grouping)
			order_list = this.tabs[0].alias + '.edited_on DESC';

		var cnt = 0;
		// console.log(sSQL + ' ' + sLimit);

		if (callback) {
			function send_data(maincnt) {
//				console.log('run sql');
				if (order_list != '')
					sSQL += ' ORDER BY ' + order_list;
				//  console.log('sql=',sSQL + ' ' + sLimit);
				var subquery = global.pgPool.query(sSQL + ' ' + sLimit, function (error, query_result, unofields2) {
					// console.log('run sql end');
					if (error === undefined) {
						iTotal = maincnt;
						iFilteredTotal = maincnt;
						var aaData = new Array();
						for (var r = 0; r < query_result.rows.length; r++) {
							aaData.push(g_table.build_document_tree(includefields, g_table.document_template, query_result.rows[r]));
						}
						if (g_table.details.length == 0 && dataset_id != 100) {
							if (!output) {
								output = {
									sEcho: parseInt(_qv(qparams, 'sEcho')),
									iTotalRecords: '' + maincnt,
									iTotalDisplayRecords: '' + maincnt,
									aaData: aaData,
								}
							}
							else {
								output.sEcho = parseInt(_qv(qparams, 'sEcho'));
								if (output.iTotalRecords === undefined || output.iTotalRecords == 0) {
									output.iTotalRecords = '' + maincnt;
									output.iTotalDisplayRecords = '' + maincnt;
								}
								output.aaData = aaData;
							}

							callback(null, output);
							return;
						}

						function load_access(row_index) {
							// console.log('load_access row_index=' + row_index );
							if (row_index == aaData.length || aaData[row_index].id===undefined) {
								console.log('return access');
								if (!output) {
									output = {
										sEcho: parseInt(_qv(qparams, 'sEcho')),
										iTotalRecords: '' + maincnt,
										iTotalDisplayRecords: '' + maincnt,
										aaData: aaData,
									}
								}
								else {
									output.sEcho = parseInt(_qv(qparams, 'sEcho'));
									if (output.iTotalRecords === undefined || output.iTotalRecords == 0) {
										output.iTotalRecords = '' + maincnt;
										output.iTotalDisplayRecords = '' + maincnt;
									}
									output.aaData = aaData;
								}
								callback(null, output);
								return;
							}
							var row = aaData[row_index];
							if ('JSON' in row ) {
								JSON_value = JSON.parse(row['JSON']);
								// JSON_value.dataset_id = row['id'];
								JSON_value.description = row['dataset_description'];
								JSON_value.rights = row['a_access'];
								
								geo_access.getaccess(req, JSON_value, function(meta){
									row['JSON'] = JSON.stringify(meta);
									load_access(row_index+1);
								});
							}
							else{
								load_access(row_index+1);
							}

						}
						if ( dataset_id == 100) {
							console.log('load access');
							load_access(0);							
							return;
						}
						//console.log('aaData',aaData);
						function loaddetails(row_index, col_index) {
							//console.log('loaddetails row_index=' + row_index + ', col_index=' + col_index);
							if (row_index == aaData.length || aaData[row_index].id===undefined) {
								if (!output) {
									output = {
										sEcho: parseInt(_qv(qparams, 'sEcho')),
										iTotalRecords: '' + maincnt,
										iTotalDisplayRecords: '' + maincnt,
										aaData: aaData,
									}
								}
								else {
									output.sEcho = parseInt(_qv(qparams, 'sEcho'));
									if (output.iTotalRecords === undefined || output.iTotalRecords == 0) {
										output.iTotalRecords = '' + maincnt;
										output.iTotalDisplayRecords = '' + maincnt;
									}
									output.aaData = aaData;
								}
								callback(null, output);
								return;
							}							
							if (col_index >= g_table.details.length) {
								row_index++;
								loaddetails(row_index, 0);
								return;
							}
							
							var c_obj = aaData[row_index];
							var det_table = g_table.details[col_index].tab;
							var det_meta = g_table.details[col_index].ref_meta;
							var col_meta = g_table.details[col_index].col_meta;
							var reftype =  g_table.details[col_index].reftype;
							var mark = col_meta.widget.properties.mark;
							
							if(includefields.length>0 && includefields.indexOf(col_meta.fieldname)==-1) {
								col_index++;
								loaddetails(row_index, col_index);
								return;
							}
							
							
							det_qparams = {};
							
							if (det_table && det_table.documentquery) {
								if(reftype=='details'){
									det_qparams['f_' + col_meta.widget.properties.ref_detail] = c_obj.id;
									det_table.documentquery(req, '', det_qparams, '', function (error, outdata) {
										function compare(a,b) {
											if (a.id < b.id)
											  return -1;
											if (a.id > b.id)
											  return 1;
											return 0;
										  }
										  
										if(!error){
											c_obj[col_meta.fieldname] = outdata.aaData;
											c_obj[col_meta.fieldname].sort(compare);
										}

										
										col_index++;
										loaddetails(row_index, col_index);
									}, output);
								}
								else{
									var sSQL="SELECT rowid, action FROM gis_admin.addinfo WHERE docid=" + c_obj.id +" AND classname='"+mark+"' ORDER BY ID";
									//console.log(sSQL);
									global.pgPool.query(sSQL, function(error, query_result, unofields2) {
										//console.log('query_result.rows.length ',query_result.rows.length);
										if (error === undefined)	{
											if(query_result.rows.length>0){
												var id_list='';
												for (var i = 0; i < query_result.rows.length; i++) {
													if(query_result.rows[i].rowid){
														id_list+=query_result.rows[i].rowid+'-PropertyIsEqualTo;';
													}
												}
												det_qparams['f_id'] = id_list;
												det_table.documentquery(req, '', det_qparams, '', function (error, outdata) {
													var reslist=[];
													for (var j = 0; j < query_result.rows.length; j++) {
														for (var l = 0; l < outdata.aaData.length; l++) {
															if(outdata.aaData[l].id==query_result.rows[j].rowid){
																outdata.aaData[l].action=query_result.rows[j].action;
																reslist.push(outdata.aaData[l]);
																break;
															}									
														}
													}
													c_obj[col_meta.fieldname] = reslist;//outdata.aaData;
													col_index++;
													loaddetails(row_index, col_index);
												}, output);
											}
											else{
												c_obj[col_meta.fieldname] = [];
												col_index++;
												loaddetails(row_index, col_index);
											}
										
										}
										else{
											console.log('aaData',aaData);
											console.log(sSQL);
											console.log(error);
											console.log('includefields',includefields);
											callback(error, null);
										}
									});
								}
								
							}
						}
						loaddetails(0, 0);

					} else {
						
						//console.log(error);
						console.log(sSQL);
						callback(error, null);
					}
				});
			}
			if (_qv(qparams, 'count_rows') != '') {
				countsql = 'select count(1) as cnt from ( ' + sSQL + ') s';
//				console.log('run count sql');
				// console.log(countsql);
				var query = global.pgPool.query(countsql, function (error, cntresult, unofields) {
//					console.log('run count sql end');
//					console.log(error);
					if (error === undefined) {
						cnt = cntresult.rows[0].cnt;
						send_data(cnt);
					} else {
						console.log(countsql);
						console.log(error);
						callback(error, null);
					}
				});
			}
			else {
//				console.log('i am here!');
				send_data(0);
			}
		}
		return sSQL;
	};

	this.insert = function (req, document, callback) {
		var g_table = this;
		// console.log('insert');
		if(typeof document!='object'){
			callback({status:'error', data:'The document is not object!'});
			console.log(document);
			console.log('dataset_id=',dataset_id);
			var stack = new Error().stack;
			console.log( stack );
			return;
		}
		if(meta.sequencename)
			id_query="select nextval('"+meta.sequencename+"'::regclass) id";
		else
			id_query="select nextval('"+g_table.tabs[0].table+"_id_seq'::regclass) id";
		console.log(id_query);
		var query = global.pgPool.query(id_query, function (error, query_result, unofields2) {
			if (error !== undefined) {
				callback({status:'error', data:error});
				console.log(error);
				return;
			}
			// console.log('got id');
			var new_id = query_result.rows[0].id;
			document.id=new_id;
			server={ };
			// server={meta_manager: geo_meta_table, user_id:user_id };
			fieldcalc.calculatefields(meta, document, 'default', server, function(error, document){
				// console.log('calculatefields');
				if (dataset_id == 2050) {
					document.hash = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
					document.confirm = 'false';
				}
				if (dataset_id == 5) {
					document.confirmhash = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
				}

				if (meta.rights.charAt(1) == '0' && !(req.user && req.user.isadmin)) {
					callback({status:'error', data:'Access denied!'});
					console.log('Access denied!');
					return;
				}
				
				var doc_str=document;
				//oc_str=sanitize(doc_str);
				doc_str=JSON.stringify(doc_str);
				
			
						
				var flds = [];
				var vls = [];
				var hash = '';
				flds.push('id');
				vls.push(new_id);
				var classname=false;
				for (var i = 0; i < g_table.fields.length; i++) {
					if(service_flds.indexOf(g_table.fields[i].fieldname)!=-1)
						continue;
					if (g_table.fields[i].object.tablename === undefined || g_table.fields[i].object.tablename != 'main')
						continue;

					if (g_table.fields[i].fieldname == 'classname' ) {
						flds.push('"' + g_table.fields[i].fieldname + '"');
						vls.push("'"+dataset_id+"'");
						classname=true;
						continue;
					}
					

					field_value = _qv(document, g_table.fields[i].fieldname)
					var sql__value = '';
					if (field_value != 'NULL' && (g_table.fields[i].fieldname in document))
						sql__value = g_table.fields[i].sql_value(document[g_table.fields[i].fieldname]);
					else if (field_value == 'NULL')
						sql__value = 'NULL';
					else
						continue;
					flds.push('"' + g_table.fields[i].fieldname + '"');
					vls.push(sql__value);

				}
				if(meta.inherit_table && meta.inherit_table!='' && !classname){
					flds.push('"classname"');
					vls.push("'"+dataset_id+"'");					
				}



				function ins() {
					if (flds.length > 0) {
						fields = flds.join(', ');
						values = vls.join(', ');
						var insert_query = 'INSERT INTO ' + g_table.tabs[0].table + '(' + fields + ', is_deleted, created_on, created_by, edited_on ) VALUES(' + values + ', false, current_timestamp, \'' + user_id + '\', current_timestamp)';
						console.log(insert_query);
						var query = global.pgPool.query(insert_query, function (error, query_result, unofields2) {
							if (error === undefined) {
								
								if(meta.objectstory){
									var hist_query = "INSERT INTO gis_admin.objectstory(datasetid,  docid, data, created_by) VALUES(" + dataset_id + ", "+new_id+", '" + doc_str + "',"+req.user.id+" )";
									//console.log(hist_query);
									var query = global.pgPool.query(hist_query, function (error, query_result, unofields2) {
										console.log('inserting into history',error);
									});
								}
								for (var i = 0; i < g_table.details.length; i++) {
									//console.log('g_table.details itaration');
									var det_table = g_table.details[i].tab;
									var col_meta = g_table.details[i].col_meta;
									var reftype =  g_table.details[i].reftype;													
									var fld_values = document[col_meta.fieldname];	
									
									//console.log('Updating reftype=',reftype, col_meta.fieldname);
									if(reftype =='details'){
										update_details(req, det_table, col_meta, new_id, fld_values);
									}
									else{									
										update_links(req, det_table, col_meta, new_id, fld_values);
									}
								
								}
								
								if (dataset_id == 2050) {
									/*
									email_addr=document.email;
									var link='http://bit.icc.ru?action=confirm&hesh='+ document.hash;
									var msg='Dear colleague, please confirm the registration of your report on this link <a href="'+link+'">'+link+'</a>';
									server.send({
									   text:    'Dear colleague, please confirm the registration of your report on this link', 
									   from:    "Organisation commitee <bit@icc.ru>", 
									   to:      email_addr,   
									   //to:      "Shumilov <shumsan1011@gmail.com>",   
									   subject: "International Conference “Information Technologies in the Research of Biodiversity”",
									   attachment: 
										   [
											  {data: "<html>"+msg+"</html>", alternative:true},
										   ]
									}, function(err, message) { 
										console.log('error=');
										console.log(err);
										console.log(err || message);
										callback('ok');
									});
									*/
								}
								else if (dataset_id == 5) {
									email_addr = document.email;
									var url = config.server.url;
									if (!url) {
										callback({status:'error', data:'fail'});
										console.log("geoportal url is empty.");
									}
									console.log('url=' + url);
									var link = url + '/dataset/update?f=5&f_confirmhash=' + document.confirmhash;
									var msg = 'Dear colleague, please confirm the registration on this link ' + link;

									var host = config.modules.mail.config.host;
									var port = config.modules.mail.config.port;
									var domain = config.modules.mail.config.domain;
									var base64 = config.modules.mail.config.base64;
									var username = config.modules.mail.config.username;
									var password = config.modules.mail.config.password;


									if (!host || !port || !domain || !username || !password) {
										callback({status:'error', data:'Mail settings are not correct'});
										return;
									}
									if (base64) {
										username = (new Buffer(username)).toString("base64");
										password = (new Buffer(password)).toString("base64");
									}

									mail.send({
										host: host,                      // smtp server hostname
										port: port,                      // smtp server port
										domain: domain,                  // domain used by client to identify itself to server
										to: email_addr,
										from: config.modules.mail.config.from,
										subject: config.server.name,
										body: msg,
										authentication: config.modules.mail.config.authentication ? 'login' : '',
										ssl: config.modules.mail.config.ssl == true ? true : false,                         // true/false
										username: username,              // Account username
										password: password               // Account password
									},
										function (err, result) {
											if (err) {
												console.log("Error in mail.js: " + err);
												//callback('fail:'+ err);
											} else {

												console.log("Email sent with result: " + result);
											}

										});
									callback({status:'ok', data:''});

								}
								else{
									callback({status:'ok', data:new_id, document:document});
								}

							} else {
								console.log(insert_query);
								console.log(error);
								if(error.message.indexOf('Users_email_key')!=-1 )
									callback({status:'error', data:'The email already exists.'});
								else		
									callback({status:'error', data:error.message});
							}
						});
					}
					else {
						callback('Missing data to insert!');
						return;
					}
				}


				// if (dataset_id == 5) {
				// 	password1 = _qv(document, 'password');
				// 	password2 = _qv(document, 'password2');
				// 	if (password1 != password2) {
				// 		callback('password is not corect');
				// 		return;
				// 	}
				// 	calipso.lib.crypto.hashmd5(password1, calipso.config.get('session:secret'), function (err, hash) {
				// 		if (err) {
				// 			callback(err);
				// 		}
				// 		flds.push('"hash"');
				// 		vls.push("'" + hash + "'");
				// 		flds.push('"locked"');
				// 		vls.push('false');
				// 		ins();
				// 	});
				// }
				// else
				ins();
			});
		});
	};

	
	
	function update_links(req, det_table, col_meta, doc_id, values){
		console.log('update_links');
		if (!Array.isArray(values) || values.length==0 || doc_id===undefined)
			return;
		
		var mark = col_meta.widget.properties.mark;	
		sSQL="SELECT id, rowid, action FROM gis_admin.addinfo WHERE docid=" + doc_id +" AND classname='"+mark+"'";
		console.log(sSQL);
		
		global.pgPool.query(sSQL, function(error, query_result, unofields2) {
			console.log('update_links, I am here');
			if (error === undefined)	{
				// удаление элементов 
				//console.log('update_links, deleting');					
				var del_list='';
				var del_objlist='';
				for (var j = 0; j < query_result.rows.length; j++) {												
					var exist_row=false;
					var action='';
					for (var i = 0; i < values.length; i++) {
						sub_doc = values[i];
						if(typeof sub_doc !='object' || sub_doc.id===undefined){
							console.log('update_links, value is not object!');
							return;
						}
						if(query_result.rows[j].rowid==sub_doc.id){
							exist_row=true;
							action=query_result.rows[j].action;
							break;
						}
					}
					if(!exist_row){
						if(del_list!='')
							del_list+=', ';
						del_list+=query_result.rows[j].id;													
						if(action=='add')
							del_objlist+=query_result.rows[j].rowid+'-PropertyIsEqualTo;';
					}
				}
				if(del_list!=''){
					delSQL="DELETE FROM gis_admin.addinfo WHERE id in (" + del_list+")";
					//console.log(delSQL);
					global.pgPool.query(delSQL, function(error, query_result2, unofields2) {
						
					});
				}
				if(del_objlist!=''){
					del_doc['f_id'] = del_objlist;
					det_table.delete(del_doc, function (res) {
						//console.log('update_links, delete');
						//console.log(res);
					})
				}
				// добавление новых элементов
				// console.log('update_links, adding');
				
				for (var i = 0; i < values.length; i++) {
					sub_doc = values[i];
					var action=sub_doc.action;
					if(typeof sub_doc !='object' || sub_doc.id===undefined){
						console.log('update_links, value is not object!');
						return;
					}
					var exist_row=false;
					//var action='';
					for (var j = 0; j < query_result.rows.length; j++) {
						if(query_result.rows[j].rowid==sub_doc.id){
							exist_row=true;
							//action=query_result.rows[j].action;
							break;
						}
					}
					if(!exist_row){
						
						
						if(sub_doc.action=='add'){
							// console.log('update_links, adddddddddddddddddddddddddddiiiiiiing', sub_doc);
							det_table.insert(sub_doc, function (res) {
								// console.log('insert of link object');
								// console.log(res);									
								
								insSQL="INSERT INTO gis_admin.addinfo(docid, datasetid, rowid, rowdatasetid, classname, action) VALUES ("+doc_id +","+ dataset_id + ","+ res.data+"," + det_table.dataset_id + ",'" + mark+"','" +action+"')";
								// console.log(insSQL);
								global.pgPool.query(insSQL, function(error, query_result2, unofields2) {
									
								});
							})
							
							
						}
						else{
							//console.log(sub_doc);
							insSQL="INSERT INTO gis_admin.addinfo(docid, datasetid, rowid, rowdatasetid, classname, action) VALUES ("+doc_id +","+ dataset_id + ","+ sub_doc.id+"," + det_table.dataset_id + ",'" + mark+"','" +sub_doc.action+"')";
							//console.log(insSQL);
							global.pgPool.query(insSQL, function(error, query_result2, unofields2) {
								
							});
						}
					}
					else if(action=='add'){
						link_id=sub_doc.id;
						sub_doc.id='NULL';
						sub_doc.f_id=link_id;
						det_table.update(req,sub_doc, function (res) {
							//console.log('update of link object');
							//console.log(res);									
						})
					}
					
				}
				//console.log('update_links, end');
			}
			else{
				console.log(error);
				// callback({status:'error', message:error.message});
				
				return;
			}
		});
											
	}
	
	function update_details(req,det_table, col_meta, doc_id, values){
			
		if (!Array.isArray(values) || values.length==0 || doc_id===undefined)
			return;
		
		var child_query = 'SELECT id FROM ' + det_table.tabs[0].table  + ' WHERE NOT is_deleted AND ' + col_meta.widget.properties.ref_detail + ' = ' + doc_id;
		console.log('update_details');
		global.pgPool.query(child_query, function (error, existrows, unofields2) {
			if (error != null){
				console.log(error);
				callback(error);
				return;
			}
			else{
				//console.log('context= ', col_meta.fieldname);
				for (var i = 0; i < values.length; i++) {
					sub_doc = values[i];											
					sub_doc[col_meta.widget.properties.ref_detail] = doc_id;
					if (sub_doc.id === undefined || sub_doc.id == 'NULL') {
						det_table.insert(req,sub_doc, function (res) {
							//console.log('insert');
							//console.log(res);
						})
					}
					else {																									
						sub_doc['f_id'] = sub_doc.id;
						det_table.update(req,sub_doc, function (res) {
							//console.log('update');
							//console.log(res);
						})											
					}
				}
				
				for(var e = 0; e < existrows.rows.length; e++){
					var exists=false;
					for (var i = 0; i < values.length; i++) {
						sub_doc = values[i];
						if(existrows.rows[e].id==sub_doc.id){
							exists=true;
							break;
						}
					}
					
					if(!exists) {
						//console.log('not exists',existrows.rows[e].id);
						sub_doc={'f_id': existrows.rows[e].id};
						det_table.delete(req,sub_doc, function (res) {
							//console.log('delete');
							console.log(res);
						})
					}
					else
						console.log('exists',existrows.rows[e].id);
				}
			}
		});
	}
		
	this.update = function (req, document, callback) {
		var user_id = req.user.id;
		console.log('req.user=',req.user)
		var g_table = this;
		flds = [];
		vls = [];
		var where = '';
		var id_filter_value = _qv(document, 'f_id');
		var f_confirmhash='';
		var confirmhash='';				
		server={ };
		// server={meta_manager: geo_meta_table, user_id:user_id };
		fieldcalc.calculatefields(meta, document, 'default', server, function(error, document){
			var doc_str=document;
			//oc_str=sanitize(doc_str);
			doc_str=JSON.stringify(doc_str);
			if(meta.objectstory && id_filter_value!=''){
				var hist_query = "INSERT INTO gis_admin.objectstory(datasetid,  docid, data,created_by ) VALUES(" + dataset_id + ", "+id_filter_value+", '" + doc_str + "',"+req.user.id+" )";
				//console.log(hist_query);
				var query = global.pgPool.query(hist_query, function (error, query_result, unofields2) {
					console.log('inserting into history',error);
				});
			}
			
					
			function upd(){
				if (flds.length > 0) {
					flds.push('edited_on = current_timestamp');
					flds.push("edited_by = '" + user_id + "'");

					var tableName = g_table.tabs[0].table;
					if (tableName.indexOf(" ") !== -1) {
						var splittedTableName = tableName.split(".");
						if (splittedTableName.length === 2) {
							var schema = splittedTableName[0];
							var table = splittedTableName[1];
							tableName = schema + ".\"" + table + "\"";
						}
					}

					fields = flds.join(', ');
					var update_query = 'UPDATE ' + tableName + ' SET ' + fields + ' WHERE ' + swhere;
					console.log(update_query);
					console.log('-------------------------------------------------------');
					var query = global.pgPool.query(update_query, function (error, query_result, unofields2) {
						if (error === undefined) {
							/*if(dataset_id==3){
								g_table.update_symbols();
							}*/
							if (dataset_id == 5 && (f_confirmhash!=''|| document.recovery)) {
								callback('userredirect');
								return;
							}
							if(id_filter_value!=''){
								for (var i = 0; i < g_table.details.length; i++) {
									console.log('g_table.details itaration');
									var det_table = g_table.details[i].tab;
									var col_meta = g_table.details[i].col_meta;
									var reftype =  g_table.details[i].reftype;													
									var fld_values = document[col_meta.fieldname];	
									
									console.log('Updating reftype=',reftype, col_meta.fieldname);
									if(reftype =='details'){
										update_details(req, det_table, col_meta, id_filter_value, fld_values);
									}
									else{									
										update_links(req, det_table, col_meta, id_filter_value, fld_values);
									}
								
								}
							}
							callback({status:'ok', document:document});
						} else {
							console.log(update_query);
							callback(error.message);
						}
					});
				}
				else {
					callback('Missing data to update!');
					return;
				}

				
			}
	//		console.log(settings);
			if (dataset_id != 5 && meta.rights.charAt(1) == '0' && !(req.user && req.user.isadmin)) {
				callback('Access denied!');
				return;
			}

			if(isNaN(id_filter_value) && typeof id_filter_value==='string' ){
				parts=id_filter_value.split(';');
				if(parts.length!=1 || (id_filter_value.indexOf('-')!=-1 && id_filter_value.indexOf('PropertyIsEqualTo')==-1))
					id_filter_value='';
			}

			console.log('id_filter_value='+id_filter_value);
			
			if(id_filter_value!='' && this.fields){
				for (var i = 0; i < this.fields.length; i++) {
					//console.log(this.fields[i].fieldname);
					f_val = _qv(document, 'f_' + this.fields[i].fieldname)
					if (f_val!='' && f_val.indexOf('geotable:')==0){
						//console.log('-');
						document['f_' + this.fields[i].fieldname]='';
					}
				}
			}
			
			swhere = g_tab.buildwhere(req, document, false, true);
	//		console.log('where='+swhere);
			if (swhere == '') {
				callback('Missing data identification!');
				return;
			}
			console.log(settings);
			if(req.user && req.user.isadmin){
				console.log('isAdmin');
			}
			else if (meta.rights.charAt(1) == '1' && !(req.user && settings.user.isAdmin) && id_filter_value!=user_id) {
				swhere += " AND created_by='" + user_id + "'";
			}

			console.log('2where='+swhere);

			
		

			if (dataset_id != 5 && meta.rights.charAt(1) != '4' && id_filter_value == '' && !(settings.user && settings.user.isAdmin)) {
				console.log('group updating is disable');
				callback('group updating is disable');
				return;

			}
			/*console.log(document);
			console.log('user_id='+settings.user);
			console.log(settings.user);
			console.log('id_filter_value='+id_filter_value);*/
			
			
			//id_filter_value=parseInt(id_filter_value);
			
			
			
			
			if (dataset_id == 5) {
				f_confirmhash = _qv(document, 'f_confirmhash')
				confirmhash = _qv(document, 'confirmhash');
				if (f_confirmhash != '') {// подтверждение email
					uid = _qv(document, 'f_id');
					swhere = " confirmhash='" + f_confirmhash + "'";
					flds.push('locked=false');
				}
				else if(id_filter_value != user_id && (!(req.user && req.user.isadmin))){			
					if (id_filter_value != user_id) {
						callback('Access denied '+id_filter_value +' '+user_id);
						return;
					}				
				}
			}
			else if((id_filter_value == '' || isNaN(id_filter_value)) && _qv(document, 'group_update') == '') {
				callback('group update error!');
				return;
			}
			

			for (var i = 0; i < g_tab.fields.length; i++) {
				if(service_flds.indexOf(g_tab.fields[i].fieldname)!=-1)
					continue;
				if (g_tab.fields[i].object.tablename === undefined || g_tab.fields[i].object.tablename != 'main')
					continue;
				if (dataset_id == 5 && f_confirmhash!='') {
					continue;
				}

				field_value = _qv(document, g_tab.fields[i].fieldname)
				if (field_value == '')
					continue;
				if (field_value != 'NULL' && g_tab.fields[i].fieldname in document)
					sql__value = g_tab.fields[i].sql_value(document[g_tab.fields[i].fieldname]);
				else if (field_value == 'NULL')
					sql__value = 'NULL';
				else
					continue;
				flds.push('"' + g_tab.fields[i].fieldname + '" = ' + sql__value);
				
				vls.push(sql__value);
				if (id_filter_value == '') {
					if (swhere != '')
						swhere += ' AND ';					

					if (g_tab.fields[i] instanceof sql_string){
						swhere += '("' + g_tab.fields[i].fieldname + '" IS NULL OR "' + g_tab.fields[i].fieldname + '" = \'\')';	
					}
					else{
						swhere += '"' + g_tab.fields[i].fieldname + '" IS NULL ';						
					}
				}
			}


			
			// if (dataset_id == 5) {
			// 	password1 = _qv(document, 'password');
			// 	password2 = _qv(document, 'password2');
			// 	console.log('password1='+password1)
			// 	if(password1 && password1!=null && password1!='' && password1!='NULL'  ){				
			// 		if (password1 != password2 || password1=='' || password1.length<6) {
			// 			callback('password is not corect');
			// 			return;
			// 		}
			// 		calipso.lib.crypto.hashmd5(password1, calipso.config.get('session:secret'), function (err, hash) {
			// 			if (err) {
			// 				callback(err);
			// 			}
			// 			flds.push('"hash"='+"'" + hash + "'");
			// 			upd();
			// 		});
			// 	}
			// 	else{
			// 		upd();				
			// 	}
			// }
			// else
				upd();
			
		});
		
		
		
	};

	this.delete = function (req,document, callback) {
		var g_table = this;
		console.log('deleting');
		if (meta.rights.charAt(1) == '0' && !(req.user && req.user.isadmin)) {
			callback('Access denied!');
			//console.log('Access denied!');
			return;
		}

		id_filter_value = _qv(document, 'f_id');
		if (id_filter_value != '') {
			swhere = 'id=' + id_filter_value;
			//console.log(settings.user);
			if (meta.rights.charAt(1) == '1' && !(settings.user && settings.user.isAdmin)) {
				swhere += " AND created_by='" + user_id + "'";
			}
			var update_query = 'UPDATE ' + this.tabs[0].table + ' SET is_deleted = true WHERE ' + swhere;
			console.log(update_query);
			var query = global.pgPool.query(update_query, function (error, query_result, unofields2) {
				if (error === undefined) {
					/*if(dataset_id==3){
						g_table.update_symbols();
					}*/
					for (var i = 0; i < g_table.details.length; i++) {
						var det_table = g_table.details[i].tab;
						var col_meta = g_table.details[i].col_meta;
						var ref_meta = g_table.details[i].ref_meta;
						var reftype =  g_table.details[i].reftype;													
						
						if(reftype =='details'){
							var delete_query = 'UPDATE ' + ref_meta.tablename + ' SET is_deleted = true WHERE ' + col_meta.widget.properties.ref_detail + ' = ' + id_filter_value;
							console.log(delete_query);
							global.pgPool.query(delete_query, function (error, query_result, unofields2) {
								if (error != null)
									console.log(error);
							});
						}
						else{									
							delSQL="DELETE FROM gis_admin.addinfo WHERE docid =" + id_filter_value;
							console.log(delSQL);
							global.pgPool.query(delSQL, function (error, query_result, unofields2) {
								if (error != null)
									console.log(error);
							});
						}
						
							
							
						
					}
					callback('ok');
				} else {
					console.log(update_query);
					callback(error.message);
				}
			});
		}
		else {
			callback('Missing data identification!');
			return;
		}
	};
}


module.exports = geo_table;
