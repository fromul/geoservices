var geo_table = require("../dataset/geo_table");




exports.loaddataset=function(req, res, next) {
    var dataset_id = req.queryString('f');
    // console.log('load dataset dataset_id=',dataset_id);
    if(dataset_id===undefined || dataset_id==null || dataset_id=='') {
        dataset_id = req.bodyString('f');
    }
    // console.log('load dataset dataset_id=',dataset_id);

    if(dataset_id!==undefined && dataset_id!=null && dataset_id!='') {
        req.dataset_id = dataset_id;
		meta_manager.get_table_json({}, dataset_id, function (meta){       
            if(meta==null){
                next();
                return;
            }
            meta.dataset_id=dataset_id;
            req.meta = meta;

            // console.log('loaddataset rights', meta.rights);

            req.g_table = new geo_table('', dataset_id, meta);
            next();
        },req);
    }
    else{
        next();
    }
    
}

exports.getaccess=function(req, meta, callback) {
    
    if(req.user && meta.owner===req.user.id){
        meta.rights='241';
        callback(meta);
    }else{
        if(req.user)
            var checktablequery = "SELECT access_mode  FROM public.table_access_rules WHERE table_id = '" + meta.dataset_id + "' AND NOT is_deleted AND NOT waiting_for_approval AND (user_id='" + req.user.id + "' OR user_id='registered'";
        else
            var checktablequery     = "SELECT access_mode  FROM public.table_access_rules WHERE table_id = '" + meta.dataset_id + "' AND NOT is_deleted AND NOT waiting_for_approval AND user_id='anonym'";
        var cquery = global.pgPool.query(checktablequery, function(error, res, fields) {
            if (error === undefined){
                r=0;
                w=0;
                p=0;
                for (var j=0; j<res.rows.length;j++){
                    if(typeof res.rows[j].access_mode =='string' && res.rows[j].access_mode.length>2 ){
                        r=max(r,res.rows[j].access_mode.charAt(0));
                        w=max(w,res.rows[j].access_mode.charAt(1));
                        p=max(p,res.rows[j].access_mode.charAt(2));
                    }
                }
                meta.rights=r+''+w+''+p;
                callback(meta);
            }
            else{
                callback(meta);
            }            
        });        
    }    
}

