// var common = require("../../common/lib/common");
var fs     = require("fs");

var WPSInputParametersProcessor = require("./WPSInputParametersProcessor");
var WPSServiceExecutor          = require("./WPSServiceExecutor");
var rootpath     = process.cwd() + '/';

var common = require(rootpath+"/controllers/utils/common");
var misc = require(rootpath+"/public/js/misc");
/**
 * Class for managing WPS services that present in the system.
 */

"use strict";

function WPSManager(req, dbclient) {
	var _req            = req;
	// var global.pgPool = dbclient;

	/**
	 * Executes provided service.
	 *
	 * @param function positivecallback Positive callback
	 * @param function failurecallback  Failure callback
	 * @param integer  id               Identificator of service
	 * @param string   username         Name of the user that launched the execution
	 *
	 * @return void
	 */

	this.executeService = function(positivecallback, failurecallback, id, username, userinputparameters, useroutputparameters, executerconfig) {
		console.log('executeService!!!!!!!=',id);
	
		global.pgPool.query("SELECT * FROM public.wps_methods WHERE (is_deleted = FALSE);", function(error, rows, fields) {
			if (error === undefined) {
				var serviceinfo;
				var servicesinfo     = rows.rows;
				// console.log(servicesinfo);
				var numberofservices = servicesinfo.length;

				for (var i = 0; i < numberofservices; i++) {
					if (rows.rows[i].id+'' === id) {
						serviceinfo = rows.rows[i];
					}
				}

				if (common.isundefined(serviceinfo) === true) {
					throw new Error("Unable to get element with ID " + id);
				}

				var filesavearray = new Array();

				var serviceinputparameters   = JSON.parse(serviceinfo.params);
				var serviceoutputparameters  = JSON.parse(serviceinfo.output_params);
				console.log('before inputparametersprocessor!!!!');
				console.log(userinputparameters)

				var inputparametersprocessor = new WPSInputParametersProcessor(_req, id);
				console.log('before inputparametersprocessor.process(serviceinputparameters!!!!');
				inputparametersprocessor.process(serviceinputparameters, userinputparameters, function (userinputparameters) {
					console.log('before inputparametersprocessor.process(serviceoutputparameters!!!!');
					inputparametersprocessor.process(serviceoutputparameters, useroutputparameters, function (useroutputparameters) {
						console.log('before executor.execute!!!!');
						console.log(userinputparameters);
						console.log(useroutputparameters);
						var executor = new WPSServiceExecutor(global.pgPool, _req, serviceinfo, servicesinfo, executerconfig);
						console.log('before executor.execute!!!!');
						executor.execute(positivecallback, failurecallback, id, userinputparameters, useroutputparameters);
					});
				});
			} else {
				console.log(error);
				throw new Error("Error querying the database");
			} //end if
		});
	} //end executeService()


	/**
	 * Returns list of wrappers that available in the system.
	 *
	 * @param function callback  Positive callback
	 * @param integer  excludeid Identificator of the service, whose wrapper should be excluded
	 *
	 * @return array
	 */

	this.getWrappers = function(callback, excludeid) {
		if (common.isundefined(excludeid) === true) {
			var additional = "";
		} else if (common.isinteger(excludeid) === false) {
			throw new Error("Provided service ID is not an integer");
		} else {
			var additional = " id <> " + excludeid + " AND";
		} //end if

		var query = "SELECT * FROM public.wps_methods WHERE " + additional + " (is_deleted <> 'true' OR is_deleted IS NULL);";
		global.pgPool.query(query, function(error, rows, fields) {
			if (error === null) {
				callback(rows.rows);
			} else {
				console.log(error);
				throw new Error("Error querying the database");
			} //end if
		});
	} //end getWrappers()


	/**
	 * Returns console output for specific service.
	 *
	 * @param function callback Positive callback
	 * @param integer  id       Identificator of the service
	 *
	 * @return array
	 */

	this.getConsoleOutput = function(callback, id) {
		// if (common.isinteger(id) === false) {
		// 	throw new Error("Provided service ID is not an integer");
		// }

		var query = "SELECT status, console_output, error_output, result FROM public.method_examples WHERE id = " + id;
		global.pgPool.query(query, function(error, rows, fields) {
			if (error === undefined) {
				fs.readFile(__dirname + "/../bin/environmentState/" + id + ".json", "utf8", function (err, contents) {
					callback({
						lines: rows.rows,
						environment: (contents) ? JSON.parse(contents) : contents
					});
				});
			} else {
				console.log(error);
				throw new Error("Error querying the database");
			} //end if
		});
	} //end getConsoleOutput()


	/**
	 * Generates raw function code for WPS service.
	 * Input parameters for generated function: "input" (object with properties, property names correspond to
	 * names of the WPS service parameters) and "mapping" (maps externally defined ValuesStores to output data
	 * of the WPS service).
	 * Asynchronous callWPS() takes following parameters: object with properties of WPS service, object with mapping of WPS
	 * service parameters to selected widgets (described later), actual input data as object and output parameters
	 * mappings to ValuesStores.
	 * Object that maps widget names to parameters has following structure: {input: {paramIN1: "file", ..}, output: {paramOUT1: "file_save"}}
	 *
	 * @param params Array
	 * @param name string
	 * @param status boolean
	 * @param array specificparams
	 *
	 * @return string Raw function code
	 */

	// var _generateFunctionCodeForWPS = function(params, name, method, status, specificparams) {
	// 	var widgetmapping = "{";
	// 	var numberofparameters = params.length;
	// 	for (var i = 0; i < numberofparameters; i++) {
	// 		var currentparameter = params[i];
	// 		widgetmapping  += currentparameter.fieldname + ': "' + params[i].widget.name + '"';
	// 		if (i < (numberofparameters - 1)) {
	// 			widgetmapping += ",";
	// 		}
	// 	}

	// 	widgetmapping += "}";
	// 	if (status === true || status === "true") {
	// 		status = "true";
	// 	} else {
	// 		status = "false";
	// 	}

	// 	var serverssettings = "[";
	// 	var numberofservers = specificparams.wpsservers.length
	// 	for (var i = 0; i < numberofservers; i++) {
	// 		serverssettings += '{method: "' + method + '", status: "' + status + '",';

	// 		var j = 0;
	// 		var currentserver      = specificparams.wpsservers[i];
	// 		var numberofproperties = Object.keys(currentserver).length;
	// 		for (var property in currentserver) {
	// 			if (currentserver.hasOwnProperty(property) === true) {
	// 				serverssettings += property + ': "' + currentserver[property] + '"';
	// 				if (j < (numberofproperties - 1)) {
	// 					serverssettings += ",";
	// 				}

	// 				j++;
	// 			}
	// 		}

	// 		serverssettings += "}";
	// 		if (i < (numberofservers - 1)) {
	// 			serverssettings += ",";
	// 		}
	// 	}

	// 	serverssettings += "]";

	// 	var rawcode = "function " + name + "(input, mapping, specification){ callWPS(" + serverssettings + ", " + widgetmapping + ", input, mapping, specification);}";
	// 	return rawcode;
	// }


	/**
	 * Registers new method.
	 *
	 * @param function callback       Positive callback
	 * @param string   name           Name of the service (human-readable)
	 * @param string   description    Service description
	 * @param string   type           Service type (js / wps)
	 * @param object   params         Input parameters
	 * @param object   output_params  Output parameters
	 * @param string   ownerid        Identitificator of the service owner
	 * @param boolean  status         Specifies if service supports long-lasting execution
	 * @param object   specificparams Specific params
	 *
	 * @return array
	 */

	// this.registerService = function(callback, name, description, map_reduce_specification, type, params, output_params, ownerid, method, status, specificparams) {
	// 	if (common.isundefined(name) === true || common.isundefined(description) === true || common.isundefined(type) === true 
	// 	|| (common.isundefined(method) === true && type !== "js") || common.isundefined(ownerid) === true || common.isundefined(status) === true) {
	// 		throw new Error("Wrong parameters were provided");
	// 	}

	// 	if (common.isobject(params) === false || common.isobject(output_params) === false || common.isobject(specificparams) === false) {
	// 		throw new Error("Invalid input, output or service-specific parameters were provided");
	// 	}

	// 	if (common.isboolean(status) === false) {
	// 		throw new Error("Invalid status was provided");
	// 	}

	// 	if (status) {
	// 		status = "TRUE";
	// 	} else {
	// 		status = "FALSE";
	// 	}

	// 	var query  = "INSERT INTO public.wps_methods (name, type, params, created_by, created_on, description, map_reduce_specification, output_params, status, is_deleted)\
	// 	VALUES ('" + name + "', '" + type + "', '" + JSON.stringify(params) + "', '" + ownerid + "', current_timestamp, '" + description + "', '" + map_reduce_specification + "',\
	// 	'" + JSON.stringify(output_params) + "', " + status + ", FALSE); SELECT LASTVAL();";

	// 	global.pgPool.query(query, function(error, rows, fields) {
	// 		if (error === null)	{
	// 			var receivedid = rows.rows[0].lastval;
	// 			switch (type) {
	// 				case "wps":
	// 					if (common.hasproperties(specificparams, ["wpsservers"]) === false) {
	// 						throw new Error("Incomplete WPS service description");
	// 					}

	// 					var rawjscode = _generateFunctionCodeForWPS(params, name, method, status, specificparams);
	// 					var subquery  = "UPDATE public.wps_methods SET js_body = '" + rawjscode + "', wpsservers = '" + JSON.stringify(specificparams.wpsservers) + "' WHERE id = " + receivedid + ";";
	// 					break;
	// 				case "js":
	// 					if (common.hasproperties(specificparams, ["funcbody"]) === false) {
	// 						throw new Error("Incomplete JS service description");
	// 					}

	// 					var subquery = "UPDATE public.wps_methods SET js_body = '" + specificparams.funcbody + "' \
	// 					WHERE id = " + receivedid + "; SELECT LASTVAL();";
	// 					break;
	// 			} //end switch

	// 			global.pgPool.query(subquery, function(error, rows, fields) {
	// 				if (error === null) {
	// 					callback(receivedid);
	// 				} else {
	// 					console.log(error);
	// 					throw new Error("Error querying the database");
	// 				}
	// 			});
	// 		} else {
	// 			console.log(query);
	// 			console.log(error);
	// 			throw new Error("Error querying the database");
	// 		}
	// 	});
	// } //end registerService()


	/**
	 * Updates previously stored service.
	 *
	 * @param function callback Positive callback
	 * @param string   name           Name of the service (human-readable)
	 * @param string   description    Service description
	 * @param string   type           Service type (js / wps)
	 * @param object   params         Input parameters
	 * @param object   output_params  Output parameters
	 * @param boolean  status         Specifies if service supports long-lasting execution
	 * @param object   specificparams Specific params
	 * @param integer  previd         Service identificator
	 *
	 * @return array
	 */

// 	this.updateService = function(callback, name, description, map_reduce_specification, type, params, output_params, method, status, specificparams, previd) {
// 		if (common.isundefined(name) === true || common.isundefined(description) === true || common.isundefined(type) === true
// 		|| common.isundefined(status) === true) {
// 			throw new Error("Wrong parameters were provided");
// 		}

// 		if (common.isobject(params) === false || common.isobject(output_params) === false || common.isobject(specificparams) === false) {
// 			throw new Error("Invalid input, output or service-specific parameters were provided");
// 		}

// 		if (common.isboolean(status) === false) {
// 			throw new Error("Invalid status was provided");
// 		}

// 		var query = "UPDATE public.wps_methods SET status = '" + status + "', name = '" + name + "',\
// 		description = '" + description + "', map_reduce_specification = '" + map_reduce_specification + "',   params = '" + JSON.stringify(params) + "', output_params = '" + JSON.stringify(output_params) + "'\
// 		WHERE id = " + previd + ";";
// 		switch (type) {
// 			case 'wps':
// 				if (common.hasproperties(specificparams, ["wpsservers"]) === false) {
// 					throw new Error("Incomplete WPS service description");
// 				}

// 				var rawjscode = _generateFunctionCodeForWPS(params, name, method, status, specificparams);
// 				query        += "UPDATE public.wps_methods SET js_body = '" + rawjscode + "', wpsservers = '" + JSON.stringify(specificparams.wpsservers) + "' WHERE id = " + previd + ";";
// 				break;
// 			case 'js':
// 				if (common.hasproperties(specificparams, ["funcbody"]) === false) {
// 					throw new Error("Incomplete JS service description");
// 				}

// 				query += "UPDATE public.wps_methods SET js_body = '" + specificparams.funcbody + "' WHERE id = " + previd + ";";
// 				break;
// 		} //end switch

// 		global.pgPool.query(query, function(error, rows, fields) {
// 			if (error === null) {
// 				callback(rows.rows);
// 			} else {
// 				console.log(query);
// 				console.log(error);
// 				throw new Error("Error querying the database");
// 			} //end if
// 		});
// 	} //end updateService()

} //end class


module.exports = WPSManager;