var rootpath  = process.cwd() + '/';
var path      = require('path');
var filelink  = require("./filelink");
var theme2shp = require("./theme2shp");
var pg        = require("pg");
var geo_table = require("../dataset/geo_table");
// var calipso   = require(path.join(rootpath, 'lib/calipso'));

// postgres = require(rootpath + 'modules/community/postgres/postgres');
// var client = postgres.connect();

var geo_meta_table = require("../dataset/geo_meta_table");
var meta_manager = new geo_meta_table(100);
// meta_manager.init();

/**
 * Class for transforming input parameters for service regarding its widget settings.
 */

"use strict";

function WPSInputParametersProcessor(req, id) {
	var _req = req;
	var _id = id;

	if ("user" in _req.session) {
		var _currentusername = _req.session.user.username;
		var _currentuserid   = _req.session.user.id;
	} else {
		var _currentusername = "nodejswpsserver";
		var _currentuserid   = null;
	}

	/**
	 * Processing input parameters according to widget politics.
	 *
	 * @param Object parameterdescriptions
	 * @param Object parametervalues
	 *
	 * @return Object
	 */

	this.process = function(parameterdescriptions, parametervalues, processingCallback) {
		var numberofparameters = parameterdescriptions.length;
		var processedFieldnames = new Array();
		var settleResult = function (fieldname, value) {
			if (value === false) {
				for (key in parametervalues) {
					if (key === fieldname) {
						delete parametervalues[key];
						break;
					}
				}
			} else {
				parametervalues[fieldname] = value;
			}
			
			for (var i = 0; i < processedFieldnames.length; i++) {
				if (processedFieldnames[i] === fieldname) {
					processedFieldnames.splice(i, 1);
					break;
				}
			}

			if (processedFieldnames.length === 0) {
				processingCallback(parametervalues);
			}
		}

		for (var i = 0; i < numberofparameters; i++) {
			processedFieldnames.push(parameterdescriptions[i].fieldname);
		}

		for (var i = 0; i < numberofparameters; i++) {
			if (parametervalues[parameterdescriptions[i].fieldname]) {
				switch (parameterdescriptions[i].widget.name) {
					case "file_save":
						_processFileSave(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], _currentusername, settleResult);
						break;
					case "file":
						if (parameterdescriptions[i].fieldname in parametervalues) {
							_processFile(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], _id, settleResult, _currentusername);
						} else {
							throw new Error("Unable to get corresponding input value for " . parameterdescriptions[i].fieldname);
						}
						break;
					case "theme_select":
						_processThemeSelect(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], settleResult);
						break;
					case "rectangle":
						_processExtent(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname], settleResult);
						break;
					default:
						settleResult(parameterdescriptions[i].fieldname, parametervalues[parameterdescriptions[i].fieldname]);
						break;
				} //end switch
			} else {
				settleResult(parameterdescriptions[i].fieldname, false);
			} //end if
		} //end for
	} //end process()


	var _processFileSave = function(fieldname, value, currentusername, callback) {
		callback(fieldname, filelink.getAbsolutePath(value, currentusername));
	} //end _processFileSave()


	/**
	 * Processing file widget.
	 *
	 * @param string value Parameter value
	 *
	 * @return string
	 */

	var _processFile = function(fieldname, value, serviceid, callback,currentusername) {
		var absolutepath = filelink.getAbsolutePath(value, currentusername);
		var link         = filelink.getLinkForFile(absolutepath, serviceid);

		callback(fieldname, link);
	} //end _processFile()


	/**
	 * Processing the theme select widget.
	 *
	 * @param string value Parameter value
	 *
	 * @return string
	 */

	var _processThemeSelect = function (fieldname, value, callback) {
		

		var tableID;
		var filterValues;
		if (value.indexOf(":") !== -1) {
			var splitValue = value.split(":");
			tableID = parseInt(splitValue[0]);
			filterValues = JSON.parse(new Buffer(splitValue[1], 'base64').toString());
			for (var key in filterValues) {
				filterValues["f_" + key] = filterValues[key];
			}
		} else {
			tableID = value;
			filterValues = {value: tableID};
		}

		

		meta_manager.curent_user_id = _currentuserid;
		console.log('tableID=',tableID)
		meta_manager.get_table_json({}, tableID, function (meta){
			var requestedGeoTable = new geo_table(_currentuserid, tableID, meta, {convertGeometries: false});
			var query = requestedGeoTable.buildquery(req, undefined, filterValues);

			
			var filetosaveslash = filelink.getRandomFileName(".shp");
			filetosave          = filetosaveslash.replace(new RegExp("/",'g'), '\\');
			var localtheme2shp = new theme2shp();
			localtheme2shp.convert({
				filename: filetosave,
				schema: _req.database_schema,
				themename: meta.tablename,
				sql: query
			});

			var link = filelink.getLinkForFile(filetosaveslash, "t2s");
			callback(fieldname, link);
		});
	} //end _processThemeSelect()


	/**
	 * Processing extent widget.
	 *
	 * @param string value Parameter value
	 *
	 * @return string
	 */

	var _processExtent = function(fieldname, value, callback) {
		var reg = /([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)\,\s?([0-9]*\.?[0-9]*)\s([0-9]*\.?[0-9]*)/;
		var myArray = reg.exec(value);

		if (myArray && myArray.length === 7) {
			x1 = myArray[1];
			y1 = myArray[2];
			x2 = myArray[5];
			y2 = myArray[6];
			value = x1 + ',' + y1 + ',' + x2 + ',' + y2 + ',urn:ogc:def:crs:EPSG:6.6:4326';
			console.log("extent",value)

			callback(fieldname, value);
		} else {
			callback(fieldname, "");
		}
	} //end _processExtent()


} //end class


module.exports = WPSInputParametersProcessor;