/**
 * Proxy for avoiding Same-Origin-Policy restrictions
 */

var rootpath = process.cwd() + '/',
path         = require('path'),
// calipso      = require(path.join(rootpath, 'lib/calipso')),
queryString  = require('querystring'),
urlman       = require('url'),
// common       = require(rootpath + 'modules/community/common/lib/common'),
http         = require('http');

function getqueryparams(req){
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.queryString(attrname);
	for (var attrname in req.body)
		qparams[attrname] = req.bodyString(attrname);
	return qparams;
}
/**
* Index page
*/
function request(req, res, next) {
	var tempo = getqueryparams(req);
	

	
		if (tempo.type == 'geonames') {
			var options = {
				host   : tempo.host,
				path   : '/' + tempo.path + '&username=' + tempo.username,
				method : 'GET'
			};
		} else if (tempo.type == 'regular') {
			var options = tempo.url;		
		} else {
			var options = {
				host   : tempo.host,
				port   : tempo.port,
				path   : tempo.wpspath + tempo.request,
				method : 'GET'
			};
		} //end if

		var username = req.user.username;

		// Transforming filepaths
		if (tempo.internalid != undefined && tempo.internalid != '0') {
			var query  = 'SELECT params FROM wps_methods WHERE id = ' + tempo.internalid;
			
			var cquery = global.pgPool.query(query, function(error, rows, fields) {
				if (error === undefined) {		
					var obj = JSON.parse(rows.rows[0].params);
					for (var i = 0; i < obj.length; i++) {
						if ((obj[i].widget.name == 'file') || (obj[i].widget.name == 'file_save')) {
							options.path = replaceFilepath(obj[i].fieldname, options.path, username);
						} else if (obj[i].widget.name == 'theme_select' ) {
							options.path = replaceThemeName(req, obj[i].fieldname, options.path, username);
						} else if (obj[i].widget.name == 'rectangle' ) {
							options.path = replaceEXTENT(req, obj[i].fieldname, options.path, username);
						}
					}
					
					var intrequest = http.request(options, function(result) {
						result.setEncoding('utf8');
					});

					intrequest.on('response', function (response) {
						var body = '';
						response.on('data', function (chunk) {
							body += chunk;
						});
						response.on('end', function () {
							res.format = "xml";
							res.end(body,"UTF-8");
							return;
						});
					});

					intrequest.on('error', function(e) {
						console.log('Problem with request: ' + e.message);
					});

					intrequest.end();
				} else {
					console.log('Problem with request: ' + e.message);
				}
			});
		} else {
			console.log('options=',options);
			var intrequest = http.request(options, function(result) {
				result.setEncoding('utf8');
			});

			intrequest.on('response', function (response) {
				var body = '';
				response.on('data', function (chunk) {
					body += chunk;
				});

				response.on('end', function () {
					res.format = "xml";
					res.end(body,"UTF-8");
					return;
				});
			});

			intrequest.on('error', function(e) {
				console.log('Problem with request: ' + e.message);
			});

			intrequest.end();
		}
}

module.exports = request;