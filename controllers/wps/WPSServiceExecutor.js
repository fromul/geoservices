var rootpath       = process.cwd() + '/';
// var common         = require("../../common/lib/common");
var ServiceExample = require('./ServiceExample');
// var misc           = require(rootpath+'themes/core/cleanslate/public/js/misc');
var fs             = require('fs');
var filelink       = require("./filelink");


var common = require(rootpath+"/controllers/utils/common");
var misc = require(rootpath+"/public/js/misc");

// var	path             = require('path');
// var calipso          = require(path.join(rootpath, 'lib/calipso'));
/**
 * Class for executing the service.
 */

function WPSServiceExecutor(client, req, serviceinfo, servicesinfo, executerconfig) {
	var _req = req;
	var _serviceexampleinstanceid = false;

	if ("user" in _req.session) {
		var _currentusername = _req.session.user.username;
	} else {
		var _currentusername = "nodejswpsserver";
	}
	
	//console.log('_currentusername',_currentusername);

	var _databaseclient  = client;
	var _serviceinfo     = serviceinfo;
	var _servicesinfo    = servicesinfo;
	var _executerconfig  = ((executerconfig) ? executerconfig : {});
	var _positivecallback, _failurecallback, _useroutputparameters;


	/**
	 * Returns identifier of the service example
	 *
	 * @return integer
	 */
	this.getServiceExampleId = function () {
		if (_serviceexampleinstanceid === false) {
			throw "Unable to get the service example identifier";
		} else {
			return _serviceexampleinstanceid;
		}
	} // end getServiceExampleId()
	
	
	/**
	 * Executing the sevice.
	 *
	 * @param function callback            Positive callback
	 * @param Object   userinputparameters User input parameters
	 *
	 * @return void
	 */
	this.execute = function(positivecallback, failurecallback, serviceid, userinputparameters, useroutputparameters) {
		_positivecallback     = positivecallback;
		_failurecallback      = failurecallback;
		_useroutputparameters = useroutputparameters;

		var rawinput               = JSON.stringify(userinputparameters);
		var serviceexampleinstance = new ServiceExample(_databaseclient, _req);

		serviceexampleinstance.create(serviceid, rawinput, function (serviceexampleid) {
			_serviceexampleinstanceid   = serviceexampleid;
			var rawuserparameters       = misc.str_replace('"', "'", rawinput);
			var rawfunctionsdefinition  = _getExecutedRawFunctionsDefinition(serviceexampleid);
			var rawvaluestoredefinition = _getValueStoreDefinition(useroutputparameters);

			var specification = "";
			if (_serviceinfo.map_reduce_specification) {
				specification = new Buffer(_serviceinfo.map_reduce_specification.replace(/(\r\n|\n|\r|\s+|\t)/gm, "")).toString('base64');
			}

			var launchedcode = rawfunctionsdefinition.rawfunctionsstring + 
			rawvaluestoredefinition.prolog +
			rawfunctionsdefinition.currentservicefunctionname +
			"(" + rawinput + ", " + rawvaluestoredefinition.mapping + ", '" + specification + "');" +
			rawvaluestoredefinition.epilog;
			launchedcode = launchedcode.replace(/"/g, "'");
			console.log('launchedcode=',launchedcode)
			// launchedcode = launchedcode.replace(/\'/g, "'");
			
			userFolder = rawvaluestoredefinition.userFolder;
			console.log(_req.hostname);
			var url = _req.hostname;//_req.app.config.get('server:url');
			if (url) {
				if (url.substr((url.length - 2), (url.length - 1)) !== "/") {
					url += "/";
				}
			} else {
				throw "Unable to retrieve server url";
			}
			
			var folder = process.cwd().replace("\\", "/");
			if (folder.substr((folder.length - 2), (folder.length - 1)) !== "/") {
				folder += "/public/";
			} else {
				folder += "public/";
			}

			var serviceparameters = [serviceexampleid, launchedcode, folder, url, userFolder];

			fs.writeFileSync("test", serviceexampleid + " \"" + launchedcode + "\" \"" + folder + "\" \"" + url + "\" \"" + userFolder + "\""); 	
			_launchExecutable(rawfunctionsdefinition, serviceparameters, serviceexampleinstance);
		}, _failurecallback);
	} //end execute


	/**
	 * Launches binary in order to execute the service.
	 *
	 * @param Object        functionsdata         Service data
	 * @param array         serviceparameters     Parameters for executable
	 * @param ServiceExample serviceexampleinstance Method example instance
	 *
	 * @return void
	 */

	var _launchExecutable = function(functionsdata, serviceparameters, serviceexampleinstance) {
		if (_executerconfig.executerpath === undefined || _executerconfig.executerpath === '') {
			_executerconfig.executerpath = __dirname + '\\..\\bin\\executer.exe';
		} //end if

		if (_executerconfig.cwd === undefined || _executerconfig.cwd === '') {
			_executerconfig.cwd = __dirname + '\\..\\bin\\';
		} //end if

		if (fs.existsSync(_executerconfig.executerpath) === false || fs.existsSync(_executerconfig.cwd) === false) {
      var errorText = "ERROR: Specified executer path (" + _executerconfig.executerpath + ") or working directory ("
        + _executerconfig.cwd + ") do not exist";
			throw errorText;
		}

    try {
		console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',serviceparameters);
      var terminal = require('child_process').spawn(_executerconfig.executerpath, serviceparameters, {
        env: process.env,
        cwd: _executerconfig.cwd
      });

      terminal.stdout.on('data', function (data) {
				// console.log('stdout', data.toString());
        var arr = misc.explode('\r\n', '' + data);
        for (var i = 0; i < arr.length; i++) {
          if (arr[i] === "") {
            arr.splice(i, 1);
          }
        }

        var valuetype = "";
        var objectwasreceived = true;
        try {
          eval('var o = ' + arr[(arr.length - 1)] + '; valuetype = typeof o;');
        } catch(error) {
          objectwasreceived = false;
        } //end try

        if (objectwasreceived === true && valuetype !== "string") {
          var result = new Object();
          if (typeof functionsdata.outputparamsforcurrentfunction !== 'undefined') {
            var outputParametersDescriptions = JSON.parse(_serviceinfo.output_params);
            for (var property in o) {
              if (o.hasOwnProperty(property)) {
                var propertyWasProcessed = false;
                for (var i = 0; i < outputParametersDescriptions.length; i++) {
                  if (outputParametersDescriptions[i].fieldname.toLowerCase() === property.toLowerCase()) {
                    if (outputParametersDescriptions[i].widget.name === "file_save") {											
											console.log('_useroutputparameters', _useroutputparameters, _currentusername);
                      if (property in _useroutputparameters) {

												result[property] = filelink.getUserFilePath(_useroutputparameters[property], _currentusername);
												
                        propertyWasProcessed = true;
                      }
                    }
                  }
                }

                if (propertyWasProcessed === false) {
                  result[property] = o[property];
                }
              } //end if
            } //end for
          } //end if

          serviceexampleinstance.updateProperty('end_time', 'NOW()');
          serviceexampleinstance.updateProperty('status', 'METHOD_EXAMPLE_SUCCEEDED');
          serviceexampleinstance.updateProperty('result', JSON.stringify(result));
        } else {
          for (var i = 0; i < arr.length; i++) {
            serviceexampleinstance.appendToSTDOUT(arr[i]);
          }
        } //end if
      });

      terminal.stderr.on('data', function (data) {
				// console.log('stderr', data.toString());
        var arr = misc.explode('\r\n', '' + data);
        if (arr[0] !== '') {
          for (var i = 0; i < arr.length; i++) {
            serviceexampleinstance.appendToSTDERR(arr[i]);
          }

          serviceexampleinstance.updateProperty('end_time', 'NOW()');
          serviceexampleinstance.updateProperty('status', 'METHOD_EXAMPLE_FAILED');
        } //end if
      });
      
      terminal.on('close', function (code) {
        console.log('child process exited with code ' + code);
        if (code !== 0) {
          serviceexampleinstance.appendToSTDERR("Error occurred, application exited with code " + code);
          serviceexampleinstance.updateProperty('end_time', 'NOW()');
          serviceexampleinstance.updateProperty('status', 'METHOD_EXAMPLE_FAILED');
        }
      });
    } catch(err) {
      console.log("Error occured during executer launch");
      console.log(err);
    }

		_positivecallback(serviceexampleinstance.getID());
	} //end _launchExecutable()


	/**
	 * Returns ready-to-use set of strings with definitions for ValueStore objects;
	 *
	 * @return Object
	 */

	var _getValueStoreDefinition = function(useroutputparameters) {
		var rawoutputparametersstring = "";
		var outputparameters          = JSON.parse(_serviceinfo.output_params);
		var numberofoutputparameters  = outputparameters.length;
		for (var i = 0; i < numberofoutputparameters; i++) {
			var parameter = outputparameters[i];

			rawoutputparametersstring += " var outputparameter" + parameter.fieldname + "Store" + i + " = new ValueStore();";
		}

		var mappingparametersstring = "{";
		var outputwidgets           = "{";
		for (var i = 0; i < numberofoutputparameters; i++) {
			var parameter = outputparameters[i];

			mappingparametersstring += parameter.fieldname + ": outputparameter" + parameter.fieldname + "Store" + i;
			outputwidgets += parameter.fieldname + ": '" + parameter.widget.name + "'";

			if (i !== (numberofoutputparameters - 1)) {
				mappingparametersstring += ",";
				outputwidgets           += ",";
			}
		}

		mappingparametersstring += "}";
		outputwidgets += "}";

		var rawoutputwaitforparametersstring = "var rawoutput = {";
		for (var i = 0; i < numberofoutputparameters; i++) {
			var parameter = outputparameters[i];

			rawoutputwaitforparametersstring += parameter.fieldname + ": outputparameter" + parameter.fieldname + "Store" + i + ".get()";
			if (i !== (numberofoutputparameters - 1)) {
				rawoutputwaitforparametersstring += ",";
			}
		}
		//console.log('============================',_currentusername);
		var saveExecutionResultsPath = filelink.getAbsolutePath("", _currentusername);
		//var saveExecutionResultsPath = calipso.config.getModuleConfig("rfm", "mainrootdir");
		//console.log('saveExecutionResultsPath=',saveExecutionResultsPath);
		
		if (saveExecutionResultsPath.slice(-1) !== "/") saveExecutionResultsPath += "/";

		userFolder = saveExecutionResultsPath;// + _currentusername;

		rawoutputwaitforparametersstring += "}; var output = processOutput(rawoutput, " + outputwidgets + ", '"
		+ userFolder + "', " + JSON.stringify(useroutputparameters).replace(/"/g, "'") + "); output";

		return {
			prolog:     rawoutputparametersstring,
			mapping:    mappingparametersstring,
			epilog:     rawoutputwaitforparametersstring,
			userFolder: userFolder + "/"
		};
	} //end _getValueStoreDefinition()


	/**
	 * Returns ready-to-use set of strings for registered service functions.
	 *
	 * @return Object
	 */

	var _getExecutedRawFunctionsDefinition = function() {
		var calledServiceBody = false;
		for (var i = 0; i < _servicesinfo.length; i++) {
			if (_servicesinfo[i].id == _serviceinfo.id) {
				calledServiceBody = misc.cleanNewLines(misc.functionReverseTags(_servicesinfo[i].js_body, true));
			}
		}

		var currentservicefunctionname = "";
		var rawfunctionsstring         = "";

		var inputparamsforcurrentfunction;
		var outputparamsforcurrentfunction;

		for (var i = 0; i < _servicesinfo.length; i++) {
			var currentFunctionName = misc.functionGetName(_servicesinfo[i].js_body);
			if (calledServiceBody.indexOf(currentFunctionName) !== -1) {
				var inputparams = JSON.parse(_servicesinfo[i].params);

				var inputparamsshortened = "{";
				for (var j = 0; j < inputparams.length; j++) {
					inputparamsshortened += inputparams[j].fieldname + ": '" + inputparams[j].widget.name + "'";
					if (j < (inputparams.length - 1)) {
						inputparamsshortened += ",";
					}
				} //end for

				inputparamsshortened += "}";

				if ((_servicesinfo[i].output_params != null) && (_servicesinfo[i].output_params !== 'undefined')) {
					var outputparams = JSON.parse(_servicesinfo[i].output_params);

					var outputparamsshortened = "{";
					for (var j = 0; j < outputparams.length; j++) {
						outputparamsshortened += outputparams[j].fieldname + ": '" + outputparams[j].type + "'";
						if (j < (outputparams.length - 1)) {
							outputparamsshortened += ",";
						} //end if							
					} //end for

					outputparamsshortened += "}";
				} else {
					outputparamsshortened = "{}";
				} //end if

				if (_servicesinfo[i].id == _serviceinfo.id) {
					eval('inputparamsforcurrentfunction = ' + inputparamsshortened);
					eval('outputparamsforcurrentfunction = ' + outputparamsshortened);
				} //end if

				rawfunctionsstring += misc.cleanNewLines(misc.functionReverseTags(_servicesinfo[i].js_body, true)) + '; ';

				if (_servicesinfo[i].id == _serviceinfo.id) {
					var currentservicefunctionname = misc.functionGetName(_servicesinfo[i].js_body);
				} //end if
			} //end if
		} //end for

		if (common.isundefined(currentservicefunctionname) === true) {
			_failurecallback();
			throw new Error("Unable to get current service function name");
		}

		var result = {
			rawfunctionsstring             : rawfunctionsstring,
			currentservicefunctionname     : currentservicefunctionname,
			inputparamsforcurrentfunction  : inputparamsforcurrentfunction,
			outputparamsforcurrentfunction : outputparamsforcurrentfunction,
		};

		return result;
	} //end _getExecutedRawFunctionsDefinition()


} //end class


module.exports = WPSServiceExecutor;