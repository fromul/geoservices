var DIR_SEPARATOR = '/';


var rootpath = process.cwd() + '/',
  path       = require('path'),
//   calipso    = require(path.join(rootpath, 'lib/calipso')),
  fs         = require('fs'),
//   common     = require(rootpath + 'modules/community/common/lib/common'),
//   misc       = require(rootpath + 'themes/core/cleanslate/public/js/misc'),
  http       = require('http'),
  os         = require("os");
var config = require("../../conf.json");
var misc = require(rootpath+"/public/js/misc");


// var pg = require('pg');
// var conString = calipso.config.getModuleConfig('postgres', 'connectionstring');


/**
 *	External exports
 */
 
exports = module.exports = {
  init: init, 
  route: route,
  getAbsolutePath:   getAbsolutePath,
  getLinkForFile:    getLinkForFile,
  fetchFileForLink:  fetchFileForLink,
  getUserFilePath:   getUserFilePath,
  getRootPath:       getRootPath,
  getRandomFileName: getRandomFileName,
  getfile:getfile,
  config: {
	maxlinklifetime: {
        "default": "0",
        "label": "Max lifetime",
        "type": "text",
        "description": "Max lifetime of the generated link (<b>0</b> value is for disabling lifetime check)"
	},
	maxfetchqueries: {
        "default": "0",
        "label": "Max number of queries",
        "type": "text",
        "description": "Max number of times the link can be accessed (<b>0</b> value is for disabling the check)"
	},
  }
};


/**
 * Router
 */
 
function route(req,res,module,app,next) {
	module.router.route(req,res,next);
};


/**
 *	Module initialization
 */
 
function init(module,app,next) {
	calipso.lib.step(function defineRoutes() {
		module.router.addRoute(new RegExp('^\/getfile\/[^>]*$', 'ig'), getFileFileLink, {admin:false}, this.parallel());
	});

	next();
};


/**
 * Returns the extension for the provided file.
 *
 * @param string filename  File name
 * @param string extension Desired extension
 *
 * @return string
 */
function changeExtension(filename, extension) {
	var oldextension         = getExtension(filename);
	var filewithnewextension = filename.replace("." + oldextension, "." + extension);
	return filewithnewextension;
} //end changeExtension()


/**
 * Returns the extension for the provided file.
 *
 * @param string filename File name
 *
 * @return string
 */
function getExtension(filename) {
	var lastdot   = filename.lastIndexOf(".");
	var extension = filename.substring((lastdot + 1), filename.length);
	return extension;
} //end getExtension()


/**
 *	Getting file according to the filelink
 *
 *	@param string filepath Absolute path of the file
 *
 *	@return string URL link to the file
 */

function getfile(req, res, next) {
	var uniqueval = req.url.split("/")[2];

	var query  = "SELECT filepath, times_accessed FROM public.filelink_data WHERE uniqueval = '" + uniqueval + "';";
	
	var cquery = global.pgPool.query(query, function(error, rows, fields) {
		

		if (error === undefined) {
			if (rows.rows.length > 0) {
				var filepath = rows.rows[0].filepath;
				var ta       = parseInt(rows.rows[0].times_accessed) + 1;
				var maxta    = parseInt(config.modules.filelink.config.maxfetchqueries);

				var queriedextension = getExtension(req.url);
				var correctfilepath  = changeExtension(filepath, queriedextension);
				console.log('correctfilepath',correctfilepath);

				if ((maxta !== 0) && (ta > maxta)) {
					res.format = "json";
					res.end(JSON.stringify({status: 'error', error: 'The link exceeded maximum number of fetch queries'}));
					return;
				}

				if (correctfilepath !== undefined) {
					fs.exists(correctfilepath, function(exists) {
						if (exists) {
							var filestatistics  = fs.statSync(correctfilepath)
							var fileSizeInBytes = filestatistics["size"];
							if (fileSizeInBytes > 0) {
								var readstream = fs.createReadStream(correctfilepath);

								res.writeHead(200);
								readstream.setEncoding("binary");

								readstream.on("data", function (chunk) {
									res.write(chunk, "binary");
								});

								readstream.on("end", function () {
								   res.end();
								   return;
								});

								readstream.on('error', function(err) {
									res.write(err + "\n");
									res.end();
									return;
								});
							}
						} else {
							res.writeHead(404);
							res.write("No data\n");
							res.end();
							return;
						}
					});
				} else {
					res.format = "json";
					res.end(JSON.stringify({status: 'error', error: 'No such file'}));
					return;
				}
			} else {
				res.format = "json";
				res.end(JSON.stringify({status: 'error', error: 'Unable to find specified file, check the link validity or access rights'}));
				return;
			}
		} else {
			res.format = "json";
			res.end(JSON.stringify({status: 'error', error: error}));
			return;
		}
	});
 };


/**
 *	Getting link for specified file and managing the database record
 *
 *	@param string filepath Absolute path of the file
 *
 *	@return string URL link to the file
 */

function getLinkForFile(absfilepath, id) {
	var uniqueval   = misc.randomString(10, true) + id;
	var filenamearr = misc.explode('/', absfilepath);
	var filename    = filenamearr[filenamearr.length - 1];

	var serverUrl = config.server.url;
	// if (serverUrl.indexOf(":") === -1) {
		serverUrl += ":" + config.server.port;
	// }

	var link   = serverUrl + "/wps/getfile/" + uniqueval + DIR_SEPARATOR + filename;
	var access = '{allow: "*"}';

	
	var query = "INSERT INTO public.filelink_data (filepath, link, access, times_accessed, method_example, created, uniqueval) VALUES ('" + absfilepath + "', '" + link + "', '" + access + "', '0', NULL, NOW(), '" + uniqueval + "');";
	var cquery = global.pgPool.query(query, function(error, rows, fields) {
		
		if (error !== undefined) {
			misc.c("Error saving generated link in the database");
			misc.c(error);
		}
	});
	return link;
}


/**
 *	Fetching file from the remote server and saving at certain location
 *
 *	@param string filelink URL of the file
 *	@param string filepath Path of the fetched file on local storage
 *
 *	@return bool Shows if the file was fetched and saved properly
 */
 
function fetchFileForLink(filelink, filepath) {
	var file = fs.createWriteStream(filepath);
	var request = http.get(filelink, function(response) {
		response.pipe(file);
		return true;
	}).on('error', function(e) {
		misc.c("Unable to fetch " + filelink + " as " + filepath + ", error:");
		misc.c(e);
	});
	return false;
}


/**
 * Returns root path
 *
 *	@return string
 */
function getRootPath() {
	return config.modules.rfm.config.mainrootdir;
}


/**
 *	Getting absolute physical path to the file
 *
 *	@param string filepath User path of the file
 *
 *	@return string Absolute path to the file
 */
function getAbsolutePath(filepath, username) {
	
	var prefix = config.modules.rfm.config.mainrootdir;
	if (prefix.substr(prefix.length - 1) !== "/") {
		prefix += "/";
	}

	if(username)
		username=username + "/";
	else
		username="";

	filepath = prefix.replace(/\\/g, "/") + filepath;
	return filepath;
}


/**
 *	Getting user-friendly filepath from absolute one
 *
 *	@param string absfilepath Absolute path of the file
 *	@param string username    Name of authenticated user
 *
 *	@return string Absolute path to the file
 */
function getUserFilePath(absfilepath, username) {
	var prefix   = config.modules.rfm.config.mainrootdir;
	var toremove = prefix;
	filepath     = misc.str_replace(toremove, "", absfilepath);
	return filepath;
}


/**
 * Generate random file according to user settings.
 *
 * @return string
 */
function getRandomFileName(extension) {
	var prefix = config.modules.filelink.config.temporaryfilesfolder;
	if (!prefix) prefix = "S:/temp/shared";

	var filename = prefix + DIR_SEPARATOR + misc.randomString(16, true) + extension;
	return filename;
} //end getRandomFileName()

