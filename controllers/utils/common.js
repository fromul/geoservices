"use strict";

var rootpath = process.cwd();
// var calipso = require(require('path').join(rootpath, 'lib/calipso'));
// var misc    = require(rootpath + '/themes/core/cleanslate/public/js/misc');
// var fs      = require('fs');
/**
 *  Shortcut for detecting if variable is undefined.
 *
 * @param mixed value Analyzed variable
 *
 * @return boolean
 */

function isundefined(value) {
    return typeof value === 'undefined';
} //end isundefined()


/**
 *  Shortcut for detecting if variable is JavaScript Object.
 *
 * @param mixed value Analyzed variable
 *
 * @return boolean
 */

function isobject(value) {
    return (value !== null && typeof value === 'object');
} //end isobject()


/**
 *  Shortcut for detecting if variable is boolean.
 *
 * @param mixed value Analyzed variable
 *
 * @return boolean
 */

function isboolean(value) {
    return typeof value === 'boolean';
} //end isboolean()


/**
 *  Shortcut for detecting if variable is integer.
 *
 * @param mixed value Analyzed variable
 *
 * @return boolean
 */

function isinteger(value) {
    return (value === parseInt(value));
} //end isinteger()


/**
 * Checks if object has certain properties.
 *
 * @param object item       Analyzed object
 * @param array  properties Checked properties
 *
 * @return boolean
 */

function hasproperties(item, properties) {
	var numberofproperties = properties.length;
	for (var i = 0; i < numberofproperties; i++) {
		if (item.hasOwnProperty(properties[i]) === false) {
			return false;
		}
	} //end for

	return true;
} //end hasproperties()


/**
*	Recusrsive object check for XSS
*/
var XSSRecursiveCheck = function (obj)
{
	for (var k in obj) {
		if (typeof obj[k] === "object")
			XSSRecursiveCheck(obj[k])
		else
			obj[k]= sanitize(obj[k]).xss();
    }
	return obj;
}

module.exports.XSSRecursiveCheck = XSSRecursiveCheck;
  
  
/**
*	Logger
*/
module.exports.Logger = function (path) {
	this.path = '';
	if (!(misc.isset(path))) {
		console.log('LOGGER: Error while setting logging path');
		this.log = function(message) {
			console.log('LOGGER: Set the proper filepath');
		}
		return;
	} else {
		this.path = path;
	}
	this.log = function(message) {
		fs.appendFile(this.path, message + '\r\n', function (err) {});
	}
}
  
  
/**
* Dump to file
*/
module.exports.dumpToFile = function (obj) {
	var out = '';
    if(obj && typeof(obj) == "object"){ for (var i in obj) { out += i + ": " + obj[i] + "\n"; }}
	else { out = obj; }
	var fs = require('fs');
	fs.writeFile("/tmp/dumpoutput.txt", out, function(err) {
		if(err) { console.log(err);	}
		else { console.log('Object was dumped to C:\\tmp\\dumpoutput.txt');}
	});
}

/**
 * Scans folder for widget files and theirs stylessheets except w_base file.
 *
 * @param string searchpath Path where files should be searched
 *
 * @return array
 */

var scanForWidgets = function(searchpath) {
	if (fs.existsSync(searchpath) === false) {
		throw new Error("Path " + searchpath + " does not exists, make sure that the proper configuration for widget directory is set in geothemes module configuration");
	}

	var warr   = fs.readdirSync(searchpath);
	var output = new Array();

	output.push('<!-- Widgets JS --!>');
	output.push('<script type="text/javascript" src="/js/widgets/widgets.js"></script>');
	for (var i = 0; i < warr.length; i++) {
		if ((warr[i].indexOf('w_') != -1) && (warr[i] != 'widgets.js')) output.push('<script type="text/javascript" src="/js/widgets/'+warr[i]+'"></script>');
	}

	warr = fs.readdirSync(searchpath + '/css');
	output.push('<!-- Widgets CSS --!>');
	for (var i = 0; i < warr.length; i++) {
		if ((warr[i].indexOf('w_') != -1)) output.push('<link href="/js/widgets/css/'+warr[i]+ '"rel="stylesheet"/>');
	}

	return output;
} //end scanForWidgets()

/**
 * Generates string of random chars of specified length.
 *
 * @param integer length Desired string length
 *
 * @return string
 */

var randomString = function (length) {
    if (!length === true)
	{
		length = 16;
	}

	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
    var str   = '';
    for (var i = 0; i < length; i++)
	{
		str += chars[Math.floor(Math.random() * chars.length)];
	}

    return str;
} //end randomString()


var isNumeric = function (n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/**
* Returns JSON for specific column
*/
module.exports.getcolumn=function(column, obj) {
	for (var i = 0; i < obj.columns.length; i++) {
		if (column == obj.columns[i].fieldname) {
			return obj.columns[i];
		}
	}
	console.log('Column "' + column + '" was not found in metadata object');
	return null;
}

/* Variable type detection */
module.exports.isundefined   = isundefined;
module.exports.isobject      = isobject;
module.exports.isboolean     = isboolean;
module.exports.isinteger     = isinteger;
module.exports.isNumeric     = isNumeric;

// module.exports.getcolumn     = getcolumn;

module.exports.randomString = randomString;
module.exports.hasproperties = hasproperties;

// Currently tested


module.exports.scanForWidgets = scanForWidgets;


