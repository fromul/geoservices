/**
 * Wps manager
 */

var config = require("../conf.json");
var rfm = require("./rfm/rfm");
var fs = require('fs');


function getqueryparams(req){
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.queryString(attrname);
	for (var attrname in req.body)
		qparams[attrname] = req.bodyString(attrname);
	return qparams;
}

var rootpath     = process.cwd() + '/',
path             = require('path'),
fs               = require('fs'), 
urlman           = require('url'),
// common           = require(rootpath + 'modules/community/common/lib/common'),
// misc             = require(rootpath + 'themes/core/cleanslate/public/js/misc'),
// expressValidator = require('express-validator'),
// sanitize         = require('validator').sanitize,
filelink         = require( './wps/filelink');

var WPSServer           = require("./wps/WPSServer");
var WPSManager          = require("./wps/WPSManager");
var WPSManagerBootstrap = require("./wps/WPSManagerBootstrap");
var wpsrequest          = require("./wps/WPSProxy");

/**
 * Database connection
 */
// var pg = require('pg');
// var conString = calipso.config.getModuleConfig('postgres', 'connectionstring');
// var client = new pg.Client(conString);
// client.connect();


/**
 * Getting permissions from configuration
 */

// var commonusagePerm = calipso.permission.Helper.hasPermission("wpsmanager:commonusage");
// var aPerm          = calipso.permission.Helper.hasPermission("geothemes:general");
/**
 * Routes this module will respond to
 */
// var routes = [
// 	{path: 'GET /wpsserver',          fn: wpsServerInitiate},
// 	{path: 'GET /proxy',              fn: proxyRequest,      admin:false, block: 'content'},
// 	{path: 'POST /wpsmanager/create', fn: createWpsManager,  admin:false, template: 'wpscreate', block: 'content'},
// 	{path: 'GET /wpsmanager/create',  fn: createWpsManager,  admin:false, template: 'wpscreate', block: 'content'},
// 	{path: 'GET /wpsmanager/ajax',    fn: ajaxWpsManager,    admin:false, block: 'content'},
// 	{path: 'GET /wpsmanager/getinfo', fn: getinfoWpsManager, admin:false, block: 'content'},
//   ];


 /**
 * Router
 */

// function route(req, res, module, app, next) {
// 	res.menu.primary.addMenuItem(req, {name:'WPS manager',              permit: commonusagePerm, path: 'wpsmanager',            url: '/wpsmanager',         description: 'WPS manager', security: [] });
// 	res.menu.primary.addMenuItem(req, {name:'Create method',            permit: commonusagePerm, path: 'wpsmanager/create',     url: '/wpsmanager/create',  description: 'Register new method', security: [] });
// 	res.menu.primary.addMenuItem(req, {name:'Available methods',        permit: commonusagePerm, path: 'wpsmanager/list',       url: '/managetable?id=185', description: 'List of available services', security: [] });
// 	res.menu.primary.addMenuItem(req, {name:'Method execution history', permit: commonusagePerm, path: 'wpsmanager/viewactive', url: '/managetable?id=1019', description: 'List of all ever executed method instances', security: [] });

// 	module.router.route(req, res, next);
// }
  
// exports = module.exports = {
//   init   : init,
//   route  : route,
//   routes : routes,
//   config : {
// 	executablepath : {
//         "default"     : "C:\\\\WPS\\\\executer\\\\Debug\\\\executer.exe",
//         "label"       : "Path to executable",
//         "type"        : "text",
//         "description" : "Path to the executable binary file"
// 	},
// 	cwd : {
//         "default"     : "C:\\\\WPS\\\\executer\\\\Debug",
//         "label"       : "Working dir",
//         "type"        : "text",
//         "description" : "Working dir where executable is placed"
// 	},
// 	verboseoutput : {
//         "default"     : true,
//         "label"       : "Full output",
//         "type"        : "checkbox",
//         "description" : "Verbose logging, generally used for debugging"
// 	},
//   }
// };

/**
 * Initialization of the module
 */

function init(module, app, next) {
	var bootstrapper = new WPSManagerBootstrap(client);
	bootstrapper.setup(calipso);

	calipso.permission.Helper.addPermission("wpsmanager:commonusage", "Access to WPSManager module", false);
	next();
} //end init()


/**
* Index page
*/

function indexWpsManager(req, res, options, next) { 
	res.layout = 'wpsmanager';
	var item = {
		username : req.helpers.user.username,
	};
 
	var template = calipso.modules.wpsmanager.templates.wpsmanager;
	var block    = options.block;

	calipso.theme.renderItem(req, res, template, block, {
		item: item
	},next);
} //end indexWpsManager()


/**
 * Executing proxied request to fetch remote server info.
 */
exports.proxy = function (req, res, next) {
	wpsrequest(req, res, next);
} //end proxyRequest()


/**
 * Pretending to be a WPS server.
 */
function wpsServerInitiate(req, res, options, next) {  
	var server     = new WPSServer(client);
	var parameters = req.url.split("?");

	if (parameters.length === 2) {
		server.serveRequest(parameters[1], res, req);
	} else {
		res.format = "json";
		res.end("{status: error, message: 'Invalid request was provided, no parameters in GET request were provided'}");
		console.log("Invalid request was provided, no parameters in GET request were provided");
	}
} //end wpsServerInitiate()


/**
* Execute wps mehod
*/

exports.execute = function(req, res, next) {
	if (req.user===undefined || req.user==null){
		res.end(JSON.stringify({"status":"error", "message": "You should log in."}),"UTF-8"); 
		console.log('Session is undefined.');
		return;
	}
	var curuser = req.user.username;
	
		var manager    = new WPSManager(req);
		var id           = req.queryString('id');
		console.log('id=',id);
		var inputparams  = req.query['inputparams'];
		var outputparams = req.query['outputparams'];	
		try {
			var userinputparameters  = JSON.parse(inputparams);
			var useroutputparameters = JSON.parse(outputparams);
		} catch(e) {
			throw new Error("Error while parsing input JSON parameter");
		}
		

		var username = req.user.username;
		
		manager.executeService(function(methodexampleid) {
			res.format = "json";
			res.end(JSON.stringify({status: 'executing', data: methodexampleid}));
		},
		function() {
			res.format = "json";
			res.end(JSON.stringify({status: 'error', error: "Application crushed"}));
		},
		id, username, userinputparameters, useroutputparameters, {
			executerpath : config.modules.wpsmanager.config.executablepath,
			cwd          : config.modules.wpsmanager.config.cwd,
			verbose      : false,
		});
		// switch (action) {
		// 	case 'getwrappers':
		// 		var excludeid = sanitize(url.query['excludeid']).toInt();
		// 		if (isNaN(excludeid) === true) {
		// 			res.format = "json";
		// 			res.end(JSON.stringify({status: 'success', data: []}));
		// 		} else {
		// 			manager.getWrappers(function(rows) {
		// 				res.format = "json";
		// 				res.end(JSON.stringify({status: 'success', data: rows}));
		// 			}, excludeid);
		// 		}
		// 		break;
		// 	case 'getconsoleoutput':
		// 		var id = sanitize(url.query['id']).toInt();
		// 		manager.getConsoleOutput(function(data) {
		// 			res.format = "json";
		// 			res.end(JSON.stringify({status: 'success', data: data}));
		// 		}, id);
		// 		break;
		// 	case 'executemethod':
		// 		var id           = sanitize(url.query['id']).toInt();
		// 		var inputparams  = url.query['inputparams'];
		// 		var outputparams = url.query['outputparams'];
		// 		console.log('before getting params');
		// 		try {
		// 			var userinputparameters  = common.XSSRecursiveCheck(JSON.parse(inputparams));
		// 			var useroutputparameters = common.XSSRecursiveCheck(JSON.parse(outputparams));
		// 		} catch(e) {
		// 			throw new Error("Error while parsing input JSON parameter");
		// 		}
		// 		console.log('after getting params');

		// 		var username = req.user.username;
		// 		console.log('ajaxWpsManager');
		// 		manager.executeService(function(methodexampleid) {
		// 			res.format = "json";
		// 			res.end(JSON.stringify({status: 'executing', data: methodexampleid}));
		// 		},
		// 		function() {
		// 			res.format = "json";
		// 			res.end(JSON.stringify({status: 'error', error: "Application crushed"}));
		// 		},
		// 		id, username, userinputparameters, useroutputparameters, {
		// 			executerpath : calipso.config.getModuleConfig('wpsmanager', 'executablepath'),
		// 			cwd          : calipso.config.getModuleConfig('wpsmanager', 'cwd'),
		// 			verbose      : false,
		// 		});
		// 		break;
		// 	default:
		// 		res.format = "json";
		// 		res.end(JSON.stringify({status: 'error', error: 'Wrong action is provided'}));
		// 		break;
		// } //end switch
	// });
}



/**
* Execute wps mehod
*/

exports.output = function(req, res, next) {
	if (req.user===undefined || req.user==null){
		res.end(JSON.stringify({"status":"error", "message": "You should log in."}),"UTF-8"); 
		console.log('Session is undefined.');
		return;
	}
	var curuser = req.user.username;
	
	var manager    = new WPSManager(req);
	var id           = req.queryString('id');
	console.log('id=',id);
	

	var username = req.user.username;
	// var id = sanitize(url.query['id']).toInt();
	manager.getConsoleOutput(function(data) {
		res.format = "json";
		res.end(JSON.stringify({status: 'success', data: data}));
	}, id);
	
}

/**
* Create method page
*/

function createWpsManager(req, res, options, next) {
	calipso.form.process(req, function(form) {
		var manager = new WPSManager(req, client);
		
		
		if (form) {
			var obj = common.XSSRecursiveCheck(JSON.parse(form.data));

			var status = false;
			if (obj.status === "true") {
				status = true;
			}

			var extraparams = new Object();
			if (obj.type === "js") {
				extraparams.funcbody = obj.funcbody;
			} else if (obj.type === "wps") {
				extraparams.wpsservers = obj.servers;
			} else {
				throw new Error("Wrong type value: " + obj.type);
			}

			if ("previd" in obj) {
				var serviceid = parseInt(obj.previd);
				manager.updateService(function() {
					res.format = "json";
					res.end(JSON.stringify({status: 'success', id: serviceid}));
				}, obj.name, obj.description, obj.map_reduce_specification, obj.type, obj.params, obj.output_params, obj.method, status, extraparams, serviceid);
			} else {
				var serviceid = parseInt(obj.previd);
				manager.registerService(function(receivedid) {
					var reply = {
						status  : 'success',
						id      : receivedid,
						message : 'Method was saved'
					};

					res.format = "json";
					res.end(JSON.stringify(reply),"UTF-8");
				}, obj.name, obj.description, obj.map_reduce_specification, obj.type, obj.params, obj.output_params, req.session.user.id, obj.method, status, extraparams);
			}
		} else {
			res.layout = 'wpsmanager';
			var widgetpath = process.cwd() + calipso.config.getModuleConfig('geothemes', 'widgetdirectory');
			wscripts   = common.scanForWidgets(widgetpath);
			s          = wscripts.join('\n');
			var item = {
				username  : req.helpers.user.username,
				w_scripts : s,
			};
			var template = calipso.modules.wpsmanager.templates.wpscreate;
			var block    = options.block;
			calipso.theme.renderItem(req, res, template, block, {
				item: item
			},next);
		}
	});
} //end createWpsManager()


/**
* Getting complete information about the method
*/

function getinfoWpsManager(req, res, options, next) {
	var url = urlman.parse(req.url, true);
	var id  = sanitize(url.query['id']).toInt();
	console.log('getinfoWpsManager');

	if (isNaN(id) === false) {
		var query = "SELECT * FROM public.wps_methods WHERE id = " + id + ";";
		var cquery = client.query(query, function(error, rows, fields) {
			var obj = rows.rows[0];
			if (error === null) {
				var subquery = "SELECT * FROM wps_methods WHERE id = " + obj.id + ";";
				var squery   = client.query(subquery, function(error, rows, fields) {			
					var subobj = rows.rows[0];
					if (error === null) {
						obj.params        = JSON.parse(obj.params);
						obj.output_params = JSON.parse(obj.output_params);
						res.format        = 'json';
						res.end(JSON.stringify({status: 'success', method: obj, methodspec: subobj}));
					} else {
						res.format = 'json';
						res.end(JSON.stringify({status: 'error'}));
					}
				});
			} else {
				res.format = 'json';
				res.end(JSON.stringify({status: 'error'}));
			} //end if
		});
	} else {
		res.format = 'json';
		res.end(JSON.stringify({status: 'error'}));
	} //end if
} //end getinfoWpsManager


/**
* View and launch method page
*/

function viewWpsManager(req, res, options, next) {
	var url = urlman.parse(req.url, true);
	var id  = sanitize(url.query['id']).toInt();

	var widgetpath = process.cwd() + calipso.config.getModuleConfig('geothemes', 'widgetdirectory');
	wscripts = common.scanForWidgets(widgetpath);
	s        = wscripts.join('\n');
	var item = {
		username  : req.helpers.user.username,
		w_scripts : s,
	};
	
	var query  = "SELECT id FROM wps_methods;";
	var cquery = client.query(query, function(error, rows, fields) {
		var obj = rows.rows;
		if (error === null)
		{
			var subquery = "SELECT * FROM wps_methods WHERE id = " + id + ";";
			var squery = client.query(subquery, function(error, rows, fields) {			
				var subobj = rows.rows[0];
				if (isNaN(id) === false)
				{
					if (error === null)
					{
						subobj.params = JSON.parse(subobj.params);
						var item = {
							name        : subobj.name,
							description : misc.str_replace('XXNEWLINEXX', '<br>', subobj.description),
							methodspec  : JSON.stringify(subobj),
							w_scripts   : s,
						}
					}
					else
					{
						var item = {
							name        : 'ERROR',
							description : 'ERROR OCCURED',		
						}
					} //end if
				} //end if

				res.layout   = 'wps';
				var template = calipso.modules.wpsmanager.templates.wpsview;
				var block    = options.block;
				calipso.theme.renderItem(req, res, template, block, {
					item: item
				},next);
			});
		}
		else
		{
			var item = {
				name: 'ERROR',
				description: 'ERROR OCCURED',		
			}

			res.layout   = 'wps';
			var template = calipso.modules.wpsmanager.templates.wpsview;
			var block    = options.block;
			calipso.theme.renderItem(req, res, template, block, {
				item: item
			},next);
		} //end if
	});
} //end viewWpsManager()