var config = require("../conf.json");
var rfm = require("./rfm/rfm");
var fs = require('fs');
var iconv            = require('iconv-lite');
var geo_table = require("./dataset/geo_table");

var support_exts=['tab', 'shp'];

function getqueryparams(req){
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.queryString(attrname);
	for (var attrname in req.body)
		qparams[attrname] = req.bodyString(attrname);
	return qparams;
}




/**
 * object list method
 */
exports.list = function(req, res, next) {
	// console.log(req.user)
}


function _qv(query, valuename){
	if (query===undefined)
		return '';
	if(valuename in query) {
		return query[valuename];
	} else {
		return '';
	}
} //end _qv()



/**
 * import method
 */
exports.import = function (req, res, next) {
	
	var dataset_import_id = req.queryString('path');
	var user_id='';
	if(req.user && req.user.id!=='' )
		user_id=req.user.id;
	
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.query[attrname];
	for (var attrname in req.body)
		qparams[attrname] = req.body[attrname];
	var first_is_head='first_is_head' in qparams;
	var delimeter=_qv(qparams, 'delimeter');	
	if(delimeter=='' || delimeter=='t')
		delimeter="\t";
	var encoding=_qv(qparams, 'encoding');	
	if(encoding=='')
		encoding='CP1251';

	
	
		var mainrootdir = config.modules.rfm.config.mainrootdir;
		var file_path=rfm.getpath(dataset_import_id);
		
		if (req.g_table) {
			var mdobj=req.meta;
			var g_table=req.g_table;
			
			for (var i = 0; i < mdobj.columns.length; i++){
				if(!(mdobj.columns[i].fieldname in qparams))
					continue;
				if(mdobj.columns[i].widget.name=='classify'){
					//mdobj.columns[i].g_table = new geo_table(user_id, mdobj.columns[i].widget.properties.dataset_id, meta_manager, mdobj.columns[i].widget.properties.ref_meta);
					mdobj.columns[i].g_table = new geo_table(user_id, mdobj.columns[i].widget.properties.dataset_id, mdobj.usingmeta[mdobj.columns[i].widget.properties.dataset_id]);
				}
			}
			var objectlist=[];
			function load_obj(loadobj, last){
				var newobj={};
				console.log('loadobj',loadobj);
				if(loadobj!=null){
					function handling_params(paramnum, obj, newobj){
						if(paramnum == mdobj.columns.length){
							console.log('INSERT LOAD');
							console.log(newobj);
							objectlist.push(newobj);
							
							return;
						}
						// console.log('target fieldname=', mdobj.columns[paramnum].fieldname);
						// console.log('qparams', qparams);

						if((mdobj.columns[paramnum].fieldname in qparams)){
							var method=qparams[mdobj.columns[paramnum].fieldname];
							// console.log('method name',method.name, mdobj.columns[paramnum].fieldname);										
							switch (method.name){											
								case 'copy':
									if(method.prm!==undefined && method.prm.length==1 && method.prm[0].value!=undefined && method.prm[0].value!=''){
										cpy_fld=method.prm[0].value;
										// console.log('source field = ',cpy_fld, obj[cpy_fld]);										
										if(cpy_fld in obj && obj[cpy_fld]!==undefined && obj[cpy_fld]!==''){
											// console.log('widget name=',mdobj.columns[paramnum].widget.name);
											if(mdobj.columns[paramnum].widget.name=='classify'){
												subqparams={};
												var props = mdobj.columns[paramnum].widget.properties;
												if(props!=undefined && props.db_field!=null && props.db_field!=undefined){
													subqparams['f_'+props.db_field[0]]=obj[cpy_fld];
													sub_sql=mdobj.columns[paramnum].g_table.buildquery(['id'], subqparams);
													console.log(sub_sql);
													var subquery = global.pgPool.query(sub_sql, function(error, query_result, unofields2) {
														console.log('run sql end');
														if (error === undefined)	{
															if(query_result.rows.length==1){
																newobj[mdobj.columns[paramnum].fieldname]=query_result.rows[0]['id'];
															}
															else
																console.log('ambiguous decision for value='+obj[cpy_fld]);
															handling_params(paramnum+1, obj, newobj);
															return;
															
														} else {
															console.log(sub_sql);
															console.log(error);
															return;
														}
													});
													return;
												}
											}
											else if(mdobj.columns[paramnum].widget.name=='number'){
												
												v=obj[cpy_fld];
												console.log('v=');
												console.log(v);
												console.log('name='+mdobj.columns[paramnum].fieldname);
												if(v!==undefined && typeof v === 'string'){
													v=v.replace(/,/g, '.');
													if(!isNaN(parseFloat(v)) && isFinite(v))
														newobj[mdobj.columns[paramnum].fieldname]=v;
												}
												else if(v!==undefined && typeof v === 'number'){
													newobj[mdobj.columns[paramnum].fieldname]=v;
												}
											}
											else{
												newobj[mdobj.columns[paramnum].fieldname]=obj[cpy_fld];
											}
										}
									}
									break;
								case 'copy_pnt':
									if(method.prm!==undefined && method.prm.length==2 && method.prm[0].value!=undefined && method.prm[1].value!=undefined && method.prm[0].value!='' && method.prm[1].value!='' ){
										cpy_fld=method.prm[0].value;
										cpy_fld2=method.prm[1].value;
										var lat='', lon='';
										if(cpy_fld in obj && obj[cpy_fld]!==undefined && obj[cpy_fld]!=''){
											lat=m_geom.parseCoordinate(obj[cpy_fld]);
										}
										if(cpy_fld2 in obj && obj[cpy_fld2]!==undefined && obj[cpy_fld2]!=''){
											lon=m_geom.parseCoordinate(obj[cpy_fld2]);
										}
										if(lat!='' && lon!=''){
											newobj[mdobj.columns[paramnum].fieldname]='MULTIPOINT('+lat+' '+lon+')';
										}
									}
									break;
								// case 'copy_binary_geom':
								// 	if(method.prm!==undefined && method.prm.length==2 && method.prm[0].value!=undefined && method.prm[1].value!=undefined && method.prm[0].value!='' && method.prm[1].value!='' ){
								// 		cpy_fld=method.prm[0].value;
										
										
								// 		newobj[mdobj.columns[paramnum].fieldname]='MULTIPOINT('+lat+' '+lon+')';
										
								// 	}
								// 	break;
								case 'copy_date':
									if(method.prm!==undefined && method.prm.length==2 && method.prm[0].value!=undefined && method.prm[1].value!=undefined && method.prm[0].value!='' && method.prm[1].value!='' ){
										cpy_fld=method.prm[0].value;
										frm=method.prm[1].value;										
										d=m_date.getDateFromFormat(obj[cpy_fld], frm);
										if (d!=0) {
											dv = new Date(d); 
											newobj[mdobj.columns[paramnum].fieldname]=m_date.formatDate(dv, 'yyyy-MM-dd HH:mm:ss');
										}
									}
									break;
								case 'const':
									if(method.prm!==undefined && method.prm.length==1 && method.prm[0].value!=undefined && method.prm[0].value!='' ){
										v=method.prm[0].value;												
										if (v!='') {													
											newobj[mdobj.columns[paramnum].fieldname]=v;
										}
									}
									break;												
							}
						}
						else{
							// console.log('not found '+mdobj.columns[paramnum].fieldname);
						}
						handling_params(paramnum+1, obj, newobj);								
					}
					handling_params(0, loadobj, newobj);
				}
				else if (!last){
					res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
				}
				if(last){
					function iterate_insert(num){
						if(objectlist.length<=num){
							output = {
								status      : 'ok',
								data		: 'ok'
							}
							res.end(JSON.stringify(output),"UTF-8");
							return;
						}
						g_table.insert(req,objectlist[num], function(result){								
							iterate_insert(num+1);
						});
					}
					console.log('start inserting=', objectlist.length);
					iterate_insert(0);
					
					return false;
				}
				return true;
			}
			ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
			if(ext=='csv' || ext=='txt'){
				filedata_load(file_path, encoding, first_is_head, delimeter, load_obj);		
			}
			else if(support_exts.indexOf(ext)!=-1){
				new_file_path = file_path.substr(0, file_path.lastIndexOf(".")) + ".json";
				jsondata_load(new_file_path, encoding, load_obj);
			}
		}		

}





///////////////////////////////



function getwktpointlist(line){
	var res='(';
	line.forEach(function(p,j){
		if(j<line.length-1)
			res += p[0] + ' ' + p[1] + ', ';
		else
			res += p[0] + ' ' + p[1] + ')';
	});
	return res;
} 
 function GeoJSON2WKT(GeoJSON){
	 if(GeoJSON==null)
		 return null;
	 switch (GeoJSON.type){
		case 'Point':
			return 'MULTIPOINT(('+GeoJSON.coordinates[0]+' '+GeoJSON.coordinates[1]+'))';
		case 'MultiPoint':
			return 'MULTIPOINT('+getwktpointlist(GeoJSON.coordinates)+')';			
		case 'LineString':
			return 'MULTILINESTRING('+getwktpointlist(GeoJSON.coordinates)+')';			
		case 'MultiLineString':
			return 'MULTILINESTRING('+getwktpointlist_of_list(GeoJSON.coordinates)+')';			
		case 'Polygon':
			return 'MULTIPOLYGON('+getwktpointlist_of_list(GeoJSON.coordinates)+')';			
		case 'MultiPolygon':
			var res='MULTIPOLYGON('
			GeoJSON.coordinates.forEach(function(polygon,i){
				if(i<GeoJSON.coordinates.length-1)
					res += getwktpointlist_of_list(polygon)+ ', ';
				else
					res += getwktpointlist_of_list(polygon) + ')';				
			});
			return res;						
	 }
	 return '';
 }
 
 function readJSON(input, func, encoding) {
  var remaining = '';
  var state=0;  
  var beg_str='"features": [';
  var scb_cnt=0;
  var obj_beg=-1;
  function extract_object(){
	var i=0;
	while (i<remaining.length){
		s=remaining.charAt(i);
		if(s=='{'){
			scb_cnt++;
			if(obj_beg==-1)
				obj_beg=i;
		}
		else if(s=='}')
			scb_cnt--;
		if(scb_cnt==0 && obj_beg!=-1){
			str_obj=remaining.substring(obj_beg, i + 1);
			remaining = remaining.substring(i + 1);
			obj_beg=-1;
			i=0;
			if(!func(str_obj, false))
				input.close();			
		}
		i++
	}	  
  }

  input.on('data', function(data) {
	if(encoding){
		try{
			data1 = iconv.decode(data, encoding);	  
			data=data1;
		} catch (ex) {
			console.log(ex);			
		}
	}	  
	remaining += data;
	switch (state){
		case 0: // looking object start
			var index = remaining.indexOf(beg_str);
			if(index==-1)
				return;					
			remaining = remaining.substring(index + beg_str.length + 1);	
			state=0;
			break;
		case 1: //object extracting
			extract_object();
			if(remaining!='' && remaining.charAt(0)==']'){
				func('', true);
				state=2;
				return;
			}			
			break;
		case 2:
			break;
	}
  });

  input.on('end', function() {
	  extract_object();
    //if (remaining.length > 0) {
      func('', true);
    //}
  });
}


function jsondata_load(file_path, encoding, callback){
	var columns=[];
	var translator;
	
	if (fs.existsSync(file_path)) {
		// if(encoding!='UTF-8'){
		// 	var Iconv  = require('iconv').Iconv;
		// 	translator = new Iconv(encoding, 'UTF-8');
		// }		
		
		function func(data, last) {
			if(data!=''){
				console.log('data=');
				console.log(data);
				row=JSON.parse(data);
				var cobj={};
				for(var fieldname in row.properties){
					cobj[fieldname]=row.properties[fieldname];
				}
				console.log(GeoJSON2WKT(row.geometry));				
				cobj.geom=GeoJSON2WKT(row.geometry);
				cobj.geomtype=row.geometry.type;
				//console.log('row=');
				//console.log(row);
				
				return callback(cobj, last);
			}
			if(last){
				return callback(null, last);				
			}
			return true;
		}
		var input = fs.createReadStream(file_path);
		readJSON(input, func, encoding);
	}
	else{
		callback(null,true);		
	}
}

function indexOfEnd(remaining){
    var index = remaining.indexOf('\n');
	if(index==-1)
		return -1;
    while (index > -1) {
		var line = remaining.substring(0, index-1);
		tmp=line.split('\t');
		if(tmp.length>7)
			return index;
		index = remaining.indexOf('\n', index+1);
	}
	return -1;
}
 
 function indexOfEnd(remaining, delimeter){
	var quote_start=false;
	var quote='';
	var i=0;
	var prevsign='';
	while (i<remaining.length) {
		var sign=remaining.charAt(i);
		var nextsign='';
		if(i+1<remaining.length)
			nextsign=remaining.charAt(i+1);
		if( (sign=="'" || sign=='"') && nextsign==sign){
			i=i+2;
			continue;
		}
		if(!quote_start && (sign=="'" || sign=='"') && (prevsign==delimeter || i==0 )) {
			quote_start=true;
			quote=sign;
		}
		else if(quote_start && sign==quote && (nextsign==delimeter || nextsign.charCodeAt(0)==13)  ){
			quote_start=false;
		}			
		else if(sign=='\n' && !quote_start)
			return i;
		prevsign=sign;
		i++;
	}
	return -1;    
}
 
 function readLines(input, func, encoding, delimeter) {
  var remaining = '';
  var read_head=false;

  input.on('data', function(data) {
	//console.log('3 symbols',data[0],data[1],data[2]);
	if(data[0]==239 && data[1]==187 && data[2]==191){
		data=data.slice(3);
	}
	if(encoding){
		try{
			data1 = iconv.decode(data, encoding);
			data=data1;
		} catch (ex) {
			console.log(ex);			
		}
	}
    remaining += data;
    //var index = remaining.indexOf('\n');
	var index = indexOfEnd(remaining, delimeter);	
    while (index > -1) {
      var line = remaining.substring(0, index);
      remaining = remaining.substring(index + 1);
      if(!func(line, false)){
		  input.close();
		  remaining='';
		  return;
		}
      //index = remaining.indexOf('\n');
	  index = indexOfEnd(remaining, delimeter);
    }
  });

  input.on('end', function() {
    //if (remaining.length > 0) {
      func(remaining, true);
    //}
  });
}



function smartsplit(data, delimeter){
	var res=[];
	var quote_start=false;
	var quote='';	
	var prevsign='';
	var value='';
	var i=0;
	while (i<data.length) {
		var sign=data.charAt(i);
		var nextsign='';
		if(i+1<data.length)
			nextsign=data.charAt(i+1);
		if( (sign=="'" || sign=='"') && nextsign==sign){
			i=i+2;
			value+=sign+nextsign;
			continue;
		}
		if(!quote_start && (sign=="'" || sign=='"') && (prevsign==delimeter || i==0 )) {
			quote_start=true;
			quote=sign;
		}
		else if(quote_start && sign==quote && nextsign==delimeter  ){
			quote_start=false;
		}			
		else if(sign==delimeter && !quote_start){
			res.push(value);
			value='';
			i++;
			continue;
		}			
		prevsign=sign;
		i++;
		value+=sign;
	}
	if(value!='')
		res.push(value);
	return res;
}

function filedata_load(file_path, encoding, first_is_head, delimeter, callback){
	var columns=[];
	var translator;
	if (fs.existsSync(file_path)) {
		// if(encoding!='UTF-8'){
		// 	var Iconv  = require('iconv').Iconv;
		// 	translator = new Iconv(encoding, 'UTF-8');
		// }
		
		var line_cnt=0;
		function func(data, last) {
			if(data!=''){				
				if(line_cnt==0){
					if(first_is_head){
						
						//if(data[0].charCodeAt(0).toString(16)=='feff'){
						//	data=data.substring(1, data.length - 1);
						//}
						tmp=smartsplit(data,delimeter);
						console.log(tmp);
						for(var i=0; i<tmp.length; i++){
							v=tmp[i];
							if(v!==undefined && v.length>1 && ((v.charAt(0)=='"' && v.charAt(v.length-1)=='"') || (v.charAt(0)=="'" && v.charAt(v.length-1)=="'")))
								v = v.substring(1, v.length - 1);								
							columns.push(v);
							console.log(v);
						}

					}
					else{
						tmp=smartsplit(data,delimeter);
						for(var i=1; i<=tmp.length; i++){
							columns.push('column'+i);
						}
					}
					line_cnt++;
					return true;					
				}
				line_cnt++;
				
				values=smartsplit(data,delimeter);
				var row={};
				for(var j=0; j<columns.length; j++){
					row[columns[j]]='';
				}				
				for(var j=0; j<values.length; j++){
					if(columns.length==j)
						columns.push('column'+(j+1));
					v=values[j];
					if(v!==undefined && v.length>1 && ((v.charAt(0)=='"' && v.charAt(v.length-1)=='"') || (v.charAt(0)=="'" && v.charAt(v.length-1)=="'")))
						v = v.substring(1, v.length - 1);
					row[columns[j]]=v;
				}
				
				return callback(row, last);
			}
			if(last){
				return callback(null, last);				
			}
			return true;
		}
		var input = fs.createReadStream(file_path);
		readLines(input, func, encoding, '\n');
	}
	else{
		callback(null,true);		
	}
}


exports.list = function(req, res, next) {
	var filehesh = req.queryString('path');
	
	var user_id='';
	if(req.user)
		user_id=req.user.id;

	var qparams = getqueryparams(req);

	var first_is_head='first_is_head' in qparams;
	var iDisplayStart=_qv(qparams, 'iDisplayStart');
	if(iDisplayStart=='')
		iDisplayStart=0;
	iDisplayStart=parseInt(iDisplayStart);
	var iDisplayLength=_qv(qparams, 'iDisplayLength');
	if(iDisplayLength=='')
		iDisplayLength=1000000;
	iDisplayLength=parseInt(iDisplayLength);
	if(first_is_head)
		iDisplayStart=iDisplayStart+1;
	var delimeter=_qv(qparams, 'delimeter');	
	if(delimeter=='' || delimeter=='t')
		delimeter="\t";
	var encoding=_qv(qparams, 'encoding');	
	if(encoding=='')
		encoding='CP1251';

	var mainrootdir = config.modules.rfm.config.mainrootdir;
	file_path=rfm.getpath(filehesh);
	//console.log('I am here='+filehesh);
	var col_cnt=0;
	var columns=[];
	var aaData     = new Array();
	res.format = "json";
	var content_str='';
	var line_cnt=0;
	
	//file = file.substr(0, file.lastIndexOf(".")) + ".htm";
	if(file_path.lastIndexOf(".")==-1){
		res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
		return;
	}
		
	
	
	
	function send_result(obj, last){
		if(obj!=null){
			line_cnt++;
			if(line_cnt>=iDisplayStart && line_cnt<iDisplayStart+iDisplayLength){
				aaData.push(obj);
			}
			if(line_cnt>=iDisplayStart+iDisplayLength-1)
				last=true;			
		}
		else if (!last){
			res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
		}
		if(last){
			iTotal         = line_cnt;
			iFilteredTotal = iTotal;
			output = {
				sEcho                : parseInt(_qv(qparams,'sEcho')),
				iTotalRecords        : '' + iTotal,
				iTotalDisplayRecords : '' + iFilteredTotal,
				aaData               : aaData,
			}
			res.end(JSON.stringify(output),"UTF-8");				
			return false;
		}
		return true;
	}
	ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
	if(ext=='csv' || ext=='txt'){
		filedata_load(file_path, encoding, first_is_head, delimeter, send_result);		
	}
	else if(support_exts.indexOf(ext)!=-1){
		var new_file_path = file_path.substr(0, file_path.lastIndexOf(".")) + ".json";
		var terminal = require('child_process').execFile('ogr2ogr', ['-f', "GeoJSON", new_file_path, file_path], function(error, stdout, stderr) {
			if (error) {
			console.log(error);
			}
			console.log(stdout);
		});
		terminal.stdout.on('end', function (rdata) {
			console.log('end');
			console.log(rdata);
			jsondata_load(new_file_path, encoding, send_result);
		});
		terminal.on('exit', function(code) {
			if (code != 0) {
				console.log('Failed: ' + code);
			}			
		});
	}
	else{
		res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
		return;		
	}

}


function filedata_info(req, res, options, next) {
	var filehesh = req.sanitize('f').escape();
	var qparams = req.query;
	for (var attrname in req.body)
		qparams[attrname] = req.body[attrname];
	console.log('session=',req.session);
	
	if(!('session' in req && 'user' in req.session && req.session && req.session.user && 'id' in req.session.user && req.session.user.id!='' )){
		res.end(JSON.stringify({status: "error", data: 'The user is not registered1.'}), "UTF-8");
		return;
	}
			
	user_id=req.session.user.id;
	calipso.modules.postgres.fn.gis_user_dir(user_id, function(user_dir){
	
		var mainrootdir = config.modules.rfm.config.mainrootdir;
		var file_path=calipso.modules.rfm.fn.getpath(filehesh);
		
		//console.log('I am here='+filehesh);	
		res.format = "json";
		
		if(file_path.lastIndexOf(".")==-1){
			res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
			return;
		}		
		ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
		var params=[];
		params.push('-d');
		params.push(file_path);
		if(common.isNumeric(_qv(qparams,'lng'))){		
			params.push('-lon');
			params.push(qparams['lng']);
		}
		if(common.isNumeric(_qv(qparams,'lat'))){		
			params.push('-lat');
			params.push(qparams['lat']);
		}
		if(ext=='tif' || ext=='tiff'){
			var stdout_str='';
			var terminal = require('child_process').spawn(rootpath + 'modules/community/geothemes/lib/rastr_info', params);
			terminal.stdout.on('data', function (rdata) {
				if(rdata)
					stdout_str+=rdata.toString();
			});
			terminal.stdout.on('end', function (rdata) {
				if(rdata)
					stdout_str+=rdata.toString();
				data=JSON.parse(stdout_str);
				res.end(JSON.stringify({status: "ok", data: data}), "UTF-8");
			});	
		}
		else{
			res.end(JSON.stringify({status: "error", data: 'The file format is not supported.'}), "UTF-8");
			return;		
		}	
	});
}

exports.meta = function(req, res, next) {
	var filehesh = req.queryString('path');
	
	var qparams = req.query;
	for (var attrname in req.body)
		qparams[attrname] = req.body[attrname];
	var first_is_head='first_is_head' in qparams;
	var delimeter=_qv(qparams, 'delimeter');	
	if(delimeter=='' || delimeter=='t')
		delimeter="\t";

	var encoding=_qv(qparams, 'encoding');	
	if(encoding=='')
		encoding='CP1251';

	
	if(req.user===undefined){		
	
		res.end(JSON.stringify({status: "error", data: 'The user is not registered2.'}), "UTF-8");
		return;
	}	
			
	user_id=req.user.id;	
	
	
	var mainrootdir = config.modules.rfm.config.mainrootdir;
	file_path=rfm.getpath(filehesh);
	var short_file_path=rfm.getpath(filehesh);
	console.log('I am here='+file_path);	
	res.format = "json";
	var content_str='';
	if(file_path.lastIndexOf(".")==-1){
		res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
		return;
	}	
	
	function send_result(obj, last){		
		if(obj!=null){
			mcolumns=[];
			var geomtype='';

			for(var fieldname in obj){
				if(fieldname=='geom'){
					continue;
				}
				if(fieldname=='geomtype'){
					geomtype=obj.geomtype;
					continue;
				}
				field={"fieldname":fieldname,"title":fieldname,"description":fieldname,"visible":true,"type":"string","widget":{"name":"edit","properties":{"size":"20"}}};
				mcolumns.push(field);
			}
			if(geomtype=='Point' || geomtype=='MultiPoint' ){
				field={"fieldname":"geom","title":"geom","description":"geom","visible":true,"type":"point","widget":{"name":"point"}};
				mcolumns.push(field);
			}
			else if(geomtype=='LineString' || geomtype=='MultiLineString' ){
				field={"fieldname":"geom","title":"geom","description":"geom","visible":true,"type":"line","widget":{"name":"line"}};
				mcolumns.push(field);
			}
			else if(geomtype=='Polygon' || geomtype=='MultiPolygon' ){
				field={"fieldname":"geom","title":"geom","description":"geom","visible":true,"type":"polygon","widget":{"name":"polygon"}};
				mcolumns.push(field);
			}
			datafile={dataset_id:filehesh, "tablename":short_file_path, "title":short_file_path,"columns":mcolumns};
			res.end(JSON.stringify({status: "ok", JSON:datafile}),"UTF-8");			
		}
		else {
			res.end(JSON.stringify({status: "error", data: 'The file is missing.'}), "UTF-8");
		}		
		return false;
	}	
	ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
	if(ext=='csv' || ext=='txt'){
		filedata_load(file_path, encoding, first_is_head, delimeter, send_result);		
	}
	else if(support_exts.indexOf(ext)!=-1){
		var new_file_path = file_path.substr(0, file_path.lastIndexOf(".")) + ".json";
		var terminal = require('child_process').execFile('ogr2ogr', ['-f', "GeoJSON", new_file_path, file_path], function(error, stdout, stderr) {
			if (error) {
			console.log(error);
			}
			console.log(stdout);
		});
		terminal.stdout.on('end', function (rdata) {
			console.log('end');
			console.log(rdata);
			jsondata_load(new_file_path, encoding, send_result);
		});
		terminal.on('exit', function(code) {
			if (code != 0) {
				console.log('Failed: ' + code);
			}			
		});
	}
	else{
		res.end(JSON.stringify({status: "error", data: 'The file format is not supported.'}), "UTF-8");
		return;		
	}

}


