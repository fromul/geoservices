/**
 * module for maps
 */
var rootpath = process.cwd() + '/',
path = require('path'),
fs = require('fs');
var config = require("../../conf.json");
var geo_table = require("../dataset/geo_table");
var common = require("../utils/common");
var rfm = require("../rfm/rfm");
// var request = require('request');

var mapserverstyle = require("../../public/js/mapserverstyle");

 

var dir_separator='/';

var meta_manager=null;

var font_symbols=[{
	name:'MapInfoSymbols',
	startsymbol: 34,
	endsymbol: 68
}];

function randomString(length) {
	var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');
	if (!length) {
		length = Math.floor(Math.random() * chars.length);
	}
	var str = '';
	for (var i = 0; i < length; i++) {
		str += chars[Math.floor(Math.random() * chars.length)];
	}
	return str;
}

function populateTemplate(data, topaste) {
	for(var key in topaste){
		data = data.replace(key, topaste[key]);
	}
	return data;
}


module.exports.init=function(module, objects) {
	meta_manager=objects.meta_manager;
}


function _qv(query, valuename){
	if (query===undefined)
		return '';
	if(valuename in query) {
		return query[valuename];
	} else {
		return '';
	}
}


function getqueryparams(req){
	var qparams = [];
	for (var attrname in req.query)
		qparams[attrname] = req.queryString(attrname);
	for (var attrname in req.body)
		qparams[attrname] = req.bodyString(attrname);
	return qparams;
}



/**
 * Converting locally processed filepath to the
 * filepath at the remote system.
 *
 * @param {String} filepath Converted filepath
 *
 * @return {String}
 */
function toMapserverPath(filepath) {
	var map_file_dir=config.modules.geothemes.config.mapfiledir;
	var remoteHostMapFilesPath = config.modules.geothemes.config.remoteHostMapFilesPath;
	if(!filepath)
		return '';
	var searchedRegExp = map_file_dir.replace(new RegExp("\\\\{1,2}", "g"), "(\\\\{1,2}|\\/{1,2})");
	var modifiedFilepath = filepath.replace(new RegExp(searchedRegExp), remoteHostMapFilesPath );
	return modifiedFilepath;
}


/**
 * Converting Windows UNC style to the Linux one (assuming
 * that target format starts with /s root folder.
 *
 * @param {String} filepath Converted filepath
 *
 * @return {String}
 */
function toUNIXUNC(filepath) {
	filepath = filepath.replace(new RegExp("[a-zA-Z]\:(\\\\|\\/)", "i"), "/s/");
	return filepath;
}

/**
 * Create map service
 */
module.exports.create_file_map  = function(req, res, next) {
	res.format = "json";
	// console.log('jjjjjjjjjjjjj');
	
	var qparams = getqueryparams(req)
	if (req.user===undefined || req.user==null){
		res.end(JSON.stringify({"status":"error", "message": "You should log in."}),"UTF-8"); 
		console.log('Session is undefined.');
		return;
	}

	console.log('qparams = ',qparams);
	var source   = qparams.source;
	var style    = qparams.style;
	var title    = qparams.title;
	// var lifetime = qparams.lifetime;
	var keywords = qparams.keywords;
	var metaid = qparams.metaid;
	var jsonstyle = qparams.jsonstyle;
	var filepath=rfm.getpath(source);
	var publish   = false;

	if(req.body.layermeta){
		try{
			var info = JSON.parse( req.body.layermeta);
		}
		catch{
			res.end(JSON.stringify({"status":"error", "message": "Meta is not correct"}),"UTF-8"); 		
			return;
		}
		createmap(info);
	}
	else{
		getfileinfo(filepath, function (info){
			// console.log('info = ',info);
			if(info.status=='ok')
				createmap(info.data);
			else{
				res.end(JSON.stringify({"status":"error1", "message": info.message }),"UTF-8"); 		
				return;
			}
		});
	}

	function createmap(fileinfo){
		// console.log('fileinfo=', fileinfo);

		if(jsonstyle){
			var styleconvertor = new mapserverstyle.mapserverstyle();
			
			try{
				
				styleconvertor.readFromJSON(jsonstyle);				
				styleconvertor.layer_json = fileinfo;
				style = styleconvertor.save2Map();					
				title = styleconvertor.title;
				publish = styleconvertor.published;
				// console.log('style=',style);
			}
			catch{
				// var stack = new Error().stack
				// console.log( stack )
				res.end(JSON.stringify({"status":"error", "message": "Style is not JSON"}),"UTF-8"); 		
				return;
			}			
		}
		
		
		var dirpath = path.dirname(filepath);
		var filenameclean = path.basename(filepath);
		var layername = filenameclean.substring(0, filenameclean.lastIndexOf('.'));
		
		var ext='';
		if(filepath.lastIndexOf(".")>-1)
			ext = filepath.substr(filepath.lastIndexOf(".")+1).toUpperCase();
		if(filepath=='' || ext=='' || !fs.existsSync(filepath)){
			res.end(JSON.stringify({"status":"error", "message": "The file source is not defined."}),"UTF-8"); 		
			console.log("ext=", ext);
			return;
		}

		var epsg='4326';
		if(qparams.epsg)
			epsg = qparams.epsg;
		var static_map=false;
		var mapserver_path = config.modules.geothemes.config.mapserver_path;

		
		var unique = randomString(8);//_qv(qparams, 'unique');
		var map_file_dir=config.modules.geothemes.config.mapfiledir;
		
		var user_id='';
		if( 'user' in req && req.user )	{
			user_id=req.user.id;
		}
		var mapfilename = 'rastr_' + unique+'.map';
		if(publish){
			var staticmapfiledir = 	config.modules.geothemes.config.staticmapfiledir;
			var mapfile = staticmapfiledir + '/' + mapfilename;
			var map_symbols_path = staticmapfiledir + '/' + mapfilename + '.sym';
		}
		else{
			var map_symbols_path = map_file_dir + '/' + mapfilename + '.sym';
			var mapfile = map_file_dir + '/' + mapfilename;
		}
			


		
		var tag_symbols = mapfilename + '.sym';
					

		var mapstyle = style;	
		// if('mapstyle' in qparams)
		// 	mapstyle=qparams['mapstyle'];
		//var mapstyle = _qv(qparams, 'mapstyle');
		var req_date;
		if('req_date' in qparams)
			req_date=qparams['req_date'];
		
		var topaste = new Object();
		var ret_style='';
		topaste.tag_style='';

		topaste.tag_style=mapstyle;

		topaste.tag_imagetype = 'PNG';
		topaste.tag_shapepath = toUNIXUNC(dirpath);
		topaste.tag_mappath = filepath.replace('/', '\\');
		topaste.tag_layer_name = layername;
		topaste.tag_layer_data = toUNIXUNC(filepath);

		topaste.tag_layer_type = fileinfo.layer_type;
		
		// console.log('-----------', fileinfo)
		
		topaste.tag_size_x = parseFloat(fileinfo.cell_size_x);
		topaste.tag_size_y = parseFloat(fileinfo.cell_size_y);

		topaste.tag_extent_minx = parseFloat(fileinfo.minx);
		topaste.tag_extent_miny = parseFloat(fileinfo.miny);

		topaste.tag_projection = "\"init=epsg:"+epsg+"\"";
		
		topaste.tag_extent_maxx = fileinfo.maxx;
		topaste.tag_extent_maxy = fileinfo.maxy;
		topaste.tag_stmax = parseFloat(fileinfo.max);
		topaste.tag_stmin = parseFloat(fileinfo.min);
		if(mapstyle!=undefined && mapstyle.indexOf('STYLE')!=-1)
			topaste.tag_processing =' 		PROCESSING   "BANDS=1"';
		else
			topaste.tag_processing = '';
		// console.log('topaste.tag_style=',topaste.tag_style);
		if(topaste.tag_style=='1'){
		topaste.tag_style='CLASS\n\
	STYLE\n\
	COLORRANGE 0 0 0   255 255 255 \n\
	DATARANGE '+topaste.tag_stmin+' '+topaste.tag_stmax+'\n\
	RANGEITEM "foobar"\n\
	END\n\
	END\n';
		}
		else{
			topaste.tag_style=topaste.tag_style.replace('#min', topaste.tag_stmin);
			topaste.tag_style=topaste.tag_style.replace('#max', topaste.tag_stmax);									
		}
		
		var data = '';

		if(ext == 'SHP'){
			data=fs.readFileSync(rootpath+'/controllers/map/maptemplates/vector_template.map', 'utf8');
		}
		else if(ext == 'TIF' || ext == 'TIFF'){
			data=fs.readFileSync(rootpath+'/controllers/map/maptemplates/rastr_template.map', 'utf8');
		}
		else{
			res.end(JSON.stringify({"status":"error", "message": "The file type is not known"}),"UTF-8"); 		
			return;
		}
			
		//data=data.replace(/&quot;/g, '"');
		data = populateTemplate(data, topaste);

		data=data.replace(/tag_filepath/g, mapfile);
		data=data.replace(/tag_mapserver_path/g, mapserver_path);
		data=data.replace(/tag_map_file_dir/g, map_file_dir);
		data=data.replace(/tag_symbols/g, tag_symbols);
		// console.log('mapfile=',mapfile);
		
		fs.writeFileSync(mapfile, data, 'utf-8', function (err) {
			if (err) {
				console.log(err);
			}
		});

		// console.log('before update_symbols');
		
		update_symbols(map_symbols_path, null);

		// console.log('after update_symbols');
		
		if(publish)
			wms_link=mapserver_path+"?map="+ config.modules.geothemes.config.remotestaticmapfiledir + '/' + mapfilename;
		else
			wms_link=mapserver_path+"?map="+ toMapserverPath(mapfile);
	

		selection_wms_link=wms_link;
		wms_layer_name=layername;
		minZoom=0;
		maxZoom=15;
		opacity=100;
		
		reply = { status: 'success', wms_link: toMapserverPath(wms_link), wms_layer_name:wms_layer_name, selection_wms_link: toMapserverPath(selection_wms_link), static_map:static_map, minZoom:minZoom, maxZoom:maxZoom, opacity:opacity}
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
		var layer={
			name:		title,
			keywords:	keywords,
			url:		reply.wms_link,
			wmsname:	reply.wms_layer_name,
			style:		jsonstyle,
			filepath:	filepath,
			mapfile:	mapfile
		};
		console.log('publish=',publish)
		if(publish){
			var checkmapquery      = "SELECT true FROM user_schema.layerlist_3020 main WHERE lower(main.mapfile) =  '" + mapfile + "'";
			var cquery = global.pgPool.query(checkmapquery, function(error, res, fields) {
				if (error === undefined){
					if(res.rows.length==0){
						console.log('inserting');
						meta_manager.get_table_json({}, 3020, function (meta){            
							if(meta==null){
								console.log('meta is null');
								return;
							}
							meta.dataset_id=3020;
							req.meta = meta;
							g_table = new geo_table('', 3020, meta);
							
							g_table.insert(req, layer, function(result){
								console.log(result);
							});
						},req);
					}
				}
			});
			
		}
		
	}
	
};


function getfileinfo( sourcepath, callback ){

	var ext='';
	if(sourcepath.lastIndexOf(".")>-1)
		ext = sourcepath.substr(sourcepath.lastIndexOf(".")+1).toLowerCase();
	if(sourcepath=='' || ext=='' || !fs.existsSync(sourcepath)){
		callback({"status":"error", "message": "The param source is not defined."}); 		
		console.log("ext=", ext);
		return;
	}

	var mainrootdir = config.modules.rfm.config.mainrootdir;
	var filenameclean = path.basename(sourcepath);
	var layername = filenameclean.substring(0, filenameclean.lastIndexOf('.'));
	
	var epsg="4326";	
	if(ext.toUpperCase()=='SHP'){
		var ogrinfostr='';
		var terminal = require('child_process').execFile('ogrinfo', ['-so', sourcepath, layername]);
		terminal.stdout.on('data', function (rdata) {
			ogrinfostr += rdata.toString();
			// console.log("ogrinfostr=", ogrinfostr);
		});
		terminal.stderr.on('data', function (data) {
			callback({"status":"error", "message": 'ogrinfo error:'+data}); 			
		  });
		terminal.stdout.on('end', function (rdata) {
			
			if(rdata)
				ogrinfostr += rdata.toString();
			
		});
		terminal.on('exit', function(code) {
			// callback({"status":"error", "message": code + ogrinfostr}); 		
			if (code != 0) {
				console.log('Failed: ' + code);
			}
			searchstr="AUTHORITY['EPSG','";
			ind=ogrinfostr.lastIndexOf("AUTHORITY['EPSG','");
			if(ind!=-1){
				ind=ind+searchstr.length;
				end=ogrinfostr.indexOf("'", ind+1);
				if(end!=-1){
					epsg=ogrinfostr.substr(ind,end-ind);
					console.log('epsg', epsg)
				}
			}
			var vinfo={extent:{},columns:[]};
			var arr = ogrinfostr.split('\r\n');
			for (var i = 0; i < arr.length; i++) {
				// Detecting extent
				if (arr[i].indexOf('Extent: ') != -1) {
					var tempo = arr[i].replace('Extent: ', '');
					tempo = tempo.replace(') - (', ', ');
					tempo = tempo.replace('(', '');
					tempo = tempo.replace(')', '');
					var tempoarr = tempo.split(', ');
					vinfo.minx = tempoarr[0];
					vinfo.miny = tempoarr[1];
					vinfo.maxx = tempoarr[2];
					vinfo.maxy = tempoarr[3];
				}
				// Detecting type
				if (arr[i].indexOf('Geometry: ') != -1) {
					if (arr[i].indexOf('Polygon') != -1) {
						vinfo.layer_type = 'POLYGON';
					} else if (arr[i].indexOf('Point') != -1) {
						vinfo.layer_type = 'POINT';
					} else if (arr[i].indexOf('Line') != -1) {
						vinfo.layer_type = 'LINE';
					}
				}
				keyvalue=arr[i].split(':');
				if(keyvalue.length==2 && (keyvalue[1].indexOf('Integer') != -1 || keyvalue[1].indexOf('Real') != -1) ){
					var field={fieldname: keyvalue[0], type:"number"};
					vinfo.columns.push(field);
				}
				else if(keyvalue.length==2 && keyvalue[1].indexOf('String') != -1  ){
					var field={fieldname: keyvalue[0], type:"string"};
					vinfo.columns.push(field);
				}
			}

			callback({
				"status" : "ok",
				"epsg"   :  epsg,
				"data" : vinfo	
			});
		});

	}
	else{
		var terminal = require('child_process').spawn('rastr_info', ['-stat', '-d',sourcepath]);	
		var fileinfostr='';
		terminal.stdout.on('data', function (rdata) {
			fileinfostr=fileinfostr+rdata.toString();
		});
	
		terminal.stdout.on('end', function (rdata) {
			if(rdata)
				fileinfostr=fileinfostr+rdata.toString();
			try {
				fileinfo=JSON.parse(fileinfostr);
			} catch (err) {				
				callback({
					"status" : "error",
					"data" : err	
				});
				return;
			}
			
			searchstr="AUTHORITY['EPSG','";
			ind=fileinfo.projection.lastIndexOf("AUTHORITY['EPSG','");
			if(ind!=-1){
				ind=ind+searchstr.length;
				end=fileinfo.projection.indexOf("'", ind+1);
				if(end!=-1){
					epsg=fileinfo.projection.substr(ind,end-ind);
					console.log('epsg', epsg)
				}
			}
			fileinfo.minx = fileinfo.left;
			fileinfo.miny = fileinfo.top;

			fileinfo.maxx = fileinfo.minx + parseFloat(fileinfo.cell_size_x) * parseFloat(fileinfo.column_count);
			fileinfo.maxy = fileinfo.miny + parseFloat(fileinfo.cell_size_y) * parseFloat(fileinfo.row_count);
			fileinfo.layer_type = 'tiff';
		
		
			callback({
				"status" : "ok",
				"epsg"   :  epsg,
				"data" : fileinfo	
			});
		});
	
	}
}

/**
 * Get map info
 */
module.exports.rastr_info=function(req, res, next) {
	res.format = "json";	
	var qparams = getqueryparams(req)
	var unique = _qv(qparams, 'unique');
	var dataset_id = _qv(qparams, 'f');
	var filehesh   = qparams.source;

	if (req.user===undefined || req.user==null){
		res.end(JSON.stringify({"status":"error", "message": "You should log in."}),"UTF-8"); 
		console.log('Session is undefined.');
		return;
	}
		

	
	if(!filehesh){
		res.end(JSON.stringify({"status":"error", "message": "filehesh is wrong."}),"UTF-8"); 
		console.log('Filehesh is wrong.');
		return;
	}
	var sourcepath=rfm.getpath(filehesh);

	getfileinfo(sourcepath, function (info){
		res.end(JSON.stringify(info),"UTF-8");
	});	
};

/**
* create map for table
*/
module.exports.create_table_map =function (req, res, next){
	var qparams = getqueryparams(req)
	var unique = _qv(qparams, 'unique');
	var dataset_id = _qv(qparams, 'f');
	if(unique==''){
		
	}
	var mapstyle = '';	
	if('mapstyle' in qparams)
		mapstyle=qparams['mapstyle'];
	//var mapstyle = _qv(qparams, 'mapstyle');
	var req_date;
	if('req_date' in qparams)
		req_date=qparams['req_date'];
	var from_color = _qv(qparams, 'from_color');
	var to_color = _qv(qparams, 'to_color');
		
	var fieldname = _qv(qparams, 'fieldname');
	//var map_file_dir=calipso.config.getModuleConfig('geothemes', 'mapfiledir');
	var map_file_dir=config.modules.geothemes.config.mapfiledir;
	console.log(map_file_dir);
	if(!fs.existsSync(map_file_dir)){
		console.log("You should set up mapfiledir in geothemes config!");
		console.log("current state of map_file_dir=" + this.map_file_dir);
		reply = { status: 'error', wms_link: '', msg:'You should set up mapfiledir in geothemes config!'};
		res.format = "json";
		res.end(JSON.stringify(reply),"UTF-8");
		return;
	}
	
	var user_id='';
	if('session' in req && 'user' in req.session && req.session && req.session.user && 'id' in req.session.user && req.session.user.id!='' )	{
		user_id=req.session.user.id;
	}
	

	meta_manager.get_table_json({},dataset_id, function(mdobj){
		if(mdobj==null){
			reply = { status: 'error', message: 'The dataset metadata was not loaded.'}
			res.format = "json";
			res.end(JSON.stringify(reply),"UTF-8");
			console.log(reply);
			return;
		}

		

		var replyarr = new Array();
		var wms_link='';
		var selection_wms_link='';
		var wms_layer_name='';
		var static_map=false;
		var minZoom=mdobj.minZoom;
		var maxZoom=mdobj.maxZoom;
		var opacity=mdobj.opacity;
		if(opacity===undefined)
			opacity=1;
		
		if (mdobj.wms_link!=undefined && mdobj.wms_link!=null && mdobj.wms_link!='' && user_id=='') {
			wms_link=mdobj.wms_link;
			wms_layer_name=mdobj.wms_layer_name;				
			static_map=true;
			reply = { status: 'success', wms_link: toMapserverPath(wms_link), wms_layer_name:wms_layer_name, selection_wms_link: toMapserverPath(selection_wms_link), static_map:static_map, minZoom:minZoom, maxZoom:maxZoom, opacity:opacity}
			res.format = "json";
			res.end(JSON.stringify(reply),"UTF-8");				
		}
		else{
			var selection_array = _qv(qparams, 'selected_array');
			
			

			fieldlist=[];
					
			for(var i=0; i<mdobj.columns.length; i++ ){
				if(!mdobj.columns[i].widget)
					continue;
				if(mdobj.columns[i].widget.name=='point' || mdobj.columns[i].widget.name=='polygon' || mdobj.columns[i].widget.name=='line' ){
					if(mdobj.columns[i].fieldname==fieldname)
						fieldlist.push(mdobj.columns[i].fieldname);
				}
				else
					fieldlist.push(mdobj.columns[i].fieldname);
			}
			var g_table = new geo_table(user_id, mdobj.dataset_id, mdobj, {convertGeometries:false, includefields:fieldlist});
		
			var sLimit='';		
			var s_order='';
			if(_qv(qparams, 'mapDisplayLength')!=''){//drawing 
				iDisplayStart=_qv(qparams, 'iDisplayStart');
				iDisplayLength=_qv(qparams, 'iDisplayLength');
				if (iDisplayStart!='' && iDisplayLength!='')	{
					sLimit = 'LIMIT ' + iDisplayLength + ' OFFSET ' + iDisplayStart;
				} //end if
				else{
						sLimit = 'LIMIT 10000 OFFSET 0';
				}
				for(var i=0; i<mdobj.columns.length; i++ ){
					fn=_qv(qparams,'calc_' + mdobj.columns[i].fieldname);
					if(fn.indexOf('distance')!=-1){
						s_order=g_table.buildselect(req, [mdobj.columns[i].fieldname], qparams, []);
						if(s_order.indexOf(" AS ")!=-1)
							s_order=s_order.substring(0,s_order.indexOf(" AS "));
						s_order+= " ASC ";
						qparams['calc_' + mdobj.columns[i].fieldname]='';					
					}
				}			
			}
			var column=common.getcolumn(fieldname, mdobj);
			if(column){
									
				sql=g_table.documentquery(req, fieldlist,qparams, '', undefined);					
				if(sLimit!=''){
					if(s_order!='')
						sql=sql + ' ORDER BY ' +s_order ;
					sql=sql + ' ' + sLimit;
				}					
				var mapfilename=unique;
				var mapserver_path = config.modules.geothemes.config.mapserver_path;
				var filepath = map_file_dir + '/' + mapfilename + '.map';
				var metadata = {pg_user: '', pg_password: '', pg_db_name: '', pg_host: '', tablename: mdobj.tablename, indexcolumn: 'id' };
				metadata.pg_user=config.modules.postgres.config.db_user;
				metadata.pg_password=config.modules.postgres.config.db_passwd;
				metadata.pg_db_name=config.modules.postgres.config.db_name;
				metadata.pg_host=config.modules.postgres.config.db_host;
				metadata.pg_port=config.modules.postgres.config.db_port;
				var map_options_path = map_file_dir + '/' + mapfilename + '.options';
				var map_symbols_path = map_file_dir + '/' + mapfilename + '.sym';
				var tag_symbols = mapfilename + '.sym';
				
				
				var map_options={ mapstyle:'',  selection:'', query:''};
				if(fs.existsSync(map_options_path)){
					map_options_str=fs.readFileSync(map_options_path);
					map_options = JSON.parse(map_options_str);
				}
			
				var data = '';
				var style_default;
				switch (column.type) {
					case 'point':
						data=fs.readFileSync(rootpath+'/controllers/map/maptemplates/point_template.map', 'utf8');
						style_default=fs.readFileSync(rootpath+'/controllers/map/maptemplates/point_template.style', 'utf8');
						break;
					case 'line':
						data=fs.readFileSync(rootpath+'/controllers/map/maptemplates/line_template.map', 'utf8');
						style_default=fs.readFileSync(rootpath+'/controllers/map/maptemplates/line_template.style', 'utf8');
						break;
					case 'polygon':
						data=fs.readFileSync(rootpath+'/controllers/map/maptemplates/polygon_template.map', 'utf8');
						style_default=fs.readFileSync(rootpath+'/controllers/map/maptemplates/polygon_template.style', 'utf8');
						break;		
					default:
						reply = { status: 'error', message: 'The drawing field is not spatial.'}
						res.format = "json";
						res.end(JSON.stringify(reply),"UTF-8");
						return;									
				}
				style_default=style_default.replace(/#fieldname/g, column.fieldname);

				data=data.replace(/tag_style/g, mapstyle);
				if(selection_array!=''){
					ids=selection_array.split(',');
					if(ids.length==0)
						str_filter = 'main.ID=-1';
					else{
						str_filter = '';
						for(var i=0; i<ids.length; i++){
							if(str_filter!='')
								str_filter+=' OR ';
							str_filter+='main.ID='+ids[i];
						}
	
					}
				}
				else{
					str_filter='main.id=-1';
				}
	
				str_filter=' '+str_filter;
				data=data.replace(/tag_selection_filter/g, str_filter);
	
				//data=data.replace(/&quot;/g, '"');
				data=data.replace(/tag_filepath/g, filepath);
				data=data.replace(/tag_fieldname/g, column.fieldname);
				data=data.replace(/tag_user/g, metadata.pg_user );
				data=data.replace(/tag_password/g, metadata.pg_password );
				data=data.replace(/tag_db_name/g, metadata.pg_db_name);
				data=data.replace(/tag_host/g, metadata.pg_host);
				data=data.replace(/tag_table/g, metadata.tablename);
				data=data.replace(/tag_host/g, metadata.pg_host);
				data=data.replace(/tag_port/g, metadata.pg_port);
				data=data.replace(/tag_mapserver_path/g, mapserver_path);
				data=data.replace(/tag_map_file_dir/g, map_file_dir);
				data=data.replace(/tag_symbols/g, tag_symbols);
				sql=sql.replace(/"/g, '\\"');
				data=data.replace(/tag_query/g, sql);

				if(column.widget.properties.label!=undefined && column.widget.properties.label!='')
					data=data.replace(/tag_indexcolumn/g, 'LABELITEM "'+column.widget.properties.label+'"');
				else
					data=data.replace(/tag_indexcolumn/g, '');
	
				data=data.replace(/tag_geom/g, column.fieldname);
				
				fs.writeFileSync(filepath, data, 'utf-8', function (err) {
					if (err) {
						console.log(err);
					}
				});
				
				map_options_str=JSON.stringify(map_options);
				fs.writeFileSync(map_options_path, map_options_str, 'utf-8', function (err) {
					if (err) {
						console.log(err);
					}
				});

				wms_link=mapserver_path+"?map="+ toMapserverPath(filepath);
				selection_wms_link=wms_link;
				wms_layer_name=fieldname;	
				reply = { status: 'success', wms_link: toMapserverPath(wms_link), wms_layer_name:wms_layer_name, selection_wms_link: toMapserverPath(selection_wms_link), static_map:static_map, minZoom:minZoom, maxZoom:maxZoom, opacity:opacity}
				res.format = "json";
				res.end(JSON.stringify(reply),"UTF-8");
				
				update_symbols(map_symbols_path, column);
				return;
			}
			else{
				reply = { status: 'error', message: 'The drawing spatial field is not defined.'}
				res.format = "json";
				res.end(JSON.stringify(reply),"UTF-8");
				console.log(reply);
				return;
			}
		}

	});

	
}

function update_symbols (symbols_path, column){
	var select_query='select name, symbol, filled from public.mapsymbols';
	var def_symbols='';
	var map_file_dir=config.modules.geothemes.config.mapfiledir;
	var def_symbols_path=map_file_dir + '/symbols/def_symbols.sym';
	if(fs.existsSync(def_symbols_path)){
		def_symbols=fs.readFileSync(def_symbols_path);
	}
	
	
	
	var symbol_content='';
	
	if(column && column.widget && column.widget.style){
		var style=JSON.parse(column.widget.style);
		if(style.style_type=='single' && style.base_rule){
			for(var j=0; j<style.base_rule.symbolizers.length; j++){						
				if(style.base_rule.symbolizers[j].image && style.base_rule.symbolizers[j].image!=''){
					file_path=calipso.modules.rfm.fn.getpath(style.base_rule.symbolizers[j].image);
					ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
					new_path=map_file_dir + '/images/' + style.base_rule.symbolizers[j].image + '.' + ext;
					filename='images/'+style.base_rule.symbolizers[j].image + '.' + ext;
					console.log(file_path);
					console.log(new_path);
					calipso.modules.rfm.fn.copyFile(file_path,new_path, function(error){
						if(error)
							console.log(error);								
					} );
					
					symbol_content+='SYMBOL\n\
					NAME "' + style.base_rule.symbolizers[j].image + '"\n\
					TYPE pixmap\n\
					IMAGE "'+filename+'"\n\
					END\n';
				}
			}
		}
		else{
			if(style.rules){
				for(var i=0; i<style.rules.length; i++){
					if(!style.rules[i].symbolizers)
						continue;
					for(var j=0; j<style.rules[i].symbolizers.length; j++){						
						if(style.rules[i].symbolizers[j].image && style.rules[i].symbolizers[j].image!=''){
							file_path=calipso.modules.rfm.fn.getpath(style.rules[i].symbolizers[j].image);
							ext = file_path.substr(file_path.lastIndexOf(".")+1).toLowerCase();
							new_path=map_file_dir + '/images/' + style.rules[i].symbolizers[j].image + '.' + ext;
							filename='images/'+style.rules[i].symbolizers[j].image + '.' + ext;
							console.log(file_path);
							console.log(new_path);
							calipso.modules.rfm.fn.copyFile(file_path,new_path, function(error){
								if(error)
									console.log(error);								
							} );
							
							symbol_content+='SYMBOL\n\
  NAME "' + style.rules[i].symbolizers[j].image + '"\n\
  TYPE pixmap\n\
  IMAGE "'+filename+'"\n\
END\n';
						}
					}
				}				
			}
		}
	}


	for(var i=0; i<font_symbols.length; i++){
		for(var j=font_symbols[i].startsymbol; j<=font_symbols[i].endsymbol; j++){
						symbol_content+='SYMBOL\n\
NAME "' + font_symbols[i].name + j +'"\n\
TYPE truetype\n\
FONT "'+font_symbols[i].name+'"\n\
CHARACTER "&#'+j+';"\n\
END\n';				
		}
	}
	

	//console.log(symbol_content);
	var query = global.pgPool.query(select_query, function(error, query_result, unofields2) {
		if (error === undefined) {			
			for(var i=0; i<query_result.rows.length; i++){
				if(!query_result.rows[i].symbol || query_result.rows[i].symbol=='' || query_result.rows[i].name=='image' || query_result.rows[i].symbol=='font symbol')
					continue;
				var sfld='';
				if(query_result.rows[i].filled)
					sfld='Filled TRUE\n';
				symbol_content+='\
Symbol\n\
Name \''+query_result.rows[i].name+'\'\n\
Type VECTOR\n\
'+sfld+'\
Points\n\
'+query_result.rows[i].symbol+'\n\
END\n\
END\n';

//Filled FALSE\n\
			}
			//console.log("symbol_content="+symbol_content);
			symbol_content=def_symbols+'\n'+symbol_content;
			if (symbol_content != '\n') {
				fs.writeFileSync(symbols_path, symbol_content, 'utf-8', function (err) {
					if (err) {
						console.log(err);
					}
				});
			}
		} else {
			console.log(select_query);
			console.log(error.message);
			callback(error.message);
		}
	});

}